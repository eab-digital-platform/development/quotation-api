const _ = require('lodash');
const router = new (require('restify-router')).Router();
const staticFiles = require('../static/Directory');
const dbAction = require('../modules/mongoDB/models/actions');
const QuotationHandler = require('../src/bz/handler/QuotationHandler');
/** 
 * In case CORS not work, add this manually 
 * response.setHeader('Access-Control-Allow-Origin', '*');
*/

const isErrorFree = (errs) => {
  let result = true;
  for (let prop in errs) {
    result = result && _.isEmpty(errs[prop]);
  }
  return result;
}

router.post('/init', async (request, response, next) => {
  console.log('==================== POST /quote/init/:productID ====================');
  //console.log(`body: ${JSON.stringify(request.body)}`);
  //console.log(`params: ${JSON.stringify(request.params)}`);
  response.setHeader('content-type', 'application/json');
  // response.setHeader('Access-Control-Allow-Origin', '*');
  const dataJson = _.get(staticFiles, `data`, null);
  const requestParams = _.get(request, `body`);
  const { productID, sessionID } = requestParams;
  if (!dataJson) { response.send(400, new Error(`Quote/init :: data lost`))};
  const targetData = _.cloneDeep(dataJson);
  const productList = await staticFiles.getProducts();
  _.setWith(targetData, `productId`, _.get(productList, `${productID}.productId`), typeof String);
  const result = await new Promise((resolve, reject) => {
    new QuotationHandler.initQuotation(targetData, staticFiles.session, (r) => {
      resolve(r);
    });
  });
  /**
  const logResult = await dbAction.addLog({
    accessTime: new Date().toString(),
    funcName: `initQuotation`,
    sessionID,
    // message: isErrorFree(result.errors) ? '' : JSON.stringify(result.errors),
  });
  */
  console.log(`Route :: /quote/init :: Response`);
  result ? response.send(200, result) : response.send(400);
  return next();
});

router.post('/quote', async (request, response, next) => {
  console.log('==================== POST /quote/ ====================');
  //console.log(`body: ${JSON.stringify(request.body)}`);
  //console.log(`params: ${JSON.stringify(request.params)}`);
  response.setHeader('content-type', 'application/json');
  // response.setHeader('Access-Control-Allow-Origin', '*');
  const requestParams = _.get(request, `body`);
  const { quotation, sessionID, } = requestParams;
  const dataJson = _.cloneDeep(staticFiles.data);
  _.set(dataJson, `quotation`, quotation, typeof quotation);
  const cacheJson = _.cloneDeep(dataJson)
  let result = await new Promise((resolve, reject) => {
    new QuotationHandler.quote(dataJson, staticFiles.session, (r) => {
      resolve(r);
    });
  });
    result = result.quotation !== cacheJson.quotation ? await new Promise((resolve, reject) => {
      new QuotationHandler.quote(dataJson, staticFiles.session, (r) => {
        resolve(r);
      });
    }) : result;
  /**
  const logResult = await dbAction.addLog({
    accessTime: new Date().toString(),
    funcName: `quote`,
    sessionID,
    // message: isErrorFree(result.errors) ? '' : JSON.stringify(result.errors),
  });
  */
  console.log(`Route :: /quote :: Response`);
  result ? response.send(200, result) : response.send(400);
  return next();
});

router.post('/genProposal', async (request, response, next) => {
  console.log('==================== POST /genProposal/ ====================');
  //console.log(`body: ${JSON.stringify(request.body)}`);
  //console.log(`params: ${JSON.stringify(request.params)}`);
  response.setHeader('content-type', 'application/json');
  // response.setHeader('Access-Control-Allow-Origin', '*');
  const requestParams = _.get(request, `body`);
  const { quotation, sessionID, } = requestParams;
  const dataJson = _.cloneDeep(staticFiles.data);
  _.set(dataJson, `quotation`, quotation, typeof quotation);
  const result = await new Promise((resolve, reject) => {
    new QuotationHandler.genProposal(dataJson, staticFiles.session, (r) => {
      resolve(r);
    });
  });
    /**
  const logResult = await dbAction.addLog({
    accessTime: new Date().toString(),
    funcName: `initQuotation`,
    sessionID,
    // message: isErrorFree(result.errors) ? '' : JSON.stringify(result.errors),
  });
  */
  console.log(`Route :: /genProposal :: Response`);
  result ? response.send(200, result) : response.send(400, result);
  return next();
});

router.post('/updateRiderList', async (request, response, next) => {
  console.log('==================== POST /updateRiderList/ ====================');
  //console.log(`body: ${JSON.stringify(request.body)}`);
  //console.log(`params: ${JSON.stringify(request.params)}`);
  response.setHeader('content-type', 'application/json');
  // response.setHeader('Access-Control-Allow-Origin', '*');
  const requestParams = _.get(request, `body`);
  const { quotation, newRiderList, sessionID, } = requestParams;
  const dataJson = _.cloneDeep(staticFiles.data);
  _.set(dataJson, `quotation`, quotation, typeof quotation);
  _.set(dataJson, `newRiderList`, newRiderList, typeof newRiderList);
  const result = await new Promise((resolve, reject) => {
    new QuotationHandler.updateRiderList(dataJson, staticFiles.session, (r) => {
      resolve(r);
    });
  });
  /** 
  const logResult = await dbAction.addLog({
    accessTime: new Date().toString(),
    funcName: `initQuotation`,
    sessionID,
    // message: isErrorFree(result.errors) ? '' : JSON.stringify(result.errors),
  });
  */
  console.log(`Route :: /updateRiderList :: Response`);
  result ? response.send(200, result) : response.send(400);
  return next();
});


module.exports = router;
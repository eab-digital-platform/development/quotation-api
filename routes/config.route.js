const _ = require('lodash');
const router = new (require('restify-router')).Router();
const dbAction = require('../modules/mongoDB/models/actions');
const staticFiles = require('../static/Directory');
/** 
 * In case CORS not work, add this manually 
 * response.setHeader('Access-Control-Allow-Origin', '*');
*/

// router.post('/:id', async (request, response, next) => {
//   console.log('==================== POST /config/:id ====================');
//   //console.log(`body: ${JSON.stringify(request.body)}`);
//   //console.log(`params: ${JSON.stringify(request.params)}`);
//   response.setHeader('content-type', 'application/json');
//   //response.setHeader('Access-Control-Allow-Origin', '*');
//   const { id } = request.params;
//   const requestParams = _.get(request, `body.json`);
//   const { sessionID, funcName } = JSON.parse(requestParams);
//   let result = {};
//   switch (id) {
//     case "productList":
//       result = await staticFiles.getProducts();
//       break;
//     case "log":
//       await dbAction.addLog({
//         accessTime: new Date().toString(),
//         funcName: funcName,
//         sessionID,
//       });
//       result ={ success: true, }
//       break;
//     default:
//       result = { success: false, msg: `Non-valid ID`, };
//       break;
//   }
//   response.send(result);
//   return next();
// });

router.get('/productlist', async (request, response, next) => {
  console.log('==================== GET /config/productlist ====================');
  response.setHeader('content-type', 'application/json');
  const bodyRes = await staticFiles.getProducts();
  bodyRes ? response.send(200, bodyRes, ) : response.send(400);
  return next();
});

router.post('/log', async (request, response, next) => {
  console.log('==================== POST /config/log ====================');
  const requestParams = _.get(request, `body`);
  const { sessionID, funcName } = requestParams;
  const insertRecord = await dbAction.addLog({
    accessTime: new Date().toString(),
    funcName: funcName,
    sessionID,});
  insertRecord.success ? response.send(200) : response.send(400);
  return next();
});

module.exports = router;
require('./src/utils/DateFormatter.js');
require('custom-env').env('dev');

// Required Modules
const restify = require('restify');
const minimist = require('minimist');

// Restify Modules
const corsMiddleware = require('restify-cors-middleware')

// Config
const easeConfig = require('./src/config/index');

// Exporter
const FileExporter = require('./src/utils/FileExporter');

// EASE Modules
const QuotValid = require('./src/bz/Quote/QuotValid');
const QuotDriver = require('./src/bz/Quote/QuotDriver');
const QuotationHandler = require('./src/bz/handler/QuotationHandler');
const QuotComparator = require('./src/bz/Quote/QuotComparator');

// Customize Modules
const staticJson = require('./static/Directory');
const JavaRunner = require('./src/bz/JavaRunner');
global.rootPath = __dirname;
global.JavaRunner = new JavaRunner();
// Routes
const configRoute = require('./routes/config.route.js');
const quoteRoute = require('./routes/quote.route.js');

const argv = minimist(process.argv.slice(2), {
  alias: {
    h: 'help',
    i: 'illustration',
    q: 'quote',
    c: 'compare',
    t: 'test',
    b: 'batch',
    p: 'port'
  },
  default: {
  }
});

//#region WEB
if (argv.w) {
  const port = argv.port || process.env.PORT || 8080;
  
  const server = restify.createServer({
    name: 'iQuote',
    version: process.env.VERSION,
  });

  const cors = corsMiddleware({
    origins: [process.env.CORS_ORIGIN],
    allowHeaders: ['API-Token'],
    exposeHeaders: ['API-Token-Expiry'],
  });

  // Restify Plugins
  server.pre(cors.preflight);
  server.use(cors.actual);
  server.use(restify.plugins.acceptParser(server.acceptable));
  server.use(restify.plugins.queryParser());
  server.use(restify.plugins.bodyParser());

  easeConfig.init((conf) => {
    // Customize Ruotes
    configRoute.applyRoutes(server, '/config');
    quoteRoute.applyRoutes(server, '/quote');

    server.get('/', (request, response, next) => {
      response.setHeader('content-type', 'text/plain');
      response.send('The service is running');
      return next();
    });

    server.listen(port, () => {
      console.log('%s listening at %s', server.name, server.url);
      console.log('==================== WEB API SERVICE ====================');
    });
  })
}
//#endregion

//#region CLI
if (argv.q) {
  easeConfig.init((r) => {
    new Promise((resolve, reject) => {
      // resolve( new QuotationHandler.quote(staticJson.data, staticJson.session, (result) => {
        resolve( new QuotationHandler.initQuotation(staticJson.data, staticJson.session, (result) => {
        const fileExporter = new FileExporter();
        fileExporter.setDirectory('quote/');
        fileExporter.toJson(result);
      }))
    })
  })
  return;
}
// QUOTATION MANDATORY CHECK
if (argv.i) {
  easeConfig.init((r) => {
    // Embed
    // staticJson.quotation = require('./output/batch/1550030262748/0.json');
    // End Embed
    new Promise((resolve, reject) => {
      resolve(new QuotationHandler.genProposal(staticJson.data, staticJson.session, (result) => {
        const fileExporter = new FileExporter();
        fileExporter.setDirectory('illustration/');
        fileExporter.toJson(result);
      }));
    });
  });
  return;
}

if (argv.d) {
  const dbAction = require('./modules/mongoDB/models/action');
  dbAction.addLog({
    accessTime: new Date().toString(),
    funcName: 'testing',
    sessionID: '1234',
  })
  return;
}
//#endregion
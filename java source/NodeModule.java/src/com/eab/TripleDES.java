package com.eab;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class TripleDES {
	private final static boolean DEBUG = false;
	private final static String CRYPTOKEY_ALGORITHM = "TripleDES";
	private final static String CRYPTOKEY_TRANSFORMATION = "TripleDES/ECB/PKCS5Padding";
	private final static int CRYPTOKEY_LENGTH = 168;		// 168 bit (DES uses 56 bit, and TripleDES uses 3 times)
	private final static String ALGORITHM_3DES = "DESede";
	
	/**
	 * Generate Random Key for TripleDES
	 * @return TripleDES Key
	 */
	public static Key GenKey() {
		KeyGenerator keyGenerator = null;
		Key key = null;
		
		try {
			keyGenerator = KeyGenerator.getInstance(CRYPTOKEY_ALGORITHM);
			keyGenerator.init(CRYPTOKEY_LENGTH, new SecureRandom());
			key = keyGenerator.generateKey();
			if (DEBUG) System.out.println("Key(Hex)=" + Util.ByteToHex(key.getEncoded()));
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		} finally {
			keyGenerator = null;
		}
		
		return key;
	}
	
	/**
	 * Convert Text to be TripleDES Key
	 * @param text - Text String (21 bytes)
	 * @return TripleDES Key
	 */
	public static Key ImportKey(String text) {
		Key key = null;
		
		try {
			//168 bit (DES uses 56 bit, and TripleDES uses 3 times)
			if (text != null && text.getBytes("UTF8").length == (CRYPTOKEY_LENGTH / 8)) {
				key = new SecretKeySpec(text.getBytes("UTF8"), ALGORITHM_3DES);
			}
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		}
		
		return key;
	}
	
	/**
	 * Convert TripleDES Key back to Text
	 * @param key - TripleDES Key
	 * @return Text String
	 */
	public static String ExportKey(Key key) {
		try {
			if (key != null)
				return new String(key.getEncoded(), "UTF8");
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * TripleDES Encryption
	 * @param key - TripleDES Key
	 * @param passphrase - Pass Phrase to be encrypted
	 * @return Encrypted Byte Array
	 */
//	public static byte[] Encrypt(Key key, String passphrase) {
//		Cipher cipher = null;
//		byte[] cipherText = null;
//		
//		try {
//			// Create a cipher using that key to initialize it
//		    cipher = Cipher.getInstance(CRYPTOKEY_TRANSFORMATION);
//		    cipher.init(Cipher.ENCRYPT_MODE, key);
//		    
//		    // Perform the actual encryption
//		    cipherText = cipher.doFinal(passphrase.getBytes("UTF8"));
//		    if (DEBUG) System.out.println("Encrypted Key: " + Util.ByteToHex(cipherText));
//		} catch(Exception e) {
//			if (DEBUG) e.printStackTrace();
//		}
//		
//		return cipherText;
//	}
	
	/**
	 * TripleDES Decryption
	 * @param key - TripleDES Key
	 * @param cipherText - Encrypted Byte Array
	 * @return Pass Phrase
	 */
//	public static String Decrypt(Key key, byte[] cipherText) {
//		String output = null;
//		Cipher cipher = null;
//		byte[] result = null;
//		
//		try {
//			cipher = Cipher.getInstance(CRYPTOKEY_TRANSFORMATION);
//			cipher.init(Cipher.DECRYPT_MODE, key);
//			
//			result = cipher.doFinal(cipherText);
//			
//			if (result != null)
//				output = new String(result, "UTF8");
//		} catch(Exception e) {
//			if (DEBUG) e.printStackTrace();
//		}
//		
//		return output;
//	}
}

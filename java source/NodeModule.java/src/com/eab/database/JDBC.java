package com.eab.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.couchbase.lite.Document;
import com.eab.Util;

public class JDBC {

	private static final String ORACLE_DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
	
	/**
	 * Create JDBC connection
	 * @param username - DB Username
	 * @param password - DB Password
	 * @param connUrl - Connection URL (e.g. jdbc:oracle:thin:@localhost:1521:GDB)
	 * @return Database Connection
	 */
	public static Connection connect(String username, String password, String connUrl) {
		Connection conn = null;
		
		try {
			Class.forName(ORACLE_DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			conn = DriverManager.getConnection(connUrl, username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return conn;
	}
	
	public static String execute(String username, String password, String connUrl, String sql) {
		String output = null;
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;	
		
		try {
			conn = connect(username, password, connUrl);
			if (conn != null) {
				statement = conn.createStatement();
				rs = statement.executeQuery(sql);
				if (rs != null) {
					output = Json.ResultSetToJsonString(rs);
				} else {
					output = Json.GetOutputJsonString(false, "DONE", "");
				}
			} else {
				output = Json.GetOutputJsonString(true, "ERR001", "Cannot connect database");
			}
		} catch(Exception e) {
			e.printStackTrace();
			output = Json.GetOutputJsonString(true, "EXCEPTION", e.getMessage());
		} finally {
			if (conn != null) {
				try { conn.close(); } catch(Exception e) { e.printStackTrace(); }
			}
			conn = null;
			rs = null;
			statement = null;
		}
		
		return output;
	}
	
	public static String executeSql(String username, String password, String connUrl, String sql, String dataStr) {
		String output = null;
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		JSONArray data = null;
		PreparedStatement prestate = null;
		try {
			data = new JSONArray(dataStr);
			conn = connect(username, password, connUrl);
			if (conn != null) {
				prestate =  conn.prepareStatement(sql);
				setupStatement(prestate, data);
				//prestate.setString(1, "nc");
				rs = prestate.executeQuery();
				if (rs != null) {
					output = Json.ResultSetToJsonString(rs);
				} else {
					output = Json.GetOutputJsonString(false, "DONE", "");
				}
			} else {
				output = Json.GetOutputJsonString(true, "ERR001", "Cannot connect database");
			}
		} catch(Exception e) {
			e.printStackTrace();
			output = Json.GetOutputJsonString(true, "EXCEPTION", e.getMessage());
		} finally {
			if (conn != null) {
				try { conn.close(); } catch(Exception e) { e.printStackTrace(); }
			}
			conn = null;
			rs = null;
			statement = null;
		}
		
		return output;
	}
	
	public static void setupStatement(PreparedStatement statement, JSONArray data) throws JSONException, SQLException{
		int size = data.length();
		for(int i = 0; i < size; i++){
			Object object = data.get(i);
			int index = i + 1;
			if(object.getClass().equals(Integer.class)){
				statement.setInt(index, (Integer)object);
				System.out.println("*** setupStatement ***" + "int :" + (Integer)object);
				
			}else if(object.getClass().equals(String.class)){
				statement.setString(index, (String)object);
				System.out.println("*** setupStatement ***" + "string :" + (String)object);
			}
		}
	}
	
	
}

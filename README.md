# iQuote-SERVER

> Updated: 29/4/2019

## Introduction

This is the backend for iQuote-UI

## CLI

There are different argv accepted in the backend. You can run it locally.

## Args
since quote-rt is build with micro-services in mind, you can start each service with different arguments.
```javascript
--q // check the quotation json
--i // generate the proposal, and illustration
--d // validate database connection and push sample data
--w // Web services
```

## Run

1. iQuote
this project is the backend of iQuote-rt and will connect to ease-web for processing
```
    npm i
    npm run web  // node index.js -w
```
2. iQuote-UI
the iQuote UI project, get the latest and run, remember to change the SERVER_URL stated at the README
```
    npm i
    npm start
    OR
    npm build
```

Once the iQuote-UI run, a landing page will appear.

## Additional Information

process.env.VAR is is acceptable to replace the static string.
Its acceptable the following variables,...

```javascripts
// General
APP_ENV
VERSION="0.0.1"
//PORT
PORT=8080
//CORS
CORS_ORIGIN='*'
//DB
MONGODB_HOST
MONGODB_USER
MONGODB_PWD
MONGODB_DB
```
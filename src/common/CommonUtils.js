var _ = require('lodash');
var crypto = require('crypto');

module.exports.calcAge = function(birthday) {
    let today = new Date();
    let birthDate = new Date(birthday);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
};

/**
 * Deep diff between two object, using lodash
 * @param  {Object} object Object compared
 * @param  {Object} base   Object to compare with
 * @return {Object}        Return a new object who represent the diff
 */
module.exports.difference = function(object, base) {
    function changes(object, base) {
        return _.transform(object, function(result, value, key) {
            if (!_.isEqual(value, base[key])) {
                result[key] = (_.isObject(value) && _.isObject(base[key])) ? changes(value, base[key]) : value;
            }
        });
    }
    return changes(object, base);
}


module.exports.getStrSignature = function(value, key) {
    if (typeof value == 'object') {
        value = JSON.stringify(value);
    }
    return crypto.createHmac('sha256', key).update(value).digest('hex');
};

module.exports.convertSGDtoInputCcy = (sgdValue, ccy, compCode, decimal) => {
    const optionsMap = !!global.optionsMap && global.optionsMap;
    const currencies = _.get(optionsMap, 'ccy.currencies');
    let convertedValue = sgdValue;
    _.each(currencies, obj => {
        if (obj.compCode === compCode) {
            _.each(obj.options, curencyObj => {
                if (curencyObj.value === ccy) {
                    convertedValue = sgdValue * curencyObj.exRate;
                }
            });
        }
    });
    var factor = Math.pow(10, decimal);
    return Math.round(convertedValue * factor) / factor;
};


module.exports.convertForeignCcytoSGD = (foreignValue, ccy, compCode, decimal) => {
    const optionsMap = !!global.optionsMap && global.optionsMap;
    const currencies = _.get(optionsMap, 'ccy.currencies');
    let convertedValue = foreignValue;
    _.each(currencies, obj => {
        if (obj.compCode === compCode) {
            _.each(obj.options, curencyObj => {
                if (curencyObj.value === ccy) {
                    convertedValue = foreignValue / curencyObj.exRate;
                }
            });
        }
    });
    var factor = Math.pow(10, decimal);
    return Math.round(convertedValue * factor) / factor;
};
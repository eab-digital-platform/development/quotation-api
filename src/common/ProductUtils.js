const _ = require('lodash');
const DateUtils = require('./DateUtils');

module.exports.getAgeByMethod = (calcAgeMethod, targetDate, dob) => {
  switch (calcAgeMethod) {
    case 'current':
      return DateUtils.getAttainedAge(targetDate, dob).year;
    case 'next':
      return DateUtils.getAgeNextBirthday(targetDate, dob);
    case 'nearest':
    default:
      return DateUtils.getNearestAge(targetDate, dob);
  }
};

module.exports.getAgeByUnit = (unit, targetDate, dob) => {
  switch (unit) {
    case 'day':
      return DateUtils.getAttainedAge(targetDate, dob).day;
    case 'month':
      return DateUtils.getAttainedAge(targetDate, dob).month;
    case 'year':
      return DateUtils.getAttainedAge(targetDate, dob).year;
    case 'nextAge':
      return DateUtils.getAgeNextBirthday(targetDate, dob);
    case 'nearestAge':
    default:
      return DateUtils.getNearestAge(targetDate, dob);
  }
};

module.exports.getOccupationClass = function (product, occupation) {
  var option = _.find(global.optionsMap.occupation.options, (opt) => opt.value === occupation);
  if (product.occupClassType && product.occupClassType !== null){
    if (product.occupClassType === 'pab'){
      return option && option.pabrider;
    } else if (product.occupClassType === 'adb'){
      return option && option.adb;
    }
  }
  return option && option.paplan;
  
};

module.exports.getCovCodeFromProductId = (productId) => {
  if (!productId) {
    return null;
  }
  let m = productId.match(/_product_(.*)_[0-9]+/);
  if (m && m.length > 1) {
    return m[1];
  }
};

module.exports.getProductId = (product) => {
  if (!product) {
    return;
  }
  return product.id || product._id || (product.compCode || '08') + '_product_' + product.covCode + '_' + (product.version ? product.version : '1');
};

module.exports.checkEntryAge = (product, iResidence, pAges, iAges) => {
  if (product){
    const entry = _.find(product.entryAge, e => e.country === '*' || e.country === iResidence);
    if (entry) {
      return ((!entry.iminAge ||
        (['year', 'month', 'day', 'nearestAge', 'nextAge'].indexOf(entry.iminAgeUnit) === -1) ||
        (entry.iminAgeUnit === 'year' && entry.iminAge <= iAges.year) ||
        (entry.iminAgeUnit === 'month' && entry.iminAge <= iAges.month) ||
        (entry.iminAgeUnit === 'day' && entry.iminAge <= iAges.day) ||
        (entry.iminAgeUnit === 'nearestAge' && entry.iminAge <= iAges.nearestAge) ||
        (entry.iminAgeUnit === 'nextAge' && entry.iminAge <= iAges.nextAge)
      ) && (!entry.imaxAge ||
        (['year', 'month', 'day', 'nearestAge', 'nextAge'].indexOf(entry.imaxAgeUnit) === -1) ||
        (entry.imaxAgeUnit === 'year' && entry.imaxAge >= iAges.year) ||
        (entry.imaxAgeUnit === 'month' && entry.iminAge >= iAges.month) ||
        (entry.imaxAgeUnit === 'day' && entry.imaxAge >= iAges.day) ||
        (entry.imaxAgeUnit === 'nearestAge' && entry.imaxAge >= iAges.nearestAge) ||
        (entry.imaxAgeUnit === 'nextAge' && entry.imaxAge >= iAges.nextAge)
      ) && (!entry.ominAge ||
        (['year', 'month', 'day', 'nearestAge', 'nextAge'].indexOf(entry.ominAgeUnit) === -1) ||
        (entry.ominAgeUnit === 'year' && entry.ominAge <= pAges.year) ||
        (entry.ominAgeUnit === 'month' && entry.ominAge <= pAges.month) ||
        (entry.ominAgeUnit === 'day' && entry.ominAge <= pAges.day) ||
        (entry.ominAgeUnit === 'nearestAge' && entry.ominAge <= pAges.nearestAge) ||
        (entry.ominAgeUnit === 'nextAge' && entry.ominAge <= pAges.nextAge)
      ) && (!entry.omaxAge ||
        (['year', 'month', 'day', 'nearestAge', 'nextAge'].indexOf(entry.omaxAgeUnit) === -1) ||
        (entry.omaxAgeUnit === 'year' && entry.omaxAge >= pAges.year) ||
        (entry.omaxAgeUnit === 'month' && entry.omaxAge >= pAges.month) ||
        (entry.omaxAgeUnit === 'day' && entry.omaxAge >= pAges.day) ||
        (entry.omaxAgeUnit === 'nearestAge' && entry.omaxAge >= pAges.nearestAge) ||
        (entry.omaxAgeUnit === 'nextAge' && entry.omaxAge >= pAges.nextAge)
      ));
    }
  } else {
    return false;
  }
  return true;
};

module.exports.getPILabelByProductLine = (productLine, productLineOptions) => {
  let result = {};
  _.each(productLineOptions, (obj, key) => {
    if (key === productLine && obj.hasFairCover) {
      result.label =  'Cover page & Policy Illustration';
      result.fileName = 'cover_page_policy_illustration.pdf';
    } else if (key === productLine) {
      result.label =  'Policy Illustration';
      result.fileName = 'policy_illustration.pdf';
    }
 });
 return result;
};

module.exports.hasFairBiCover = (productLine, productLineOptions) => {
  let result = false;
  _.each(productLineOptions, (obj, key) => {
    if (key === productLine && obj.hasFairCover) {
      result = obj.hasFairCover;
    }
 });
 return result;
};

module.exports.getPILabelByQuotType = (quotType) => {
  let result = {};
  if (quotType === 'SHIELD') {
    result.label =  'Policy Illustration';
    result.fileName = 'policy_illustration.pdf';
  } else {
    result.label =  'Policy Illustration Document(s)';
    result.fileName = 'policy_illustration_document(s).pdf';
  }
 return result;
};
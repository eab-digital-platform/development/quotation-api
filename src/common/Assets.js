const _ = require('lodash');
const { optionsMap } = global;

/**
 * Handler that convert the data in db/staticJson to usable info
 * Data bypassed due to useless: 
 * 1. optionsMap.productLine
 * 2. optionsMap.relationship
 * 3. optionsMap.countryTelCode
 * 4. optionsMap.country
 */
var Assets = function () {
  this.optionsMap = {
    /**
     * @param  {} _options
     */
    getCurrencyList: (_options) => {
      const { ccy } = optionsMap;
      let result = {};
      for (var p in ccy.currencies[0].options) {
        if (_options && _options.filter && _options.filter.indexOf(ccy.currencies[0].options[p].value) > -1) {
          continue;
        }
        result[ccy.currencies[0].options[p].value] = ccy.currencies[0].options[p];
      }
      return result;
    },
    getOccupationList: (_options) => {
      const { occupation } = optionsMap;
      let result = _options && _options.filter.onlyValue ? [] : {};
      for(var p in occupation.options) {
        if (_options && _options.filter.onlyValue) {
          result.push(occupation.options[p].value);
          continue;
        }
        result[occupation.options[p].value] = occupation.options[p];
      }
      return result;
    },
    /**
     * @param  {} _options
     */
    getIndustryList: (_options, _occupationList) => {
      const isOccupationConditionExist = (_id) => {
        const result = _.find(occupations, o => o.condition == _id) > -1;
        return result;
      }
      const { industry } = optionsMap;
      const { occupations } = _occupationList || this.optionsMap.getOccupationList();
      let result = _options && _options.filter.onlyValue? [] : {};
      for (var p in industry[0].options) {
        if (_options && _options.filter.onlyValue && isOccupationConditionExist) {
          result.push(industry[0].options[p].value);
          continue;
        }
        if (isOccupationConditionExist) {
          result[industry[0].options[p].value] = industry[0].options[p];
        }
      }
      return result;
    },
    /**
    getCountryList: (_options) => {
      const { country } = optionsMap;
      let result = _options.filter.onlyValue? [] : {};
      for (var p in country.options) {
        if (_options.filter.onlyValue) {
          result.push(country.options[p]);
          continue;
        }
        result[p.value] = country.options[p];
    }
    },*/
    getMaritalList: (_options) => {
      const { marital } = optionsMap;
      let result = _options && _options.filter.onlyValue? [] : {};
      for (var p in marital.options) {
        if (_options && _options.filter.onlyValue) {
          result.push(marital.options[p].value);
          continue;
        }
        result[marital.options[p].value] = marital.options[p];
      }
      return result;
    },
    getNationalityList: (_options) => {
      const { nationality } = optionsMap;
      let result = _options && _options.filter.onlyValue? [] : {};
      for (var p in nationality.options) {
        if (_options && _options.filter.onlyValue) {
          result.push(nationality.options[p].value);
          continue;
        }
        result[nationality.options[p].value] = nationality.options[p];
      }
      return result;
    },
    getResidencyList: (_options) => {
      const { residency } = optionsMap;
      let result = _options && _options.filter.onlyValue? [] : {};
      for (var p in residency[0].options) {
        if (_options && _options.filter.onlyValue) {
          result.push(residency[0][p].value);
          continue;
        }
        result[residency[0].options[p].value] = residency[0].options[p];
      }
      return result;
    },
  }
}

module.exports = Assets;


const io = require('socket.io')();
io.use(require('socketio-wildcard')());

/**
 * Work as a event emitted by socket.io
 * emit the event to socket when needed
 */
const Notifier = function () {
  this.port = 60300;
  this.io = io.listen(this.port);

  // list of connected socket clients
  const clientMap = new Map();

  this.start = () => {
    console.log(`Notifier.js :: INIT :: PORT: ${this.port}...`);

    this.io.on('connection', (socket) => {
      console.log(`Notifier.js :: CLIENT :: CONNECTED :: [id=${socket.id}]`);
      clientMap.set(socket, 1);

      // when socket disconnects, remove it from the list:
      socket.on('disconnect', () => {
        clientMap.delete(socket);
        console.log(`Notifier.js :: CLIENT :: LEFT :: [id=${socket.id}]`);
      });
    });
  };

  this.sendMsg = ({ event, content }) => {
    console.log(`Notifier.js :: DEBUG :: SendMsg :: ${event} | ${JSON.stringify(content)} `);
    for (const [client] of clientMap.entries()) {
      client.emit(`${event}`, content);
    }
  };

  this.sendJobComplete = (_jobId) => {
    console.log(`send message to users. id:${_jobId}`);
    const data = JSON.stringify({ id: _jobId, time: `${new Date().getTime()}` });
    console.log(data);
    for (const [client] of clientMap.entries()) {
      client.emit('JOB_COMPLETED', data);
    }
  };
};
module.exports = Notifier;

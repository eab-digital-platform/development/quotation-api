const _ = require('lodash');
const fs = require('fs');
const path = require('path');

/**
 * Export the file as provided dir
 */
var FileExporter = function () {
  this.dir = './output/';
  this.name = new Date().getTime().toString();
  this.extension = '.json';
  this.encode = 'utf-8';
  /**
   * Set the o/p root directory
   * @param  {} _dir Ensure the end with '/'
   */
  this.setRootDirectory = (_dir) => {
    this.dir = './'.concat(_dir);
  }
  /**
   * Set the o/p directory
   * @param  {} _dir Ensure the end with '/'
   */
  this.setDirectory = (_dir) => {
    this.dir = this.dir.concat(_dir);
  };
  this.setName = (_name) => {
    this.name = _name;
  };
  this.setExtension = (_ext) => {
    this.extension = _ext;
  }
  this.setEncode = (_encode) => {
    this.encode = _encode;
  }

  this.checkDirectory = (_path) => {
    const getPath = (_path, _id) => {
      let path = _path.split(`/`);
      let result = path[0].concat(`/`, path[1]);
      for(var i=2;i<=_id;i++) {
        result = result.concat(`/`+path[i]);
      }
      return result;
    }
    let dirName = path.dirname(_path);
    dirName = dirName.split(`/`);
    for(var i=1; i<dirName.length; i++) {
      if (!fs.existsSync(getPath(path.dirname(_path) ,i))) {
        try {
          fs.mkdirSync(getPath(path.dirname(_path) ,i));
        } catch (err) {
          throw console.error(`FileExporter.js :: checkDirectory :: ERR :: ${dirName} :: ${err.message}`);
        }
      }
    }
    return true;
  }

  /**
   * @param  {String} contents
   * @param  {Object} options
   * @param  {boolean=} options.logError Log the error by global defined method
   * @param  {boolean=} options.opError Export the Error to file
   * @param  {boolean=} options.elapsedTime
   */
  this.toJson = (contents, options = {}) => {
    let logError = !options.logError ? true : options.logError;
    let opError = !options.opError ? true : options.opError;
    let elapsedTime = options.elapsedTime ? options.elapsedTime : null;

    if (elapsedTime) {
      Object.assign(contents, {
        ElapsedTime: elapsedTime
      });
    }

    if (contents && contents.errors) {
      _.each(contents.errors, e => {
        if (Object.keys(e).length > 0) {
          console.error(`FileExporter.js :: Logs :: Error occurred :: See Json for more details`);
          return;
        }
      });
    }

    const writePath = `${this.dir}${this.name}${this.extension}`;
    //new Promise((resolve, reject) => {
    //  resolve(this.checkDirectory(writePath));
    //}).then((result) => {
      if (this.checkDirectory(writePath)) {
        return new Promise((resolve, reject) => {
          fs.writeFile(writePath, JSON.stringify(contents), this.encode, (error) => {
            if (error) {
              if (logError) {
                console.log(`FileExporter.js :: ToJson :: LogError :: ${error}`);
              }
              if (opError) {
                fs.writeFile(`${this.dir}Error/${this.name}${this.extension}`, JSON.stringify(error), (subErr) => {
                    if (subErr) {
                      console.log(`FileExporter.js :: ToJson :: ERROR :: ${subErr}`);
                    }
                  },
                  console.log(`FileExporter.js :: ToJson :: ERROR ::See ${this.dir}Error${this.name}${this.extension}`))
              }
              reject();
            } else {
              console.log(`FileExporter.js :: ToJson :: EXPORT :: See: ${this.dir}${this.name}${this.extension}`);
              resolve();
            }
          })
        })
      } else {
        return false;
      }
    //})
  }
  /**
   * @param  {} _ref quotation Object
   * @param  {} _kvp new quotation object to replace
   */
  this.toQuotation = (_ref, _kvp, _options) => {
    let newQuotation = _.cloneDeep(_ref);
    if (_.isEmpty(_kvp) || _.isEmpty(_ref)) {
      return console.error(`FileExporter.js :: toQuotation :: ERR :: inputParameters is null`);
    }
    for (var p in _kvp) {
      _.set(newQuotation, p, _kvp[p]);
    }
    return newQuotation;
    // return this.toJson(newQuotation, _options);
  }
};

module.exports = FileExporter;
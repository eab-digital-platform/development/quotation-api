const NodeMailer = require('nodemailer');
const config = require('../../static/Configuration/Mail.json');

/**
 * Send mail by provided SMTP connection options
 * and its content
 */
const Mailer = function () {
  this.options = config.options;
  this.sender = 'sender@localhost';
  this.receiver = 'receiver@localhost';
  this.subject = 'sample subject';
  this.text = 'sample content';

  this.Send = (options) => {
    const transporter = NodeMailer.createTransport(this.options);
    const email = {
      from: options.sender || this.sender,
      to: options.receiver || this.receiver,
      subject: options.subject || this.subject,
      text: options.text || this.text
    };
    transporter.sendMail(email, (error, info) => {
      if (error) { throw error; }
      console.log('NodeMailer.js :: send :: Email sent!');
    });
  };
};

module.exports = Mailer;

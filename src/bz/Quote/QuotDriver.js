const _ = require('lodash');
// import { Sentry } from "react-native-sentry";
const QuotValid = require('./QuotValid');
const QuotCalc = require('./QuotCalc');
const QuotContext = require('./QuotContext');
const math = require('./math2.js');

const DateUtils = require('../../common/DateUtils');
const {getAgeByMethod, checkEntryAge} = require('../../common/ProductUtils');
const {parseDate, formatDate, getAges} = DateUtils;
const {getCurrency, getCurrencyByCcy, debug} = require('./utils');
const {getIllustrationStandard} = require('./illustrationMapping');

const logger = global.logger || console;
var FormulaParser = require('hot-formula-parser').Parser;
var parser = new FormulaParser();

var runExcelFunc = function(...args) {
  try{
    var funcStr = '';
    _.forEach(args, (arg, index) => {
      if (index === 0) {
        funcStr = arg + '(';
      } else {
        let varId = `VAR_${index}`;
        parser.setVariable(varId, arg);
        funcStr += (index > 1 ? ';' : '') + varId;
      }
    });
    funcStr += ')';

    var resultObj = parser.parse(funcStr);
    return resultObj.result;
  } catch (e) {
    return null;
  }

}

var QuotDriver = function () {
  this.context = new QuotContext();
  this.quotValid = new QuotValid(this.context, this);
  this.quotCalc = new QuotCalc(this.context, this);

  this.runFunc = (fnStr, para1, para2, para3, para4) => {
    let quotDriver = this;
    let quotValid = this.quotValid;
    let quotCalc = this.quotCalc;
    let dateUtils = DateUtils;

    if (typeof fnStr == 'string' && fnStr != 'undefined') {
      try {
        let fn = eval('(' + fnStr + ')');
        if (typeof fn == 'function') {
          let res = fn(para1, para2, para3, para4);
          return res;
        } else {
          debug.errFn = fn;
        }
      } catch (ex) {
        logger.log('QuotDriver.js :: ERROR :: QuotDriver.runfunc; ' + ex);
        if (ex.errCode) {
          throw ex;
        } else {
          debug.ex = ex.message || ex;
          debug.exStr = ex.stack;
          debug.funcStr = fnStr;
        }
      }
    } else if (fnStr && typeof fnStr == 'function') {
      return fnStr(para1, para2, para3, para4);
    }
    return null;
  };

  let runFunc = this.runFunc;
  
  this.filterProducts = (suitability, products, agent, profiles, extraParams) => {
    let productList = products;
    if (suitability.checkAgentForProductFunc) {
      let result = this.runFunc(suitability.checkAgentForProductFunc, suitability, agent, productList, extraParams);
      if (result && result.error) {
        return result;
      } else {
        productList = result.productList;
      }
    }
    if (suitability.checkClientForProductFunc) {
      let result = this.runFunc(suitability.checkClientForProductFunc, suitability, profiles, productList, extraParams);
      if (result && result.error) {
        return result;
      } else {
        productList = result.productList;
      }
    }
    return { productList: productList };
  };

  this.getProducts = (suitability, products, agent, profiles, fnaInfo, extraParams) => {
    let result = this.filterProducts(suitability, products, agent, profiles, extraParams);
    if (result.error) {
      return { error: result.message };
    }
    let productList = result.productList;
    if (fnaInfo) {
      productList = _.filter(productList, (product) => this.checkProdSuitForView(suitability, fnaInfo, product, profiles) === true);
    }
    productList = _.filter(productList, (product) => this.checkHidden(product, agent, extraParams.quickQuote) === true);
    return { productList: productList };
  };

  this.groupProducts = function (suitability, fnaInfo, products) {
    var fna = fnaInfo.fna;
    var aspects = fna && fna.aspects && fna.aspects.split(',');
    let groups = {};
    _.each(products, (product) => {
      let covCode = product.covCode;
      let suitableProd = suitability.products && suitability.products[covCode];
      if (suitableProd && suitableProd.needs) {
        let keys = _.filter(suitableProd.needs, n => aspects.indexOf(n) > -1);
        let groupKey = keys.length > 0 ? keys.join(',') : 'otherProds';
        if (!groups[groupKey]) {
          groups[groupKey] = {
            info: keys,
            products: []
          };
        }
        groups[groupKey].products.push(covCode);
      }
    });
    return groups;
  };

  /**
   * Checks whether the product can be viewed by the FNA info.
   * Returns true if product is viewable, or error message to prompt.
   *
   * @param {*} suitability suitability json
   * @param {*} fnaInfo the fna information holder json
   * @param {*} product the product json
   */
  this.checkProdSuitForView = (suitability, fnaInfo, product, profiles) => {
    if (suitability && suitability.checkProdSuitForViewFunc) {
      return this.runFunc(suitability.checkProdSuitForViewFunc, suitability, fnaInfo, product, profiles);
    }
    return true;
  };

  /**
   * Checks whether the product can be viewed in Quick Quote/ Products.
   * Returns true if product is viewable, or error message to prompt.
   *
   * @param {*} suitability suitability json
   * @param {*} fnaInfo the fna information holder json
   * @param {*} product the product json
   */
  this.checkHidden = (product, agent, quickQuote) => {
    if (quickQuote) {
      if (product.prodFeature && product.prodFeature.en){
        return !product.prodFeature.en.includes('quickQuoteHidden');
      }
    } else {
      if (product.prodFeature && product.prodFeature.en && agent.channel.type !== 'FA'){
        return !product.prodFeature.en.includes('productHidden');
      }
    }
    return true;
  };

  /**
   * Checks whether the product can be allowed for quotation by the FNA info.
   * Returns true if product can be quoted, or error message to prompt.
   *
   * @param {*} suitability suitability json
   * @param {*} fnaInfo the fna information holder json
   * @param {*} product the product json
   */
  this.checkProdSuitForQuot = (suitability, fnaInfo, product) => {
    if (suitability && suitability.checkProdSuitForQuotFunc) {
      return this.runFunc(suitability.checkProdSuitForQuotFunc, suitability, fnaInfo, product);
    }
    return true;
  };

  
  /**
   * @param  {} suitability
   * @param  {} fnaInfo
   * @param  {} product
   * @param  {} profiles
   */
  this.validateProdSuitability = (suitability, fnaInfo, product, profiles) => {
    return (
      this.checkProdSuitForView(suitability, fnaInfo, product, profiles) === true &&
      this.checkProdSuitForQuot(suitability, fnaInfo, product, profiles) === true
    );
  };

  /**
   * Checks whether the quotation inputs are allowed by the FNA info.
   * Returns true if allowed to proceed to proposal, or error message to prompt.
   *
   * @param {*} suitability
   * @param {*} fnaInfo
   * @param {*} quotation
   */
  this.validateQuotSuitability = (suitability, fnaInfo, quotation) => {
    if (suitability && suitability.checkQuotSuitabilityFunc) {
      return this.runFunc(suitability.checkQuotSuitabilityFunc, suitability, fnaInfo, quotation);
    }
    return true;
  };

  /**
   * @param  {} quotationJson
   * @param  {} planDetailsJson
   */
  this.calculateQuotation = (quotationJson, planDetailsJson) => {
    let resultObj = {};
    try {
      let quotation = 0, planDetails = 0;
      // Initialize variables based on the input parameters
      if (typeof quotationJson === 'string') {
        if (JSON && typeof JSON.parse === 'function') {
          quotation = JSON.parse(quotationJson);
          planDetails = JSON.parse(planDetailsJson);
        } else {
          eval('quotation = ' + quotationJson);
          eval('planDetails = ' + planDetailsJson);
        }
      } else if (typeof quotationJson === 'object') {
        quotation = quotationJson;
        planDetails = _.cloneDeep(planDetailsJson);
      }

      if (quotation && planDetails) {
        const bpDetail = planDetails[quotation.baseProductCode];

        let riskCommenDate = (quotation.isBackDate === 'Y') ? parseDate(quotation.riskCommenDate) : new Date();
        quotation.iAge = getAgeByMethod(bpDetail.calcAgeMethod, riskCommenDate, parseDate(quotation.iDob));
        quotation.pAge = getAgeByMethod(bpDetail.calcAgeMethod, riskCommenDate, parseDate(quotation.pDob));
        quotation.iAttainedAge = getAgeByMethod('current', riskCommenDate, parseDate(quotation.iDob));
        quotation.pAttainedAge = getAgeByMethod('current', riskCommenDate, parseDate(quotation.pDob));
        quotation.riskCommenDate = formatDate(riskCommenDate);

        let retVal = this.validateQuotation(quotation, planDetails);
        resultObj.quotation = retVal.quotation || quotation;

        planDetails = retVal.planDetails || planDetails;
        if (planDetails) {
          resultObj.inputConfigs = {};
          _.each(quotation.plans, (plan) => {
            resultObj.inputConfigs[plan.covCode] = planDetails[plan.covCode].inputConfig;
          });
        }
      } else {
        resultObj.error = {
          message: 'Quotation or Plan details missing.'
        };
      }
    } catch (ex) {
      debug.exception = {
        message: ex.message,
        stack: ex.stack
      };
    }
    return this.context.getResult(resultObj);
  };

  // generate illurstration and report data
  /*  parameter:
   *      quotation               : Object, quotation
   *      planDetails             : Object, map of covCode vs Product Configuration
   *      extraPara               : Object
   *          BI Options          : Object, key value map of BI options
   *          illustProps         : Object, map of covCode vs Illustration properties and rates
   *          templateFuncs       : Object, map of covCode vs PDF template formulas ( prepareReportDataFunc and prepareIlluDataFunc )
   *          requireReportData   : bool, indicate whether need to generate report data
   *  return: Object
   *      quotation               : quotation with illursation data
   *      reportData              : report data for Proposal
   */
  this.generateIllustrationData = (quotationJson, planDetailsJson, extraParaJson) => {
    var resultObj = {};
    var startTime = new Date();

    try {
      // Initialize variables based on the input parameters
      var quotation = 0, planDetails = 0, extraPara = 0;

      if (typeof quotationJson == 'string') {
        if (JSON && typeof JSON.parse === 'function') {
          quotation = JSON.parse(quotationJson);
          planDetails = JSON.parse(planDetailsJson);
          extraPara = JSON.parse(extraParaJson);
          // let globalValidation = JSON.parse(validationJson);
        } else {
          eval('quotation = ' + quotationJson);
          eval('planDetails = ' + planDetailsJson);
          eval('extraPara = ' + extraParaJson);
          // eval('let globalValidation = ' + validationJson);
        }
      } else if (typeof quotationJson == 'object') {
        quotation = (quotationJson);
        planDetails = (planDetailsJson);
        extraPara = (extraParaJson);
        // let globalValidation = (validationJson);
      }

      resultObj.error = null;

      if (quotation && planDetails) {
        try {
          var subStartTime = new Date();

          let retVal = this.validateQuotation(quotation, planDetails);
          quotation = retVal.quotation || quotation;
          planDetails = retVal.planDetails || planDetails;

          var subElapsedTimeMs = new Date() - subStartTime;
          logger.log("EXEC: generateIllustrationData | validateQuotation | Elapsed " + subElapsedTimeMs + "ms");
        } catch (ex) {
          ex.message = 'warning: failure to recal the quotation';
          throw ex;
        }

        // generate illustration
        // let illustrationList = [];
        var subStartTime2 = new Date();
        let retVal = this.generateIllustration(quotation, planDetails, extraPara);
        var subElapsedTimeMs2 = new Date() - subStartTime2;
        logger.log("EXEC: generateIllustrationData | generateIllustration | Elapsed " + subElapsedTimeMs2 + "ms");

        if (retVal.isIllustrationGenerated) {
          resultObj.illustrations = retVal.illustrations;

          if (extraPara.requireReportData) {
            extraPara.illustrations = retVal.illustrations;
            if(retVal.warningMsg){
              extraPara.warningMsg = retVal.warningMsg;
            }
            try {
              var subStartTime3 = new Date();
              resultObj.reportData = this.prepareReportData(quotation, planDetails, extraPara);
              var subElapsedTimeMs3 = new Date() - subStartTime3;
              logger.log("EXEC: generateIllustrationData | prepareReportData | Elapsed " + subElapsedTimeMs3 + "ms");
            } catch (ex) {
              ex.message = 'Failed to prepare data for PDF';
              throw ex;
            }
          }
        } else {
          // Failed to generate illustration
          let ex = {
            message: 'Failed to generate PDF due to generating illustration failure'
          };
          debug.err = retVal;
          throw ex;
        }
      }
    } catch (ex) {
      debug.exception = {
        message: ex.message,
        stack: ex.stack
      };
    }

    var elapsedTimeMs = new Date() - startTime;
    logger.log("EXEC: generateIllustrationData | Elapsed " + elapsedTimeMs + "ms");

    return this.context.getResult(resultObj);
  };

  this.convertIllustrationDataToStandard = (quotationJson,illustrationJson) => {
    var resultObj = {};
    var startTime = new Date();

    //const { reportData } = illustrationJson;
    //_.get(reportData, `root.mainInfo.name`); => {} / null 
    try {
      // This is the common part and valid for all products
      var lifeAssured = [{
        name: illustrationJson.reportData.root.mainInfo.name || "", 
        age: illustrationJson.reportData.root.mainInfo.age || "",
        gender: illustrationJson.reportData.root.mainInfo.gender || "",
        isSmoker: illustrationJson.reportData.root.mainInfo.isSmoker || "",
        dob: illustrationJson.reportData.root.mainInfo.dob || "",
        nationality: illustrationJson.reportData.root.mainInfo.nationality || "",
        occupation: illustrationJson.reportData.root.mainInfo.occupation || "",
        residence: illustrationJson.reportData.root.mainInfo.residence || "",
      }];
      var proposer = [{
        name: illustrationJson.reportData.root.mainInfo.pName || "",
        age: illustrationJson.reportData.root.mainInfo.pAge || "",
        gender: illustrationJson.reportData.root.mainInfo.pGender || "",
        isSmoker: illustrationJson.reportData.root.mainInfo.pIsSmoker || "",
        dob: illustrationJson.reportData.root.mainInfo.pDob || "",
        nationality: illustrationJson.reportData.root.mainInfo.pNationality || "",
        occupation: illustrationJson.reportData.root.mainInfo.pOccupation || "",
        residence: illustrationJson.reportData.root.mainInfo.pResidence || "",
      }];

      // remove the unnecessary fields from funds.
      var funds = {};
      if(quotationJson.fund != null) {
        funds = quotationJson.fund.funds;
        for (var i = 0; i < funds.length; i++) {
          //delete $products[i].position;
          delete funds[i].fundName;
          delete funds[i].topUpAlloc;
       }
      }

      // rename the planInfo fields
      var planInfos = [];
      const illustPlanInfos = illustrationJson.reportData.root.planInfos
      if(illustPlanInfos != null) {
        ///planInfos = illustrationJson.reportData.root.planInfos;
        for (var i = 0; i < illustPlanInfos.length; i++) {
          planInfos[i] = {
            planName: illustPlanInfos[i].planName || "",
            postfix: illustPlanInfos[i].postfix || "",
            sumAssured: illustPlanInfos[i].sumAssured || "",
            policyTerm: illustPlanInfos[i].policyTerm || "",
            modalPremium: illustPlanInfos[i].premium || "",
            annualPremium: illustPlanInfos[i].APremium || "",
            halfYearPrem: illustPlanInfos[i].HPremium || "",
            quarterlyPremium: illustPlanInfos[i].QPremium || "",
            monthlyPremium: illustPlanInfos[i].MPremium || "",
            singlePremium: illustPlanInfos[i].LPremium || "",
            premiumPayTerm: illustPlanInfos[i].permTerm || "",
            paymentMode: illustPlanInfos[i].payMode || "",
            planIndicator: illustPlanInfos[i].planInd || "",
            planCode: illustPlanInfos[i].covCode || "",
            productLine: illustPlanInfos[i].productLine || "",
          }
       }
      }
      
      var input = {
        lifeAssured: lifeAssured || "",
        proposer: proposer || "",
        currency: illustrationJson.reportData.root.mainInfo.currency || "",
        riskCommenDate: illustrationJson.reportData.root.mainInfo.riskCommenDate || "",
        basicPlanName: illustrationJson.reportData.root.mainInfo.basicPlanName || "",
        basicPlanCode: illustrationJson.reportData.root.mainInfo.basicPlanCode || "",
        postfix: illustrationJson.reportData.root.mainInfo.postfix || "",
        singlePremium: illustrationJson.reportData.root.mainInfo.singlePremium || "",
        modalPremium: illustrationJson.reportData.root.mainInfo.premium || "",
        annualPremium: illustrationJson.reportData.root.mainInfo.yearlyPremium || "",
        halfYearlyPremium: illustrationJson.reportData.root.mainInfo.halfYearlyPremium || "",
        quarterlyPremium: illustrationJson.reportData.root.mainInfo.quarterlyPremium || "",
        monthlyPremium: illustrationJson.reportData.root.mainInfo.monthlyPremium || "",
        sumAssured: illustrationJson.reportData.root.mainInfo.sumAssured || "",
        // the below tax fields are for Band Aids only, other products are 0.
        annualPremiumTax: "",
        halfYearlyPremiumTax: "",
        quarterlyPremiumTax: "",
        monthlyPremiumTax: "",
        /////
        planInfos: planInfos,
        policyOptions: quotationJson.policyOptions,
        funds: funds,
        agent: illustrationJson.reportData.root.mainInfo.agent,
        releaseVersion: illustrationJson.reportData.root.releaseVersion || ""
      }

      illustration = getIllustrationStandard(illustrationJson.reportData.root.mainInfo.basicPlanCode, illustrationJson);


      // combine together
      resultObj = {
        input: input,
        illustration: illustration
      }
    } catch (ex) {
      debug.exception = {
        message: ex.message,
        stack: ex.stack
      };
    }

    var elapsedTimeMs = new Date() - startTime;
    logger.log("EXEC: convertIllustrationDataToStandard | Elapsed " + elapsedTimeMs + "ms");

    return this.context.getResult(resultObj);
  };

  this.assignExtraPropertiesToPlanDetail = (planDetail, quotation, planDetails) => {
    planDetail.inputConfig = {};

    if (planDetail.planInd === 'B') {
      if (planDetail.formulas && planDetail.formulas.prepareQuotConfigs) {
        this.runFunc(planDetail.formulas.prepareQuotConfigs, quotation, planDetail, planDetails);
      } else {
        this.prepareQuotConfigs(quotation, planDetail, planDetails);
      }
    }

    if (planDetail.formulas && planDetail.formulas.preparePolicyOptions) {
      this.runFunc(planDetail.formulas.preparePolicyOptions, planDetail, quotation, planDetails);
    } else {
      this.preparePolicyOptions(planDetail, quotation, planDetails);
    }

    if (planDetail.formulas && planDetail.formulas.prepareClassConfig) {
      this.runFunc(planDetail.formulas.prepareClassConfig, planDetail, quotation, planDetails);
    } else {
      this.prepareClassConfig(planDetail, quotation, planDetails);
    }

    if (planDetail.formulas && planDetail.formulas.prepareTermConfig) {
      this.runFunc(planDetail.formulas.prepareTermConfig, planDetail, quotation, planDetails);
    } else {
      this.prepareTermConfig(planDetail, quotation, planDetails);
    }

    if (planDetail.formulas && planDetail.formulas.prepareSAPremConfig) {
      this.runFunc(planDetail.formulas.prepareSAPremConfig, planDetail, quotation, planDetails);
    } else {
      this.prepareAmountConfig(planDetail, quotation, planDetails);
    }

    if (planDetail.planInd === 'B') {
      if (planDetail.formulas && planDetail.formulas.prepareAttachableRider) {
        this.runFunc(planDetail.formulas.prepareAttachableRider, planDetail, quotation, planDetails);
      } else {
        this.prepareAttachableRider(planDetail, quotation, planDetails);
      }
    }

    return planDetail;
  };

  this.prepareQuotConfigs = function (quotation, planDetail, planDetails) {
    if (!quotation.ccy) {
      quotation.ccy = planDetail.currencies[0].ccy[0];
    }

    const payModes = [];
    _.each(planDetail.payModes, (payMode) => {
      payModes.push(_.clone(payMode));
      if (!quotation.paymentMode && payMode.default === 'Y') {
        quotation.paymentMode = payMode.mode;
      }
    });
    planDetail.inputConfig.payModes = payModes;
  };

  this.preparePolicyOptions = (planDetail, quotation, planDetails) => {
    let policyOptions = [];
    for (let p in planDetail.policyOptions) {
      let po = _.cloneDeep(planDetail.policyOptions[p]);
      if (po.type === 'datepicker' ||
        (po.type === 'text' && (po.subType === 'currency' || po.subType === 'number'))) {
        if (po.max || po.max === 0) {
          po.max = parseInt(po.max, 10);
        }
        if (po.min || po.min === 0) {
          po.min = parseInt(po.min, 10);
        }
      }

      if (po.type === 'picker' && po.options) {
        if (!quotation.policyOptions[po.id] && po.value && po.options.length) {
          if (_.find(po.options, opt => opt.value === po.value)) {
            quotation.policyOptions[po.id] = po.value;
          } else if (po.value === '999') {
            quotation.policyOptions[po.id] = po.options[po.options.length - 1].value;
          } else {
            quotation.policyOptions[po.id] = null;
          }
        }
      } else {
        if (!quotation.policyOptions[po.id] && (po.value || po.value === 0 || po.value === '')) {
          if (po.type === 'text' && (po.subType === 'currency' || po.subType === 'number')) {
            quotation.policyOptions[po.id] = parseFloat(po.value);
          } else {
            quotation.policyOptions[po.id] = po.value;
          }
        }
      }
      policyOptions.push(po);
    }
    planDetail.inputConfig.policyOptions = policyOptions;
  };

  this.preparePremLimit = (planDetail, quotation) => {

    let planInfo = {
      classType: planDetail.inputConfig.defaultClassType || '',
      policyTerm: planDetail.inputConfig.defaultPolicyTerm || '',
      premTerm: planDetail.inputConfig.defaultPremTerm || ''
    };

    if (quotation.plans) {
      for (let p in quotation.plans) {
        if (quotation.plans[p].covCode == planDetail.covCode) {
          planInfo = Object.assign(planInfo, quotation.plans[p]);
        }
      }
    }

    let saInput = planDetail.saInput;
    if (saInput && saInput.length > 0) {
      for (let i in saInput) {
        if (saInput[i].ccy === quotation.ccy) {
          planDetail.inputConfig.saInput = saInput[i];
          break;
        }
      }
    }

    let premInput = planDetail.premInput;
    if (premInput && premInput.length > 0) {
      for (let i in premInput) {
        if (premInput[i].ccy === quotation.ccy) {
          planDetail.inputConfig.premInput = premInput[i];
          break;
        }
      }
    }

    if (planDetail.premInputInd === 'Y') {
      let premlim = [];
      if (!planDetail.premlim) {
        // calc from benlim and put to input config
        if (planDetail.benlim && planDetail.benlim.length && planDetail.formulas && planDetail.formulas.saFunc) {
          for (let b in planDetail.benlim) {
            let bl = planDetail.benlim[b];

            if ((!bl.country || bl.country == '*' || bl.country == quotation.iResidence)
              && (!bl.classType || bl.classType == '*' || bl.classType == planInfo.classType)
              && quotation.iAge >= bl.ageFr && quotation.iAge <= bl.ageTo) {

              for (let l in bl.limits) {
                let lim = bl.limits[l];
                if (lim.ccy == quotation.ccy && lim.payMode == quotation.paymentMode) {
                  premlim = this.calcMinMaxPrem(planDetail, quotation, planInfo, lim);
                  break;
                }
              }
            }
          }
        }
      } else {
        // massage premlim and put to input config
        if (planDetail.premlim.length) {
          for (let p in planDetail.premlim) {
            let pl = planDetail.premlim[p];

            if ((!pl.country || pl.country == '*' || pl.country == quotation.iResidence)
              && (!pl.classType || pl.classType == '*' || pl.classType == planInfo.classType)
              && quotation.iAge >= pl.ageFr && quotation.iAge <= pl.ageTo) {
              for (let l in pl.limits) {
                let lim = pl.limits[l];
                if (lim.ccy == quotation.ccy && lim.payMode == quotation.paymentMode) {
                  premlim = {
                    max: lim.max,
                    min: lim.min
                  };
                  break;
                }
              }
            }
          }
        }
      }
      planDetail.inputConfig.premlim = premlim;
    }

    if (planDetail.saInputInd === 'Y') {
      let benlim = [];
      if (!planDetail.benlim) {
        // calc from benlim and put to input config
        if (planDetail.premlim && planDetail.premlim.length && planDetail.formulas && planDetail.formulas.saFunc) {
          for (let p in planDetail.premlim) {
            let pl = planDetail.premlim[p];

            if ((!pl.country || pl.country == '*' || pl.country == quotation.iResidence)
              && (!pl.classType || pl.classType == '*' || pl.classType == planInfo.classType)
              && quotation.iAge >= pl.ageFr && quotation.iAge <= pl.ageTo) {

              for (let l in pl.limits) {
                let lim = pl.limits[l];
                if (lim.ccy == quotation.ccy && lim.payMode == quotation.paymentMode) {
                  benlim = this.calcMinMaxSa(planDetail, quotation, planInfo, lim);
                  break;
                }
              }
            }
          }
        }
      } else {
        // massage premlim and put to input config
        if (planDetail.benlim.length) {
          for (let b in planDetail.benlim) {
            let bl = planDetail.benlim[b];

            if ((!bl.country || bl.country == '*' || bl.country == quotation.iResidence)
              && (!bl.classType || bl.classType == '*' || bl.classType == planInfo.classType)
              && quotation.iAge >= bl.ageFr && quotation.iAge <= bl.ageTo) {

              for (let l in bl.limits) {
                let lim = bl.limits[l];
                if (lim.ccy == quotation.ccy && lim.payMode == quotation.paymentMode) {
                  benlim = {
                    max: lim.max,
                    min: lim.min
                  };
                  break;
                }
              }
            }
          }
        }
      }

      planDetail.inputConfig.benlim = benlim;
    }
  };

  this.calcMinMaxSa = (planDetail, quotation, planInfo, limit) => {

    planInfo.premium = limit.min;
    let minSA = 0;
    if (limit.min != 0) {
      minSA = this.runFunc(planDetail.saFunc || (planDetail.formulas && planDetail.formulas.saFunc), quotation, planInfo, planDetail);
    }

    planInfo.premium = limit.max;
    let maxSA = 0;
    if (limit.max != 0) {
      maxSA = this.runFunc(planDetail.saFunc || (planDetail.formulas && planDetail.formulas.saFunc), quotation, planInfo, planDetail);
    }

    return {
      min: minSA,
      max: maxSA
    };
  };

  this.calcMinMaxPrem = (planDetail, quotation, planInfo, limit) => {

    planInfo.sumInsured = limit.min;
    let minPrem = 0;
    if (limit.min != 0) {
      minPrem = this.runFunc(planDetail.premFunc || (planDetail.formulas && planDetail.formulas.premFunc), quotation, planInfo, planDetail);
    }

    planInfo.sumInsured = limit.max;
    let maxPrem = 0;
    if (limit.max != 0) {
      maxPrem = this.runFunc(planDetail.premFunc || (planDetail.formulas && planDetail.formulas.premFunc), quotation, planInfo, planDetail);
    }

    return {
      min: minPrem,
      max: maxPrem
    };
  };

  this.prepareAmountConfig = (planDetail, quotation) => {
    let canEditSumAssured = false;
    let canEditPremium = false;
    let canViewSumAssured = false;

    let missPolTermInput = false;
    for (let i = 0; i < quotation.plans.length; i++) {
      let plan = quotation.plans[i];
      if (plan.covCode === planDetail.covCode && planDetail.polTermInd === 'Y' && !plan.policyTerm && !planDetail.inputConfig.defaultPolicyTerm) {
        missPolTermInput = true;
      }
    }

    if (quotation.paymentMode && !missPolTermInput) {
      if (planDetail.saInputInd && planDetail.saInputInd === 'Y') {
        canEditSumAssured = true;
      }
      if (planDetail.premInputInd && planDetail.premInputInd === 'Y') {
        canEditPremium = true;
      }
    }

    if (planDetail.saViewInd && planDetail.saViewInd === 'Y') {
      canViewSumAssured = true;
    }

    planDetail.inputConfig.canEditSumAssured = canEditSumAssured && canViewSumAssured;
    planDetail.inputConfig.canEditPremium = canEditPremium;
    planDetail.inputConfig.canViewSumAssured = canViewSumAssured;

    planDetail.inputConfig.canViewOthSa = planDetail.othSaInd === 'Y';
    planDetail.inputConfig.canEditOthSa = planDetail.othSaInputInd === 'Y';

    this.preparePremLimit(planDetail, quotation);
  };

  this.prepareClassConfig = (planDetail, quotation) => {
    if (planDetail.classList) {
      let classList = planDetail.classList;
      planDetail.inputConfig.hasClass = planDetail.classInd === 'Y';
      planDetail.inputConfig.canEditClassType = planDetail.classInputInd === 'Y';

      let classTypeList = [];
      for (let i = 0; i < classList.length; i++) {
        let classType = classList[i];
        if (classType.default && classType.default === 'Y') {
          planDetail.inputConfig.defaultClassType = classType.covClass;
        }
        classTypeList.push({
          value: classType.covClass,
          title: classType.className
        });
      }
      if (classTypeList.length > 0) {
        planDetail.inputConfig.classTypeList = classTypeList;
      }
      planDetail.inputConfig.classTitle = planDetail.classTitle;
    } else {
      planDetail.inputConfig.canEditClassType = false;
    }
  };

  this.prepareAttachableRider = (planDetail, quotation, planDetails) => {
    const riderList = [];
    if (planDetail.riderList && planDetail.riderList.length) {
      for (let r in planDetail.riderList) {
        const rider = planDetail.riderList[r];

        if (rider.condition && rider.condition.length) {
          for (let c in rider.condition) {
            const cond = rider.condition[c];

            if ((!quotation.iResidence || cond.country === '*' || quotation.iResidence === cond.country) &&
              (!quotation.dealerGroup || cond.dealerGroup === '*' || quotation.dealerGroup === cond.dealerGroup) &&
              (!cond.endAge || quotation.iAge <= cond.endAge) &&
              (!cond.staAge || quotation.iAge >= cond.staAge)) {

              const riderDetail = planDetails[rider.covCode];
              if (riderDetail) {
                if (!_.find(riderDetail.payModes, pm => pm.mode === quotation.paymentMode)) {
                  continue;
                }
                const validRider = {};
                _.each(rider, (value, key) => {
                  if (key !== 'condition') {
                    validRider[key] = value;
                  }
                });
                _.each(cond, (value, key) => {
                  if (['country', 'dealerGroup', 'staAge', 'endAge', 'amount'].indexOf(key) === -1) {
                    validRider[key] = value;
                  }
                });
                _.each(cond.amount, (amt) => {
                  if (quotation.ccy === amt.ccy) {
                    Object.assign(validRider, amt);
                  }
                });
                riderList.push(validRider);
              }
            }
          }
        }
      }
      planDetail.inputConfig.riderList = riderList;
    }
  };

  this.polTermsFunc = (planDetail, quotation) => {
    let policyTermList = null;

    let foundDefault = false;
    if (planDetail.polMaturities) {
      policyTermList = [];
      for (let p in planDetail.polMaturities) {
        let mat = planDetail.polMaturities[p];
        if (!mat.country || mat.country == '*' || mat.country == quotation.residence && mat.minTerm && mat.maxTerm) {
          let interval = mat.interval || 1;
          for (let m = mat.minTerm;
            m <= mat.maxTerm && (!mat.maxAge || !quotation.iAge || (m + quotation.iAge) <= mat.maxAge)
            && (!mat.ownerMaxAge || !quotation.pAge || (m + quotation.pAge) <= mat.ownerMaxAge);
            m += interval) {
            foundDefault = foundDefault || (mat.default === m);
            policyTermList.push({
              "value": m + "",
              "title": m + (quotation.iAge ? "(@" + (quotation.iAge + m) + ")" : ""),
              "default": mat.default === m
            });
          }
          if (mat.default === 0) {
            policyTermList[0].default = true;
          } else if (mat.default === 999 || !foundDefault) {
            policyTermList[policyTermList.length - 1].default = true;
          }
        }
      }
    } else if (planDetail.wholeLifeInd && planDetail.wholeLifeInd == 'Y' && planDetail.wholeLifeAge) {
      return [{
        "value": "999",
        "title": "To Age " + planDetail.wholeLifeAge,
        "default": true
      }];
    }
    return policyTermList;
  };

  this.premTermsFunc = (planDetail, quotation) => {
    let premTermList = null;
    let foundDefault = false;
    planDetail.inputConfig.isPolTermHide = planDetail.isPolTermHide == "Y";
    planDetail.inputConfig.isPremTermHide = planDetail.isPremTermHide == "Y";
    if (planDetail.premMaturities) {
      premTermList = [];
      for (let p in planDetail.premMaturities) {
        let mat = planDetail.premMaturities[p];
        if (!mat.country || mat.country == '*' || mat.country == quotation.residence && mat.minTerm && mat.maxTerm) {
          let interval = mat.interval || 1;
          for (let m = mat.minTerm;
            m <= mat.maxTerm && (!mat.maxAge || !quotation.iAge || (m + quotation.iAge) <= mat.maxAge)
            && (!mat.ownerMaxAge || !quotation.pAge || (m + quotation.pAge) <= mat.ownerMaxAge);
            m += interval) {
            foundDefault = foundDefault || (mat.default === m);
            premTermList.push({
              "value": m + "",
              "title": m + (quotation.iAge ? "(@" + (quotation.iAge + m) + ")" : ""),
              "default": mat.default === m
            });
          }

          if (mat.default === 0) {
            premTermList[0].default = true;
          } else if (mat.default === 999 || !foundDefault) {
            premTermList[premTermList.length - 1].default = true;
          }

        }
      }
    }
    return premTermList;
  };

  this.prepareTermConfig = (planDetail, quotation, planDetails) => {

    let policyTermList = [];
    let premTermList = [];

    if (planDetail.premMatSameAsBasic === 'Y') {
      let basicDetail = planDetails[quotation.baseProductCode];
      premTermList = basicDetail.inputConfig.premTermList;
    } else {
      if (planDetail.formulas && planDetail.formulas.premTermsFunc) {
        premTermList = this.runFunc(planDetail.formulas.premTermsFunc, planDetail, quotation);
      } else {
        premTermList = this.premTermsFunc(planDetail, quotation);
      }
    }

    if (planDetail.policyMatSameAsBasic === 'Y') {
      let basicDetail = planDetails[quotation.baseProductCode];
      policyTermList = basicDetail.inputConfig.policyTermList;
    } else {
      if (planDetail.formulas && planDetail.formulas.polTermsFunc) {
        policyTermList = this.runFunc(planDetail.formulas.polTermsFunc, planDetail, quotation);
      } else {
        policyTermList = this.polTermsFunc(planDetail, quotation);
      }
    }

    if (planDetail.polMatSameAsPremMat === 'Y') {
      policyTermList = premTermList;
    } else if (planDetail.premMatSameAsPolMat === 'Y') {
      premTermList = policyTermList;
    }

    if (policyTermList && policyTermList.length) {
      for (let p in policyTermList) {
        if (policyTermList[p].default) {
          planDetail.inputConfig.defaultPolicyTerm = policyTermList[p].value;
          break;
        }
      }
      planDetail.inputConfig.canEditPolicyTerm = planDetail.polTermInd === 'Y';
    }
    planDetail.inputConfig.policyTermList = policyTermList;

    if (premTermList && premTermList.length) {
      for (let p in premTermList) {
        if (premTermList[p].default) {
          planDetail.inputConfig.defaultPremTerm = premTermList[p].value;
          break;
        }
      }
      planDetail.inputConfig.canEditPremTerm = planDetail.premTermInd === 'Y';
    }
    planDetail.inputConfig.premTermList = premTermList;
    // planDetail.inputConfig.termChangeTogether = planDetail.polTermSameAsPremTerm || planDetail.premTermSameAsPolTerm;
  };

  this.validateQuotation = (quotation, planDetails) => { //, planInfoList, globalValidation, basicPlanInfo) {
    let retVal = {};

    if (quotation.baseProductCode && planDetails[quotation.baseProductCode]) {

      let basicPlanInfo = 0;
      let basicPlanCode = '';
      let planInfoList = 0;
      let basicPlanDetail = 0;
      let newQuot = false;

      // check if it is initializing
      if (quotation.plans && quotation.plans.length) {
        basicPlanInfo = quotation.plans[0];
        basicPlanCode = basicPlanInfo.covCode;
        basicPlanDetail = planDetails[basicPlanCode];
      } else if (quotation.baseProductCode) {
        newQuot = true;
        basicPlanCode = quotation.baseProductCode;
        basicPlanDetail = planDetails[basicPlanCode];
        basicPlanInfo = {
          covCode: basicPlanDetail.covCode
        };
        quotation.plans = [];
        quotation.plans.push(basicPlanInfo);
      }
      planInfoList = quotation.plans;

      let riderList = [];

      if (basicPlanDetail) {
        // assign extra properties for basic plan first
        let details = this.assignExtraPropertiesToPlanDetail(basicPlanDetail, quotation, planDetails);

        if (details && details.inputConfig) {
          // check auto attach rider for new quotation and compulsory rider
          if (details.inputConfig.riderList && details.inputConfig.riderList.length) {
            const riskCommenDate = quotation.riskCommenDate ? parseDate(quotation.riskCommenDate) : new Date();
            const pAges = getAges(riskCommenDate, parseDate(quotation.pDob));
            const iAges = getAges(riskCommenDate, parseDate(quotation.iDob));
            riderList = details.inputConfig.riderList = _.filter(details.inputConfig.riderList, (rider) => {
              const riderDetail = planDetails[rider.covCode];
              return checkEntryAge(riderDetail, quotation.iResidence, pAges, iAges);
            });
            _.each(riderList, (rider) => {
              if ((newQuot && rider.autoAttach === 'Y') || rider.compulsory === 'Y') {
                if (!_.find(quotation.plans, p => p.covCode === rider.covCode)) {
                  let riderDetail = planDetails[rider.covCode];
                  planInfoList.push({
                    covCode: riderDetail.covCode,
                    covName: riderDetail.covName,
                    version: riderDetail.version,
                    productLine: riderDetail.productLine,
                    hiddenRider: rider.hiddenRider,
                    hiddenInSelectRiderDialog: rider.hiddenInSelectRiderDialog,
                    addSum: rider.addSum
                  });
                }
              }
            });
          }
          let bpInfo = quotation.plans[0];

          bpInfo.covName = basicPlanDetail.covName;
          bpInfo.productLine = basicPlanDetail.productLine;
          bpInfo.version = basicPlanDetail.version;


          if (details.premTermRule == "BT") {
            bpInfo.premTerm = bpInfo.policyTerm;
          }
          if (details.polTermRule == "PT") {
            bpInfo.policyTerm = bpInfo.premTerm;
          }

          if (newQuot) {
            bpInfo.policyTerm = details.inputConfig.defaultPolicyTerm;
            bpInfo.premTerm = details.inputConfig.defaultPremTerm;
            bpInfo.classType = details.inputConfig.defaultClassType;
          }

          // reset basic plan's sa and prem to fulfill the limit and rule
          let saInput = details.inputConfig.saInput;
          let premInput = details.inputConfig.premInput;
          let benlim = details.inputConfig.benlim;
          let premlim = details.inputConfig.premlim;

          /** Addon */
          bpInfo.sumInsured = Number(bpInfo.sumInsured);

          if (bpInfo.calcBy == 'sumAssured' && (bpInfo.sumInsured || bpInfo.sumInsured == 0) && typeof bpInfo.sumInsured == 'number') {
            // reset sa for decimal and factor if required
            if (saInput && saInput.decimal) {
              let scale = math.pow(10, saInput.decimal);
              bpInfo.sumInsured = math.number(math.divide(math.floor(math.multiply(math.bignumber(bpInfo.sumInsured), scale)), scale));
            }

            if (saInput && saInput.factor) {
              bpInfo.sumInsured = math.number(math.multiply(math.round(math.divide(math.bignumber(bpInfo.sumInsured), saInput.factor)), saInput.factor));
            }

            if (benlim) {
              if (benlim.min && benlim.min > bpInfo.sumInsured) {
                bpInfo.sumInsured = benlim.min;
              }
              if (benlim.max && benlim.max < bpInfo.sumInsured) {
                bpInfo.sumInsured = benlim.max;
              }
            }
          } else if (bpInfo.calcBy == 'premium' && (bpInfo.premium || bpInfo.premium == 0) && typeof bpInfo.premium == 'number') {
            // reset premium for decimal and factor if required
            if (premInput && premInput.decimal) {
              let scale = math.pow(10, premInput.decimal);
              bpInfo.premium = math.number(math.divide(math.floor(math.multiply(math.bignumber(bpInfo.premium), scale)), scale));
            }

            if (premInput && premInput.factor) {
              bpInfo.premium = math.number(math.multiply(math.round(math.divide(math.bignumber(bpInfo.premium), premInput.factor)), premInput.factor));
            }

            if (premlim) {
              if (premlim.min && premlim.min > bpInfo.premium) {
                bpInfo.premium = premlim.min;
              }
              if (premlim.max && premlim.max < bpInfo.premium) {
                bpInfo.premium = premlim.max;
              }
            }
          }

          // planDetails[basicPlanCode].inputConfig = details.inputConfig;
        }

        // sort the rider in order
        var seqMap = {};
        if (basicPlanDetail.inputConfig && basicPlanDetail.inputConfig.riderList) {
          for (var rr = 0; rr < basicPlanDetail.inputConfig.riderList.length; rr++) {
            var rd = basicPlanDetail.inputConfig.riderList[rr];
            seqMap[rd.covCode] = rr;
          }
        }


        planInfoList.sort(function (a, b) {
          if (a.covCode == basicPlanCode) {
            return -1;
          } else if (b.covCode == basicPlanCode) {
            return 1;
          }
          return seqMap[a.covCode] - seqMap[b.covCode];
        });

        // quotation.plans = planInfoList;

        // assign extra properties for rider
        for (let p = 1; p < planInfoList.length; p++) {
          let plan = planInfoList[p];
          let planDetail = planDetails[plan.covCode];
          let details = this.assignExtraPropertiesToPlanDetail(planDetail, quotation, planDetails);

          // perform rider config. override by basic plan
          if (riderList && riderList.length) {
            for (let r in riderList) {
              let rider = riderList[r];
              if (rider.covCode == plan.covCode) {
                if (rider.maxSA) {
                  details.inputConfig.benlim.max = rider.maxSA;
                }
                if (rider.minSA) {
                  details.inputConfig.benlim.min = rider.minSA;
                }
                break;
              }
            }
          }
          plan.covName = planDetail.covName;
          plan.saPostfix = planDetail.saPostfix;
          plan.productLine = planDetail.productLine;
          plan.version = planDetail.version;

          if (details.polTermRule == "BPT") {
            plan.policyTerm = basicPlanInfo.premTerm;
          } else if (details.polTermRule == "BBT") {
            plan.policyTerm = basicPlanInfo.policyTerm;
          } else if (!quotation.plans[p].policyTerm) {
            plan.policyTerm = details.inputConfig.defaultPolicyTerm;
          }

          if (details.premTermRule == "BPT") {
            plan.premTerm = basicPlanInfo.premTerm;
          } else if (details.premTermRule == "BBT") {
            plan.premTerm = basicPlanInfo.policyTerm;
          } else if (!quotation.plans[p].policyTerm) {
            plan.premTerm = details.inputConfig.defaultPremTerm;
          }

          if (details.premTermRule == "BT") {
            plan.premTerm = plan.policyTerm;
          }
          if (details.polTermRule == "PT") {
            plan.policyTerm = plan.premTerm;
          }

          plan.classType = quotation.plans[p].classType || details.inputConfig.defaultClassType;
          // planDetails[plan.covCode].inputConfig = details.inputConfig;

          // reset basic plan's sa and prem to fulfill the limit
          let saInput = details.inputConfig.saInput;
          let premInput = details.inputConfig.premInput;
          let benlim = details.inputConfig.benlim;
          let premlim = details.inputConfig.premlim;

          if (plan.calcBy == 'sumAssured' && (plan.sumInsured || plan.sumInsured == 0) && typeof plan.sumInsured == 'number') {
            // reset sa for decimal and factor if required
            if (saInput && saInput.decimal) {
              let scale = math.pow(10, saInput.decimal);
              plan.sumInsured = math.number(math.divide(math.floor(math.multiply(math.bignumber(plan.sumInsured), scale)), scale));
            }

            if (saInput && saInput.factor) {
              plan.sumInsured = math.number(math.multiply(math.round(math.divide(math.bignumber(plan.sumInsured), saInput.factor)), saInput.factor));
            }

            if (benlim) {
              if (benlim.min && benlim.min > plan.sumInsured) {
                plan.sumInsured = benlim.min;
              }
              if (benlim.max && benlim.max < plan.sumInsured) {
                plan.sumInsured = benlim.max;
              }
            }
          } else if (plan.calcBy == 'premium' && (plan.premium || plan.premium == 0) && typeof plan.premium == 'number') {
            // reset premium for decimal and factor if required
            if (premInput && premInput.decimal) {
              let scale = math.pow(10, premInput.decimal);
              plan.premium = math.number(math.divide(math.floor(math.multiply(math.bignumber(plan.premium), scale)), scale));
            }

            if (premInput && premInput.factor) {
              plan.premium = math.number(math.multiply(math.round(math.divide(math.bignumber(plan.premium), premInput.factor)), premInput.factor));
            }

            if (premlim) {
              if (premlim.min && premlim.min > plan.premium) {
                plan.premium = premlim.min;
              }
              if (premlim.max && premlim.max < plan.premium) {
                plan.premium = premlim.max;
              }
            }
          }
        }

        for (let i = 0; i < planInfoList.length; i++) {
          let detials = planDetails[planInfoList[i].covCode];
          if (detials.formulas && detials.formulas.willValidQuotFunc) {
            this.runFunc(detials.formulas.willValidQuotFunc, quotation, planInfoList[i], planDetails);
          }
        }
        //saViewInd, policy term and premium term title
        for (let i = 0; i < planInfoList.length; i++) {
          let plan = planInfoList[i];
          let pDetail = planDetails[planInfoList[i].covCode];
          let policyTermList = pDetail.inputConfig.policyTermList || [];
          for (let j = 0; j < policyTermList.length; j++) {
            if (policyTermList[j].value == plan.policyTerm) {
              plan.polTermDesc = policyTermList[j].title;
            }
          }
          let premTermList = pDetail.inputConfig.premTermList || [];
          for (let j = 0; j < premTermList.length; j++) {
            if (premTermList[j].value == plan.premTerm) {
              plan.premTermDesc = premTermList[j].title;
            }
          }
          plan.saViewInd = pDetail.saViewInd;
          plan.othSaInd = pDetail.othSaInd;
        }
        //store picker option title as description when picker, use value otherwise
        quotation.policyOptionsDesc = {};
        for (var po in quotation.policyOptions) {
          quotation.policyOptionsDesc[po] = quotation.policyOptions[po] ? quotation.policyOptions[po] : '-';
          var configPolOpts = planDetails[quotation.baseProductCode].inputConfig.policyOptions;
          if (configPolOpts) {
            for (var j = 0; j < configPolOpts.length; j++) {
              var configPolOpt = configPolOpts[j];
              if (configPolOpt.id === po && configPolOpt.options) {
                var opts = configPolOpt.options;
                for (var k = 0; k < opts.length; k++) {
                  var opt = opts[k];
                  if (opt.value == quotation.policyOptions[po]) {
                    quotation.policyOptionsDesc[po] = opt.title;
                    break;
                  }
                }
                break;
              }
            }
          }
        }

        // start calc quotation
        quotation.premium = 0;
        quotation.totYearPrem = 0;
        quotation.totHalfyearPrem = 0;
        quotation.totQuarterPrem = 0;
        quotation.totMonthPrem = 0;
        quotation.sumInsured = 0;

        if (basicPlanDetail.formulas && basicPlanDetail.formulas.validQuotFunc) {
          let retVal = this.runFunc(basicPlanDetail.formulas.validQuotFunc, quotation, planDetails);
          retVal.quotation = retVal.quotation || quotation;
          retVal.planDetails = retVal.planDetails || planDetails;
        } else {


          // validate the input before calc quotation
          for (let i = 0; i < planInfoList.length; i++) {
            let detials = planDetails[planInfoList[i].covCode];

            if (detials.formulas && detials.formulas.validBeforeCalcFunc) {
              this.runFunc(detials.formulas.validBeforeCalcFunc, quotation, planInfoList[i], planDetails);
            } else {
              this.quotValid.validatePlanBeforeCalc(quotation, planInfoList[i], planDetails);
            }
          }

          retVal.planDetails = planDetails;

          let lastCalcList = {};
          let removeRiderList = [];

          for (let i = 0; i < planInfoList.length; i++) {
            let planInfo = planInfoList[i];
            let code = planInfo.covCode;

            // check if it is a valid rider by find it in input config of basic plan
            let curRiderConf = false;
            let riderDetail = planDetails[code];
            if (i >= 1) {
              for (let r in riderList) {
                let riderConf = riderList[r];
                if (code == riderConf.covCode) {
                  curRiderConf = riderConf;
                  break;
                }
              }

              // check if it has SA override rule
              if (curRiderConf && (!planInfo.sumInsured || !riderDetail.inputConfig.canEditSumAssured)) {
                if (curRiderConf.saRule == 'BP') {
                  planInfo.sumInsured = basicPlanInfo.yearPrem * (curRiderConf.saRate || 1);
                } else if (curRiderConf.saRule == 'BSA') {
                  planInfo.sumInsured = basicPlanInfo.sumInsured * (curRiderConf.saRate || 1);
                } else if (curRiderConf.saRule == 'PP') {
                  lastCalcList[i + ""] = curRiderConf;
                  continue; // skip and calc later
                } else if (curRiderConf.saRule == 'F') {
                  planInfo.sumInsured = curRiderConf.saFix || 0;
                }
              }
            }

            if (i == 0 || curRiderConf) { // perform calc if it is basic plan or valid rider
              if (riderDetail.formulas && riderDetail.formulas.calcQuotFunc) {
                this.runFunc(riderDetail.formulas.calcQuotFunc, quotation, planInfo, planDetails);
              } else {
                this.quotCalc.calcQuotPlan(quotation, planInfo, planDetails);
              }
            } else if (!curRiderConf) { // remove invalid rider
              removeRiderList.push(code);
            }
          }

          // calc for post calc rider
          let policyPremium = quotation.premium;

          for (let l in lastCalcList) {
            let planInfo = planInfoList[l];
            let curRiderConf = lastCalcList[l];
            if (curRiderConf && (!planInfo.sumInsured || !planDetails[planInfo.covCode].inputConfig.canEditSumAssured)) {
              // override SA if required
              if (curRiderConf.saRule === 'PP') {
                planInfo.sumInsured = policyPremium * (curRiderConf.saRate || 1);
              }
            }

            // recalc it
            if (planDetails[planInfo.covCode].formulas && planDetails[planInfo.covCode].formulas.calcQuotFunc) {
              this.runFunc(planDetails[planInfo.covCode].formulas.calcQuotFunc, quotation, planInfo, planDetails);
            } else {
              this.quotCalc.calcQuotPlan(quotation, planInfo, planDetails);
            }
          }

          // remove invalid rider
          for (let l in removeRiderList) {
            let code = removeRiderList[l];
            for (let p = 1; p < quotation.plans.length; p++) {
              if (quotation.plans[p].covCode === code) {
                quotation.plans.splice(p, 1);
              }
            }
          }

          basicPlanInfo = quotation.plans[0];
          quotation.sumInsured = quotation.sumInsured || basicPlanInfo.sumInsured;
          quotation.policyTerm = basicPlanInfo.benefitTerm;
          quotation.premTerm = basicPlanInfo.premTerm;

          retVal.quotation = quotation;

          // Validate each plan by calculated values
          for (let i = 0; i < planInfoList.length; i++) {
            let detail = planDetails[planInfoList[i].covCode];
            // NANI? ERROR?
            if (detail.formulas && detail.formulas.validAfterCalcFunc) {
              this.runFunc(detail.formulas.validAfterCalcFunc, quotation, planInfoList[i], detail);
            } else {
              this.quotValid.validatePlanAfterCalc(quotation, planInfoList[i], detail);
            }
          }

          // Validate by global rules
          this.quotValid.validateGlobal(quotation, planInfoList, planDetails);
        }
      }
    }
    return retVal;
  };

  this.generateIllustration = (quotation, planDetails, extraPara) => {
    let illustrations = {};
    let isIllustrationGenerated = true;
    extraPara.illustrations = illustrations;
    let warningMsg = null;
    try {
      for (let p=0; p< quotation.plans.length; p++) {
        let planInfo = quotation.plans[p];
        if (planDetails && planDetails[planInfo.covCode]) {
          let planDetail = planDetails[planInfo.covCode];
          if (planDetail.formulas && planDetail.formulas.illustrateFunc) {
            let illust = this.runFunc(planDetail.formulas.illustrateFunc, quotation, planInfo, planDetails, extraPara);
            if (illust) {
              try{
                warningMsg = illust[0].warningMsg;
              } catch(ex){
                warningMsg = null;
              }
              illustrations[planInfo.covCode] = illust;
            } else {
              isIllustrationGenerated = false;
            }
          }
        }
      }
      const bpDetail = planDetails[quotation.baseProductCode];
      if (bpDetail.formulas && bpDetail.formulas.getIllustrateSummary) {
        illustrations = this.runFunc(bpDetail.formulas.getIllustrateSummary, quotation, planDetails, extraPara, illustrations);
      }
    } catch (ex) {
      logger.log("generateIllustration(); Error -" + ex);
      isIllustrationGenerated = false;
    }

    return {
      'illustrations': illustrations,
      'isIllustrationGenerated': isIllustrationGenerated,
      'warningMsg': warningMsg
    };
  };

  /// function to prepare the data for genreate PDF
  ///     quotation - Quotation Model
  ///     planDetails - Map of covCode vs ProductModel
  ///     extraPara = {
  /// *          BI Options          : Object, key value map of BI options
  /// *          templateFuncs       : Object, map of template code vs PDF template formulas ( prepareReportDataFunc )
  /// *          requireReportData   : bool, indicate whether need to generate report data
  /// *          illustrations       : Object, map of plan code vs plan illustration if any
  ///                 }
  this.prepareReportData = (quotation, planDetails, extraPara) => {
    let reportData = {
      root: {
        mainInfo: {},
        planInfos: [],
        removeBookmark: []
      }
    };

    //expiry date
    let expiryDate = new Date();
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "Devember"];
    expiryDate = new Date(new Date(expiryDate).setMonth(expiryDate.getMonth() + 1));
    expiryDate = new Date(expiryDate.getTime() - 24 * 60 * 60 * 1000);
    let expiryDateStr = expiryDate.getDate() + ' ' + months[expiryDate.getMonth()] + ' ' + expiryDate.getFullYear();
    // MainInfo
    reportData.root.mainInfo.username = quotation.agent.agentCode;
    reportData.root.agent = quotation.agent;

    reportData.root.mainInfo.name = quotation.iFullName;
    reportData.root.mainInfo.age = quotation.iAge;
    reportData.root.mainInfo.gender = quotation.iGender;
    reportData.root.mainInfo.isSmoker = quotation.iSmoke;
    reportData.root.mainInfo.currency = quotation.ccy;
    reportData.root.mainInfo.createDate = quotation.createDate;
    reportData.root.mainInfo.expiryDate = expiryDateStr;
    reportData.root.mainInfo.occupation = quotation.occupation;
    reportData.root.mainInfo.nationality = quotation.nationality;
    reportData.root.mainInfo.residence = quotation.residence;
    reportData.root.mainInfo.dob = quotation.iDob;
    reportData.root.mainInfo.riskCommenDate = quotation.riskCommenDate;

    reportData.root.mainInfo.sameAs = quotation.sameAs;

    if (quotation.pLastName || quotation.pFirstName) {
      reportData.root.mainInfo.pName = quotation.pFullName;
      reportData.root.mainInfo.pAge = quotation.pAge;
      reportData.root.mainInfo.pGender = quotation.pGender;
      reportData.root.mainInfo.pIsSmoker = quotation.pSmoker;
      reportData.root.mainInfo.pOccupation = quotation.pOccupation;
      reportData.root.mainInfo.pNationality = quotation.pNationality;
      reportData.root.mainInfo.pResidence = quotation.pResidence;
      reportData.root.mainInfo.pDob = quotation.pDob;
    }
    reportData.root.agentName = quotation.agent.name;
    reportData.root.sysDate = extraPara.systemDate;
    reportData.root.releaseVersion = extraPara.releaseVersion;
    reportData.root.warningMsg = extraPara.warningMsg;

    let basicPlanDetail = planDetails[quotation.baseProductCode];

    if (basicPlanDetail) {
      reportData.root.mainInfo.basicPlanName = !basicPlanDetail.covName ? "" : (typeof basicPlanDetail.covName == 'string' ? basicPlanDetail.covName : (basicPlanDetail.covName.en || basicPlanDetail.covName[Object.keys(basicPlanDetail.covName)[0]]));
      reportData.root.mainInfo.basicPlanCode = basicPlanDetail.covCode;
    }
    reportData.root.mainInfo.singlePremium = getCurrency(quotation.totYearPrem, null, 2);
    reportData.root.mainInfo.yearlyPremium = getCurrency(quotation.totYearPrem, null, 2);
    reportData.root.mainInfo.halfYearlyPremium = getCurrency(quotation.totHalfyearPrem, null, 2);
    reportData.root.mainInfo.quarterlyPremium = getCurrency(quotation.totQuarterPrem, null, 2);
    reportData.root.mainInfo.monthlyPremium = getCurrency(quotation.totMonthPrem, null, 2);
    reportData.root.mainInfo.premium = getCurrency(quotation.premium, null, (quotation.premium + '').indexOf('.') > 0 ? 2 : 0);
    reportData.root.mainInfo.sumAssured = getCurrency(quotation.sumInsured, null, (quotation.sumInsured + '').indexOf('.') > 0 ? 2 : 0);
    reportData.root.mainInfo.quickQuote = extraPara.quickQuote ? 'Y' : 'N';

    // Plans
    let planInfoList = quotation.plans;

    for (let i = 0; i < planInfoList.length; i++) {
      let planInfo = planInfoList[i];
      let planDetail = planDetails[planInfo.covCode];
      let plan = {};

      plan.planName = !planInfo.covName ? "" : (typeof planInfo.covName == 'string' ? planInfo.covName : (planInfo.covName.en || planInfo.covName[Object.keys(planInfo.covName)[0]]));

      plan.postfix = !planInfo.saPostfix ? "" : (typeof planInfo.saPostfix == 'string' ? planInfo.saPostfix : (planInfo.saPostfix.en || planInfo.saPostfix[Object.keys(planInfo.saPostfix)[0]]));
      plan.sumAssured = getCurrency(planInfo.sumInsured, ' ', 0);
      plan.policyTerm = (extraPara.langMap && extraPara.langMap[planInfo.policyTerm]) || planInfo.policyTerm;
      plan.premium = getCurrency(planInfo.premium, ' ', 2);
      plan.APremium = getCurrency(planInfo.yearPrem, ' ', 2);
      plan.HPremium = getCurrency(planInfo.halfYearPrem, ' ', 2);
      plan.QPremium = getCurrency(planInfo.quarterPrem, ' ', 2);
      plan.MPremium = getCurrency(planInfo.monthPrem, ' ', 2);
      plan.LPremium = getCurrency(planInfo.yearPrem, ' ', 2);
      plan.permTerm = (extraPara.langMap && extraPara.langMap[planInfo.premTerm]) || planInfo.premTerm;
      plan.payMode = quotation.paymentMode;
      plan.planInd = planDetail.planInd;
      plan.covCode = planDetail.covCode;
      plan.productLine = planDetail.productLine;

      reportData.root.planInfos.push(plan);
    }

    // execute prepare report data func in template
    if (basicPlanDetail.reportTemplate && extraPara.templateFuncs) {
      for (let rt in basicPlanDetail.reportTemplate) {
        let template = basicPlanDetail.reportTemplate[rt];
        let tempFunc = extraPara.templateFuncs[template.pdfCode];
        if (tempFunc) {
          if (template.covCode && template.covCode != '0' && template.covCode != 'na') {
            let found = false;
            for (let p in planInfoList) {
              if (planInfoList[p].covCode == template.covCode) {
                reportData[template.pdfCode + "/" + template.covCode] = this.runFunc(tempFunc, quotation, planInfoList[p], planDetails, extraPara);
                found = true;
                break;
              }
            }
            if (!found) {
              reportData[template.pdfCode] = this.runFunc(tempFunc, quotation, planInfoList[0], planDetails, extraPara);
            }
          } else {
            reportData[template.pdfCode] = this.runFunc(tempFunc, quotation, planInfoList[0], planDetails, extraPara);
          }
        }
      }
    }
    return reportData;
  };
};

module.exports = QuotDriver;

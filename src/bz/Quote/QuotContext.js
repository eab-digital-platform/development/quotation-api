var utils = require('./utils');

var QuotContext = function () {
  var planErrors = {};
  var policyOptionErrors = {};
  var mandatoryErrors = {};
  var otherErrors = [];

  var addError = function (error) {
    if (error.type === 'policyOption' && error.key) {
      policyOptionErrors[error.key] = error;
    } else if (error.type === 'mandatory' && error.key) {
      if (!mandatoryErrors[error.key]) {
        mandatoryErrors[error.key] = [];
      }
      mandatoryErrors[error.key].push(error);
    } else if (error.covCode) {
      if (!planErrors[error.covCode]) {
        planErrors[error.covCode] = [];
      }
      planErrors[error.covCode].push(error);
    } else {
      otherErrors.push(error);
    }
  };

  var getResult = function (result) {
    var resultObj = Object.assign({}, result);
    resultObj.errors = {
      planErrors: planErrors,
      policyOptionErrors: policyOptionErrors,
      mandatoryErrors: mandatoryErrors,
      otherErrors: otherErrors
    };
    return utils.getResult(resultObj);
  };

  this.addError = addError;
  this.getResult = getResult;
};

module.exports = QuotContext;

const _ = require('lodash');

var QuotComparator = function (isDebug) {
  let resultObj = {
    hasError: true
  };

  if (isDebug) {
    Object.assign(resultObj, {
      debugger: {}
    })
  }

  /**
   * Acknowledgement
   * >> Difference.js
   * >> https://gist.github.com/Yimiprod/7ee176597fef230d1451
   */
  this.isEqual = (object, base) => {
    const getChanges = function (object, base) {
      return _.transform(object, function(result, value, key) {
        if (!_.isEqual(value, base[key])) {
          result[key] = (_.isObject(value) && _.isObject(base[key])) ? getChanges(value, base[key]) : value;
        }
      });
    }
    return getChanges(object, base);
  };
};

module.exports = QuotComparator;
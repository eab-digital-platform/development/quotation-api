// This is the generic part and for tailor-made for each products


var getIllustrationStandard = function(covCode, illustrationJson) {
    try {
        switch (covCode) {  
            case "SAV":
            {
                // for savvy saver, the supplementary TDC is sum of all riders illustration.
                var ridersTotal = [];
                for( var name in illustrationJson.illustrations) {
                    if (name === "SAV") continue;
                    var riders = illustrationJson.illustrations[name];
                    for (var i = 0; i < riders.length; i++) {
                    if(ridersTotal[i] == undefined)
                    {
                        ridersTotal[i] = {
                        annualPremium: riders[i].annaulPrem,
                        totalDistribCost: riders[i].totalDistribCost,
                        }
                    } else {
                        ridersTotal[i] = {
                        annualPremium: ridersTotal[i].annualPremium + riders[i].annaulPrem,
                        totalDistribCost: ridersTotal[i].totalDistribCost + riders[i].totalDistribCost,
                        }
                    }
                    }
                }

                var SAV = illustrationJson.illustrations.SAV;
                var illustration = {};
                var mainDB = [];
                var mainSV = [];
                var mainTOD = null;
                var mainTDC = [];
                var supplementaryDB = [];
                var supplementarySV = [];
                var supplementaryTOD = null;
                var supplementaryTDC = [];
                for (var i = 0; i < SAV.length; i++) {
                mainDB[i] = [ SAV[i].age, SAV[i].totalPremPaid, SAV[i].GteedDB, SAV[i].nonGteedDB['3.25'], SAV[i].totalDB['3.25'], SAV[i].nonGteedDB['4.75'], SAV[i].totalDB['4.75']];
                mainSV[i] = [ SAV[i].age, SAV[i].totalPremPaid, SAV[i].GteedAnnualCashback, SAV[i].GteedSV, SAV[i].nonGteedSV['3.25'], SAV[i].totalSV['3.25'], SAV[i].nonGteedSV['4.75'], SAV[i].totalSV['4.75']];
                mainTDC[i] = [ SAV[i].age, SAV[i].totalPremPaid, SAV[i].totalDistribCost];
                supplementaryDB[i] = [ SAV[i].age, SAV[i].totalPremPaid, SAV[i].GteedDB_SUPP, SAV[i].nonGteedDB_SUPP['3.25'], SAV[i].totalDB_SUPP['3.25'], SAV[i].nonGteedDB_SUPP['4.75'], SAV[i].totalDB_SUPP['4.75']];
                supplementarySV[i] = [ SAV[i].age, SAV[i].totalPremPaid, SAV[i].GteedAnnualCashback, SAV[i].GteedSV_SUPP, SAV[i].nonGteedSV_SUPP['3.25'], SAV[i].totalSV['3.25'], SAV[i].nonGteedSV_SUPP['4.75'], SAV[i].totalSV_SUPP['4.75']];
                
                // the supplemenatryTDC is obatined from the ridersTotals, the riders array and the SAV should have the same number of rows
                supplementaryTDC[i] = [ SAV[i].age, ridersTotal[i].annualPremium, ridersTotal[i].totalDistribCost];
                }

                // combine illustration
                illustration = {
                mainDB: mainDB,
                mainSV: mainSV,
                mainTOD: mainTOD,
                mainTDC: mainTDC,
                supplementaryDB: supplementaryDB,
                supplementarySV: supplementarySV,
                supplementaryTOD: supplementaryTOD,
                supplementaryTDC: supplementaryTDC
                }
                return illustration;
            }
            case "TPX": 
            case "TPPX": 
            {
                // get the riders objects total sum
                var ridersTotal = [];
                for( var name in illustrationJson.illustrations) {
                    if (name === "TPX") continue;
                    var riders = illustrationJson.illustrations[name];
                    for (var i = 0; i < riders.length; i++) {
                    if(ridersTotal[i] == undefined)
                    {
                        ridersTotal[i] = {
                        yearPrem: riders[i].yearPrem,
                        totComm: riders[i].totComm,
                        }
                    } else {
                        ridersTotal[i] = {
                        yearPrem: ridersTotal[i].yearPrem + riders[i].yearPrem,
                        totComm: ridersTotal[i].totComm + riders[i].totComm,
                        }
                    }
                    }
                }

                // build the illustration
                var basicPlan = illustrationJson.illustrations.TPX;
                var illustration = {};
                var mainDB = [];
                var mainSV = [];
                var mainTOD = null;
                var mainTDC = [];
                var supplementaryDB = [];
                var supplementarySV = null;
                var supplementaryTOD = null;
                var supplementaryTDC = [];
                var age = illustrationJson.reportData.root.mainInfo.age + 1;
                for (var i = 0; i < basicPlan.length; i++) {
                mainDB[i] = [ age+i, basicPlan[i].totYearPrem, basicPlan[i].guaranteedDB, basicPlan[i].guaranteedSV];
                mainSV[i] = [ age+i, basicPlan[i].yearPrem, basicPlan[i].guaranteedDB, basicPlan[i].guaranteedSV];
                mainTDC[i] = [ age+i, basicPlan[i].totTdc, basicPlan[i].bpTdc];
                supplementaryDB[i] = [ age+i, basicPlan[i].riderTdc, basicPlan[i].guaranteedSV];
                             
                // the supplemenatryTDC is obatined from the ridersTotals, the riders array and the SAV should have the same number of rows
                supplementaryTDC[i] = [ age+i, ridersTotal[i].yearPrem, ridersTotal[i].totComm];
                }

                // combine illustration
                illustration = {
                mainDB: mainDB,
                mainSV: mainSV,
                mainTOD: mainTOD,
                mainTDC: mainTDC,
                supplementaryDB: supplementaryDB,
                supplementarySV: supplementarySV,
                supplementaryTOD: supplementaryTOD,
                supplementaryTDC: supplementaryTDC
                }
                return illustration;

            }
            case "PUL": 
            {
                // build the illustration
                var basicPlan = illustrationJson.illustrations.PUL.illustration;
                var illustration = {};
                var mainDB = [];
                var mainSV = [];
                var mainTOD = [];
                var mainTDC = [];
                var supplementaryDB = null;
                var supplementarySV = null;
                var supplementaryTOD = null;
                var supplementaryTDC = null;
                var age = illustrationJson.reportData.root.mainInfo.age + 1;
                for (var i = 0; i < basicPlan.length; i++) {
                mainDB[i] = [ basicPlan[i].age, basicPlan[i].totalPremiumPaidToDate, basicPlan[i].nonGuaranteedAccValue[0], basicPlan[i].totalDeathBenefit[0], basicPlan[i].nonGuaranteedAccValue[1], basicPlan[i].totalDeathBenefit[1]];
                mainSV[i] = [ basicPlan[i].age, basicPlan[i].totalPremiumPaidToDate, basicPlan[i].nonGuaranteedSurValue[0], basicPlan[i].nonGuaranteedSurValue[1]];
                mainTOD[i] = [ basicPlan[i].age, basicPlan[i].valueOfPremiums[0], basicPlan[i].effectOfDeductions[0], 
                                basicPlan[i].valueOfPremiums[0] - basicPlan[i].effectOfDeductions[0],
                                basicPlan[i].valueOfPremiums[1], basicPlan[i].effectOfDeductions[1],
                                basicPlan[i].valueOfPremiums[1] - basicPlan[i].effectOfDeductions[1]
                                ];
                
                mainTDC[i] = [ basicPlan[i].age, basicPlan[i].totalPremiumPaidToDate, basicPlan[i].totalDistributionCost];
                }

                // combine illustration
                illustration = {
                mainDB: mainDB,
                mainSV: mainSV,
                mainTOD: mainTOD,
                mainTDC: mainTDC,
                supplementaryDB: supplementaryDB,
                supplementarySV: supplementarySV,
                supplementaryTOD: supplementaryTOD,
                supplementaryTDC: supplementaryTDC
                }
                return illustration;

            }
            default: 
                return null;
        }
    } catch (ex) {
        debug.exception = {
            message: ex.message,
            stack: ex.stack
        };
    }
}

module.exports.getIllustrationStandard = getIllustrationStandard;
const _ = require('lodash');

var debug = {
  isDebug: true,
  debugOnly: false,
  messages: ''
};

/**
 * !! resultObj -> resultJson DISABLED || Return resultObj ONLY
 * Stringify resultObj -> resultJson to Couchbase capable  
 * @param  {} resultObj 
 * @return {} resultJson
 */
var getResult = function(resultObj) {
  /** 
    var resultJson = '';
    if (process.env.NODE_ENV == 'development') {
      logger.debug("quotation return result:", resultObj);
    }
    if (debug.isDebug) {
        if (debug.debugOnly) {
            resultJson = JSON.stringify(debug);
        } else {
          resultObj.debug = Object.assign(debug, resultObj.debug || {});
          resultJson = JSON.stringify(resultObj);
        }
    } else {
        resultJson = JSON.stringify(resultObj);
    }
    return resultJson;
    */
   return resultObj;
};

/**
 * @param  {} value
 * @param  {} sign
 * @param  {} decimals
 */
var getCurrency = function(value, sign, decimals) {
    if (!_.isNumber(value)) {
        return value;
    }
    if (!_.isNumber(decimals)) {
        decimals = 2;
    }
    var text = (sign && sign.trim().length > 0 ? sign : '') + parseFloat(value).toFixed(decimals);
    var parts = text.split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return parts.join('.');
};
/**
 * @param  {} value
 * @param  {} decimals
 * @param  {} compCode
 * @param  {} ccy
 */
const getCurrencyByCcy = function (value, decimals, compCode, ccy) {
  return getCurrency(value, getCcySign(compCode, ccy), decimals);
};
/**
 * @param  {} compCode
 * @param  {} ccy
 */
const getCcySign = function (compCode, ccy) {
  const currency = _.find(global.optionsMap.ccy.currencies, c => c.compCode === compCode);
  if (currency) {
    const option = _.find(currency.options, opt => opt.value === ccy);
    if (option) {
      return option.symbol;
    }
  }
  return 'S$';
};
/**
 * @param  {} ccy
 */
var getCurrencySign = function(ccy) {
  var sign = '$';
  if (ccy === 'SGD') {
    sign = 'S$ ';
  } else if (ccy === 'USD') {
    sign = 'US$ ';
  } else if (ccy === 'GBP') {
    sign = '£ ';
  } else if (ccy === 'EUR') {
    sign = '€ ';
  } else if (ccy === 'AUD') {
    sign = 'A$ ';
  }
  return sign;
};

if (typeof exports === 'object' && typeof module === 'object') {
  module.exports.debug = debug;
  module.exports.getCurrency = getCurrency;
  module.exports.getCurrencySign = getCurrencySign;
  module.exports.getCurrencyByCcy = getCurrencyByCcy;
  module.exports.getResult = getResult;
}

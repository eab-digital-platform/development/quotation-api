var _ = require('lodash');

const dao = require('../cbDaoFactory').create();
var applicationDao = require('../cbDao/application');
var quotDao = require('../cbDao/quotation');
var prodDao = require('../cbDao/product');
var nDao = require('../cbDao/needs');
var bDao = require('../cbDao/bundle');
// const appFormMapping = 'application_form_mapping';

//Add by Alex Tse for CR49 start ***********************
const productPaymentMapping = 'product_to_payment_method';
//Add by Alex Tse for CR49 end *************************

var clientDao = require('../cbDao/client');
var needsHandler = require('../NeedsHandler');
var PDFHandler = require('../PDFHandler');
var ApprovalHandler = require('../ApprovalHandler');
var aEmaillHandler = require('../ApprovalNotificationHandler');
var clientChoiceHandler = require('./ClientChoiceHandler');
var Iconv = require('iconv').Iconv;
var crypto = require('crypto');
const async = require('async');
const CommonFunctions = require('../CommonFunctions');
const approvalStatusModel = require('../model/approvalStatus');
const moment = require('moment');
var Exception = require('../model/Exception');
const eAppConstants = require('../../common/EAppConstants');
const {createPdfToken, setPdfTokensToRedis} = require('../utils/TokenUtils');
var SuppDocsUtils = require('../utils/SuppDocsUtils');
const commonApp = require('./application/common');
const defaultApplication = require('./application/default/application');
const defaultSignature = require('./application/default/signature');
const defaultSupportDocument = require('./application/default/supportDocument');
const shieldSupportDocument = require('./application/shield/supportDocument');
const shieldSignature = require('./application/shield/signature');
const payment = require('../handler/application/shield/payment');
import axiosWrapper from '../../../../../app/utilities/axiosWrapper';


var {
  callApi,
  callApiByGet,
  callApiComplete
} = require('../utils/RemoteUtils');
var logger = global.logger || console;

//TO-DO: MOVE to ./application/common.js
const fnaPdfName = 'fnaReport';
const proposalPdfName = 'proposal';
const appPdfName = 'appPdf';
const eCpdPdfName = 'eCpdPdf';
// const coverPdfName = 'cover';

let getLabel = (appList, callback) => {
  const rpPaymentMethod = ['A', 'S', 'Q', 'M'];
  const spPaymentMethod = ['L'];
  const lang = 'en';

  appList.forEach((item) => {
    let nameLabel = 'RP';
    let paymentMode = item.type === 'quotation' ? _.get(item, 'paymentMode') : _.get(item, 'quotation.paymentMode');
    let isShield = (item.type === 'quotation' ? _.get(item, 'quotType') : _.get(item, 'quotation.quotType')) === 'SHIELD';
    if (isShield || rpPaymentMethod.indexOf(paymentMode) > -1) {
      nameLabel = 'RP';
    } else if (spPaymentMethod.indexOf(paymentMode) > -1) {
      nameLabel = item.type === 'quotation' ? _.get(item, `policyOptionsDesc.paymentMethod.${lang}`, 'SP') : _.get(item, `quotation.policyOptionsDesc.paymentMethod.${lang}`, 'SP');
    }

    item.paymentMethod = nameLabel;
  });

  callback();
};

module.exports.getAppListView = function(data, session, cb) {
  let cid = _.get(data, 'profile.cid');
  const productInvalidList = [];
  logger.log('INFO: getAppListView - start', cid);

  let getSupervisorApproval = function(appList, bundleAppList){
    logger.log('INFO: getAppListView - getSupervisorApproval', cid);
    return new Promise((resolve)=>{
      let promises = [];
      _.forEach(appList, (app)=>{
        promises.push(
          new Promise((resolve2)=>{
            let matchedBundleApp = _.find(bundleAppList, (bundleApp)=>{
              return bundleApp.applicationDocId === app.id;
            });
            if (_.get(app, 'type') === 'masterApplication') {
              _.set(app, 'policyNumber', _.replace(app.id, 'SA', 'SP'));
            }
            if (_.get(app, 'policyNumber') && (_.get(matchedBundleApp, 'appStatus') === 'SUBMITTED')) {
              // approvalStatus
              ApprovalHandler.searchApprovalCaseById({id: app.policyNumber}, session, (approvalCase) => {
                if (approvalCase.foundCase) {
                  if (['A', 'E', 'R'].includes(_.get(approvalCase.foundCase, 'approvalStatus')) || _.get(matchedBundleApp, 'appStatus') === 'INVALIDATED_SIGNED') {
                    app.approvalStatus = _.get(approvalStatusModel.STATUS, _.get(approvalCase.foundCase, 'approvalStatus'), '');
                    dao.getDoc(`U_${_.get(approvalCase.foundCase, 'agentProfileId')}_RLSSTATUS`, (rlsCb)=>{
                      var isTransferred = false;
                      if (rlsCb.RLSSTATUS) {
                        if (_.get(app, 'quotation.quotType') === 'SHIELD') {
                          isTransferred = _.get(approvalCase.foundCase, 'policiesMapping', []).every((policy) => {
                            return (rlsCb.RLSSTATUS[policy.policyNumber] || {}).rlsSuccess
                          })
                        } else {
                          isTransferred = (rlsCb.RLSSTATUS[_.get(approvalCase.foundCase, 'policyId')] || {}).rlsSuccess;
                        }
                      }
                      app.approvalStatus = isTransferred ? eAppConstants.PAYMENTTRANSFERRED : app.approvalStatus;
                      resolve2(app);
                    });
                  } else {
                    app.approvalStatus = _.get(approvalStatusModel.STATUS, _.get(approvalCase.foundCase, 'approvalStatus'), '');
                    resolve2(app);
                  }
                }
              });
            } else if(_.get(app, 'policyNumber') && _.get(matchedBundleApp, 'appStatus') === 'INVALIDATED_SIGNED') {
              dao.getDoc(`U_${_.get(session, 'agent.profileId')}_RLSSTATUS`, (rlsCb)=>{
                var isTransferred = false;
                if (rlsCb.RLSSTATUS) {
                  if (_.get(app, 'quotation.quotType') === 'SHIELD') {
                    var policyIdArray = [];
                    _.each(_.get(app, 'iCidMapping', []), (cid)=>{
                      _.each(cid, policyNumber => policyIdArray.push(policyNumber))
                    })
                    isTransferred = policyIdArray.every(policyNumber => (rlsCb.RLSSTATUS[policyNumber] || {}).rlsSuccess);
                  } else {
                    isTransferred = (rlsCb.RLSSTATUS[_.get(app, 'policyNumber')] || {}).rlsSuccess;
                  }
                }
                app.approvalStatus = isTransferred ? eAppConstants.PAYMENTTRANSFERRED : app.approvalStatus;
                resolve2(app);
              });
            } else {
              resolve2(app);
            }
          })
        );
      });
      Promise.all(promises).then((results)=>{
        resolve(results);
      }).catch((error)=>{
        logger.error("Error in getAppListView->Promise.all: ", error);
      });
    });
  };

  let checkFNACompleted = function(cid){
    logger.log('INFO: getAppListView - checkFNACompleted', cid);
    return new Promise((resolve) => {
      nDao.isFNACompleted(cid).then((isFNACompleted)=>{
        resolve(isFNACompleted);
      }).catch((error)=>{
        logger.error("Error in checkFNACompleted->nDao.isFNACompleted: ", error);
      });
    });
  };

  bDao.getBundle(_.get(data, 'profile.cid'), data.bundleId, (bundle) => {
    if (bundle) {
      applicationDao.getAppListView('01', data.profile.cid, bundle.id).then((rawAppList) => {
        const productList = _.map(rawAppList, obj => obj.baseProductCode);
        prodDao.prepareLatestProductVersion(productList).then(result => {
          getSupervisorApproval(rawAppList, bundle.applications).then((appList)=>{
            _.each(appList, app => {
              if (result[app.baseProductCode] && result[app.baseProductCode].success) {
                app.version = result[app.baseProductCode].version || 1;
              } else {
                app.version = null;
              }
              if (!result[app.baseProductCode].success){
                productInvalidList.push(app.baseProductCode);
              }
            });
            //Add by Alex Tse for CR49 Start ***
            getLabel(appList, () => {
              logger.log('INFO: getAppListView - getInfoForAppList end', cid);
              let {isClientChoiceCompleted, quotCheckedList} = getQuotCheckedList(appList, bundle);
              let agentChannel = session.agent.channel.type;

              if (agentChannel === 'FA') {
                // FA Channel does not have FNA, so isFNACompleted set to true
                clientDao.getAllDependantsProfile(data.profile.cid).then((dependantProfiles) => {
                  logger.log('INFO: getAppListView - end [RETURN=1]', cid);
                  cb({
                    success: true,
                    applicationsList:appList,
                    pdaMembers:getPdaMember(data.profile, dependantProfiles),
                    clientChoiceFinished:isClientChoiceCompleted,
                    quotCheckedList:quotCheckedList,
                    agentChannel: agentChannel,
                    isFNACompleted: true,
                    productInvalidList
                  });
                }).catch((error) => {
                  logger.error('ERROR: getAppListView - getAllDependantsProfile', cid, error);
                });
              } else {
                checkFNACompleted(_.get(data, 'profile.cid')).then((isFNACompleted)=>{
                  let needData = data;
                  needData.cid = data.profile.cid;
                  needData.action = 'initNeeds';
                  needsHandler.initNeeds(needData, session, (resp)=>{
                    logger.log('INFO: getAppListView - end [RETURN=2]', cid);
                    cb({
                      success: true,
                      applicationsList:appList,
                      pdaMembers:getPdaMember(data.profile, resp.dependantProfiles, resp.pda),
                      clientChoiceFinished:isClientChoiceCompleted,
                      quotCheckedList:quotCheckedList,
                      agentChannel: agentChannel,
                      isFNACompleted,
                      productInvalidList
                    });
                  });
                }).catch((error) => {
                  logger.error('ERROR: getAppListView - checkFNACompleted', cid, error);
                });
              }
            });
          //Add by Alex Tse for CR49 End****
          }).catch((err)=>{
            logger.error('ERROR: getAppListView - getInfoForAppList', cid, err);
          });
        }).catch(getProductVersionError => {
          logger.error('ERROR: getAppListView :: getProductVersionError', cid, getProductVersionError);
        });
      }).catch((err)=>{
        logger.error('ERROR: getAppListView', cid, err);
      });
    } else {
      logger.log('INFO: getAppListView - CANNOT GET BUNDLE');
      cb({success: false});
    }
  });
};
/**FUNCTION ENDS: getAppListView */

var getPdaMember = function(profile, dependantProfiles, pda) {

  // let {profile, dependantProfiles} = client;
  // let {pda} = needs;

  let result = [];
  if(profile){
    result.push({
      cid: profile.cid,
      fullName: profile.fullName,
      relationship: "OWNER"
    })
  }

  if (pda) {
    let _deps = pda.dependants && pda.dependants.split(",");
    let _jointSpo = "";

    if(pda.applicant==="joint"){
      profile.dependants.filter((dependant)=>{
        if(dependant.relationship == "SPO"){
          let _obj = JSON.parse(JSON.stringify(dependant));
          // let _obj = _.cloneDeep(dependant);
          _obj.relationship = "SPOUSE";
          _obj.fullName = dependantProfiles[_obj.cid].fullName;
          _jointSpo = _obj.cid;
          result.push(_obj);
        }
      });
    }

    if(_deps){
      profile.dependants && profile.dependants.filter((dependant)=>{
        if(_deps.indexOf(dependant.cid)>-1 && dependant.cid != _jointSpo){
          let _obj = JSON.parse(JSON.stringify(dependant));
          // let _obj = _.cloneDeep(dependant);
          _obj.filter = _obj.relationship;
          _obj.relationship = "DEPENDANTS";
          _obj.fullName = dependantProfiles[_obj.cid].fullName;
          // _obj.fullName = _.at(dependantProfiles, `${_obj.cid}.fullName`)[0];
          result.push(_obj);
        }
      });
    }
  } else {
    profile.dependants && profile.dependants.filter((dependant)=>{
      if (dependantProfiles[dependant.cid]){
        let _obj = _.cloneDeep(dependant);
        _obj.filter = _obj.relationship;
        _obj.relationship = 'DEPENDANTS';
        _obj.fullName = dependantProfiles[_obj.cid].fullName;
        // _obj.fullName = _.at(dependantProfiles, `${_obj.cid}.fullName`)[0];
        result.push(_obj);
      }
    });
  }

  return result;
}

//MOVE to ./application/common.js
// return a array of policy number
var getPolicyNumber = function(numOfPolicyNumber, cb) {
  commonApp.getPolicyNumberFromApi(numOfPolicyNumber, false, cb);
};
module.exports.getPolicyNumber = getPolicyNumber;


module.exports.saveForm = function(data, session, cb){
  defaultApplication.saveAppForm(data, session, cb);
};

// let savePersonalDetailsToClientProfile = function(cid, appFormInfo){
//   logger.log('INFO: savePersonalDetailsToClientProfile starts');
//   const whiteList = ['A', 'B'];
//   return new Promise((resolve)=>{
//     clientDao.getProfile(cid, true).then((clientProfile)=>{
//       _.forEach(whiteList, (id)=>{
//         clientProfile[id] = _.get(appFormInfo, id) || _.get(clientProfile, id);
//       });
//       clientDao.updateProfile(cid, clientProfile).then(()=>{
//         logger.log('INFO: savePersonalDetailsToClientProfile ends');
//         resolve();
//       });
//     });
//   });
// };

// let getPersonalDetailsFromClientProfile = function(){

// };

module.exports.addOtherDocument = function(data, session, cb){
  let rootValues = data.rootValues;
  let tabId = data.tabId;
  let values;
  let changedValues = rootValues[tabId];
  let docName = data.docName ? data.docName : data.docNameOption;
  let docNameOption = data.docNameOption ? data.docNameOption : 'Other';
  if (docName && typeof(docName) === 'string'){
    docName = docName.trim();
  }
  let docNameTitle = (docNameOption !== 'Other') ? docNameOption : docName;


  if (docNameTitle && typeof(docNameTitle) === 'string'){
    docNameTitle = docNameTitle.trim();
  }
  let docId = '';
  let docTitleId = 'otherDoc';
  if (docNameTitle && typeof(docNameTitle) === 'string'){
    if (docNameTitle.replace(/\s+/g, '') !== ''){
      docTitleId = docNameTitle.replace(/\s+/g, '');
    }
  }
  docId = docTitleId + '_' + (new Date).getTime();
  let appId = data.appId;

  logger.log('INFO: addOtherDocument - start', appId);
  applicationDao.getApplication(data.appId, function(app){
    defaultSupportDocument.validateOtherDocumentName(docName, app, data.tabId).then((isDuplicated) => {
      if (!isDuplicated) {
        if (!app.error) {
          if (changedValues['otherDoc']){
            changedValues.otherDoc.template.push({
              'type' : 'subSection',
              'id' : docId,
              'title' : docNameTitle,
              'docNameOption':docNameOption,
              'docName':docName
            });
            _.set(changedValues, "otherDoc.values['docId']", []);
          } else {
            changedValues['otherDoc'] = {};
            changedValues.otherDoc['template'] = [];
            changedValues.otherDoc.template.push({
              "type":"subSection",
              "id": docId,
              "title":docNameTitle,
              'docNameOption':docNameOption,
              'docName':docName
            });
            _.set(changedValues, "otherDoc['values']", {});
            _.set(changedValues, "otherDoc.values['docId']", []);
          }
          let suppDoc = app['supportDocuments'];
          // The support documents values will be updated after Submit button clicked
          bDao.getApplicationByBundleId(_.get(app, 'bundleId'), app.id).then((bApp)=>{
            if (_.get(bApp, 'appStatus') === 'SUBMITTED') {
              values = rootValues;
              values[tabId] = changedValues;

              logger.log('INFO: addOtherDocument - end [RETURN=1]', appId);
              cb({
                success: true,
                values: values
              });
            } else {
              values = _.get(app, 'supportDocuments.values');
              values[tabId] = changedValues;

              suppDoc['values']= values;
              app['supportDocuments']=suppDoc;
              applicationDao.upsertApplication(app._id, app, function(resp){
                if(resp){
                  logger.log('INFO: addOtherDocument - end [RETURN=2]', appId);
                  cb({
                    success: true,
                    values: values
                  });
                }else{
                  logger.error('ERROR: addOtherDocument - end [RETURN=-3]', appId);
                  cb({success:false});
                }
              });
            }
          }).catch((err)=>{
            logger.error('ERROR: addOtherDocument - end [RETURN=-2]', appId, err);
            cb({success:false});
          });
        } else {
          logger.error('ERROR: addOtherDocument - end [RETURN=-1]', appId);
          cb({success:false});
        }
      } else {
        logger.log('INFO: addOtherDocument - end [RETURN=3] - documentName is duplicated', appId);
        cb({
          success: true,
          duplicated: true
        });
      }
    }).catch((error)=>{
      logger.error('Error in addOtherDocument - validateOtherDocumentName: ', error);
    });
  });
};

module.exports.updateOtherDocumentName = function(data, session, cb){
  defaultSupportDocument.updateOtherDocumentName(data, session, cb);
};

module.exports.submitSuppDocsFiles = function(data, session, cb){
  logger.log('INFO: submitSuppDocsFiles - start' + data.appId);
  let localTime = CommonFunctions.getLocalTime();
  let timeLabel = localTime.format("dd/mm/yyyy HH:MM:ss");
  let sendHealthDeclarationNoti;

  let updateViewedList = function(viewedList, pendingSubmitList){
    _.forEach(viewedList, function(viewedListObj, agentCode){
      _.forEach(pendingSubmitList, function(fileId){
        if (data.isSupervisorChannel && session.agentCode === agentCode) {
          viewedListObj[fileId] = true;
        } else {
          viewedListObj[fileId] = false;
        }
      });
    });
  };

  let hasHealthDeclarationFile = function(supportDocumentValues, pendingSubmitList){
    const uploadedFiles = _.get(supportDocumentValues, 'policyForm.mandDocs.healthDeclaration');
    let isContain = false;

    for (let index in uploadedFiles) {
      if (_.includes(pendingSubmitList, uploadedFiles[index].id)) {
        isContain = true;
        break;
      }
    }

    return isContain;
  };

  let setSysDocsTime = function(app){
    if (app.supportDocuments.values.policyForm && app.supportDocuments.values.policyForm.sysDocs) {
      let docs = app.supportDocuments.values.policyForm.sysDocs
      for (let i in docs) {
        docs[i]["uploadDate"] = timeLabel;
      }
    }
  };

  applicationDao.getApplication(data.appId, app => {
    if (!app.error) {
      app.supportDocuments.values = data.values;
      if (app.supportDocuments.pendingSubmitList) {
        sendHealthDeclarationNoti = hasHealthDeclarationFile(data.values, app.supportDocuments.pendingSubmitList);
        // update all agents' viewedList
        updateViewedList(app.supportDocuments.viewedList, app.supportDocuments.pendingSubmitList);
        delete app.supportDocuments['pendingSubmitList'];
      }
      // app.supportDocuments.viewedList[session.agentCode] = data.viewedList;
      app.supportDocuments.isAllViewed = false;
      setSysDocsTime(app);
      applicationDao.upsertApplication(data.appId, app, resp => {
        if (resp) {
          logger.log('INFO: submitSuppDocsFiles - end [RETURN=1]' + data.appId);
          cb({
            success: true,
            pendingSubmitList: [],
            sendHealthDeclarationNoti
          });
        } else {
          logger.error('ERROR: submitSuppDocsFiles - end [RETURN=-2]' + data.appId);
          cb({success: false});
        }
      });
    } else {
      logger.error('ERROR: submitSuppDocsFiles - end [RETURN=-1]' + data.appId);
      cb({success: false});
    }
  });
};

const _checkUserUploadSuppDocFileSize = function(data, session) {
  return new Promise((resolve, reject) => {
    // Get current upload file size
    let totalFileSize = _.get(data, 'attachmentValues.length', 0);
    applicationDao.getApplication(data.applicationId, function(application){
      const attachments = _.get(application, '_attachments');
      _.each(attachments, (individualAtt, key) => {
        // Identify this is upload by user
        if (key && key.length === 22 && key.indexOf('suppDocs') === 0 && _.isNumber(individualAtt.length)) {
          totalFileSize += individualAtt.length;
        }
      });
      let isShield = _.get(application, 'quotation.quotType') === 'SHIELD';
      let noOfProposer = 1;
      let noOfLA = (isShield) ? _.keys(_.get(application, 'iCidMapping')).length : 1;

      (totalFileSize <= (global.config.SUPPORT_DOCUMENTS_MAX_FILE_SIZE_IN_BYTE * (noOfProposer + noOfLA))) ? resolve(true) : resolve(false);
    });
  });
};

module.exports.upsertSuppDocsFile = function (data, session, cb) {
  let rootValues = data.rootValues;

  _checkUserUploadSuppDocFileSize(data, session).then((allowUpload) => {
    if (allowUpload) {
      logger.log('INFO: upsertSuppDocsFile - start', data.applicationId);
      let isSubmittedApp = false;

      let genFileProps = function(attachmentValues) {
        let {fileName, fileSize, fileType, length} = attachmentValues;
        let serverTime = new Date();
        let localTime = CommonFunctions.getLocalTime();
        let fileProps = {
          "id": 'suppDocs' + localTime.format("yyyymmddHHMMss"),
          "title": localTime.format("yyyymmdd") + "_" + fileName,
          "fileSize": fileSize,
          "uploadDate": localTime.format('dd/mm/yyyy HH:MM:ss'),
          "backendUploadDate": localTime.format("yyyy-mm-dd'T'HH:MM:ss'Z'"),
          "fileType":fileType,
          "length": length
        };
        return fileProps;
      };

      let updateViewedList = function(viewedList, attId){
        if (data.isSupervisorChannel) {
          viewedList[attId] = true;
        } else {
          viewedList[attId] = false;
        }
      };

      let updateSuppDocs = function(isSubmittedApp, application, fileProps, valueLocation, tabId, suppDocsValues, viewedList, cb){
        addSupportDocuments(isSubmittedApp, application, fileProps, valueLocation, tabId, suppDocsValues, session.agentCode, rootValues, (resp)=>{
          if (resp.success == true){
            // if (isSubmittedApp) {
              updateViewedList(viewedList, fileProps.id);
            // }

            let tokens = [createPdfToken(application.id, fileProps.id, Date.now(), session.loginToken)];
            let tokensMap = {};
            tokensMap[fileProps.id] = tokens[0].token;
            if (session.platform === "ios") {
              commonApp.updateChildSupportDocumentValue(application, () => {
                logger.log('INFO: upsertSuppDocsFile - end [RETURN=1]', data.applicationId);
                cb({
                  success: true,
                  values:resp.values,
                  viewedList: viewedList,
                  pendingSubmitList: resp.pendingSubmitList,
                  tokensMap
                });
              });
            } else {
              setPdfTokensToRedis(tokens, () => {
                commonApp.updateChildSupportDocumentValue(application, () => {
                  logger.log('INFO: upsertSuppDocsFile - end [RETURN=1]', data.applicationId);
                  cb({
                    success: true,
                    values:resp.values,
                    viewedList: viewedList,
                    pendingSubmitList: resp.pendingSubmitList,
                    tokensMap
                  });
                });
              });
            }
          } else {
            logger.error('ERROR: upsertSuppDocsFile - end [RETURN=-4]', data.applicationId);
            cb({success: false});
          }
        });
      };

      var attachments = {};
      let fileProps = genFileProps(data.attachmentValues);
      var subStrPattern = "base64,";
      var index = data.attachment.indexOf(subStrPattern);

      attachments[fileProps.id] = data.attachment.substring(index+subStrPattern.length);
      applicationDao.getApplication(data.applicationId, function(application){
        if (!application.error) {
          bDao.getApplicationByBundleId(_.get(application, 'bundleId'), application.id).then((bApp)=>{
            if (bApp) {
              if (_.get(bApp, 'appStatus') === 'SUBMITTED') {
                isSubmittedApp = true;
              }
              applicationDao.upsertAppAttachments(
                data.applicationId,
                application._rev,
                // Convert attachment into attachments(array) for upsertAppAttachments inparm
                attachments,
                (res)=>{
                  if (res.success == true){
                    updateSuppDocs(isSubmittedApp, application, fileProps, data.valueLocation, data.tabId, data.suppDocsValues, data.viewedList, cb);
                  }
                }
              );
            } else {
              logger.error('ERROR: upsertSuppDocsFile - end [RETURN=-3]', data.applicationId);
              cb({success : false});
            }
          }).catch((err)=>{
            logger.error('ERROR: upsertSuppDocsFile - end [RETURN=-2]', data.applicationId, err);
            cb({success : false});
          });
        } else {
          logger.error('ERROR: upsertSuppDocsFile - end [RETURN=-1]', data.applicationId);
          cb({success : false});
        }
      });
    } else {
      cb({
        success: true,
        showTotalFilesSizeLargeDialog: true
      });
    }

  }).catch(error => {
    logger.error(`ERROR: upsertSuppDocsFile ${error}`);
  });
};
/**FUNCTION ENDS: upsertSuppDocsFile */

var addSuppDocsPendingReviewFiles = function(app, agentCode, toAddFiles) {
  if (app.supportDocuments.viewedList) {
    if (!app.supportDocuments.viewedList[agentCode]) {
      app.supportDocuments.viewedList[agentCode] = {};
    }
    _.forEach(toAddFiles, file => {
      app.supportDocuments.viewedList[agentCode][file] = false;
    });
  }
}

module.exports.updateSuppDocsViewedList = function(data, session, cb){
  let agentCode = session.agentCode;
  let appId = data.appId;
  let viewedList = data.viewedList;
  logger.log('INFO: updateSuppDocsViewedList - start', appId);

  applicationDao.getApplication(appId, application => {
    if (!application.error) {
      if (application.supportDocuments.viewedList[agentCode]) {
        application.supportDocuments.viewedList[agentCode] = viewedList;
        applicationDao.upsertApplication(appId, application, function(resp){
          if (resp) {
            logger.log('INFO: updateSuppDocsViewedList - end [RETURN=1]', appId);
            cb({success: true});
          } else {
            logger.error('ERROR: updateSuppDocsViewedList - end [RETURN=-3]', appId);
            cb({success: false});
          }
        });
      } else {
        logger.error('ERROR: updateSuppDocsViewedList - end [RETURN=-2]', appId);
        cb({success: false});
      }
    } else {
      logger.error('ERROR: updateSuppDocsViewedList - end [RETURN=-1]', appId);
      cb({success: false});
    }
  });
}

var getSuppDocsPolicyFormTemplate = function(app, session, pProfile, pCid, eApprovalCase, appStatus){
  logger.log('INFO: getSuppDocsPolicyFormTemplate');
  let sysDocsDocuments = [];
  let mandDocsDocuments = [];
  let optDocsDocuments = [];
  // let payerName = payerProfile.fullName;
  let pName = pProfile.fullName;
  let {isThirdPartyPayer, payerName} = commonApp.getPayerInfo(app);

  if (app.appStep && app.appStep > 0) {
    sysDocsDocuments.push({
      "type":"file",
      "id":"appPdf"
    });
  }
  if (session.agent.channel.type != "FA") {
    sysDocsDocuments.push({
      "type":"file",
      "id":"fnaReport"
    });
  }

  sysDocsDocuments.push({
    "type":"file",
    "id":"proposal"
  });

  if (['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(_.get(app, 'payment.initPayMethod')) > -1 && app.isSubmittedStatus){
    sysDocsDocuments.push({
      "type":"file",
      "id":eCpdPdfName
    });
  }
  let showSupervisorStatus = ['A', 'R', 'PFAFA'];
  if ((showSupervisorStatus.indexOf(eApprovalCase.approvalStatus) > -1) && eApprovalCase.isFACase === true) {
    //Show when the approver is not the director
    if(eApprovalCase.agentId !== eApprovalCase.approveRejectManagerId  && _.get(eApprovalCase, '_attachments.eapproval_supervisor_pdf') !== undefined) {
      sysDocsDocuments.push({
        "type":"file",
        "id":"eapproval_supervisor_pdf"
      });
    }

    //Show when it is approved by FA Admin
    if (eApprovalCase.approver_FAAdminCode !== '' && _.get(eApprovalCase, '_attachments.faFirm_Comment') !== undefined) {
      sysDocsDocuments.push({
        "type":"file",
        "id":"faFirm_Comment"
      });
    }

  } else if ((eApprovalCase.approvalStatus === 'A' || eApprovalCase.approvalStatus === 'R') && _.get(eApprovalCase, '_attachments.eapproval_supervisor_pdf') !== undefined){
    sysDocsDocuments.push({
      "type":"file",
      "id":"eapproval_supervisor_pdf"
    });
  }

  // mandDocsDocuments
  // Application is in FA channel
  if (session.agent.channel.type === 'FA'){
    mandDocsDocuments.push({
      "type":"subSection",
      "id":"cAck",
      "title":"Client acknowledgement/ FNA"
    });
  }
  // proposer is singaporean of singapore pr, N1 stands for singapore
  if (pProfile.nationality == 'N1' || pProfile.prStatus == 'Y') {
    mandDocsDocuments.push({
      "type":"subSection",
      "id":"pNric",
      "title":"Proposer's Singapore NRIC "
    });
  } else {
    if (pProfile.pass == 'ep' || pProfile.pass == 'wp' || pProfile.pass == 'dp' || pProfile.pass == 's' || pProfile.pass == 'lp' || pProfile.pass === 'sp') {
      mandDocsDocuments.push({
        "type":"subSection",
        "id":"pPass",
        "title":"Proposer's Copy of valid pass in Singapore ",
        "toolTips": "Copy of valid Employment Pass / S Pass / Work Permit / Dependant / Student / Long Term Visit Pass. (both sides)"
      });
    } else {
      mandDocsDocuments.push({
        "type":"subSection",
        "id":"pPassport",
        "title":"Proposer's Copy of valid passport (first page) ",
        "toolTips": "Passport cover page which contain all personal details "
      });
      mandDocsDocuments.push({
        "type":"subSection",
        "id":"pPassportWStamp",
        "title":"Proposer's Copy of valid passport pages with entry stamps issued by Singapore immigration ",
        "toolTips": "Passport entry stamp is required as proof of entry into Singapore. "
      });
    }
  }
  mandDocsDocuments.push({
    "type":"subSection",
    "id":"pAddrProof",
    "title":"Proposer's Proof of Residential Address "
  });
  // If payer is third party payer
  if (isThirdPartyPayer) {
    mandDocsDocuments.push({
      "type":"subSection",
      "id":"thirdPartyID",
      "title":"Third Party Payor's ID copy ",
      "toolTips": "Example of ID copy for third party payor: NRIC / Valid Pass in Singapore / Passport "
    });
    mandDocsDocuments.push({
      "type":"subSection",
      "id":"thirdPartyAddrProof",
      "title":"Third Party Payor's Proof of Residential Address "
    });
  }

  // optDocsDocuments
  // proposer is China nationality and type of pass is others
  if (pProfile.nationality == 'N4' && pProfile.pass == 'o'){
    optDocsDocuments.push({
      "type":"subSection",
      "id":"pChinaVisit",
      "title":"Proposer's Mainland China Visitor Declaration Form ",
      "toolTips": "Mainland China Visitor Declaration Form is compulsory for applicants who are China residents. "
    });
  }

  // proposer is Japanese nationality and All product except Shield
  if (pProfile.nationality === 'N81') {
    optDocsDocuments.push({
      "type": "subSection",
      "id": "pJapaneseNotShield",
      "title": "Declaration Form Status (Japanese)"
    });
  }


  if (appStatus == 'SUBMITTED'
    || _.get(session, 'agent.agentCode') === _.get(session, 'agent.managerCode')) {
    // Check initial payment method: AXA/SAM or Cheque/Cashier Order or Telegraphic Transfer or Cash
    if (app.payment && app.payment.initPayMethod) {
      if (app.payment.initPayMethod == 'axasam') {
        mandDocsDocuments.push({
          "type":"subSection",
          "id":"axasam",
          "title":"AXS/SAM Receipt "
        });
      } else if (app.payment.initPayMethod == 'chequeCashierOrder') {
        mandDocsDocuments.push({
          "type":"subSection",
          "id":"chequeCashierOrder",
          "title":"Cheque / Cashier Order Copy "
        });
      } else if (app.payment.initPayMethod == 'teleTransfter') {
        mandDocsDocuments.push({
          "type":"subSection",
          "id":"teleTransfer",
          "title":"Telegraphic Transfer Receipt "
        });
      } else if (app.payment.initPayMethod == 'cash') {
        mandDocsDocuments.push({
          "type":"subSection",
          "id":"cash",
          "title":"Conditional Receipt of Cash "
        });
      }
    }
  } else {
    // Check initial payment method: AXA/SAM or Cheque/Cashier Order or Telegraphic Transfer or Cash
    if (app.payment && app.payment.initPayMethod) {
      if (app.payment.initPayMethod == 'axasam') {
        optDocsDocuments.push({
          "type":"subSection",
          "id":"axasam",
          "title":"AXS/SAM Receipt "
        });
      } else if (app.payment.initPayMethod == 'chequeCashierOrder') {
        optDocsDocuments.push({
          "type":"subSection",
          "id":"chequeCashierOrder",
          "title":"Cheque / Cashier Order Copy "
        });
      } else if (app.payment.initPayMethod == 'teleTransfter') {
        optDocsDocuments.push({
          "type":"subSection",
          "id":"teleTransfer",
          "title":"Telegraphic Transfer Receipt "
        });
      } else if (app.payment.initPayMethod == 'cash') {
        optDocsDocuments.push({
          "type":"subSection",
          "id":"cash",
          "title":"Conditional Receipt of Cash "
        });
      }
    }
  }

  let template = [];

  template.push(
    {
      "id":"sysDocs",
      "title": "System Document",
      "hasSubLevel": "false",
      "disabled": "true",
      "type":"section",
      "items":sysDocsDocuments
    }
  );

  template.push(
      {
        "id":"mandDocs",
        "hasSubLevel": "false",
        "disabled": "false",
        "title": "Mandatory Document",
        "type":"section",
        "items":mandDocsDocuments
      }
    );

  if (optDocsDocuments.length > 0) {
    template.push(
      {
        "id":"optDoc",
        "hasSubLevel": "false",
        "disabled": "false",
        "title": "Optional Document",
        "type":"section",
        "items":optDocsDocuments
      }
    );
  }

  template.push(
    {
      "id":"otherDoc",
      "hasSubLevel": "true",
      "disabled": "false",
      "title": "Other Document",
      "type":"section",
      "items":[]
    }
  );

  return template;
};

var getSuppDocsProposerTemplate = function(app, session, pProfile){
  logger.log('INFO: getSuppDocsProposerTemplate');
  let pName = pProfile.fullName;
  let optDocsDocuments = [];

  optDocsDocuments.push({
    "type":"subSection",
    "id":"pMedicalTest",
    "title":"Copy of medical test / investigation / check up report ",
    "toolTips": "Please submit copy of medical report as declared in Medical and Health information"
  });

  let template = [
    {
      "id":"optDoc",
      "hasSubLevel": "false",
      "disabled": "false",
      "title": "Optional Document",
      "type":"section",
      "items":optDocsDocuments
    },
    {
      "id":"otherDoc",
      "hasSubLevel": "true",
      "disabled": "false",
      "title": "Other Document",
      "type":"section",
      "items":[]
    }
  ]
  return template;
}

var getSuppDocsInsuredTemplate = function(app, session, iProfile){
  logger.log('INFO: getSuppDocsInsuredTemplate');
  const firstInsuredInsurabilityValues =  _.get(app, 'applicationForm.values.insured[0].insurability', {});
  let iName = iProfile.fullName;
  let mandDocsDocuments = [];
  let optDocsDocuments = [];
  // mandDocsDocuments
  // proposer is singaporean of singapore pr, N1 stands for singapore
  if (iProfile.nationality == 'N1' || iProfile.prStatus == 'Y') {
    mandDocsDocuments.push({
      "type":"subSection",
      "id":"iNric",
      "title":"Singapore NRIC /Birth Certificate "
    });
  } else {
    if (iProfile.pass == 'ep' || iProfile.pass == 'wp' || iProfile.pass == 'dp' || iProfile.pass == 's' || iProfile.pass == 'lp' || iProfile.pass === 'sp') {
      mandDocsDocuments.push({
        "type":"subSection",
        "id":"iPass",
        "title":"Copy of valid pass in Singapore ",
        "toolTips": "Copy of valid Employment Pass / S Pass / Work Permit / Dependant / Student / Long Term Visit Pass. (both sides)"
      });
    } else {
      mandDocsDocuments.push({
        "type":"subSection",
        "id":"iPassport",
        "title":"Copy of valid passport (first page) ",
        "toolTips": "Passport cover page which contain all personal details "
      });
      mandDocsDocuments.push({
        "type":"subSection",
        "id":"iPassportWStamp",
        "title":"Copy of passport pages with entry stamps issued by Singapore immigration ",
        "toolTips": "Passport entry stamp is required as proof of entry into Singapore. "
      });
    }
  }

  // optDocsDocuments
  // insured is China nationality and type of pass is others
  if (iProfile.nationality == 'N4' && iProfile.pass == 'o'){
    optDocsDocuments.push({
      "type":"subSection",
      "id":"iChinaVisit",
      "title":"Mainland China Visitor Declaration Form ",
      "toolTips": "Mainland China Visitor Declaration Form is compulsory for applicants who are China residents. "
    });
  }

  // insured is Japanese nationality and All product except Shield
  if (iProfile.nationality === 'N81') {
    optDocsDocuments.push({
      "type": "subSection",
      "id": "iJapaneseNotShield",
      "title": "Declaration Form Status (Japanese)"
    });
  }


  // [Medical & Health Info.] If answer for "In the PAST FIVE YEARS, have you had any tests done such as X-ray, ultrasound, CT scan, biopsy, electrocardiogram (ECG), blood or urine test?" is YES
  if (_.get(firstInsuredInsurabilityValues, 'HEALTH11') === 'Y') {
    optDocsDocuments.push({
      "type":"subSection",
      "id":"iMedicalTest",
      "title":"Copy of medical test / investigation / check up report ",
      "toolTips": "Please submit copy of medical report as declared in Medical and Health information."
    });
  }

  if (_.find(
        // find parm 0
        _.at(firstInsuredInsurabilityValues, ['HEALTH14', 'HEALTH15', 'HEALTH16', 'HEALTH17']),
        // find parm 1
        (value)=>{return value === 'Y';}
        )
      )
    {
      optDocsDocuments.push({
        "type":"subSection",
        "id":"iJuvenileQuestions",
        "title":"Copy of latest Child Health Booklet including all assessments done to date ",
        "toolTips": " Child Health Booklet is required due to declaration in Juvenile Question in Medical and Health Information. "
      });
  }

  let template = [
    {
      "id":"mandDocs",
      "hasSubLevel": "false",
      "disabled": "false",
      "title": "Mandatory Document",
      "type":"section",
      "items": mandDocsDocuments
    }
  ];

  if (optDocsDocuments.length > 0) {
    template.push(
      {
        "id":"optDoc",
        "hasSubLevel": "false",
        "disabled": "false",
        "title": "Optional Document",
        "type":"section",
        "items":optDocsDocuments
      }
    );
  }

  template.push(
    {
      "id":"otherDoc",
      "hasSubLevel": "true",
      "disabled": "false",
      "title": "Other Document",
      "type":"section",
      "items":[]
    }
  );
  return template;
}

var getSupportDocumentsTemplate = function(app, session, pProfile, iProfile, appStatus, pCid, iCid, eApprovalCase){
  logger.log('INFO: getSupportDocumentsTemplate');
  // TODO: Remove appStatus passed from FrontEnd
  let template = {
    policyForm: getSuppDocsPolicyFormTemplate(app, session, pProfile, pCid, eApprovalCase, appStatus)
  }

  // [Medical & Health Info.] If answer for "In the PAST FIVE YEARS, have you had any tests done such as X-ray, ultrasound, CT scan, biopsy, electrocardiogram (ECG), blood or urine test?" is YES
  if (_.get(app, 'applicationForm.values.proposer.insurability.HEALTH11') === 'Y') {
    template['proposer'] = getSuppDocsProposerTemplate(app, session, pProfile);
  }

  if (pCid != iCid) {
    template['insured'] = getSuppDocsInsuredTemplate(app, session, iProfile);
  }
  return template;
}

var genSuppDocsPolicyFormValues = function(){
  let values = {
    "sysDocs":{
      "fnaReport":{
        "id":"fnaReport",
        "title": "e-FNA",
        "fileType":"application/pdf",
        "fileSize": ""
      },
      "proposal":{
        "id":"proposal",
        "title": "e-PI",
        "fileType":"application/pdf",
        "fileSize": ""
      },
      "appPdf":{
        "id":"appPdf",
        "title": "e-App",
        "fileType":"application/pdf",
        "fileSize": ""
      },
      "eCpdPdf":{
        "id":eCpdPdfName,
        "title": "e-CPD",
        "fileType":"application/pdf",
        "fileSize": ""
      },
      "eapproval_supervisor_pdf":{
        "id":"eapproval_supervisor_pdf",
        "title": "Supervisor Validation",
        "fileType":"application/pdf",
        "fileSize": ""
      },
      "faFirm_Comment":{
        "id":"faFirm_Comment",
        "title": "Comments by FA Firm Admin",
        "fileType":"application/pdf",
        "fileSize": ""
      }
    },
    "mandDocs":{
      "cAck":[],
      "pNric":[],
      "pPass":[],
      "pPassport":[],
      "pPassportWStamp":[],
      "pAddrProof":[],
      "thirdPartyID":[],
      "thirdPartyAddrProof":[],
      "axasam":[],
      "chequeCashierOrder":[],
      "teleTransfer":[],
      "cash":[]
    },
    "optDoc":{
      "axasam":[],
      "chequeCashierOrder":[],
      "teleTransfer":[],
      "cash":[],
      "pChinaVisit":[],
      "pJapaneseNotShield":[]
    }
  }
  return values;
}

var genSuppDocsProposerValues = function(){
    let values = {
    "mandDocs":{

    },
    "optDoc":{
      "pMedicalTest":[]
    }
  }
  return values;
}

var genSuppDocsInsuredValues = function(){
  let values = {
    "mandDocs":{
      "iNric":[],
      "iPass":[],
      "iPassport":[],
      "iPassportWStamp":[],
    },
    "optDoc":{
      "iChinaVisit":[],
      "iJapaneseNotShield":[],
      "iMedicalTest":[],
      "iJuvenileQuestions":[],
    }
  }
  return values;
}

var genSupportDocumentsValues = function(){
  let values = {
    policyForm:genSuppDocsPolicyFormValues(),
    proposer:genSuppDocsProposerValues(),
    insured:genSuppDocsInsuredValues()
  };
  return values;
}

var deleteSuppDocsFile = function(app, attachmentId, valueLocation, tabId, cb){
  logger.log('INFO: deleteSuppDocsFile - start', app._id);
  let {_attachments, supportDocuments} = app;
  let {sectionId, subSectionId} = valueLocation;
  // Modify _attachments Json
  // if (_attachments){
  //   delete _attachments[attachmentId];
  //   app._attachments = _attachments;
  // }
  // Modify supportDocuments Json
  let tabValues = supportDocuments.values[tabId];
  let sectionValue = tabValues[sectionId];
  let subSectionValue;
  if (sectionId == 'otherDoc') {
    subSectionValue = sectionValue.values[subSectionId];
  } else {
    subSectionValue = sectionValue[subSectionId];
  }
  if(subSectionValue){
    var changedSubSectionValue = subSectionValue.filter((item)=>{
      return item.id != attachmentId;
    });
  }

  if (sectionId == 'otherDoc'){
    sectionValue.values[subSectionId] = changedSubSectionValue;
  } else {
    sectionValue[subSectionId] = changedSubSectionValue;
  }
  tabValues[sectionId] = sectionValue;
  supportDocuments.values[tabId] = tabValues;
  app.supportDocuments = supportDocuments;
  applicationDao.upsertApplication(app._id, app, function(resp){
    if(resp){
      commonApp.updateChildSupportDocumentValue(app, () => {
        logger.log('INFO: deleteSuppDocsFile - end [RETURN=1]', app._id);
        cb({
          success: true,
          supportDocuments: supportDocuments
        });
      });
    } else {
      logger.error('ERROR: deleteSuppDocsFile - end  [RETURN=-1]', app._id);
      cb({success:false});
    }
  });
};

var addSupportDocuments = function(isSubmittedApp=false, app, fileProps, valueLocation, tabId, suppDocsValues, agentCode, rootValues, cb){
  logger.log('INFO: addSupportDocuments - start', app._id);
  let {sectionId, subSectionId} = valueLocation;
  let values = isSubmittedApp ? rootValues : app.supportDocuments.values;
  let tabValues = isSubmittedApp? suppDocsValues : values[tabId];
  let sectionValue = tabValues[sectionId];
  let submittedApp = isSubmittedApp?_.cloneDeep(app):null;

  if (sectionId == 'otherDoc') {
    if (_.isEmpty(sectionValue.values) || _.isEmpty(sectionValue.values[subSectionId])) {
      _.set(sectionValue, `values[${subSectionId}]`, []);
    }
    sectionValue.values[subSectionId].push({
      "id": fileProps.id,
      "title": fileProps.title,
      "fileSize": fileProps.fileSize,
      "uploadDate": fileProps.backendUploadDate,
      "feUploadDate": fileProps.uploadDate,
      "fileType": fileProps.fileType,
      "length": fileProps.length
    });
    // let subSectionValue = sectionValue.values[subSectionId];
    //   subSectionValue.push({
    //     "id": fileProps.id,
    //     "title": fileProps.title,
    //     "fileSize": fileProps.fileSize,
    //     "uploadDate": fileProps.backendUploadDate,
    //     "fileType": fileProps.fileType,
    //     "length": fileProps.length
    //   });
  } else {
    if (_.isEmpty(sectionValue[subSectionId])) {
      _.set(sectionValue, `[${subSectionId}]`, []);
    }
    sectionValue[subSectionId].push({
      "id": fileProps.id,
      "title": fileProps.title,
      "fileSize": fileProps.fileSize,
      "uploadDate": fileProps.backendUploadDate,
      "feUploadDate": fileProps.uploadDate,
      "fileType": fileProps.fileType,
      "length": fileProps.length
    });
    // let subSectionValue = sectionValue[subSectionId];
    //   subSectionValue.push({
    //     "id": fileProps.id,
    //     "title": fileProps.title,
    //     "fileSize": fileProps.fileSize,
    //     "uploadDate": fileProps.backendUploadDate,
    //     "fileType": fileProps.fileType,
    //     "length": fileProps.length
    //   });
    // sectionValue[subSectionId] = subSectionValue;
  }
  // Don't remove/comment out this line
  values[tabId] = tabValues;

  if (isSubmittedApp) {
    // upsert a pending to update list
    if (!submittedApp.supportDocuments.pendingSubmitList) {
      submittedApp.supportDocuments['pendingSubmitList'] = [];
    }
    submittedApp.supportDocuments.pendingSubmitList.push(fileProps.id);
    applicationDao.upsertApplication(submittedApp._id, submittedApp, function(resp){
      if(resp){
        logger.log('INFO: addSupportDocuments - end [RETURN=1]', app._id);
        cb({
          success: true,
          values: values,
          pendingSubmitList: _.get(submittedApp, 'supportDocuments.pendingSubmitList')
        });
      } else {
        logger.error('ERROR: addSupportDocuments - end [RETURN=-1]', app._id);
        cb({success:false});
      }
    });
  } else {
    applicationDao.upsertApplication(app._id, app, function(resp){
      if(resp){
        logger.log('INFO: addSupportDocuments - end [RETURN=2]', app._id);
        cb({
          success: true,
          values: values,
          pendingSubmitList: _.get(submittedApp, 'supportDocuments.pendingSubmitList', [])
        });
      } else {
        logger.error('ERROR: addSupportDocuments - end [RETURN=-2]', app._id);
        cb({success:false});
      }
    });
  }
}
/**FUNCTION ENDS: addSupportDocuments */

// Disabled
// var getSuppDocsAppView = function(app) {
//   let suppDocsAppView = {
//     id: app.id,
//     iCid: app.iCid,
//     pCid: app.pCid,
//     iFullName: app.quotation.iFullName,
//     pFullName: app.quotation.pFullName
//   };
//   return suppDocsAppView;
// }

let checkMandDocsStatus = function(template, values) {
  logger.log('INFO: checkMandDocsStatus');
  let result = {};
  let allUploaded = true;

  _.forEach(template, (tabTemplate, tabId)=>{
    let tabContent = tabTemplate.tabContent || tabTemplate;
    let mandDocsTemplate = _.get(
      _.find(tabContent, (section)=>{
        return section.id === 'mandDocs';
      }),
      'items');
    let mandDocsValues = _.get(values[tabId], 'mandDocs', {});

    _.forEach(mandDocsTemplate, (fileTemplate)=>{
      if (_.isEmpty(_.get(mandDocsValues, _.get(fileTemplate, 'id')))) {
        result[tabId] = false;
        allUploaded = false;
        return false;
      }
    });
  });

  result['allUploaded'] = allUploaded;
  return result;
};

module.exports.closeSuppDocs = function(data, session, cb){
  logger.log('INFO: closeSuppDocs - start', data.appId);
  let mandDocsAllUploaded = true;

  let prepareSubmissionTemplateValues = function(currApp) {
    getSubmissionTemplate(currApp, function(tmpl) {
      getSubmissionValues(currApp, session, '', '', function (resValues) {
        logger.log('INFO: closeSuppDocs - end [RETURN=1]', data.appId);
        cb({
          success: true,
          template: tmpl,
          values: resValues.values,
          isMandDocsAllUploaded: mandDocsAllUploaded,
          isSubmitted: false
        });
      });
    })
  }

  applicationDao.getApplication(data.appId, (app)=>{
    // if (_.get(app, 'type') === 'masterApplication') {
    //   data.appDoc = app;
    //   shieldHandler.closeSuppDocs(data, session, cb);
    // } else {
      mandDocsAllUploaded = _.get(
        checkMandDocsStatus(data.template, _.get(app, 'supportDocuments.values', {})),
        'allUploaded'
      );
      commonApp.deletePendingSubmitList(app).then((resp) => {
        if (resp.success) {
          let application = resp.application;
          application.isMandDocsAllUploaded = mandDocsAllUploaded;
          applicationDao.upsertApplication(application.id, application, function(resp){
            if (resp) {
              prepareSubmissionTemplateValues(application);
            } else {
              logger.error('ERROR: closeSuppDocs - end [RETURN=-3]', data.appId);
              cb({success: false});
            }
          });
        } else {
          logger.error('ERROR: closeSuppDocs - end [RETURN=-2]', data.appId);
          cb({success: false});
        }
      }).catch((error) => {
        logger.error('ERROR: closeSuppDocs - deletePendingSubmitList [RETURN=-1]', data.appId, error);
      });
    // }
  });
};
/**FUNCTION ENDS: closeSuppDocs*/

module.exports.getSupportDocuments = function(data, session, cb){
  logger.log('INFO: getSupportDocuments - start', data.applicationId);
  let agentCode = session.agentCode;

  let initViewedList = function(app, template){
  // let diffWithSysDocs = function(app, template){
    let idList = [];
    // let newViewedList = [];

    if (agentCode === app.agentCode) {
      idList = ['appPdf', 'proposal'];
      if (session.agent.channel.type !== 'FA') {
        idList.push('fnaReport');
      }
    } else {
      idList = _.keys(_.get(app, 'supportDocuments.viewedList.' + app.agentCode, {}));

      let sysDocsIds = ['appPdf', 'proposal'];
      if (session.agent.channel.type !== 'FA') {
        sysDocsIds.push('fnaReport');
      }
      idList = _.union(idList, sysDocsIds);
    }

    if (session.agent.channel.type === "FA") {
      // if in FA Channel, need to add cAck's files into ViewedList, as the documents of FNA in FA Channel
      _.forEach(
        _.get(app, 'supportDocuments.values.policyForm.mandDocs.cAck', []),
        (cAckFile) => {
          idList.push(cAckFile.id);
        }
      );
    }
    _.forEach(idList, (id)=>{
      if (!_.includes(
            _.keys(_.get(app, 'supportDocuments.viewedList.'+agentCode))
            , id)) {
        _.set(app, 'supportDocuments.viewedList.'+agentCode + '.' + id, false);
      }
    });
  };

  let updateViewedList = function(app, template, eApprovalCase){
    if (!app.supportDocuments.viewedList[agentCode]) {
      app.supportDocuments.viewedList[agentCode] = {};
      initViewedList(app, template);
      // diffWithSysDocs(app, template);
    } else {
      if (agentCode !== app.agentCode) {
        let loginAgentViewedObj = app.supportDocuments.viewedList[agentCode];
        if (_.get(eApprovalCase, 'approvalStatus') && !_.includes(['A', 'R', 'E'], _.get(eApprovalCase, 'approvalStatus'))) {
          if (_.isEmpty(loginAgentViewedObj)) {
            logger.log('INFO: Support Document - Recover supervisor viewedList - non Shield - empty object :', app.id);

            loginAgentViewedObj =  {
              'appPdf'    : false,
              'proposal'  : false
            };
            if (session.agent.channel.type !== 'FA') {
              loginAgentViewedObj['fnaReport'] = false;
            }
            app.supportDocuments.viewedList[agentCode] = loginAgentViewedObj;
          } else if (!loginAgentViewedObj.hasOwnProperty('appPdf')) {
            logger.log('INFO: Support Document - Recover supervisor viewedList - non Shield - missed eApp : ', app.id);

            loginAgentViewedObj['appPdf'] = false;
            app.supportDocuments.viewedList[agentCode] = loginAgentViewedObj;
          }
        }
      }
    }
  };

  let getDefaultDocNameList = function() {
    const defaultDocNameListId = 'suppDocsDefaultDocNames';
    return new Promise((resolve)=>{
      dao.getDoc(defaultDocNameListId, function(list) {
        resolve(list);
      });
    });
  };

  let getOtherDocNameList = function(docNamesList,iIsJapaneseAndNotShield,pIsJapaneseAndNotShield) {
    const otherDocNameListId = 'otherDocNames';
    let ikey = iIsJapaneseAndNotShield ? 'otherDocNames_JapaneseAndNotShield' : 'otherDocNames';
    let pkey = pIsJapaneseAndNotShield ? 'otherDocNames_JapaneseAndNotShield' : 'otherDocNames';
    return new Promise((resolve)=>{
      dao.getDoc(otherDocNameListId, function(obj) {
        let otherDocNames = obj ? {
          'la': obj[ikey].la,
          'ph': obj[pkey].ph
        } : {'la':[],'ph':[]};
        docNamesList.otherDocNames = otherDocNames;
        resolve(docNamesList);
      });
    });
  };

  let genTemplateValues = function(app, session, pProfile, iProfile, appStatus, pCid, iCid, isReadOnly, cb, eApprovalCase){
    logger.log('INFO: getSupportDocuments - genTemplateValues', data.applicationId);
    let template = getSupportDocumentsTemplate(app, session, pProfile, iProfile, appStatus, pCid, iCid, eApprovalCase);
    let suppDocsAppView = {
      id: app.id,
      iCid: app.iCid,
      pCid: app.pCid,
      iFullName: app.quotation.iFullName,
      pFullName: app.quotation.pFullName
    };
    let tokensMap = {};

    let iIsJapaneseAndNotShield = false;
    let pIsJapaneseAndNotShield = false;
    if(app.type && app.type==='application'){
      if(iProfile.nationality === 'N81'){
        iIsJapaneseAndNotShield = true;
      }
      if(pProfile.nationality === 'N81'){
        pIsJapaneseAndNotShield = true;
      }
    }
    // Default DOC NAME LIST for checking duplication of document name
    getDefaultDocNameList()
    .then((docNamesList) => {
      //get other doc selction list for non-Shield,update docNamesList
      return getOtherDocNameList(docNamesList,iIsJapaneseAndNotShield,pIsJapaneseAndNotShield);
    })
    .then((docNamesList) => {
    // Init Support Documents in DB
      if (!app.supportDocuments){
        let values = genSupportDocumentsValues();
        app['supportDocuments'] = {};
        app.supportDocuments["values"] = values;
        app.supportDocuments["viewedList"] = {};
        app.supportDocuments["isAllViewed"] = false;
        updateViewedList(app, template, eApprovalCase);

        let supportDocDetails = {
          otherDocNameList: docNamesList.otherDocNames
        };
        applicationDao.upsertApplication(app._id, app, function(resp){
          if(resp){
            logger.log('INFO: getSupportDocuments - end [RETURN=1]', data.applicationId);
            cb({
              success: true,
              template: template,
              values: values,
              suppDocsAppView: suppDocsAppView,
              viewedList: app.supportDocuments.viewedList[agentCode],
              isReadOnly: isReadOnly,
              defaultDocNameList: docNamesList.defaultDocumentNames,
              supportDocDetails:supportDocDetails,
              tokensMap: tokensMap,
              application: app
            });
          } else {
            logger.error('ERROR: getSupportDocuments - end [RETURN=-2]', data.applicationId);
            cb({success:false});
          }
        });
      } else{
        if (!app.supportDocuments.viewedList) {
          app.supportDocuments["viewedList"] = {};
        }
        updateViewedList(app, template, eApprovalCase);
        let supportDocDetails = {
          otherDocNameList: docNamesList.otherDocNames
        };
        applicationDao.upsertApplication(app._id, app, function(resp){
          if(resp){

            //generate token for image in mandDocs & optDoc
            let now = new Date().getTime();
            let tokens = [];
            _.forEach(SuppDocsUtils.getNonSysDocs(app, ['application/pdf']), (doc) => {
              let token = createPdfToken(app._id, doc.id, now, session.loginToken);
              tokens.push(token);
              tokensMap[doc.id] = token.token;
            })
            // no need to set pdf token if its IOS
            if (session.platform === "ios") {
                logger.log('INFO: getSupportDocuments - end [RETURN=2]', data.applicationId);
                cb({
                  success: true,
                  template: template,
                  values: app.supportDocuments.values,
                  suppDocsAppView: suppDocsAppView,
                  viewedList: app.supportDocuments.viewedList[agentCode],
                  isReadOnly: isReadOnly,
                  defaultDocNameList: docNamesList.defaultDocumentNames,
                  supportDocDetails: supportDocDetails,
                  tokensMap: tokensMap,
                  application: app
                });
            } else {
              setPdfTokensToRedis(tokens, () => {
                logger.log('INFO: getSupportDocuments - end [RETURN=3]', data.applicationId);
                cb({
                  success: true,
                  template: template,
                  values: app.supportDocuments.values,
                  suppDocsAppView: suppDocsAppView,
                  viewedList: app.supportDocuments.viewedList[agentCode],
                  isReadOnly: isReadOnly,
                  defaultDocNameList: docNamesList.defaultDocumentNames,
                  supportDocDetails: supportDocDetails,
                  tokensMap: tokensMap,
                  application: app
                });
              });
            }
          } else {
            logger.error('ERROR: getSupportDocuments - end [RETURN=-4]', data.applicationId);
            cb({success:false});
          }
        });
      }
    });
  };

  applicationDao.getApplication(data.applicationId, function(appDoc){
    if (!appDoc.error){
      commonApp.deletePendingSubmitList(appDoc).then((resp) => {
        let app = resp.application || appDoc;
        let pCid = app.quotation.pCid;
        let iCid = app.quotation.iCid;
        // let payerName = getPayerName(app);
        // if (payerCid == '') payerCid = pCid;
        ApprovalHandler.searchApprovalCaseById({id: app.policyNumber}, session, (resp) => {
          commonApp.isSuppDocsReadOnly(app, session.agent, session.agentCode).then(function(isReadOnly){
            logger.log('isReadOnly :' + isReadOnly);
            clientDao.getProfileById(pCid, function(pProfile){
              bDao.getApplicationByBundleId(_.get(app, 'bundleId'), app.id).then((bApp)=>{
                if (pCid == iCid) {
                  genTemplateValues(app, session, pProfile, pProfile, bApp.appStatus, pCid, iCid, isReadOnly, cb, resp.foundCase);
                } else {
                  clientDao.getProfileById(iCid, function(iProfile){
                    genTemplateValues(app, session, pProfile, iProfile, bApp.appStatus, pCid, iCid, isReadOnly, cb, resp.foundCase);
                  });
                }
              });
            });
          })
        });
      });
    } else {
      logger.error('ERROR: getSupportDocuments - end [RETURN=-1]', data.applicationId);
      cb({success: false});
    }
  });
}
/**FUNCTION ENDS: getSupportDocuments*/

module.exports.checkSuppDocsProperties = function(data, session, cb){
  logger.log('INFO: checkSuppDocsProperties - start', data.appId);
  const currentAgentCode = session.agentCode;
  let showHealthDeclarationPrompt = false;
  let isShieldApp = false;
  let approvalDocId;
  let agentViewedList;

  applicationDao.getApplication(data.appId, app => {
    if (!app.error) {
      isShieldApp = app.type === 'masterApplication';

      // health declaration check
      if (isShieldApp && commonApp.isShowHealthDeclaration(app)) {
        if (_.isEmpty(_.get(app, 'supportDocuments.values.policyForm.mandDocs.healthDeclaration', []))) {
          showHealthDeclarationPrompt = true;
        }
      }

      if (_.get(app, 'supportDocuments.viewedList')) {
        let reviewDone = true;

        if (isShieldApp) {
          approvalDocId = ApprovalHandler.getMasterApprovalIdFromMasterApplicationId(app.id);
        } else {
          approvalDocId = app.policyNumber;
        }

        dao.getDocFromCacheFirst(approvalDocId, (approvalDoc) => {
          if (_.includes(['A', 'R'], _.get(approvalDoc, 'approvalStatus', ''))) {
            agentViewedList = app.supportDocuments.viewedList[ _.get(approvalDoc, 'approveRejectManagerId')];
          } else {
            agentViewedList = app.supportDocuments.viewedList[currentAgentCode];
          }

          if (_.isEmpty(agentViewedList) || !agentViewedList.hasOwnProperty('appPdf')) {
            reviewDone = false;
          } else {
            for (let i in agentViewedList) {
              if (!agentViewedList[i]) {
                reviewDone = false;
                break;
              }
            }
          }

        /**
         * Clear Supp Docs Cached and useless data, Notify AXA - START
         */
          clearCacheSuppDocData(app, isShieldApp, session, (clearCacheResult) => {
            if (clearCacheResult.notifyAXA && !app.notifyAbnormalCase) {
              app.notifyAbnormalCase = true;
              applicationDao.upsertApplication(app._id, app, function(resp){
                if(resp && !resp.error){
                  logger.log(`INFO:: Successfully updated the application and ready to send email to AXA:: ${app._id}`);
                  aEmaillHandler.notifyAXAwithAbnormalApprovalCase(app, clearCacheResult.failedReason);
                }else{
                  logger.error(`ERROR:: Failed to Update Application:: ${app._id}`);
                }
              });
            };
            logger.log('INFO: checkSuppDocsProperties - end [RETURN=1]', data.appId);
            cb({
              success: true,
              reviewDone: clearCacheResult.allowToApprove || (!clearCacheResult.missingEapp && reviewDone),
              showMissingEappPrompt: (clearCacheResult.missingEapp) ? true : false,
              showHealthDeclarationPrompt
            });
          });
         /**
          * Clear Supp Docs Cached and useless data, Notify AXA - END
          */
        });
      } else {
        logger.log('INFO: checkSuppDocsProperties - end [RETURN=2]', data.appId);
        cb({
          success: true,
          reviewDone: false,
          showHealthDeclarationPrompt
        });
      }

    } else {
      logger.error('ERROR: checkSuppDocsProperties - end [RETURN=-1]', data.appId);
      cb({success: false});
    }
  });
};
/**FUNCTION ENDS: checkSuppDocsProperties*/

const clearCacheSuppDocData = function(app, isShieldApp, session, cb) {
  let result;
  let template;
  const eApprovalCase = {};
  const pProfile = {};
  const iProfile = {};
  async.waterfall([
    (callback) => {
    /**  // Get all related docs for generating template
        let promises = [];
        let docIds = [];
        if (isShieldApp) {
          docIds.push(ApprovalHandler.getMasterApprovalIdFromMasterApplicationId(app.id));
        } else {
          docIds.push(_.get(app, 'pCid'));
          docIds.push(_.get(app, 'iCid'));
          docIds.push(_.get(app, 'policyNumber'));
        }
        _.each(docIds, id => {
          promises.push(new Promise(resolve => {
            dao.getDoc(id, (doc) => {
              resolve(doc);
            });
          }))
        });
        Promise.all(promises).then((docs) => {
          callback(null, docs);
        }).catch((error) => {
            logger.error(`ERROR: Get Docs in clearCacheSuppDocData: ${error}`);
            callback(error);
        });
    },
    (allDocs, callback) => {
      // Prepare Supporting Docs UI Template for checking mandatory docs and system docs
      if (isShieldApp) {
        let eApprovalCase = allDocs[0];
        template = shieldSupportDocument.getSupportDocumentsTemplate(app, 'SUBMITTED', session.agent, eApprovalCase);
      } else {
        let pProfile = allDocs[0];
        let iProfile = allDocs[1];
        let eApprovalCase = allDocs[2];
        template = getSupportDocumentsTemplate(app, session, pProfile, iProfile, 'SUBMITTED', pProfile.cid, iProfile.cid, eApprovalCase);
      }
      callback(null, template);
    },
    (template, callback) => {
      let mandDocIds = {};
      _.each(template, (tabTemplate, tabKey) => {
        mandDocIds[tabKey] = {};
        _.each(tabTemplate, (sectionTemplate, sectionKey) => {
          if (sectionTemplate && (sectionTemplate.id === 'mandDocs' || sectionTemplate.id === 'sysDocs')) {
            mandDocIds[tabKey][sectionTemplate.id] = [];
            _.each(sectionTemplate.items, item => {
              //if ((sectionKey === 'sysDocs' && item.id.indexOf('appPdf') > -1)|| sectionKey === 'mandDocs') {
              if (sectionKey === 'mandDocs') {
                mandDocIds[tabKey][sectionTemplate.id].push(item.id);
              }
            })
          }
        });
      });
      callback(null, mandDocIds);
    },
    (mandDocIds, callback) => {
      let result = {};
      // Check are all mandatory files displayed and able to review
      let successfullyDisplayAllMandDocs = true;
      _.each(mandDocIds, (tab, tabKey) => {
        _.each(tab, (section, sectionKey) => {
          _.each(section, id => {
            let mandObj = _.get(app, `supportDocuments.values.${tabKey}.${sectionKey}.${id}`);
            if(mandObj !== undefined && _.isEmpty(mandObj)) {
              successfullyDisplayAllMandDocs = false;
            }
          });
        });
      });

      // Missing e-App
      let missingeApp = false;

      const noteAppAttachmentIds = ['fnaReport', 'proposal'];
      // Check any cached useless data in supporting docs
      let attachmentsKeys = _.keys(app._attachments);
      let viewedList =_.get(app, `supportDocuments.viewedList.${session.agent.agentCode}`);
      let viewedListKeys = _.filter(_.keys(viewedList), id => noteAppAttachmentIds.indexOf(id) === -1 );
      // Empty - All viewedListKeys has been uploaded succesffully and has attachmentId
      // let allMandUploaded = _.isEmpty(_.difference(viewedListKeys, attachmentsKeys));

      // Check UI and ViewedListMappng
      _.each(_.get(app, 'supportDocuments.values'), tabObj => {
        _.each(tabObj, sectionObj => {
          _.each(sectionObj, docsObj => {
            viewedListKeys = _.remove(viewedListKeys, (id) => {
              // Prepare the list the do datapatch the viewedList, do datapatch when there is any remaining items
              return docsObj.id === id && (viewedListKeys.indexOf(docsObj.id) > -1 || viewedList[docsObj.id] === true);
            });
          });
        });
      });

      let uiViewedListMapped = _.isEmpty(viewedListKeys);

      if (missingeApp) {
        result = {
          success: true,
          notifyAXA: true,
          missingEapp: true,
          allowToApprove: false
        }
      } else if (successfullyDisplayAllMandDocs) {
        result = {
          success: true,
          notifyAXA: true,
          allowToApprove: uiViewedListMapped ? false : true
        }
      } else {
        result ={
          success: true,
          notifyAXA: false
        };
      }*/
      let eAppShowing = true;
      if (isShieldApp) {
        // Check Proposer Show the eapp
        eAppShowing = _.get(app, 'applicationForm.values.proposer.extra.isPdfGenerated');
        // Check Insured show the eApp
        _.each(_.get(app, 'iCids', []), (iCid) => {
          let foundInsuredObj = _.find(_.get(app, 'applicationForm.values.insured'), (insured) => {
            return insured.personalInfo.cid === iCid;
          });
          if (!_.get(foundInsuredObj, 'extra.isPdfGenerated')) {
            eAppShowing = false;
          }
        });
      } else {
        eAppShowing = app.appStep && app.appStep > 0
      }

      if (app && app.isMandDocsAllUploaded && eAppShowing) {
        let viewedList = _.get(app, `supportDocuments.viewedList.${session.agent.agentCode}`);
        let supportDocumnetsValues = _.get(app, `supportDocuments.values`);

        // remove all viewedList when it appears in UI, remain the not matched case
        let viewedAllDisplayedSuppDocs = true;
        _.each(supportDocumnetsValues, tab => {
          _.each(tab, section => {
            _.each(section, docsArray => {
              let docs;
              if (section.values) {
                // Other docs go 1 more level deeper
                _.each(section.values, otherDocsArray => {
                  _.each(otherDocsArray,  obj => {
                    if (_.findKey(viewedList, (value, key) => key ===  obj.id) && _.get(viewedList, obj.id) === false) {
                      viewedAllDisplayedSuppDocs = false
                    }
                    viewedList = _.omit(viewedList, [obj.id]);
                  });
                })
              } else {
                _.each(docsArray, obj => {
                  // Check any supporting docs (matched the UI values) not yet viewed
                  if ((_.findKey(viewedList, (value, key) => key ===  docsArray.id) && _.get(viewedList, docsArray.id) === false)
                  || (_.findKey(viewedList, (value, key) => key ===  obj.id) && _.get(viewedList, obj.id) === false)) {
                    viewedAllDisplayedSuppDocs = false
                  }
                  // Remove the supporting docs that Matched the UI
                  viewedList = _.omit(viewedList, [obj.id]);
                });
              }
              // Remove System Docs Id
              viewedList = _.omit(viewedList, [docsArray.id]);
            });
          });
        });

        let notMatchedUIandViewedList = !_.isEmpty(viewedList);

        if (notMatchedUIandViewedList && viewedAllDisplayedSuppDocs) {
          logger.error(`clearCacheSuppDocData:: UI and viewedList are not matched:: ${app && app.id}`);
          result = {
            success: true,
            notifyAXA: true,
            allowToApprove: true,
            failedReason: 'Agent has approved the case, please extract the case to check the root cause'
          }
        } else {
          result = {
            success: true,
            notifyAXA: false
          }
        }
      } else if  (!eAppShowing) {
        logger.error(`clearCacheSuppDocData:: e-App is not showing in the UI:: ${app && app.id}`);
        result = {
          success: true,
          notifyAXA: true,
          missingEapp: true,
          allowToApprove: false,
          failedReason: 'Manager cannot approve the case, please extract the case to check the root cause'
        }
      } else {
        result = {
          success: true,
          notifyAXA: false
        }
      }

      callback(null, result);
    }
  ], (err, result) => {
    if (err) {
      logger.error("Error in clearCacheSuppDocData: ", err);
      cb({
        success: false,
        notifyAXA: false
      });
    } else {
      cb(result);
    }
  });
};

let createNewViewedList = function(app, agentCode) {
  _.forEach(app.supportDocuments.viewedList, (item) => {
    if (!_.isEmpty(item)) {
      app.supportDocuments.viewedList[agentCode] = item;
      _.forEach(Object.keys(app.supportDocuments.viewedList[agentCode]), (file) => {
        app.supportDocuments.viewedList[agentCode][file] = false;
      });
    }
  });
};

module.exports.viewedSuppDocsFile = function(data, session, cb){
  logger.log('INFO: viewedSuppDocsFile - start', data.applicationId);
  let agentCode = session.agentCode;

  let now = Date.now();
  let signDocConfig =  {
    // pdfUrl: '',
    attUrl: global.config.signdoc.getPdf,
    postUrl: global.config.signdoc.postUrl,
    resultUrl: global.config.signdoc.resultUrl,
    dmsId: global.config.signdoc.dmsid,
    docid: data.attachmentId,
    auth: genSignDocAuth(data.attachmentId, now),
    docts: now
  };

  let checkAllFilesViewed = function(supportDocuments){
    let allViewed = true;
    let files = supportDocuments.viewedList[agentCode];
    for (let i in files) {
      if (!files[i]) {
        allViewed = false;
      }
    }
    if (allViewed) {
      supportDocuments.isAllViewed = true;
    }
  }

  let updateViewedList = function(appId, attachmentId){
    logger.log('INFO: viewedSuppDocsFile - updateViewedList', data.applicationId);
    if (data.isSupervisorChannel && data.updateReviewList) {
      applicationDao.getApplication(appId, (app) => {
        if (!app.supportDocuments.viewedList[agentCode]) {
          createNewViewedList(app, agentCode);
        }
        if (!_.isUndefined(app.supportDocuments.viewedList[agentCode][attachmentId])) {
          app.supportDocuments.viewedList[agentCode][attachmentId] = true;
        }
        // _.set(app, 'supportDocuments.viewedList[' + agentCode + '][' + attachmentId + ']', true);

        if (!app.supportDocuments.isAllViewed) {
          checkAllFilesViewed(app.supportDocuments);
        }
        applicationDao.upsertApplication(appId, app, (resp) => {
          if (!resp) {
            logger.error('ERROR: viewedSuppDocsFile - updateViewedList [RETURN=-1]', data.applicationId);
          }
        });
      });
    }
  }

  let uploadFNAReport = function(app, bundle) {
    return new Promise((resolve) => {
      needsHandler.generateFNAReport({cid: app.pCid}, session, (pdfResult) => {
        if(!pdfResult.fnaReport){
          logger.error('ERROR: viewedSuppDocsFile - end cannot gen FNA Report [RETURN=-30]', app.id);
          resolve(false);
        }
        else {
          dao.uploadAttachmentByBase64(bundle.id, data.attachmentId, bundle._rev, pdfResult.fnaReport, 'application/pdf', (res)=>{
            resolve(true);
          });
        }
      });
    });
  }

  if (data.applicationId == null) {
    logger.error('ERROR: viewedSuppDocsFile - end [RETURN=-1]');
      cb({success : false});
  } else {
    if (data.attachmentId === appPdfName) {
      applicationDao.getApplication(data.applicationId, (app)=>{
        if (!app.error){
          updateViewedList(data.applicationId, data.attachmentId);

          let tokens = [createPdfToken(data.applicationId, data.attachmentId, now, session.loginToken)];
          setPdfTokensToRedis(tokens, () => {
            logger.log('INFO: viewedSuppDocsFile - end [RETURN=8]', data.applicationId);
            cb({
              success: true,
              docId: data.applicationId,
              token: tokens[0].token,
              isSigned: app.isApplicationSigned,
              signDocConfig: signDocConfig
            });
          });
        } else {
          logger.error('ERROR: viewedSuppDocsFile - end [RETURN=-8]', data.applicationId);
          cb({success: false});
        }
      });
    } else if (data.attachmentId === proposalPdfName) {
      applicationDao.getApplication(data.applicationId, (app)=>{
        if (!app.error){
          updateViewedList(data.applicationId, data.attachmentId);

          let tokens = [createPdfToken(app.quotationDocId, data.attachmentId, now, session.loginToken)];
          setPdfTokensToRedis(tokens, () => {
            logger.log('INFO: viewedSuppDocsFile - end [RETURN=2]', data.applicationId);
            cb({
              success: true,
              docId: app.quotationDocId,
              token: tokens[0].token,
              isSigned: app.isProposalSigned,
              signDocConfig: signDocConfig
            });
          });
        } else {
          logger.error('ERROR: viewedSuppDocsFile - end [RETURN=-2]', data.applicationId);
          cb({success: false});
        }
      });
    } else if (data.attachmentId === fnaPdfName) {
      applicationDao.getApplication(data.applicationId, (app)=>{
        if (!app.error){
          let bundleId = _.get(app, 'bundleId');
          bDao.getBundle(app.pCid, bundleId, function(bundle) {
            let isFnaReportSigned = _.get(bundle, 'isFnaReportSigned', false);
            updateViewedList(data.applicationId, data.attachmentId);

            let tokens = [createPdfToken(bundleId, data.attachmentId, now, session.loginToken)];
            setPdfTokensToRedis(tokens, () => {
              logger.log('INFO: viewedSuppDocsFile - end [RETURN=3]', data.applicationId);
              if (!isFnaReportSigned && bundle.status < bDao.BUNDLE_STATUS.START_GEN_PDF) {
                uploadFNAReport(app, bundle).then((success)=>{
                  if (success) {
                    cb({
                      success: true,
                      docId: bundleId,
                      token: tokens[0].token,
                      isSigned: isFnaReportSigned,
                      signDocConfig: signDocConfig
                    });
                  } else {
                    logger.error('ERROR: uploadFNAReport unsuccessfully - end [RETURN=-3]', data.applicationId);
                    cb({success: false});
                  }
                });
              } else {
                cb({
                  success: true,
                  docId: bundleId,
                  token: tokens[0].token,
                  isSigned: isFnaReportSigned,
                  signDocConfig: signDocConfig
                });
              }
            });
          })
        } else {
          logger.error('ERROR: viewedSuppDocsFile - end [RETURN=-3]', data.applicationId);
          cb({success: false});
        }
      });
    } else if (data.attachmentId === eCpdPdfName) {
      updateViewedList(data.applicationId, data.attachmentId);
      let tokens = [createPdfToken(data.applicationId, data.attachmentId, now, session.loginToken)];
      setPdfTokensToRedis(tokens, () => {
        logger.log('INFO: viewedSuppDocsFile - end [RETURN=4]', data.applicationId);
        cb({
          success: true,
          docId: data.applicationId,
          token: tokens[0].token,
          isSigned: false,
          signDocConfig: signDocConfig
        });
      });
    } else if (data.attachmentId == 'eapproval_supervisor_pdf') {
      applicationDao.getApplication(data.applicationId, (app)=>{
        if (!app.error){
          let approvalDocId;
          if (_.get(app, 'quotation.quotType') === 'SHIELD') {
            approvalDocId = ApprovalHandler.getMasterApprovalIdFromMasterApplicationId(data.applicationId);
          } else {
            approvalDocId = app.policyNumber;
          }
          let tokens = [createPdfToken(approvalDocId, data.attachmentId, now, session.loginToken)];
          setPdfTokensToRedis(tokens, () => {
            logger.log('INFO: viewedSuppDocsFile - end [RETURN=5]', data.applicationId);
            cb({
              success: true,
              docId: app.policyNumber,
              token: tokens[0].token,
              isSigned: false,
              signDocConfig: signDocConfig
            });
          });
        } else {
          logger.error('ERROR: viewedSuppDocsFile - end [RETURN=-5]', data.applicationId);
          cb({success: false});
        }
      });
    } else if (data.attachmentId == 'faFirm_Comment') {
      applicationDao.getApplication(data.applicationId, (app)=>{
        if (!app.error){
          let approvalDocId;
          if (_.get(app, 'quotation.quotType') === 'SHIELD') {
            approvalDocId = ApprovalHandler.getMasterApprovalIdFromMasterApplicationId(data.applicationId);
          } else {
            approvalDocId = app.policyNumber;
          }
          let tokens = [createPdfToken(approvalDocId, data.attachmentId, now, session.loginToken)];
          setPdfTokensToRedis(tokens, () => {
            logger.log('INFO: viewedSuppDocsFile - end [RETURN=6]', data.applicationId);
            cb({
              success: true,
              docId: app.policyNumber,
              token: tokens[0].token,
              isSigned: false,
              signDocConfig: signDocConfig
            });
          });
        } else {
          logger.error('ERROR: viewedSuppDocsFile - end [RETURN=-6]', data.applicationId);
          cb({success: false});
        }
      });
    } else {
      updateViewedList(data.applicationId, data.attachmentId);
      let tokens = [createPdfToken(data.applicationId, data.attachmentId, now, session.loginToken)];
      setPdfTokensToRedis(tokens, () => {
        logger.log('INFO: viewedSuppDocsFile - end [RETURN=7]', data.applicationId);
        cb({
          success: true,
          docId: data.applicationId,
          token: tokens[0].token,
          isSigned: false,
          signDocConfig: signDocConfig
        });
      });
    }
  }
}

// This function is used in 'Other Document' type
module.exports.deleteSupportDocument = function(data, session, cb){
  logger.log('INFO: deleteSupportDocument - start', data.applicationId);
  applicationDao.getApplication(data.applicationId, function(application){
    if(!application.error){
      let supportDocuments = application.supportDocuments;
      let tabValues = supportDocuments.values[data.tabId];
      // let otherDoc = supportDocuments.values.otherDoc;
      let otherDoc = tabValues.otherDoc;
      let values = otherDoc.values;
      let template = otherDoc.template;
      let attachmentIdsArr = [];
      let successList = [];
      let failList = [];
      if (values[data.documentId]){
        values[data.documentId].forEach((item)=>{
          attachmentIdsArr.push(item.id);
        });
      }
      if (attachmentIdsArr.length == 0) {
        template.splice(template.findIndex((item)=>{return item.id == data.documentId}),1);
        delete values[data.documentId];
        tabValues.otherDoc['template'] = template;
        tabValues.otherDoc['values'] = values;
        supportDocuments.values[data.tabId] = tabValues;
        application.supportDocuments = supportDocuments;
        applicationDao.upsertApplication(data.applicationId, application, function(upsertAppResp){
          logger.log('INFO: deleteSupportDocument - end [RETURN=1]', data.applicationId);
          cb({
            values:supportDocuments.values
          });
        })
      } else {
        applicationDao.getApplication(data.applicationId, function(app){
          if (app && !app.error) {
            let attachments = app['_attachments'];
            attachmentIdsArr.forEach((docId)=>{
              delete attachments[docId];
            })
            app['_attachments'] = attachments;

            dao.updDoc(data.applicationId, app, function(result){
              if (!result.error || result.success) {
                // delete success
                dao.updateViewIndex("main", "summaryApps");
                template.splice(template.findIndex((item)=>{return item.id == data.documentId}),1);
                delete values[data.documentId];
                tabValues.otherDoc['template'] = template;
                tabValues.otherDoc['values'] = values;
                supportDocuments.values[data.tabId] = tabValues;
                application.supportDocuments = supportDocuments;
                applicationDao.upsertApplication(data.applicationId, application, function(upsertAppResp){
                  // update fail
                  if (upsertAppResp.success === false) {
                    logger.log('INFO: deleteSupportDocument - end [RETURN=2]', data.applicationId);
                    cb({
                      values:supportDocuments.values,
                      success: false,
                      error: upsertAppResp.error
                    });
                  }
                  logger.log('INFO: deleteSupportDocument - end [RETURN=3]', data.applicationId);
                  cb({
                    success: true,
                    values:supportDocuments.values
                  });
                });
              } else {
                // delete fail
                logger.log('INFO: deleteSupportDocument - end [RETURN=4]', data.applicationId);
                cb({
                  success: false,
                  values:supportDocuments.values,
                  error: result.error
                });
              }
            })
          } else {
            // can't get application
            logger.log('INFO: deleteSupportDocument - end [RETURN=5]', data.applicationId);
            cb({
              success: false,
              values:supportDocuments.values,
              error: app.error
            });
          }
        });
      }
    } else {
      logger.error('INFO: deleteSupportDocument - end [RETURN=-1]', data.applicationId);
      cb({success: false});
    }
  })
};

module.exports.editSupportDocument = function(data, session, cb){
  logger.log('INFO: editSupportDocument - start', data.applicationId);
  applicationDao.getApplication(data.applicationId, function(application){
    if(!application.error){
      let supportDocuments = application.supportDocuments;
      let tabValues = supportDocuments.values[data.tabId];
      // let otherDoc = supportDocuments.values.otherDoc;
      let otherDoc = tabValues.otherDoc;
      let values = otherDoc.values;
      let template = otherDoc.template;
      let attachmentIdsArr = [];
      let successList = [];
      let failList = [];
      if (values[data.documentId]){
        values[data.documentId].forEach((item)=>{
          attachmentIdsArr.push(item.id);
        });
      }

      let changeData = otherDoc.template.find((temp) => temp.id === data.documentId);
      changeData.docName = data.docName;
      changeData.docNameOption = data.docName;
      changeData.title = data.docName;

      applicationDao.upsertApplication(data.applicationId, application, function(upsertAppResp){
        logger.log('INFO: editSupportDocument - end [RETURN=1]', data.applicationId);
        cb({
          values:supportDocuments.values
        });
      })
    } else {
      logger.error('INFO: deleteSupportDocument - end [RETURN=-1]', data.applicationId);
      cb({success: false});
    }
  })
};

module.exports.deleteSupportDocumentFile = function(data, session, cb){
  logger.error('INFO: deleteSupportDocumentFile - start', data.applicationId);
  applicationDao.deleteAppAttachment(data.applicationId, data.attachmentId, session, (resp)=>{
    if(resp.success == true){
      applicationDao.getApplication(data.applicationId, function(application){
        if(!application.error){
          deleteSuppDocsFile(application, data.attachmentId, data.valueLocation, data.tabId, (delResp)=>{
            if(delResp.success == true){
              logger.log('INFO: deleteSupportDocumentFile - end [RETURN=1]', data.applicationId);
              cb({
                success: true,
                supportDocuments: delResp.supportDocuments
              });
            } else {
              logger.error('ERROR: deleteSupportDocumentFile - end [RETURN=-3]', data.applicationId);
              cb({success: false});
            }
          });
        }else{
          logger.error('ERROR: deleteSupportDocumentFile - end [RETURN=-2]', data.applicationId);
          cb({success: false});
        }
      })
    } else {
      logger.error('ERROR: deleteSupportDocumentFile - end [RETURN=-1]', data.applicationId);
      cb({success: false});
    }
  });
};

// set the signExpiryShown as true
module.exports.setSignExpiryShown = function(data, session, cb){
  if (data.appId) {
    applicationDao.getApplication(data.appId, function(app) {
      if (app && !app.error) {
        app.signExpiryShown = true;
        applicationDao.upsertApplication(data.appId, app, function(upsertResult) {
          if (upsertResult && !upsertResult.error) {
            cb({
              success: true
            })
          } else {
            cb({
              success: false
            })
          }
        }, true);
      } else {
        cb({
          success: false
        })
      }
    })
  } else {
    cb({
      success: false
    })
  }
}

module.exports.goApplication = function(data, session, cb){
  let appId = data.id;
  let result = {};
  let crossAgeData = {appId: appId};
  let isFaChannel = session.agent.channel.type === 'FA';

  logger.log('INFO: goApplication - start', appId);

  async.waterfall([
    (callback) => {
      applicationDao.getApplication(appId, function(application){
        if (application && !application.error) {
          bDao.getBundle(application.pCid, application.bundleId, (bundle) => {
            if (bundle && !bundle.error) {
              callback(null, application, bundle);
            } else {
              callback('Fail to get bundle ' + _.get(bundle, 'error'));
            }
          })
        } else {
          callback('Fail to get application ' + _.get(application, 'error'));
        }
      });
    }, (application, bundle, callback) => {
      defaultApplication.getAppFormTemplate(application.quotation, function(template, sections) {
        if (bundle.isFnaReportSigned) {
          logger.log('INFO: goApplication - frozenAppFormTemplateFnaQuestions', appId);
          commonApp.frozenAppFormTemplateFnaQuestions(template);
        }
        commonApp.getOptionsList(application, template, function(){
          // if start signature, frozen the application value
          if(application.isStartSignature){
            result.application = application;
            logger.log('INFO: goApplication - getApplication frozenTemplate', appId);
            if (session.platform) {
              commonApp.frozenTemplateMobile(template);
            } else {
              commonApp.frozenTemplate(template);
            }
            result.template = template;
            callback(null, result.application, false);
          } else {
            result.template = template;
            // if it is not frozen, populate form values from Bundle and any update is make
            // if (isFaChannel) {
            //   result.application = application;
            //   getPNumber(result.application);
            // } else {
              commonApp.populateFromBundle(application, sections, isFaChannel).then((fvApp)=>{
                result.application = fvApp;
                callback(null, result.application, true);
              });
            // }
          }
        })
      });
    }, (application, checkPolicyNumber, callback) => {
      if (checkPolicyNumber) {
        let values = application.applicationForm.values;

        logger.log('INFO: goApplication - getPNumber has PNumber', appId, (application.policyNumber ? true : false));
        if (application.policyNumber) {
          callback(null);
        } else {
          //determinate if next menu is insurability
          //if no menu insurability, check declaration
          let menus = values.menus || [];
          let checkedMenu = values.checkedMenu || [];
          let completedMenus = values.completedMenus || [];
          let targetMenu = (menus.indexOf("menu_insure")>-1) ? "menu_insure" : "menu_declaration";
          let toGetPyNo = false;
          for(let mIndex = 0 ; mIndex < menus.length; mIndex++){
            let curMenu = menus[mIndex];
            if(checkedMenu.indexOf(curMenu) == -1 || completedMenus.indexOf(curMenu) == -1){
              if(curMenu == targetMenu){
                toGetPyNo = true;
              }
              break;
            }
          }
          logger.log('INFO: goApplication - getPNumber get', toGetPyNo, 'for', appId);
          if(toGetPyNo){
            getPolicyNumber(1, function(pNumbers){
              let pNumber = pNumbers ? pNumbers[0] : null;
              application.policyNumber = pNumber;
              callback(null);
            });
          } else{
            callback(null);
          }
        }
      } else {
        callback(null);
      }
    }, (callback) => {
      logger.log('INFO: goApplication - checkCrossAge/SignatureExpiry', appId);

      // delete appPdf if the values changed
      let signExpireWarningDay = global.config.signExpireWarningDay;
      logger.debug('DEBUG: check signature expiry for showing warning: ',result.application.biSignedDate , result.application.appStatus , result.application.signExpiryShown, appId);
      // check signature expiry for showing warning
      if (result.application.biSignedDate
        && !result.application.isSubmittedStatus
        && !result.application.signExpiryShown) {
        let expiryTime = new Date();
        let biSignDate = new Date(result.application.biSignedDate);
        expiryTime.setDate(expiryTime.getDate() - signExpireWarningDay);
        if (biSignDate.getTime() < expiryTime.getTime()) {
          result.showSignExpiryWarning = true;
        }
      }

      // checkCrossAge(crossAgeData, session, (crossAgeResp) => {
      defaultApplication.checkCrossAge(result.application, (crossAgeResp) => {

        result.crossAgeObj = {};
        if (crossAgeResp.success && (crossAgeResp.insuredStatus === commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED || crossAgeResp.proposerStatus === commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED)) {
          result.crossAgeObj = {
            showCrossedAgeSignedMsg : true,
            insuredCrossed : crossAgeResp.insuredStatus === commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED,
            proposerCrossed : crossAgeResp.proposerStatus === commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED
          };
        }

        // TODO: deprecate crossAgeObj, use crossAge
        if (crossAgeResp.success) {
          result.crossAge = crossAgeResp;
        }

        if (crossAgeResp.success && crossAgeResp.crossedAge) {
          result.application.isCrossAge = true;
        }

        logger.debug('DEBUG: goApplication - result', appId, result.showSignExpiryWarning, result.crossAgeObj.showCrossedAgeSignedMsg, result.application.isCrossAge);

        applicationDao.upsertApplication(result.application.id, result.application, function(resp){
          if (resp && !resp.error) {
            result.success = true;
            callback(null);
          } else {
            result.success = false;
            callback('Fail to update appliation ' + _.get(resp, 'error'));
          }
        });
      });
    }
  ], (err) => {
    if (err) {
      logger.error('ERROR: goApplication - end [RETURN=-1]', appId);
    } else {
      logger.log('INFO: goApplication - end [RETURN=1]', appId);
    }
    cb(result);
  })
};

//generate a new application
module.exports.apply = function(data, session, cb){
  let isFaChannel = session.agent.channel.type === 'FA';
  let quotationId = data.id;

  logger.log('INFO: apply application - start', quotationId);
  if(!quotationId) {
    logger.log('INFO: apply application - end [RETURN=-1]');
    cb({success: true, error:"please use the app properly"});
  }

  //get quotation
  let result = {success: true};

  quotDao.getQuotation(quotationId, (quotation) => {
    // let cid = _.get(quotation, 'pCid');
    let pCid = _.get(quotation, 'pCid');
    let iCid = _.get(quotation, 'iCid');
    let bundleId = _.get(quotation, 'bundleId');
    bDao.getDocByQuotId(pCid, quotationId).then((bQuotation)=>{
      if (_.get(bQuotation, 'appStatus') === 'INVALIDATED') {
        logger.log('INFO: apply action, document has been invalidated', quotationId);
        applicationDao.getAppListView('01', pCid, bundleId).then((appList)=>{
          //Modify by Alex Tse for CR49 Start**************
          getLabel(appList, () => {
            cb({
              success: true,
              isInvalidated: true,
              appList: appList
            });
          });
          //Modify by Alex Tse for CR49 End****************
        });
      } else {
        new Promise((resolve) => {
          bDao.getCurrentBundle(pCid).then((bundle) => {
            defaultApplication.getAppFormTemplate(quotation, function(template, sections, dynTemplate){
              if (bundle.isFnaReportSigned) {
                logger.log('INFO: apply application - frozenAppFormTemplateFnaQuestions', quotationId);
                commonApp.frozenAppFormTemplateFnaQuestions(template);
              }
              //getOptionsList is done in genApplicationDoc
              genApplicationDoc(session, quotation, bundle, sections, template, (app)=>{
                _.set(app, 'applicationForm.values.appFormTemplate.dynTemplate', dynTemplate);
                result.template = template;
                // if (isFaChannel) {
                //   resolve(app);
                // } else {
                  commonApp.populateFromBundle(app, sections, isFaChannel).then((fvApp)=>{
                    resolve(fvApp);
                  }).catch(error=>{
                    logger.error('ERROR: populateFromBundle', _.get(app, 'id'), error);
                  });;
                // }
              })
            })
          })
        }).then((application) => {
          commonApp.getApplicationId(session, function(id){
            application.id = id;
            applicationDao.updApplication(id, application, function(){
              dao.updateViewIndex("main", "summaryApps");
              dao.updateViewIndex("main", "quotationByAgent");

              bDao.onCreateApplication(pCid, quotation.id, application.id).then(() => {
                result.application = application;
                // dao.updateViewIndex("main", "contacts");
                // cb(result);
                clientDao.getProfile(pCid, true).then((profile)=>{
                  bDao.getApplyingApplicationsCount(application.bundleId).then((count)=>{
                    profile.applicationCount = count;
                    clientDao.updateProfile(pCid, profile).then(()=>{
                      // result.profile = profile;
                      dao.updateViewIndex("main", "contacts");

                      logger.log('INFO: apply application - end', quotationId, id);
                      cb(result);
                    });
                  }).catch((error)=>{
                    logger.error('ERROR: getApplyingApplicationsCount', error);
                  });;
                });
              });
            });
          });
        }).catch((err) => {
          logger.error('ERROR: apply application [Fail]', quotationId, err);
          cb({success: false});
        });
      }
    });
  });
};

var getPlanDetailType = function(planCode){
  let typeMap = {
    "SAV":1
  }

  return typeMap[planCode];
}

let getQuotCheckedList = function(applicationsList, bundle = {}){
  let quotation = {};
  let quotCheckedList = [];
  const invalidStatuses = ['INVALIDATED', 'INVALIDATED_SIGNED'];

  if (_.get(bundle, 'isFnaReportSigned')) {
    quotCheckedList = _.get(bundle, 'clientChoice.quotCheckedList');
  } else {
    // bundle is not exists, or isFnaReportSigned is false
    for (let i in applicationsList){
      if (!_.includes(invalidStatuses, _.get(applicationsList[i], 'appStatus'))){
        if (applicationsList[i].type === 'application' || applicationsList[i].type === 'masterApplication'){
          quotation = applicationsList[i].quotation;
        } else {
          quotation = applicationsList[i];
        }

        quotCheckedList.push({
          id: quotation.id,
          checked: _.get(quotation, 'clientChoice.isClientChoiceSelected', false)
        });
      }
    }
  }

  return ({
    isClientChoiceCompleted: _.get(bundle, 'clientChoice.isClientChoiceCompleted', true),
    quotCheckedList: quotCheckedList
  });
};

var deleteApplications = function(data, session, cb){
  let applicationIds = data.deleteItemsSelectedArray;
  let pCid = _.get(data, 'applicationsList[0].pCid'); // consider getting the pCid from request
  // Bundle ID of applications that going to delete
  let exBundleId = _.get(data, 'applicationsList[0].bundleId');
  let currentBundleId;
  if (exBundleId) {
    let isFaChannel = session.agent.channel.type == 'FA'
    logger.log('INFO: deleteApplications - start', pCid);

    /** DO NOT REMOVE start */
    bDao.getCurrentBundle(pCid).then(currBundle => {
      const applications = _.get(currBundle, "applications", []);
      const applyingAppIds = _.filter(_.map(applications, app => app.appStatus === "APPLYING" ? app.applicationDocId : ""), id => id.length > 0);
      let deleteApplyingIdCount = 0;
      _.forEach(applyingAppIds, id => {
        if (applicationIds.indexOf(id) >= 0) {
          deleteApplyingIdCount += 1;
        }
      });
      const rollbackBundle = data.rollbackBundle || deleteApplyingIdCount === applyingAppIds.length;
      /** DO NOT REMOVE end */

      return Promise.all(_.map(applicationIds, (docId) => {
        return new Promise((resolve) => {
          applicationDao.getApplication(docId, (application) => {
            if (application && !application.error) {
              if (_.get(application, 'childIds', []).length > 0) {
                //for shield
                let deletePromises = application.childIds.map((childId) => {
                  return new Promise((cResolve, cReject) => {
                    applicationDao.getApplication(childId, (childApp) => {
                      if (childApp && !childApp.error) {
                        applicationDao.deleteApplication(childId, childApp._rev, (dRes) => {
                          cResolve(dRes.success && childId);
                        });
                      } else if (childApp.error === 'not_found') {
                        //deleted
                        cResolve(childId);
                      } else {
                        cReject(new Error('Fail to delete child application for' + docId + _.get(childApp, 'error')))
                      }
                    })
                  });
                });
                Promise.all(deletePromises).then((dResults) => {
                  logger.log('INFO: deleteApplications - delete child', docId, dResults);
                  applicationDao.deleteApplication(docId, application._rev, (res) => {
                    resolve(res.success && docId);
                  });
                })
              } else {
                applicationDao.deleteApplication(docId, application._rev, (res) => {
                  resolve(res.success && docId);
                });
              }
            } else {
              resolve(null);
            }
          });
        });
      })).then((ids) => {
        let deletedIds = _.filter(ids, id => id);
        return bDao.getCurrentBundle(pCid).then((bundle) => {
          // No need to update bundle Status when the on deleting applications are not in current bundle
          currentBundleId = bundle.id;
          let hasCasesOtherThanInvalidate = false;
          // If the delete cases only contain invalidated cases
          // no need to rollback bundle status
          _.each(bundle.applications, app => {
            if (app.appStatus !== 'INVALIDATED' && (deletedIds.indexOf(app.applicationDocId) > -1 || deletedIds.indexOf(app.quotationDocId) > -1)) {
              hasCasesOtherThanInvalidate = true;
            }
          })
          if (bundle.id === exBundleId && hasCasesOtherThanInvalidate) {
            /** DO NOT REMOVE rollbackBundle, don't use data.rollbackBundle */
            if (rollbackBundle && bundle.status < bDao.BUNDLE_STATUS.FULL_SIGN) {
              return bDao.rollbackStatus(pCid, bDao.BUNDLE_STATUS.HAS_PRE_EAPP).then(() => {
                return {ids, bundleId: bundle.id};
              });
            } else {
              return {ids, bundleId: bundle.id};
            }
          } else {
            return {ids, bundleId: bundle.id};
          }
        })
      }).then((result) => {
        let deletedIds = _.filter(_.get(result, 'ids'), id => id);
        // Seperate to delete invalidate Applications only
        // No bundle Status Update
        return bDao.onDeleteInvalidateApplications(pCid, deletedIds, isFaChannel, exBundleId, currentBundleId).then((bundleId) => {
          return result;
        })
      }).then((result) => {
        // Deleting invalidated cases in old bundle
        // No need to update bundle again, handled in onDeleteInvalidateApplications
        if (exBundleId === currentBundleId) {
          let deletedIds = _.filter(_.get(result, 'ids'), id => id);
          return bDao.onDeleteApplications(pCid, deletedIds, isFaChannel, exBundleId, currentBundleId).then((bundleId) => {
            if (bundleId) {
              return bundleId;
            } else {
              throw new Error('Fail to get bundle in onDeleteApplications');
            }
          })
        } else {
          return exBundleId;
        }

      }).then((bundleId) => {
        // update contacts applying count
        if (currentBundleId === exBundleId) {
          clientDao.getProfile(pCid, true).then((profile)=>{
            bDao.getApplyingApplicationsCount(bundleId).then((count)=>{
              profile.applicationCount = count;
              clientDao.updateProfile(pCid, profile).then(()=>{
                dao.updateViewIndex("main", "contacts");
                applicationDao.getAppListView('01', pCid, bundleId).then((appList) => {
                  //Modify by Alex Tse for CR49 Start**********
                  getLabel(appList, () => {
                    let {quotCheckedList} = getQuotCheckedList(appList);
                    logger.log('INFO: deleteApplications - end [RETURN=1]', pCid);
                    cb({ success: true, result: appList, quotCheckedList: quotCheckedList});
                  });
                  //Modify by Alex Tse for CR49 End ***********
                });
              });
            });
          });
        } else {
          applicationDao.getAppListView('01', pCid, exBundleId).then((appList) => {
            //Modify by Alex Tse for CR49 Start**********
            getLabel(appList, () => {
              let {quotCheckedList} = getQuotCheckedList(appList);
              logger.log('INFO: deleteApplications - end [RETURN=1]', pCid);
              cb({ success: true, result: appList, quotCheckedList: quotCheckedList});
            });
            //Modify by Alex Tse for CR49 End ***********
          });
        }

      })
    }).catch((err) => {
      logger.error('ERROR: deleteApplications - end [RETURN=-1]', pCid, err);
      cb({ success: false });
    });
  } else {
    logger.error('ERROR: deleteApplications - end [RETURN=-1] CANNOT find bundleId in applications that going to delete', pCid, applicationIds);
    cb({ success: false });
  }
}
module.exports.deleteApplications = deleteApplications;

// confirm applicatino paid after invalidated (signed)
module.exports.confirmAppPaid = function(data, session, cb) {
  const updateApplication = application => {
    applicationDao.updApplication(application.id, application, (result) => {
      if (result && !result.error) {
        dao.updateViewIndex("main", "summaryApps");
        cb({success: true})
      } else {
        cb({success: false, error: 'Update Application failure:' +application.id})
      }
    })
  };

  if (data && data.appId) {
    applicationDao.getApplication(data.appId, function(application) {
      if (application && !application.error) {
        application.isInvalidatedPaidToRLS = false;
        application.isInitialPaymentCompleted = true;
        application.lastUpdateDate = (new Date()).getTime();
        if (!application.isSubmittedStatus) {
          application.isSubmittedStatus = true;
          application.applicationSubmittedDate = (new Date()).getTime();
          logger.debug('DEBUG: Submit invalidated paid application (paid after invalidated):', application.id);

          if (session.platform) {
            updateApplication(application);
          } else {
            callApiComplete("/submitInvalidApp/"+application.id, "GET", {}, null, true, (resp) => {
              logger.log('INFO: Submit invalidated paid application (paid after invalidated):', application.id, resp);
              if (resp && resp.success) {
                updateApplication(application);
              }
            });
          }

        } else {
          updateApplication(application);
        }
      } else {
        cb({success: false, error: 'Invalid Application:' + data.appId})
      }
    })
  } else {
    cb({success: false, error: 'Invalid Application'})
  }
}

var genApplicationDoc = function(session, quotation, bundle, sections, template, cb){
  const lang = 'en';
  let quotationId = quotation.id;
  logger.log('INFO: genApplicationDoc - starts', quotationId);
  let exFormValue = _.get(bundle, 'formValues');
  let now = new Date().getTime();
  let application = {
    type: "application",
    // appStatus: 'APPLYING', // TODO: still store appStatus in application?
    quotationDocId: quotation._id,
    applicationForm:{
      values:{}
    },
    isSubmittedStatus: false,
    isValid: true,
    isFailSubmit: false,
    isApplicationSigned: false,
    isProposalSigned: false,
    isStartSignature: false,
    isFullySigned: false,
    isBackDate: false,
    isMandDocsAllUploaded: false,
    applicationStartedDate: moment().toISOString(),
    applicationSignedDate: 0,
    applicationSubmittedDate: 0,
    applicationInforceDate: 0,
    appStep: 0,
    sameAs: quotation.sameAs,
    lastUpdateDate: now,
    createDate: now,
    quotation: quotation,
    bundleId: bundle.id || bundle._id,
    compCode: session.agent.compCode,
    agentCode: session.agent.agentCode,
    dealerGroup: session.agent.channel.code
  };

  let channel = session.agent ? session.agent.channel.code : '';
  let oneyearpremium = quotation.premium;
  let payMode = quotation.paymentMode;
  switch (payMode) {
    case 'S':
      oneyearpremium = quotation.premium * 2
      break;
    case 'Q':
      oneyearpremium = quotation.premium * 4
      break;
    case 'M':
      oneyearpremium = quotation.premium * 12
      break;
    default:
      oneyearpremium = quotation.premium;
  }
  let extra = {
    isPhSameAsLa : (quotation.pCid == quotation.iCid) ? "Y" : "N",
    ccy:quotation.ccy,
    channel: commonApp.getChannelType(channel),
    channelName: channel,
    premium: oneyearpremium,
    baseProductCode: quotation.baseProductCode
  }
  let propHasTrustedIndividual = "";
  let laHasTrustedIndividual = [];

  let formValues = application.applicationForm.values;
  var phCid = "";
  var laCids = [];

  if(quotation.insured){
    for(let laIndex = 0; laIndex < quotation.insured.length; laIndex++){
      laCids.push(quotation.insured[laIndex]['cid'])
    }
  }else{
    laCids.push({cid:quotation.iCid});
  }

  if(quotation.proposer){
    phCid = quotation.proposer.cid;
  }else{
    phCid = quotation.pCid;
  }

  //todo: iCid would be totally different for multiple LA
  application.iCid = quotation.iCid;
  application.pCid = quotation.pCid;

  //set extra props for LA, such as channel, premium...
  let setExtraProp = function(fValues){
    logger.log('INFO: genApplicationDoc - setExtraProp', quotationId);

    fValues.proposer.extra = _.cloneDeep(extra);
    fValues.proposer.extra.hasTrustedIndividual = propHasTrustedIndividual;

    if(fValues.insured && fValues.insured.length > 0){
      for(let __laIndex = 0; __laIndex < fValues.insured.length; __laIndex++){
        fValues.insured[__laIndex].extra = _.cloneDeep(extra);

        //set relations
        let _la = fValues.insured[__laIndex];
        let dps = fValues.proposer.personalInfo.dependants;
        for(let dpIndex = 0; dpIndex < dps.length; dpIndex++){
          let dp = dps[dpIndex];
          if(dp.cid == _la.personalInfo.cid){
            //let rship = (dp.relationship == "OTH") ? dp.relationshipOther : dp.relationship;
            fValues.insured[__laIndex].personalInfo.relationship = dp.relationship;
            fValues.insured[__laIndex].personalInfo.relationshipOther = dp.relationshipOther;
          }
        }
      }
    }
  }

  // set up plans values
  let handlePlans = function() {
    logger.log("INFO: genApplicationDoc - handlePlans", quotationId);

    let transformPlanSumAssured = function(plans){
      let changedPlans = _.cloneDeep(plans);
      _.forEach(changedPlans, (plan)=>{
        if (plan.saViewInd !== "Y") {
          plan.sumInsured = " - ";
        }
        if (_.has(plan, 'multiplyBenefit') && plan.othSaInd !== 'Y') {
          plan.multiplyBenefit = ' - ';
        }
      });
      return changedPlans;
    }

    let planDetails = {
      planList: transformPlanSumAssured(quotation.plans),
      type: getPlanDetailType(quotation.baseProductCode),
      ccy: quotation.ccy,
      isBackDate: quotation.isBackDate,
      paymentMode:quotation.paymentMode,
      riskCommenDate:quotation.riskCommenDate,
      totYearPrem:quotation.premium,
      premium: quotation.premium,
      sumInsured: quotation.sumInsured,
      paymentTerm: quotation.paymentMode === 'L' ? 'Single' : 'Regular'
    }

    planDetails.displayPlanList = _.take(_.cloneDeep(planDetails.planList));
    planDetails.displayPlanList[0].totalPremium = planDetails.premium;
    planDetails.riderList = _.drop(_.cloneDeep(planDetails.planList));
    planDetails.hasRiders = _.isEmpty(planDetails.riderList) ? 'N' : 'Y';

    planDetails.insuranceCharge = _.get(quotation, 'policyOptionsDesc.coi[' + lang + ']');
    planDetails.rspSelect = _.get(quotation, 'policyOptions.rspSelect') || _.get(quotation, 'policyOptions.rspInd');

    if (quotation.plans[0].covClass) {
      planDetails.covClass = quotation.plans[0].covClass;
    }

    if (quotation.fund && quotation.fund.funds) {
      planDetails.fundList = quotation.fund.funds;
    }

    if (quotation.policyOptions) {
      planDetails = Object.assign(planDetails, quotation.policyOptions);
      if(quotation.policyOptionsDesc && quotation.policyOptionsDesc.multiFactor){
        planDetails.multiFactorDesc = quotation.policyOptionsDesc.multiFactor.en;
      }
    }

    var biName = null;
    var baseProductCode = quotation.baseProductCode;
    var plans = quotation.plans;
    for(let pIndex =0; pIndex<plans.length; pIndex++){
      let plan = plans[pIndex];
      if(plan.covCode == baseProductCode){
        biName = plan.covName;
      }
    }
    planDetails.planTypeName = biName;
    formValues.planDetails = planDetails;

    dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, function(mapping) {
      // set extra value such as channel
      formValues.appFormTemplate = mapping[quotation.baseProductCode].appFormTemplate;
      setExtraProp(formValues);
      application.applicationForm.values = formValues;

      // populateFromBundle(application, sections).then((app)=>{
        // REMOVE the code 'let app = application;' below if uncomment 'populateFromBundle'
        let app = application;
        commonApp.getOptionsList(app, template, function() {

          //Remove generated values which is not found in template
          let appTemplate = template.items[0].items[0]; //contain items of menuSection
          let appValuesOrg = app.applicationForm.values;
          let appValuesTemp = _.cloneDeep(app.applicationForm.values);
          let appValuesNew = _.cloneDeep(app.applicationForm.values);
          let trigger = {};

          //for create trigger object using template
          logger.log('INFO: genApplicationDoc - replaceAppFormValuesByTemplate start', quotationId);
          commonApp.replaceAppFormValuesByTemplate(appTemplate, appValuesOrg, appValuesTemp, trigger, [], 'en', null);
          logger.log('INFO: genApplicationDoc - replaceAppFormValuesByTemplate end', quotationId);

          //remove appForm values which is not found in template + trigger
          for (let key in appValuesOrg) {
            if (key == 'proposer') {
              logger.log('INFO: genApplicationDoc - removeAppFormValuesByTrigger: handle', key, quotationId);
              commonApp.removeAppFormValuesByTrigger(key, appValuesOrg[key], appValuesNew[key], appValuesOrg[key], trigger[key], false,appValuesOrg)
            } else if (key == 'insured') {
              for (let i = 0; i < appValuesOrg[key].length; i ++) {
                let iOrgValues = appValuesOrg[key][i];
                let iNewValues = appValuesNew[key][i];
                let iTrigger = trigger[key][i];
                logger.log('INFO: genApplicationDoc - removeAppFormValuesByTrigger: handle', key, i, quotationId);
                commonApp.removeAppFormValuesByTrigger(key, iOrgValues, iNewValues, iOrgValues, iTrigger, false,appValuesOrg);
              }
            }
          }

          app.applicationForm.values = appValuesNew;

          logger.log("INFO: genApplicationDoc - end", quotationId);
          cb(app);
        });
      // });
    })
  }

  let handlePhRop = function(ePolicies, exFormValue) {
    logger.log("INFO: genApplicationDoc - handlePhRop", quotationId);

    let policies = Object.assign({}, {
      existLife: CommonFunctions.toNumber(ePolicies.existLife),
      existTpd: CommonFunctions.toNumber(ePolicies.existTpd),
      existCi: CommonFunctions.toNumber(ePolicies.existCi),
      existPaAdb: CommonFunctions.toNumber(ePolicies.existPaAdb),
      existTotalPrem: CommonFunctions.toNumber(ePolicies.existTotalPrem),
      replaceLife: 0,
      replaceTpd: 0,
      replaceCi: 0,
      replacePaAdb: 0,
      replaceTotalPrem: 0,
      existLifeAXA: 0,
      existTpdAXA: 0,
      existCiAXA: 0,
      existPaAdbAXA: 0,
      existTotalPremAXA: 0,
      existLifeOther: 0,
      existTpdOther: 0,
      existCiOther: 0,
      existPaAdbOther: 0,
      existTotalPremOther: 0,
      replaceLifeAXA: 0,
      replaceTpdAXA: 0,
      replaceCiAXA: 0,
      replacePaAdbAXA: 0,
      replaceTotalPremAXA: 0,
      replaceLifeOther: 0,
      replaceTpdOther: 0,
      replaceCiOther: 0,
      replacePaAdbOther: 0,
      replaceTotalPremOther: 0,
      replacePolTypeAXA: '-',
      replacePolTypeOther: '-'
    })
    if(quotation.clientChoice && quotation.clientChoice.recommendation){
      let ccRop = quotation.clientChoice.recommendation.rop;
      policies.replaceLife = CommonFunctions.toNumber(ccRop.replaceLife);
      policies.replaceTpd = CommonFunctions.toNumber(ccRop.replaceTpd);
      policies.replaceCi = CommonFunctions.toNumber(ccRop.replaceCi);
      policies.replacePaAdb = CommonFunctions.toNumber(ccRop.replacePaAdb);
      policies.replaceTotalPrem = CommonFunctions.toNumber(ccRop.replaceTotalPrem);
    }

    if (extra.channel == "FA"){
      policies.havExtPlans = "";
      policies.havAccPlans = "";
      policies.havPndinApp = "";
      policies.isProslReplace = "";
    } else {
      //set up in fe
      policies.havExtPlans = (policies.existLife > 0 || policies.existTpd > 0 || policies.existCi > 0) ? "Y" : "N";
      policies.havAccPlans = policies.existPaAdb > 0 ? "Y" : "N";

      //only display for LA = PH
      if (extra.isPhSameAsLa) {
        policies.isProslReplace = (policies.replaceLife > 0 || policies.replaceTpd > 0 || policies.replaceCi > 0 || policies.replacePaAdb > 0 || policies.replaceTotalPrem > 0) ? "Y" : "N";
      } else {
        policies.isProslReplace = "";
      }
    }

    //retrieve from bundle
    if (exFormValue && exFormValue.policies) {
      logger.log("INFO: genApplicationDoc - pop pending policies from bundle", quotationId);
      policies.havPndinApp = exFormValue.policies.havPndinApp? exFormValue.policies.havPndinApp: "";
      policies.pendingLife = CommonFunctions.toNumber(exFormValue.policies.pendingLife);
      policies.pendingTpd = CommonFunctions.toNumber(exFormValue.policies.pendingTpd);
      policies.pendingCi = CommonFunctions.toNumber(exFormValue.policies.pendingCi);
      policies.pendingPaAdb = CommonFunctions.toNumber(exFormValue.policies.pendingPaAdb);
      policies.pendingTotalPrem = CommonFunctions.toNumber(exFormValue.policies.pendingTotalPrem);
    }

    formValues.proposer.policies = policies;
    handlePlans();
  }

  let handleLaRop = function(sLA, ePolicies) {
    logger.log("INFO: genApplicationDoc - handleLaRop", quotationId);

    //avoid string format
    let policies = Object.assign({}, {
      existLife: CommonFunctions.toNumber(ePolicies.existLife),
      existTpd: CommonFunctions.toNumber(ePolicies.existTpd),
      existCi: CommonFunctions.toNumber(ePolicies.existCi),
      existPaAdb: CommonFunctions.toNumber(ePolicies.existPaAdb),
      existTotalPrem: CommonFunctions.toNumber(ePolicies.existTotalPrem),
      replaceLife: 0,
      replaceTpd: 0,
      replaceCi: 0,
      replacePaAdb: 0,
      replaceTotalPrem: 0,
      existLifeAXA: 0,
      existTpdAXA: 0,
      existCiAXA: 0,
      existPaAdbAXA: 0,
      existTotalPremAXA: 0,
      existLifeOther: 0,
      existTpdOther: 0,
      existCiOther: 0,
      existPaAdbOther: 0,
      existTotalPremOther: 0,
      replaceLifeAXA: 0,
      replaceTpdAXA: 0,
      replaceCiAXA: 0,
      replacePaAdbAXA: 0,
      replaceTotalPremAXA: 0,
      replaceLifeOther: 0,
      replaceTpdOther: 0,
      replaceCiOther: 0,
      replacePaAdbOther: 0,
      replaceTotalPremOther: 0,
      replacePolTypeAXA: '-',
      replacePolTypeOther: '-'
    })
    if(quotation.clientChoice && quotation.clientChoice.recommendation){
      let ccRop = quotation.clientChoice.recommendation.rop;
      policies.replaceLife = CommonFunctions.toNumber(ccRop.replaceLife);
      policies.replaceTpd = CommonFunctions.toNumber(ccRop.replaceTpd);
      policies.replaceCi = CommonFunctions.toNumber(ccRop.replaceCi);
      policies.replacePaAdb = CommonFunctions.toNumber(ccRop.replacePaAdb);
      policies.replaceTotalPrem = CommonFunctions.toNumber(ccRop.replaceTotalPrem);
    }

    if(extra.channel == "FA"){
      policies.havExtPlans = "";
      policies.havAccPlans = "";
      policies.havPndinApp = "";
      policies.isProslReplace = "";
    } else {
      policies.havExtPlans = (policies.existLife > 0 || policies.existTpd > 0 || policies.existCi > 0) ? "Y" : "N";
      policies.havAccPlans = policies.existPaAdb > 0 ? "Y" : "N";
      policies.isProslReplace = (policies.replaceLife > 0 || policies.replaceTpd > 0 || policies.replaceCi > 0 || policies.replacePaAdb > 0 || policies.replaceTotalPrem > 0) ? "Y" : "N";
    }
    sLA.policies = policies;
  }

  //la
  let newLA = [];

  //get previous ph values or default one
  let getPHProfile = function(){
    logger.log("INFO: genApplicationDoc - getPHProfile", quotationId);

    formValues.insured = newLA;
    clientDao.getClientById(phCid, function(phDoc) {
      let hasTrustedIndividual = 'N';
      dao.getDoc(bundle.fna[nDao.ITEM_ID.PDA], (pdaDoc) => {
        if (pdaDoc && !pdaDoc.error) {
          logger.log('INFO: genApplicationDoc - getPHProfile - populate PDA data', quotationId);
          hasTrustedIndividual = _.get(pdaDoc, 'trustedIndividual', 'N');
        }

        commonApp.removeInvalidProfileProp(phDoc);
        phDoc.cid = phDoc._id || phDoc.id;
        //push personal info
        let newPH = {
          personalInfo: phDoc,
          declaration: {
            trustedIndividuals: (hasTrustedIndividual == "Y" ? phDoc.trustedIndividuals : {}),
            ccy: quotation.ccy
          },
          residency: {},
          extra: extra,
          insurability: {
            "LIFESTYLE01": phDoc.isSmoker
          }
        };
        propHasTrustedIndividual = hasTrustedIndividual;
        formValues.proposer = newPH;

        //policies
        clientChoiceHandler.getExistingPolicies(phCid, phCid).then((ePolicies) => {
          handlePhRop(ePolicies);
        }).catch((error) => {
          logger.error('ERROR: getExistingPolicies', error);
          cb(null)
        });
      });
    });
  }

  //multiple LA
  let getLAProfile = function(laIndex) {
    logger.log("INFO: genApplicationDoc - getLAProfile", quotationId, 'laIndex', laIndex);

    if(laIndex < laCids.length) {
      let cid = laCids[laIndex].cid;
      clientDao.getClientById(cid, function(laDoc) {
        if (laDoc && !laDoc.error) {
          clientChoiceHandler.getExistingPolicies(phCid, cid).then((ePolicies) => {
            commonApp.removeInvalidProfileProp(laDoc);
            laDoc.cid = laDoc._id;

            let sLA = {
              personalInfo: laDoc,
              declaration: {},
              residency: {},
              extra: extra,
              insurability: {
                "LIFESTYLE01": laDoc.isSmoker
              }
            };
            handleLaRop(sLA, ePolicies);
            newLA.push(sLA);

            getLAProfile(laIndex + 1);
          }).catch((error) => {
            logger.error('ERROR: getExistingPolicies', quotationId, error);
          });
        } else {
          logger.error('ERROR: genApplicationDoc - getLAProfile', quotationId, _.get(laDoc, 'error'));
          cb(null);
        }
      })
    } else{
      getPHProfile();
    }
  }


  //check if it has only 1 person for the app: ph = la
  //todo: check if PH is one of LA in batch 2
  //laCids.length == 1 && laCids[0].cid == phCid
  if(quotation.sameAs == 'Y'){
    getPHProfile();
  } else {
    getLAProfile(0);
  }
};

var getRunningSeqNo = function(agentCode, cb){
  let docId = "RUN_SEQ_" +  agentCode;
  let seq = -1;

  let update = function(result){
    if(result && !result.error){
      cb(seq)
    }else{
      cb(seq)
    }
  }

  let afterGet = function(result){
    if(result){
      delete result.error
      delete  result.reason
      seq = result.seq || 0;
      result.seq = seq + 1;
      dao.updDoc(docId, result, update);
    }else{
      seq = 1;
      let newDoc = {
        seq: 1
      }
      dao.updDoc(docId, newDoc, update);
    }
  }

  dao.getDoc(docId, afterGet)
}

module.exports.getRunningSeqNo = getRunningSeqNo;

//NO-USD
// var getDynFormId = function(planCode, planCat){
//   return "appform_" + planCode + "_DYN_" + planCat;
// }

// var getAppFormSections = function(sections){
//  let signSection = {
//    "id": "stepSign",
//    "type": "section",
//    "title": "Signature",
//    "detailSeq": 2,
//    "items": [
//      {
//        "id": "sign",
//        "type": "signature"
//      }
//    ]
//  }

//  let paymentSection = {
//    "id": "stepPay",
//    "type": "section",
//    "title": "Payment",
//    "detailSeq": 3,
//    "items": [
//      {
//        "id": "pay",
//        "type": "payment"
//      }
//    ]
//  }

//  let submitSection = {
//    "id": "stepSubmit",
//    "type": "section",
//    "title": "Submit",
//    "detailSeq": 4,
//    "items": [
//      {
//        "id": "submit",
//        "type": "submission"
//      }
//    ]
//  }

//  sections.push(signSection);
//  sections.push(paymentSection);
//  sections.push(submitSection);
// }

//Function: for retrieving values in Payment Page
const getQuotationPremiumDetails = function(docId, cb) {
  logger.log('INFO: getQuotationPremiumDetails - start', docId);

  var values = null;
  var initBasicPrem = 0;
  var backdatingPrem = 0;
  var topupPrem = 0;
  var initRspPrem = 0;
  var initTotalPrem = 0;
  var quot = null;
  var backdatedDay = 0;

  var signedDate = null;
  var backdate = null;

  applicationDao.getApplication(docId, function(app) {
    if (app._id) {
      quot = app.quotation;
      let retValues = function() {
        logger.log('INFO: getQuotationPremiumDetails - end', docId);
        cb({
          values:values,
          paymentMethod: (quot.policyOptions && quot.policyOptions.paymentMethod)?quot.policyOptions.paymentMethod:"",
          covCode: quot.baseProductCode,
          paymentMode: quot.paymentMode,
          isMandDocsAllUploaded: app.isMandDocsAllUploaded,
          isSubmitted: app.isSubmittedStatus,
          notPaybySGD: (quot.ccy !== 'SGD') ? true : false
        });
      }

      let wrapUp = function() {
        applicationDao.upsertApplication(app._id, app, (upApp) => {
          if (upApp && !upApp.error) {
            retValues();
          } else {
            logger.error('ERROR: getQuotationPremiumDetails - end [RETURN=-3]', _.get(upApp, 'error'));
            cb({success:false, result:upApp});
          }
        })
      }

      //Retrieve stored values
      if (app.payment) {
        values = app.payment;
        if (app.payment.trxNo && ['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(app.payment.initPayMethod) >= 0) {
          let status = app.payment.trxStatus;
          let trxStatusRemark = '';
          if (status == 'I') {
            trxStatusRemark = 'O'
          } else {
            trxStatusRemark = status
          }
          if (trxStatusRemark != app.payment.trxStatusRemark) {
            app.payment.trxStatusRemark = trxStatusRemark;
            values = app.payment;
            wrapUp();
          } else {
            retValues();
          }
        } else {
          retValues();
        }
      } else {
        values = {
          proposalNo: '',
          policyCcy: '',
          initBasicPrem: 0,
          backdatingPrem: 0,
          topupPrem: 0,
          initRspPrem: 0,
          initTotalPrem: 0,
          initPayMethod: '',
          initPayMethodButton: false,
          ttRemittingBank: '',
          ttDOR: '',
          ttRemarks: '',
          subseqPayMethod: '',
          paymentMode:'',
          rspSelect: false,
          covCode:'',
          paymentMethod:'',
          cpfisoaBlock: {
            idCardNo: ''
          },
          cpfissaBlock: {
            idCardNo: ''
          },
          srsBlock: {
            idCardNo: ''
          }
        };

        clientDao.getProfileById(quot.pCid, function(pProfile) {
          if (pProfile && !pProfile.error) {
            values.initPayMethodButton = app.hasOwnProperty('isInitialPaymentCompleted')? app.isInitialPaymentCompleted: false;
            values.proposalNo = app.policyNumber || app._id;
            values.policyCcy = quot.ccy;
            values.baseProductCode = quot.baseProductCode;
            values.paymentMode = quot.paymentMode;
            values.rspSelect = _.get(quot, 'policyOptions.rspSelect') || _.get(quot, 'policyOptions.rspInd') ? 'Y' : 'N' ;
            values.covCode = quot.baseProductCode;
            values.paymentMethod = (quot.policyOptions && quot.policyOptions.paymentMethod)?quot.policyOptions.paymentMethod:"";
            values.cpfisoaBlock.idCardNo = pProfile.idCardNo;
            values.cpfissaBlock.idCardNo = pProfile.idCardNo;
            values.srsBlock.idCardNo = pProfile.idCardNo;
            // if payment mode === "Single Premium"
            if (values.paymentMode === "L" || app.quotation.ccy !== "SGD") {
              values.subseqPayMethod = undefined;
            } else {
              values.subseqPayMethod = quot.paymentMode === "M" ? "Y" : "N";
            }

            // Calculate All Premium
            initBasicPrem = quot.premium * (quot.paymentMode === 'M'? 2: 1);

            //- Backdating Premium
            if (quot.isBackDate == 'Y' || app.isBackDate) {
              signedDate = new Date(app.applicationSignedDate.substring(0, 10) + 'T00:00:00.000Z');
              backdate = new Date(quot.riskCommenDate);
              backdatedDay = Math.ceil((signedDate.getTime() - backdate.getTime()) / (1000 * 3600 * 24));
              var backdatedMonth = 0;
              if (backdatedDay == 0) {
                backdatedMonth = 0;
              } else if (backdatedDay > 0 && backdatedDay <= 30) {
                backdatedMonth = 1;
              } else if (backdatedDay > 30 && backdatedDay <= 61) {
                backdatedMonth = 2;
              } else if (backdatedDay > 61 && backdatedDay <= 91) {
                backdatedMonth = 3;
              } else if (backdatedDay > 91 && backdatedDay <= 122) {
                backdatedMonth = 4;
              } else if (backdatedDay > 122 && backdatedDay <= 152) {
                backdatedMonth = 5;
              } else if (backdatedDay > 152) {
                backdatedMonth = 6;
              }

              backdatingPrem = 0;
              if (quot.paymentMode == 'M') {
                backdatingPrem = quot.premium * backdatedMonth;
              } else if (quot.paymentMode == 'Q') {
                if (backdatedMonth > 1 && backdatedMonth <= 4) {
                  backdatingPrem = quot.premium;
                } else if (backdatedMonth > 4 && backdatedMonth <= 6) {
                  backdatingPrem = quot.premium * 2;
                }
              } else if (quot.paymentMode == 'S') {
                backdatingPrem = quot.premium * (backdatedMonth > 4 && backdatedMonth <= 6? 1: 0);
              }
            }

            // Calculate RSP Premium
            initRspPrem = 0;
            let rspAmount = _.get(quot, 'policyOptions.rspAmount') || _.get(quot, 'policyOptions.rspAmt', 0)
            if (rspAmount > 0) {
              // rspPayFreq is quotation field from SAV, IND, SHIELD. rspFrequency is quotation field from FLEXI
              const rspPayFreq = _.get(quot, 'policyOptions.rspPayFreq');

              // Init RSP Premium should be two times of RSPAmount for monthly mode, and equals to RSPAmount for Annaul/Semi-Annual/Quarter modes
              if (rspPayFreq === "monthly" || rspPayFreq === "M") {
                initRspPrem = rspAmount * 2;
              } else {
                initRspPrem = rspAmount;
              }

            }

            //TODO: Update the folloiwng premium logic
            topupPrem = _.get(quot, 'policyOptions.topUpAmt', 0);

            initTotalPrem = initBasicPrem + backdatingPrem + topupPrem + initRspPrem;

            values.initBasicPrem = initBasicPrem;
            if (backdatingPrem > 0) {
              values.backdatingPrem = backdatingPrem;
            }
            if (topupPrem > 0) {
              values.topupPrem = topupPrem;
            }
            if (initRspPrem > 0) {
              values.initRspPrem = initRspPrem;
            }
            values.initTotalPrem = initTotalPrem;

            app.payment = _.cloneDeep(values);
            wrapUp()
          } else {
            logger.error('ERROR: getQuotationPremiumDetails - end [RETURN=-2]', docId, quot.pCid);
            cb(false);
          }
        });
      }
    } else {
      logger.error('ERROR: getQuotationPremiumDetails - end [RETURN=-1]', docId);
      cb(false);
    }
  })
};

const setupPaymentOptions = function(paymentTemplate, payOptionsTmpl, premiumValues, isFaChannel) {

  const payMethod = paymentTemplate.items[1];
  const payMethod12Box = payMethod.items[0];
  let payOptions = payMethod12Box.items[0].options;
  let subseqPayCheckbox = payMethod.items[payMethod.items.length - 1].items[0];

  let getOptionsAvailable = function(payOptionsTmpl, covCode, paymentMethod, paymentMode, notPaybySGD) {
    let foundObj = {};
    let optionsByCovCode = _.filter(payOptionsTmpl.items, (item) => {
      return item.covCode === covCode;
    });

    _.forEach(optionsByCovCode, (item) => {
      if (notPaybySGD) {
        if (item.notPaybySGD) {
          foundObj = item;
          return false;
        }
      } else {
        if (item.paymentMethod === paymentMethod && _.includes(item.paymentMode, paymentMode)) {
          foundObj = item;
          return false;
        }
      }
    });

    return foundObj.payOptions || [];
  };

  let optionsAvailable = getOptionsAvailable(payOptionsTmpl, premiumValues.covCode, premiumValues.paymentMethod, premiumValues.paymentMode, premiumValues.notPaybySGD) || [];

  if (isFaChannel) {
    optionsAvailable.push("payLater");
  }

  // If Initial Payment includes topup, then dont display credit card and dbs creadit card
  // If it is single premium mode, then dont display credit card and dbs creadit card
  if (_.get(premiumValues, 'values.topupPrem') > 0) {
    _.remove(optionsAvailable, (item) => {
      return item === 'crCard' || item === 'dbsCrCardIpp';
    });
  } else if (premiumValues.paymentMode === 'L') {
    _.remove(optionsAvailable, (item) => {
      return item === 'crCard';
    });
  }

  if (_.get(premiumValues, 'values.initTotalPrem') >= 5000) {
    _.remove(optionsAvailable, (item) => {
      return item === 'cash';
    });
  }

  payOptions = _.filter(payOptions, (item) => {
    return _.includes(optionsAvailable, item.value);
  });

  _.set(paymentTemplate, 'items[1].items[0].items[0].options', payOptions);

  //Set default Subsequent Payment Method for monthly
  if (_.get(premiumValues, 'values.paymentMode') === "M") {
    subseqPayCheckbox.value = "Y";
    subseqPayCheckbox["disabled"] = true;
  }
};

const _getPaymentTemplateValues = function(data, session, cb) {
  const payOptionsId = 'payOptions';
  const docId = data.docId;
  const isFaChannel = session.agent.channel.type === 'FA';

  var template = {};
  var values = {};

  logger.log('INFO: getPaymentTemplateValues - start', docId);

  let callBackFalse = function (number) {
    logger.error('ERROR: getPaymentTemplateValues - end [RETURN=-' + number + ']', docId);
    cb(false);
  };

  dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.PAYMENT, function(template) {
    if (template && !template.error) {
      let retTemplate = _.cloneDeep(template);

      getQuotationPremiumDetails(docId, function(premiumValues) {
        if (premiumValues) {
          dao.getDoc(payOptionsId, function(payOptionsTmpl) {
            if (payOptionsTmpl && !payOptionsTmpl.error) {

              setupPaymentOptions(retTemplate, payOptionsTmpl, premiumValues, isFaChannel);

              values = _.cloneDeep(premiumValues.values);
              if (premiumValues.isSubmitted) {
                logger.log('INFO: getPaymentTemplateValues - frozenTemplate', docId);
                if (session.platform) {
                  commonApp.frozenTemplateMobile(retTemplate);
                } else {
                  commonApp.frozenTemplate(retTemplate);
                }
              }

              logger.log('INFO: getPaymentTemplateValues - end [RETURN=1]', docId);
              cb({
                success:true,
                template:retTemplate,
                values:values,
                isMandDocsAllUploaded: premiumValues.isSubmitted || premiumValues.isMandDocsAllUploaded,
                isSubmitted: premiumValues.isSubmitted
              });
            } else {
              callBackFalse(3);
            }
          });
        } else {
          callBackFalse(2);
        }
      });
    } else {
      callBackFalse(1);
    }
  })
};
module.exports.getPaymentTemplateValues = _getPaymentTemplateValues;

const _getTransactionNoForShield = function(appId) {
  let raw = appId.substr(2, 12) + '-' + parseInt(new Date().getTime()/1000);
  let strs = raw.split('-');

  let trxNo = "";

  for (let i in strs) {
    let str = strs[i];
    var n = Math.floor(Number(str));
    if (String(n) === str && n >= 0) {
      trxNo += n.toString(16);
    } else {
      trxNo += n;
    }
  }
  return trxNo;
}

let _getTransactionNo = function(policyNo) {
  let raw = policyNo + '-' + parseInt(new Date().getTime()/1000);
  let strs = raw.split('-');

  let trxNo = "";

  for (let i in strs) {
    let str = strs[i];
    var n = Math.floor(Number(str));
    if (String(n) === str && n >= 0) {
      trxNo += n.toString(16);
    } else {
      trxNo += n;
    }
  }
  return trxNo;
}

const _getPolicyNumberByMapping = (iCidMapping) => {
  let policyNumbers = [];
  _.each(iCidMapping, cidObj => {
    _.each(cidObj, obj => {
      if (obj.policyNumber) {
        policyNumbers.push(obj.policyNumber);
      }
    });
  });
  return policyNumbers;
}

module.exports.getPaymentUrl = function(data, session, cb) {
  let { docId, authToken, webServiceUrl } = data;
  logger.log('INFO: getPaymentUrl - start', docId);
  _updatePaymentMethods(data, session, (app) => {

    if (app && !app.error && app._id) {
      const isShield = _.get(app, 'quotation.quotType') === 'SHIELD';
      var trxNo = (isShield) ?  _getTransactionNoForShield(app.id) : _getTransactionNo(app.policyNumber);
      var amount = (isShield) ? app.payment.totCashPortion : app.payment.initTotalPrem;
      var quotationCcy;
      if (isShield) {
        _.each(_.get(app, 'quotation.insureds'), obj =>{
          quotationCcy = obj.ccy;
          return false;
        })
      }
      var currency = app.payment.policyCcy || _.get(app, 'applicationForm.values.insured[0].extra.ccy') || _.get(app, 'applicationForm.values.proposer.extra.ccy') || quotationCcy;

      axiosWrapper({
        url: "/payment/url",
        data: {
          trxNo: trxNo,
          appId: app._id,
          policyNo: (isShield) ? app.id : app.policyNumber,
          amount: amount,
          currency: currency,
          method: data.values.initPayMethod,
          isShield: isShield,
          lob: (isShield) ? 'Health' : 'Life',
          relatedPolicyNumber: (isShield) ? _getPolicyNumberByMapping(_.get(app, 'iCidMapping')) : [],
          relatedApplicationNumber: (isShield) ? _.get(app, 'childIds') : []
        },
        method: "POST",
        headers: {
          Authorization: `Bearer ${authToken}`
        },
        webServiceUrl
      })
      .then(({data:{result}})=>{
        if (result && !result.error) {
          let url = result.url;

          let params = result.params;
          let paramStr = "";
          for (var p in params) {
            if (p) {
              paramStr += (paramStr?"&":"?") + (p+"="+params[p])
            }
          }

          app.payment.trxNo = trxNo;
          app.payment.trxStatus = result.status;
          app.payment.trxStartTime = result.timestamp;
          app.payment.trxMethod = app.payment.initPayMethod = data.values.initPayMethod;
          app.payment.trxStatusRemark = result.status
          app.payment.trxEnquiryStatus = "No"

          applicationDao.upsertApplication(app._id, app, (upApp) => {
            if (upApp) {
              logger.log('INFO: getPaymentUrl - end [RETURN=1]', docId);
              if (isShield) {
                payment.saveParentPaymenttoChild(app, (error, parentApplication) => {
                  cb({
                    success: true,
                    url: url + paramStr,
                    application: app
                  })
                })
              } else {
                cb({
                  success: true,
                  url: url + paramStr,
                  application: app
                })
              }

            } else {
              logger.error('ERROR: getPaymentUrl - end [RETURN=-2]', docId, _.get(upApp, 'error'));
              cb({
                success: false,
                error: "field to upsert application"
              })
            }
          })
        } else {
          logger.error('ERROR: getPaymentUrl - end [RETURN=-1]', docId, _.get(result, 'error'));
          cb({
            success: false,
            error: "field to generate url"
          })
        }
      })
      .catch(e=> {
        console.log(e);
        cb({
          success: false,
          error: e
        })
      });
    }
  })
}

module.exports.checkPaymentStatus = function(data, session, cb) {
  let trxNo = data.trxNo;
  let appId = data.appId;

  logger.log('INFO: checkPaymentStatus - start', appId);
  applicationDao.getApplication(appId, function(app) {
    if (app && !app.error && app.payment) {
      let status = app.payment.trxStatus;
      let trxStatusRemark = '';
      if (status == 'I') {
        trxStatusRemark = 'O';
      } else {
        trxStatusRemark = status;
      }
      if (trxStatusRemark != app.payment.trxStatusRemark) {
        app.payment.trxStatusRemark = trxStatusRemark;
        applicationDao.upsertApplication(appId, app, (upsertResult) => {
          if (upsertResult && !upsertResult.error) {
            logger.log('INFO: checkPaymentStatus - end [RETURN=1]', appId);
            cb({
              success: true,
              paymentValues: app.payment
            })
          } else {
            logger.error('ERROR: checkPaymentStatus - end [RETURN=-2]', appId, _.get(upsertResult, 'error'));
            cb({
              success: false
            })
          }
        })
      } else {
        logger.log('INFO: checkPaymentStatus - end [RETURN=2]', appId);
        cb({
          success: true,
          paymentValues: app.payment
        })
      }
    } else {
      logger.error('ERROR: checkPaymentStatus - end [RETURN=-1]', appId, _.get(app, 'error'));
      cb({
        success: false
      })
    }
  })
}

var getSubmissionTemplate = function(app, cb) {
  logger.log('INFO: getSubmissionTemplate');
  dao.getDoc(commonApp.TEMPLATE_NAME.SUBMISSION, function(res){
    if (res && !res.error) {
      cb(res);
    } else {
      cb(false);
    }
  });
}


var getSubmissionValues = function(app, session, submitStatus, failMsg, cb) {
  logger.log('INFO: getSubmissionValues');
  var quot = null;
  var values = {
    agentChannelType: (session.agent.channel.type == 'FA'? 'Y': 'N'),
    submitStatus: submitStatus,
    receiveEmailFa: (app.submission && app.submission.receiveEmailFa? app.submission.receiveEmailFa: ""),
    mandDocsAllUploaded: submitStatus == 'SUCCESS' ? 'Y' : (app.isMandDocsAllUploaded? 'Y': 'N')
  };
  let covName;

  quot = app.quotation;
  if (_.get(quot, 'quotType') === 'SHIELD') {
    covName = _.get(app, 'quotation.covName');
  } else {
    var basicPlan = quot.plans.find(function(plan) {
      return plan.covCode == quot.baseProductCode;
    });
    covName = _.get(basicPlan, 'covName');
  }

  if (submitStatus == 'FAIL') {
    values.failMsg = failMsg;
    values.lastSubmitDateTime = CommonFunctions.getFormattedDateTime(new Date());
  } else if (submitStatus == 'SUCCESS') {
    values.eSubmitDateTime = CommonFunctions.getFormattedDateTime(app.applicationSubmittedDate);
    values.proposerName = quot.pFullName;
    values.planName = covName;
    values.policyNumber = app.policyNumber?app.policyNumber:"";
  }
  cb({values:values});
}

var appFormSubmission = function(data, session, cb) {
  logger.log('INFO: appFormSubmission', data.docId);
  var docId = data.docId;
  var template = {};
  var values = data.values;
  let agentCode = session.agentCode;
  let agentProfile = data.agentProfile;

  let errorCB = function(app, msg) {
    logger.error('ERROR: appFormSubmission - end [RETURN=-2]', docId, msg);
    getSubmissionValues(app, session, 'FAIL', 'Submission Fail. Please try again later', (res) => {
      cb({
        success: false,
        values: res.values,
        isMandDocsAllUploaded: true,
        isSubmitted: false
      });
    })
  };

  let updateViewedList = function(app){
    if (_.get(app, 'payment.initialPayment.paymentMethod') && _.get(_.get(app, 'supportDocuments.viewedList'), agentCode)) {
      app.supportDocuments.viewedList[agentCode][app.payment.initialPayment.paymentMethod] = false;
    }
  }

  //Step 1
  let genPdfBeforeSubmission = function(app, lang, callback) {
    logger.log('INFO: appFormSubmission - (1) genPdfBeforeSubmission', docId);
    genAppFormPdfByPaymentData(app, lang, function(newApp) {
      if (newApp && !newApp.error) {
        getECpdPdfByAppFormData(newApp, lang, function (newApp2) {
          if (newApp2 && !newApp2.error) {
            callback(newApp2);
          } else {
            callback(false);
          }
        })
      } else {
        callback(false);
      }
    });
  }

  //Step 2
  let doSubmission = function(app, callback) {
    logger.log('INFO: appFormSubmission - (2) doSubmission', docId);

    //Prepare Submission Email
    applicationDao.getSubmissionEmailTemplate(session.agent.channel.type == 'FA', (result) => {
      if (result && !result.error) {
        // Check whether need to send email
        let now = Date.now();
        let filteredTemplate = _.filter(result.eSubmissionEmails, function (template) {
          return template.productCode.indexOf(app.quotation.baseProductCode) != -1;
        });

        if (filteredTemplate && filteredTemplate.length != 0) {
          logger.log('INFO: appFormSubmission - (2) doSubmission filteredTemplate', app.quotation.baseProductCode, session.agent.channel.type, docId);
          let emailValues = Object.assign({}, data);
          let { attachmentKeys } = filteredTemplate[0];
          let eCpdIndex = attachmentKeys.indexOf('eCpd');
          if (['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(_.get(app, 'payment.initPayMethod')) < 0 && eCpdIndex > -1) {
            attachmentKeys.splice(eCpdIndex, 1);
          }
          emailValues['agentName'] = _.get(data, 'agentProfile.name');
          emailValues['agentContactNum'] = _.get(data, 'agentProfile.mobile');
          emailValues['agentEmail'] = _.get(data, 'agentProfile.email');
          emailValues['agentTitle'] = '';
          emailValues['clientName'] = _.get(data, 'clientProfile.fullName');
          emailValues['clientEmail'] = _.get(data, 'clientProfile.email');
          emailValues['clientId'] = _.get(data, 'clientProfile.cid');
          emailValues['clientTitle'] = _.get(data, 'clientProfile.title');
          emailValues["emailContent"] = filteredTemplate[0].emailContent;
          emailValues["attKeys"] = attachmentKeys;
          emailValues["policyNumber"] = app.policyNumber ? app.policyNumber : "";
          emailValues["submissionDate"] = CommonFunctions.getFormattedDate(now);
          emailValues["quotId"] = app.quotation.id;
          emailValues["appId"] = app._id;
          emailValues["receiveEmailFa"] = session.agent.channel.type == 'FA'? (values.receiveEmailFa? values.receiveEmailFa: "N"): "Y";

          //Create Approval Case and update applciation status in bundle
          ApprovalHandler.createApprovalCase({ids: [app._id]}, session, (eAppResult) =>{
            dao.updateViewIndex("main", "applicationsByAgent");
            dao.updateViewIndex("main", "approvalDetails");
            bDao.onSubmitApplication(app.pCid, app._id).then((result) => {
              if (result.ok && eAppResult.success){
                //Send email
                aEmaillHandler.SubmittedCaseNotification({id: app.policyNumber}, session);
                commonApp.prepareSubmissionEmails(emailValues, session, (resp) => {
                  if (resp && resp.success) {
                      app.isSubmittedStatus = true;
                      // app.appStatus = 'SUBMITTED'; // TODO: still store appStatus inside application?
                      app.applicationSubmittedDate = new Date().toISOString();
                      if (session.agent.channel.type == 'FA') {
                        app.submission = {};
                        app.submission.receiveEmailFa = values.receiveEmailFa;
                      }
                      if (_.get(session, 'agent.agentCode') !== _.get(session, 'agent.managerCode')) {
                        commonApp.transformSuppDocsPolicyFormValues(app);
                      }
                      updateViewedList(app);
                      applicationDao.upsertApplication(app._id, app, (res)=>{
                        if (res && !res.error) {
                          callback(res);
                        } else {
                          callback(new Exception.UpdateDocException('Fail to update status'));
                        }
                      });
                  } else {
                    app.isFailSubmit = false;
                    applicationDao.upsertApplication(app._id, app, (res)=>{
                      if (res && !res.error) {
                        callback(res);
                      } else {
                        callback(new Exception.EmailException('Fail to send email'));
                        //errorCB(false, 'Fail to send email');
                      }
                    });
                  }
                });
              } else {
                callback(new Exception.CreateDocException('Fail to create/ update Bundle'));
              }
            }).catch(Err =>{
              logger.error('ERROR: appFormSubmission', Err);
              callback(new Exception.CreateDocException('Fail to create/ update Bundle'));
            })
          })
        } else {
          callback(new Exception.GetTemplateException('Fail to get email template from product'));
          //errorCB(false, 'Fail to get email template from product');
        }
      } else {
        callback(new Exception.GetTemplateException('Fail to get email template'));
      }
    });
  }

  //Step 3
  let prepareSubmissionTemplateValues = function(app, callback) {
    logger.log('INFO: appFormSubmission - (3) prepareSubmissionTemplateValues');
    getSubmissionTemplate(app, function(tmpl) {
      if (tmpl) {
        template = _.cloneDeep(tmpl);
        if (session.agent.channel.type == 'FA') {
          if (session.platform) {
            commonApp.frozenTemplateMobile(template);
          } else {
            commonApp.frozenTemplate(template);
          }
        }
        getSubmissionValues(app, session, 'SUCCESS', '', function (resValues) {
          if (resValues) {
            callback({
              template: template,
              values: resValues.values
            })
          } else {
            callback(new Exception.EmailException('Fail to get submission values to display'));
            //errorCB(resValues)
          }
        });
      } else {
        callback(new Exception.GetTemplateException('Fail to get template'));
        //errorCB(tmpl)
      }
    })
  }

  //Starting function call
  applicationDao.getApplication(docId, function(app) {
    let backupApp = _.cloneDeep(app);
    logger.log('INFO: appFormSubmission - getApplication', docId);
    if (app._id && !app.error) {
      let lang = 'en';
      try {
        if (agentProfile.role != '04' && !agentProfile.managerCode) {
          //No supervisor is found
          getSubmissionTemplate(app, function(tmpl) {
            if (tmpl) {
              getSubmissionValues(app, session, 'FAIL', 'Supervisor detail not available. Case cannot be submitted.', function (resValues) {
                if (resValues) {
                  logger.log('INFO: appFormSubmission - end [RETURN=-1]', docId);
                  cb({
                    success: false,
                    template: _.cloneDeep(tmpl),
                    values: resValues.values,
                    isMandDocsAllUploaded: true,
                    isSubmitted: false
                  });
                } else {
                  handleRollBackAppEapp(new Exception.EmailException('Fail to get submission values to display'), backupApp, errorCB);
                }
              });
            } else {
              handleRollBackAppEapp(new Exception.GetTemplateException('Fail to get template'), backupApp, errorCB);
            }
          })
        } else {
          genPdfBeforeSubmission(app, lang, function (res) {
            if (res && !res.error) {
              doSubmission(app, function(submitRes) {
                if(submitRes instanceof Error) {
                  handleRollBackAppEapp(submitRes, backupApp, errorCB);
                } else if (submitRes) {
                  // Case: Successfully go to Submission
                  prepareSubmissionTemplateValues(app, function(res) {
                    if (res instanceof Error) {
                      handleRollBackAppEapp(res, backupApp, errorCB);
                    } else {
                      bDao.getCurrentBundle(app.pCid).then((bundle) => {
                        // bDao.updateStatus(app.pCid, bDao.BUNDLE_STATUS.SUBMIT_APP, function(newBundle) {
                        bDao.updateStatus(app.pCid, bDao.BUNDLE_STATUS.SUBMIT_APP).then((newBundle)=>{
                          logger.log('INFO: appFormSubmission - end [RETURN=1]', docId);
                          cb({
                            success: true,
                            template: res.template,
                            values: res.values,
                            isMandDocsAllUploaded: true,
                            isSubmitted: true
                          })
                        }).catch((error)=>{
                          logger.error('ERROR: appFormSubmission - end [RETURN=1] fails :', docId, error);
                          cb({success: false});
                        });
                      })
                    }
                  })
                } else {
                  handleRollBackAppEapp(new Exception.UnknownException('Unknown Exception in doSubmission'), backupApp, errorCB);
                }
              })
            } else {
              handleRollBackAppEapp(new Exception.UnknownException('Fail to generate eApp Form payment page in doSubmission') , backupApp, errorCB);
            }
          })
        }
      } catch(e) {
        logger.error('ERROR: appFormSubmission - catch error:', e);
        handleRollBackAppEapp(new Exception.UnknownException('Unexpected error:' + e) , backupApp, errorCB);
      }
    } else {
      handleRollBackAppEapp(new Exception.UnknownException('No application ID.') , backupApp, errorCB);
    }
  });
}
module.exports.appFormSubmission = appFormSubmission;
/**FUNCTION ENDS: appFormSubmission*/

let handleRollBackAppEapp = function(exception, appDoc, callback){
  logger.log('INFO: handleRollBackAppEapp', appDoc._id, exception.code);
  if (exception.code === Exception.ROLL_BACK){
    let eApprovalDocId = _.get(appDoc, 'policyNumber')
    bDao.onApplyApplication(appDoc.pCid, appDoc._id).then((resp) =>{
      applicationDao.upsertApplication(appDoc._id, appDoc, (res)=>{
        dao.getDoc(eApprovalDocId, (exDoc) =>{
          dao.delDocWithRev(eApprovalDocId, exDoc._rev, (resp) => {
            callback(appDoc, exception.message);
          });
        });
      });
    });
  }else {
    callback(appDoc, exception.message);
  }
}

module.exports.getSubmissionTemplateValues = function(data, session, cb) {
  var docId = data.docId;
  var template = {};
  var values = {};

  logger.log('INFO: getSubmissionTemplateValues - start', docId);

  let errorCB = function(res) {
    logger.error('ERROR: getSubmissionTemplateValues - end [RETURN=-1]', _.get(res, 'error'));
    cb(false)
  }

  applicationDao.getApplication(docId, function(app) {
    if (app._id) {
      getSubmissionTemplate(app, function(tmpl) {
        if (tmpl) {
          getSubmissionValues(app, session, app.isSubmittedStatus ? 'SUCCESS' : '', '', function (resValues) {
            if (resValues) {
              values = _.cloneDeep(resValues.values);
              if (app.isSubmittedStatus) {
                logger.log('INFO: getSubmissionTemplateValues - getFrozenTemplate', docId);
                if (session.platform) {
                  commonApp.frozenTemplateMobile(tmpl);
                } else {
                  commonApp.frozenTemplate(tmpl);
                }
              }
              logger.log('INFO: getSubmissionTemplateValues - end', docId);
              cb({
                success:true,
                template: tmpl,
                values: values,
                isMandDocsAllUploaded: app.isMandDocsAllUploaded,
                isSubmitted: app.isSubmittedStatus
              })
            } else {
              errorCB(resValues)
            }
          });
        } else {
          errorCB(tmpl)
        }
      })

      if (!app.isMandDocsAllUploaded) {
        app.isFailSubmit = true;
        applicationDao.upsertApplication(app._id, app, (res)=>{
          if (!res) {
            errorCB(res);
          }
        });
      }
    } else {
      errorCB(app)
    }
  })
}

//MOVE to ./application/common.js
//Function: for Signature, SignDoc Auth
var genSignDocAuth = function(docid, docts) {
  const secretKey = global.config.signdoc.secretKey;
  const input = docid + docts + secretKey;
  const iconv = new Iconv('UTF-8', 'ISO-8859-1');
  const buffer = iconv.convert(input);
  const hash = crypto.createHash('sha1');
  hash.update(buffer);
  const md = hash.digest();
  const auth = md.toString('base64');
  return auth;
}
module.exports.generateSignDocAuth = genSignDocAuth;

//Function: for Signature, prepare SignDoc parameter from web
let getSignatureStatusFromCb = function(data, session, cb){
  if (session.platform) {
    defaultSignature.getSignatureInitMobile(data, session, cb);
  } else {
    defaultSignature.getSignatureInitUrl(data, session, cb);
  }

}
module.exports.getSignatureStatusFromCb = getSignatureStatusFromCb;


//Function: for Signature, return urls with new token for signed pdf and next signing pdf
module.exports.getUpdatedAttachmentUrl = function(data, session, cb) {
  if (session.platform) {
    defaultSignature.getSignatureUpdatedPdfString(data, session, cb);
  } else {
    defaultSignature.getSignatureUpdatedUrl(data, session, cb);
  }
}

//COMMON FUNCTION
//Function: for Signature, receive POST from SignDoc, with PDF base64 data
module.exports.getSignedPdfFromSignDocPost = function(sdDocid, pdfData, tiffData, cb) {
  var ids = commonApp.getIdFromSignDocId(sdDocid);
  const docId = ids.docId;
  const attId = ids.attId;

  logger.log('INFO: getSignedPdfFromSignDocPost - start', docId);

  //Test if the data is validated tiff
  //CommonFunctions.base64ToFile(tiffData, 'temp.tiff', function(res) {
  //});

  if (docId.startsWith('SA')) {
    //Shield application
    shieldSignature.saveSignedPdfFromSignDoc(docId, attId, pdfData, cb);
  } else {
    defaultSignature.saveSignedPdfFromSignDoc(docId, attId, pdfData, cb);
  }

}

module.exports.saveSignedPdf = function(data, session, cb) {
  const { docId, attId, pdfData } = data;
  if (docId.startsWith('SA')) {
    //Shield application
    shieldSignature.saveSignedPdf(docId, attId, pdfData, cb);
  } else {
    defaultSignature.saveSignedPdf(docId, attId, pdfData, cb);
  }
}

module.exports.updateAppStep = function(data, session, cb) {
  const docId = data.docId;
  const stepperIndex = data.stepperIndex;

  logger.log('INFO: updateAppStep - start', docId);

  applicationDao.getApplication(docId, function(app) {
    if (app._id) {
      if (app.appStep == stepperIndex) {
        app.appStep = stepperIndex + 1;
        applicationDao.upsertApplication(app._id, app, function(resp){
          if(resp && !resp.error){
            logger.log('INFO: updateAppStep - end [RETURN=1]', docId);
            cb({success:true, appStep: app.appStep});
          }else{
            logger.error('ERROR: updateAppStep - end [RETURN=-2]', docId);
            cb({success:false, result:'Fail to update Application'});
          }
        });
      } else {
        logger.log('INFO: updateAppStep - end [RETURN=2]', docId);
        cb({success: true});
      }
    } else {
      logger.error('ERROR: updateAppStep - end [RETURN=-1]', _.get(app, 'error'));
      cb({success:false, result:'Fail to get Application'});
    }
  })
}

//TO-DO: fade out this function and MOVE to ./application/common.js
var mergePdfBase64ToAppFormPdf = function (app, pdfStr, callback) {
  logger.log('INFO: mergePdfBase64ToAppFormPdf - start', app._id);
  //get eApp form and merge with premium payment
  applicationDao.getAppAttachment(app._id, appPdfName, function(appPdf) {
    if (appPdf.success) {
      if (pdfStr && pdfStr.length > 0) {
        logger.log('INFO: mergePdfBase64ToAppFormPdf - pdfString length', pdfStr.length, app._id);
        //merge Pdf
        CommonFunctions.mergePdfs([appPdf.data, pdfStr], function(mergedPdf) {
          if (mergedPdf) {
            logger.log('INFO: mergePdfBase64ToAppFormPdf - mergedPdf not null', app._id);
            dao.uploadAttachmentByBase64(app._id, appPdfName, app._rev, mergedPdf, 'application/pdf', function(res) {
              if (res && res.rev && !res.error) {
                app._rev = res.rev;
                logger.log('INFO: mergePdfBase64ToAppFormPdf - end [RETURN=1]', app._id);
                callback(app);
              } else {
                logger.error('ERROR: mergePdfBase64ToAppFormPdf - end [RETURN=-4]', app._id);
                callback(false);
              }
            });
          } else {
            logger.error('ERROR: mergePdfBase64ToAppFormPdf - end [RETURN=-3]', app._id);
            callback(false);
          }
        });
      } else {
        logger.error('ERROR: mergePdfBase64ToAppFormPdf - end [RETURN=-2]', app._id);
        callback(app);
      }
    } else {
      logger.error('ERROR: mergePdfBase64ToAppFormPdf - end [RETURN=-1]', app._id);
      callback(false);
    }
  });
}

//COMMON FUNCTION
//batch 2 report
var genAppFormPdfByPaymentData = function(app, lang, callback) {
  logger.log('INFO: genAppFormPdfByPaymentData - start', app._id);
  let trigger = {};
  let paymentValues = _.cloneDeep(app.payment);
  const isShowDollarSign = _.get(app, 'applicationForm.values.appFormTemplate.properties.dollarSignForAllCcy', true);
  const ccy = isShowDollarSign ? null : _.get(app, 'quotation.ccy');

  dao.getDoc(commonApp.TEMPLATE_NAME.PAYMENT, function(paymentTmpl) {
    logger.log('INFO: genAppFormPdfByPaymentData - replaceAppFormValuesByTemplate start', app._id);
    commonApp.replaceAppFormValuesByTemplate(paymentTmpl, app.payment, paymentValues, trigger, [], lang, ccy);
    logger.log('INFO: genAppFormPdfByPaymentData - replaceAppFormValuesByTemplate end', app._id);

    let reportData = {
      root: {
        payment: paymentValues,
        originalData: app.payment,
        policyOptions: app.quotation.policyOptions
      }
    };

    let tplFiles = [];
    commonApp.getReportTemplates(tplFiles, ['appform_report_payment_main', 'appform_report_common', 'appform_report_premium_payment'], 0, function() {
      if (tplFiles.length) {
        PDFHandler.getPremiumPaymentPdf(reportData, tplFiles, lang, function(pdfStr) {
          //get eApp form and merge with premium payment
          mergePdfBase64ToAppFormPdf(app, pdfStr, (app) => {
            if (app && !app.error) {
              logger.log('INFO: genAppFormPdfByPaymentData - end [RETURN=1]', app._id);
              callback(app);
            } else {
              logger.error('ERROR: genAppFormPdfByPaymentData - end [RETURN=-2]', _.get(app, 'error'));
              callback(false);
            }
          });
        })
      } else {
        logger.error('ERROR: genAppFormPdfByPaymentData - end [RETURN=-1]', app._id);
        callback(false);
      }
    })
  })
}

//COMMON FUNCTION
//eCPD report
var getECpdPdfByAppFormData = function(app, lang, callback) {
  let initPayMethod = _.get(app, 'payment.initPayMethod');
  logger.log('INFO: getECpdPdfByAppFormData - start', app._id);

  if (['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(initPayMethod) > -1) {
    let formValues = _.cloneDeep(app.applicationForm.values);
    formValues.receiptDate = CommonFunctions.getFormattedDate(app.payment.trxTime, 'dd/MMMM/yyyy');
    formValues.policyNumber = app.policyNumber || app._id;
    formValues.agent = app.quotation.agent;
    let reportData = {
      root: formValues
    };

    let tplFiles = [];
    commonApp.getReportTemplates(tplFiles, ['appform_report_ecpd_main'], 0, function() {
      if (tplFiles.length) {
        PDFHandler.getECpdPdf(reportData, tplFiles, lang, function(pdfStr) {
          dao.uploadAttachmentByBase64(app._id, eCpdPdfName, app._rev, pdfStr, 'application/pdf', function(res) {
            if (res && res.rev && !res.error) {
              app._rev = res.rev;
              logger.log('INFO: getECpdPdfByAppFormData - end [RETURN=1]', app._id);
              callback(app);
            } else {
              logger.error('ERROR: getECpdPdfByAppFormData - end [RETURN=-2]', app._id, tplFiles);
              callback(false);
            }
          });
        })
      } else {
        logger.error('ERROR: getECpdPdfByAppFormData - end [RETURN=-1]', app._id, tplFiles);
        callback(false);
      }
    });
  } else {
    logger.log('INFO: getECpdPdfByAppFormData - end [RETURN=2]', app._id);
    callback(app);
  }
}

var _updatePaymentMethods = function(data, session, cb) {
  var docId = data.docId;
  var values = data.values;
  var stepperIndex = data.stepperIndex;
  var now = Date.now();
  var updateStepper = data.updateStepper;

  logger.log('INFO: _updatePaymentMethods - start', docId);

  applicationDao.getApplication(docId, function(app) {
    if (app && !app.error && app._id) {
      // if (app.appStep == stepperIndex) {
        if (!app.hasOwnProperty('payment')) {
          app['payment'] = {};
        }
        if (!app.hasOwnProperty('isInitialPaymentCompleted')) {
          app['isInitialPaymentCompleted'] = false;
        }
        app.payment.initPayMethod = values.initPayMethod;
        app.payment.subseqPayMethod = values.subseqPayMethod;

        if (app.payment.trxNo && ['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(app.payment.initPayMethod) >= 0) {
          // updated from api already
        } else {
          app.payment.trxNo = "";
          app.payment.trxTime = 0;
          app.payment.trxStartTime = 0;
          app.payment.trxStatus = "";
          app.payment.trxRemark = "";
          app.payment.trxAmount = 0;
          app.payment.trxCcy = "";
          app.payment.trxPayType = "";
          app.payment.trxMethod = "";
          app.payment.ccNo = "";
          app.payment.trxStatusRemark = ""
        }

        for (var v in values) {
          if (["covCode", "paymentMode", "axsOnline", "samOnline"].indexOf(v) == -1) {
            if (["ttRemittingBank", "ttDOR", "ttRemarks"].indexOf(v) == -1) {
              app.payment[v] = values[v];
            } else {
              if (values.initPayMethod == 'teleTransfter') {
                if (v == 'ttDOR') {
                  app.payment[v] = values.ttDOR ? new Date(values.ttDOR).toISOString() : ""
                } else {
                  app.payment[v] = values[v];
                }
              }
            }
          }
        }

        // app.payment.initBasicPrem = values.initBasicPrem || 0;
        // app.payment.backdatingPrem = values.backdatingPrem || 0;
        // app.payment.initRspPrem = values.initRspPrem || 0;
        // app.payment.initTotalPrem = values.initTotalPrem || 0;
        // app.payment.topupPrem = values.topupPrem || 0;

        if (updateStepper && !data.isShield) {
          app.appStep = stepperIndex + 1
        }
        logger.log('INFO: _updatePaymentMethods - end [RETURN=1]', docId);
        cb(app);
      // } else {
      //   cb(app);
      // }
    } else {
      logger.error('ERROR: _updatePaymentMethods - end [RETURN=-1]', docId, _.get(app, 'error'));
      cb(app);
    }
  })
}

let resetMandDocsStatusForDirector = function(app, session) {
  if (_.get(session, 'agent.agentCode') === _.get(session, 'agent.managerCode')) {
    let paymentMethod = _.get(app, 'payment.initPayMethod');
    let mandDocs = _.get(app, 'supportDocuments.values.policyForm.mandDocs');
    if (paymentMethod && mandDocs) {
      if (_.get(mandDocs, '[' + paymentMethod + ']') && _.isEmpty(mandDocs[paymentMethod])) {
        _.set(app, 'isMandDocsAllUploaded', false);
      } else if(paymentMethod === "teleTransfter" && !(_.isEmpty(mandDocs.teleTransfter) || _.isEmpty(mandDocs.teleTransfer))) {
        // ridiculous bug because the string of the paymentMethod can be "teleTransfter" OR "teleTransfer" !!!!!!!!!!!!!!
        // check the web yourself !!!!!!!!!!!!!
        _.set(app, 'isMandDocsAllUploaded', false);
      } else {
        _.set(app, 'isMandDocsAllUploaded', true);
      }
    }
  }
}

module.exports.updatePaymentMethods = function(data, session, cb) {
  var updateStepper = data.updateStepper;
  var docId = data.docId;

  logger.log('INFO: updatePaymentMethods - start', docId);

  _updatePaymentMethods(data, session, (app) => {
    resetMandDocsStatusForDirector(app, session);
    if (app && !app.error && app._id) {
      applicationDao.upsertApplication(app._id, app, (upApp) => {
        if (upApp && !upApp.error) {
          let lang = session.lang || 'en';
          logger.log('INFO: updatePaymentMethods - end [RETURN=1]', docId);
          cb({success:true, application:app});
        } else {
          logger.error('ERROR: updatePaymentMethods - end [RETURN=-2]', docId, _.get(upApp, 'error'));
          cb({success:false, result:upApp});
        }
      })
    } else {
      logger.error('ERROR: updatePaymentMethods - end [RETURN=-1]', docId, _.get(app, 'error'));
      cb({success:false, result:app});
    }
  })
}

module.exports.getAppSumEmailAttKeys = function(data, session, callback){
  applicationDao.getAppSumEmailAttKeys(session, (result) => {
    if (result && !result.error) {
      callback({
        success: true,
        attKeysMap: result.items
      });
    } else {
      callback({
        success: false
      });
    }
  });
}

module.exports.getEmailTemplate = function (data, session, callback) {
  applicationDao.getEmailTemplate((result) => {
    if (result && !result.error) {
      callback({
        success: true,
        clientSubject: result.clientSubject,
        clientContent: result.clientContent,
        agentSubject: result.agentSubject,
        agentContent: result.agentContent,
        embedImgs: result.embedImgs
      });
    } else {
      callback({
        success: false
      });
    }
  });
};

const sendApplicationEmail = function(data, session, cb) {
  commonApp.sendApplicationEmail(data, session, cb);
};
module.exports.sendApplicationEmail = sendApplicationEmail;

module.exports.updateBIBackDateTrue = function(data, session, callback){
  let { appId } = data;
  let signatureData = {docId: appId};
  logger.log('INFO: updateBIBackDateTrue - start', appId);

  applicationDao.getApplication(appId, function(app){
    if (app && !app.error) {
      _.set(app, 'quotation.isBackDate', 'Y');
      _.set(app, 'applicationForm.values.planDetails.isBackDate', 'Y');
      _.set(app, 'isBackDate', true);
      _.set(app, 'applicationForm.values.planDetails.riskCommenDate', _.get(app, 'quotation.riskCommenDate'));
      applicationDao.upsertApplication(app.id, app, function(result){
        defaultApplication.genApplicationPDF(app, session, (genAppCb)=>{
          getSignatureStatusFromCb(signatureData, session, (signCb)=>{
            if (genAppCb.success) {
              //update view
              dao.updateViewIndex("main", "summaryApps");
              if (result) {
                logger.log('INFO: updateBIBackDateTrue - end [RETURN=1]', app.id);
                callback({
                  success: true,
                  isBackDate: 'Y',
                  changedValues: _.get(app, 'applicationForm.values'),
                  application: signCb.application.application,
                  attachments: signCb.application.attachments
                });
              } else {
                logger.error('ERROR: updateBIBackDateTrue - end [RETURN=-3]', appId);
                callback({success:false});
              }
            } else {
              logger.error('ERROR: updateBIBackDateTrue - end [RETURN=-2]', appId);
              callback({success:false});
            }
          });
        });
      });
    } else {
      logger.error('ERROR: updateBIBackDateTrue - end [RETURN=-1]', appId, _.get(app, 'error'));
      callback(app);
    }
  });
}

module.exports.invalidateApplication = function(data, session, callback){
  logger.log('INFO: invalidateApplication in appHandler')
  applicationDao.getApplication(data.appId, function(app){
    bDao.onInvalidateApplicationById(app.quotation.pCid, data.appId).then((resp)=>{
      callback(resp);
    });
  })
}

module.exports.applicationSubmit = function(data, session, callback){
  defaultApplication.applicationSubmit(data, session, response => {
    callback(response);
  });
}

module.exports.genFNA = function(data, session, cb){
  defaultApplication.genFNA(data, session, cb);
};

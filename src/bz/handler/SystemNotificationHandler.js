var _ = require('lodash');

var notifyDao = require('../cbDao/systemNotification');
var async = require('async');

var logger = global.logger || console;

const getValidSystemNotification = function(compCode, cb) {
  const currentDateTime = Date.parse(new Date().toUTCString());
  const expiryDate = Date.parse(new Date('9999-12-31').toISOString());
  async.waterfall([
      (callback) => {
        let promises = [];
        promises.push(notifyDao.getValidEndNotification(compCode, currentDateTime, expiryDate));

        Promise.all(promises).then(data => {
          let validMessages = [];
          let message;
          let messageId;
          _.each(data, (campaignView, key) => {
            _.each(campaignView && campaignView.rows, rowObj => {
              message = _.get(rowObj, 'value');
              messageId = _.get(rowObj, 'value.messageId');
              if (message && message.startTime && currentDateTime > Date.parse(message.startTime) && messageId) {
                validMessages.push(message);
              }
            });
          });
          callback(null, validMessages);
        });
      }
  ], (err, validMessage) => {
      if (err) {
          logger.error('ERROR in getvalidMessage: ', err);
          cb([]);
      } else {
          cb(validMessage);
      }
  });
};

module.exports.getValidSystemNotification = getValidSystemNotification;

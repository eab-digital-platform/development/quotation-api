'use strict';

var _ = require('lodash');

// const dao = require('../cbDaoFactory').create();
// var bundleDao = require('../cbDao/bundle');
// var prodDao = require('../cbDao/product');
// var needDao = require('../cbDao/needs');
// var quotDao = require('../cbDao/quotation');

// const EmailUtils = require('../utils/EmailUtils');
const CommonFunctions = require('../CommonFunctions');
// const TokenUtils = require('../utils/TokenUtils');
const QuotUtils = require('./quote/QuotUtils');
const ProposalUtils = require('./quote/ProposalUtils');
const {getQuotDriver, getProfile, getCompanyInfo, getFNAInfo, validateClient, validateFNAInfo, validateClientFNAInfo, checkAllowQuotProduct} = require('./quote/common');
const {formatDatetime, parseDatetime, dayDiff, parseDate} = require('../../common/DateUtils');
const {getCovCodeFromProductId, getProductId} = require('../../common/ProductUtils');
// const {getApplication} = require('../cbDao/application');
var async = require('async');

const staticJson = require('../../../static/Directory');
const {getProductSuitability} = staticJson;

const logger = global.logger || console;
global.quotCache = {
  planDetails: {},
  illustrationRates: {}
};

var queryQuickQuotes = function (data, session, callback) {
  const {pCid} = data;
  quotDao.queryQuickQuotes(session.agent.compCode, pCid, (quickQuotes) => {
    callback({
      success: true,
      quickQuotes: quickQuotes
    });
  });
};
module.exports.queryQuickQuotes = queryQuickQuotes;

module.exports.deleteQuickQuotes = function (data, session, callback) {
  const {quotIds, pCid} = data;
  Promise.all(_.map(quotIds, (quotId) => {
    return new Promise((resolve) => {
      quotDao.getQuotation(quotId, (quot) => {
        if (quot.pCid === pCid && session.agent.agentCode === quot.agent.agentCode) {
          quotDao.deleteQuotation(quot, false, resolve);
        } else {
          resolve();
        }
      });
    });
  })).then(() => {
    return quotDao.updateQuotViewIndex();
  }).then(() => {
    queryQuickQuotes({ pCid: pCid }, session, callback);
  }).catch((error) => {
    logger.error('Quotation :: deleteQuickQuotes :: failed to delete quick quotes\n', error);
    callback({ success: false });
  });
};

const checkAllowCreateQuot = (requireFNA, agent, pCid, confirm) => {
  if (!requireFNA) {
    return Promise.resolve(true);
  } else {
    return bundleDao.onCreateQuotation(agent, pCid, confirm).then((result) => {
      logger.log('Quotation :: checkAllowCreateQuot :: bundle is fully signed, require confirmation');
      return result && (!result.code || result.code === bundleDao.CHECK_CODE.VALID);
    });
  }
};

const getAvailableInsureds = (quotation, planDetails, requireFNA) => {
  const promises = [];
  promises.push(QuotUtils.getRelatedProfiles(quotation.pCid));
  if (requireFNA) {
    promises.push( needDao.getItem(quotation.pCid, needDao.ITEM_ID.PDA));
    promises.push(needDao.getItem(quotation.pCid, needDao.ITEM_ID.NA));
  }
  return Promise.all(promises).then(([profiles, pda, fna]) => {
    const quotDriver = getQuotDriver(quotation);
    let availableApplicants = profiles;
    if (quotDriver.getAvailableApplicants) {
      availableApplicants = quotDriver.getAvailableApplicants(quotation, planDetails, profiles, { pda, fna });
    }
    let eligibleClients = profiles;
    if (quotDriver.getEligibleClients) {
      eligibleClients = quotDriver.getEligibleClients(quotation, planDetails, profiles, { pda, fna });
    }
    let availableClassMap = {};
    if (quotDriver.getAvailableClasses) {
      availableClassMap = quotDriver.getAvailableClasses(quotation, planDetails, eligibleClients);
    }
    return _.map(availableApplicants, (profile) => {
      return {
        profile,
        availableClasses: availableClassMap[profile.cid],
        valid: validateClient(profile),
        eligible: !!_.find(eligibleClients, c => c === profile)
      };
    });
  });
};

module.exports.initQuotation = function(data, session, callback) {
  // confirm removed as no confirmation needed
  const {quickQuote, iCid, pCid, productId } = data;
  const ccy = data.params && data.params.ccy;
  const requireFNA = // session.agent.channel.type === 'AGENCY' && !quickQuote;
  false;
  logger.log('QuotationHandler.js :: initQuotation :: 1. Creating new quotation of product:', productId);
  var promises = [];
  promises.push(getProfile(iCid));
  promises.push(getProfile(pCid && pCid !== iCid ? pCid : null));
  promises.push(getProductSuitability());
  promises.push(getCompanyInfo());
  return Promise.all(promises).then((args) => {
    let [insured, proposer, suitability, companyInfo, fe, fna] = args;
    proposer = proposer || insured;
    const covCode = getCovCodeFromProductId(productId);
    return getPlanDetails(session, covCode, true).then((planDetails) => {
      var basicPlan = planDetails[covCode];
      if (basicPlan) {
        let quotation;
        /** No Shield
        if (basicPlan.covCode === 'ASIM') { // TODO: check flag from planDetail
          logger.log('Quotation :: initQuotation :: profiles:', proposer.cid);
          quotation = QuotUtils.genShieldQuotation(basicPlan, session.agent, companyInfo, proposer, quickQuote);
        } else {
          */
          logger.log('QuotationHandler.js :: initQuotation :: 2. Retreived profiles:', insured.cid, proposer.cid);
          quotation = QuotUtils.genQuotation(basicPlan, session.agent, companyInfo, proposer, insured, quickQuote, ccy, suitability, fna);
        //}
        quotation.extraFlags = !quotation.extraFlags ? {} : quotation.extraFlags;
        quotation.extraFlags.init = true;
        let calcResult = JSON.parse(JSON.stringify(getQuotDriver(quotation).calculateQuotation(quotation, planDetails)));
        // let calcResult = JSON.parse(getQuotDriver(quotation).calculateQuotation(quotation, planDetails));
        quotation = session.quotation = calcResult.quotation || quotation;
        delete quotation.extraFlags.init;
        const warnings = QuotUtils.getQuotationWarnings(basicPlan, quotation, suitability);
        return getAvailableInsureds(quotation, planDetails, requireFNA).then((availableInsureds) => {
          callback({
            success: true,
            errors: calcResult.errors,
            quotation: quotation,
            planDetails: trimClientPlanDetails(planDetails),
            inputConfigs: calcResult.inputConfigs,
            quotWarnings: warnings,
            availableInsureds: availableInsureds,
            profile: proposer
          });
        });
      } else {
        callback({
          success: false,
          error: 'Basic plan details not found!'
        });
      }
    });
  });
}
/** 
 * Bypass FNA
module.exports.initQuotation = function(data, session, callback) {
  const {quickQuote, iCid, pCid, confirm, productId} = data;
  const ccy = data.params && data.params.ccy;
  const requireFNA = session.agent.channel.type === 'AGENCY' && !quickQuote;

  checkAllowCreateQuot(requireFNA, session.agent, pCid, confirm).then((allowCreate) => {
    if (!allowCreate) {
      callback({
        success: true,
        requireConfirm: true
      });
      return;
    }
    logger.log('Quotation :: initQuotation :: creating new quotation of product:', productId);
    var promises = [];
    promises.push(getProfile(iCid));
    promises.push(getProfile(pCid && pCid !== iCid ? pCid : null));
    promises.push(prodDao.getProductSuitability());
    promises.push(getCompanyInfo());
    if (requireFNA) {
      promises.push(needDao.getItem(pCid || iCid, needDao.ITEM_ID.FE));
      promises.push(needDao.getItem(pCid || iCid, needDao.ITEM_ID.NA));
    }
    return Promise.all(promises).then((args) => {
      let [insured, proposer, suitability, companyInfo, fe, fna] = args;
      proposer = proposer || insured;
      const covCode = getCovCodeFromProductId(productId);
      return getPlanDetails(session, covCode, true).then((planDetails) => {
        var basicPlan = planDetails[covCode];
        if (basicPlan) {
          if (requireFNA) {
            let suitResult = getQuotDriver().checkProdSuitForQuot(suitability, { fna, fe }, basicPlan);
            if (suitResult !== true) {
              logger.log('Quotation :: initQuotation :: product is not allowed to be quoted');
              callback({
                success: true,
                errorMsg: suitResult,
                profile: proposer
              });
              return;
            }
          }
          let quotation;
          if (basicPlan.covCode === 'ASIM') { // TODO: check flag from planDetail
            logger.log('Quotation :: initQuotation :: profiles:', proposer.cid);
            quotation = QuotUtils.genShieldQuotation(basicPlan, session.agent, companyInfo, proposer, quickQuote);
          } else {
            logger.log('Quotation :: initQuotation :: profiles:', insured.cid, proposer.cid);
            quotation = QuotUtils.genQuotation(basicPlan, session.agent, companyInfo, proposer, insured, quickQuote, ccy, suitability, fna);
          }
          if (!quotation.extraFlags) {
            quotation.extraFlags = {};
          }
          quotation.extraFlags.init = true;
          let calResult = JSON.parse(getQuotDriver(quotation).calculateQuotation(quotation, planDetails));
          quotation = session.quotation = calResult.quotation || quotation;
          delete quotation.extraFlags.init;
          const warnings = QuotUtils.getQuotationWarnings(basicPlan, quotation, suitability);
          return getAvailableInsureds(quotation, planDetails, requireFNA).then((availableInsureds) => {
            callback({
              success: true,
              errors: calResult.errors,
              quotation: quotation,
              planDetails: trimClientPlanDetails(planDetails),
              inputConfigs: calResult.inputConfigs,
              quotWarnings: warnings,
              availableInsureds: availableInsureds,
              profile: proposer
            });
          });
        } else {
          callback({
            success: false,
            error: 'Basic plan details not found!'
          });
        }
      });
    });
  }).catch((err) => {
    logger.error('Quotation :: initQuotation :: failed to init quotation\n', err);
    callback({ success: false });
  });
};
*/

const prefetchIllustrationRates = function (planDetails) {
  let p = Promise.resolve();
  let hasPrefetch = false;
  _.each(planDetails, (planDetail, covCode) => {
    let prefetchRates;
    if (planDetail.formulas.getPrefetchIllustrationRates) {
      prefetchRates = getQuotDriver().runFunc(planDetail.formulas.getPrefetchIllustrationRates);
    }
    if (prefetchRates) {
      _.each(prefetchRates, (config, rateKey) => {
        logger.log('Quotation :: prefetchIllustrationRates :: caching rates:', covCode, rateKey);
        const {docId} = config;
        if (docId) {
          hasPrefetch = true;
          p = p.then(() => {
            return new Promise((resolve) => {
              dao.getDoc(docId, (doc) => {
                if (doc && doc.rates && global.quotCache.planDetails[covCode]) {
                  logger.log('Quotation :: prefetchIllustrationRates :: rates fetched:', docId);
                  if (!global.quotCache.illustrationRates[planDetail._id]) {
                    global.quotCache.illustrationRates[planDetail._id] = {};
                  }
                  global.quotCache.illustrationRates[planDetail._id][rateKey] = doc.rates;
                }
                resolve();
              });
            });
          });
        }
      });
    }
  });
  if (hasPrefetch) {
    global.quotCache.fetching = p;
    p.then(() => {
      logger.log('Quotation :: prefetchIllustrationRates :: rates prefetch completed');
      delete global.quotCache.fetching;
    });
  }
};

/**
 * Return the static Basic Plan JSON
 * @param {*} compCode 
 * @param {*} covCode 
 * @param {*} planInd 
 * @param {*} getOldest 
 */
const getPlanByCovCode = function (compCode, covCode, planInd, getOldest) {
  return new Promise(resolve => {
    if (planInd == 'B') {
      resolve(_.get(staticJson.basicPlan, covCode)); 
    } else if (planInd == 'R') {
      if (staticJson.rider[covCode] == null) {
        logger.log('QuotationHandler.js :: getPlanByCovCode :: static JSON (RIDER) not added:', covCode);
        resolve();
      }
      resolve(staticJson.rider[covCode]);
    } 
    resolve();
  });
}

const _preparePlanDetails = function (compCode, covCode) {
  return getPlanByCovCode(compCode, covCode, 'B').then(basicPlan => {
    const planDetails = {};
    planDetails[basicPlan.covCode] = basicPlan;
    const promises = _.map(basicPlan.riderList, (rider) => {
      return getPlanByCovCode(compCode, rider.covCode, 'R').then((riderDetail) => {
        if (riderDetail && riderDetail.covCode) {
          logger.log('QuotationHandler.js :: preparePlanDetails :: found rider details:', rider.covCode);
          planDetails[riderDetail.covCode] = riderDetail;
        } else {
          logger.error('QuotationHandler.js :: preparePlanDetails :: cannot find plan detail:', rider.covCode);
        }
      });
    });
    // console.log(promises);
    return Promise.all(promises).then(() => planDetails);
  });
};

const getPlanDetails = (session, baseProductCode, noCache) => {
  if (noCache || !global.quotCache.planDetails[baseProductCode]) {
    return new Promise(resolve => {
      _preparePlanDetails(session.agent.compCode, baseProductCode).then((planDetails) => {
        prefetchIllustrationRates(planDetails);
        resolve(planDetails);
        // No campaign is required
        // preparePlanDetailsWithCampaign(session, planDetails, resolve);
      });
    });
  } else {
    const planDetails = {};
    const bpDetail = global.quotCache.planDetails[baseProductCode];
    planDetails[baseProductCode] = bpDetail;
    _.each(bpDetail.riderList, (rider) => {
      planDetails[rider.covCode] = global.quotCache.planDetails[rider.covCode];
    });
    return Promise.resolve(planDetails);
  }
};
module.exports.getPlanDetails = getPlanDetails;

/**
 * Prepare Valid Campaign for Plan Details
 * @param {*} planDetails 
 */
const preparePlanDetailsWithCampaign = (session, planDetails, resolve) => {
  async.waterfall([
      (callback) => {
        getValidCampaign(session.agent.compCode, (result) => {
          if (result.success && result.validCampaign) {
            callback(null, result.validCampaign);
          } else {
            //Not going to modify the planDetails
            callback(result);
          }
        });
      }, (validCampaign, callback) => {
        planDetails = addCampaignToPlanDetails(planDetails, validCampaign);
        callback(null, planDetails);
      }
  ], (err, modifiedPlanDetails) => {
    let result;
    if (err){
      logger.error('Error in preparePlanDetailsWithCampaign ', err);
      result = planDetails;
    } else {
      result = modifiedPlanDetails;
    }
    _.each(modifiedPlanDetails, (planDetail, covCode) => {
      logger.log('Quotation :: getPlanDetails :: preparePlanDetailsWithCampaign :: update plan details in cache:', covCode);
      global.quotCache.planDetails[covCode] = planDetail;
    });
    resolve(result);
  });
};

const addCampaignToPlanDetails = function(planDetails, validCampaign) {
  // Group by covCode
  let groupedCampaign = {};
  _.each(validCampaign, campaign => {
    let campaignCode = campaign.campaignCode;
    let campaignId = campaign.campaignId;
    _.each(campaign && campaign.impactedPlan, (plan, key) => {
      // Add the campaignCode and campaignId to individual campaign
      _.each(plan, campaignPlanWithDiffCondition => {
        campaignPlanWithDiffCondition.campaignCode = campaignCode;
        campaignPlanWithDiffCondition.campaignId = campaignId;
      });

      let individualCampaign = groupedCampaign[key];
      if (individualCampaign) {
        groupedCampaign[key] = _.concat(groupedCampaign[key], plan);
      } else {
        groupedCampaign[key] = plan;
      }
    });
  });

  planDetails = _.each(planDetails, (planDetail, key) => {
    planDetail.campaign = [];
    let campaign = _.get(groupedCampaign, key);
    (campaign) ? planDetail.campaign = campaign : [];
  });

  return planDetails;
}

const getValidCampaign = function(compCode, cb) {
  const currentDateTime = Date.parse(new Date().toUTCString());
  const expiryDate = Date.parse(new Date('9999-12-31').toISOString());
  async.waterfall([
      (callback) => {
        let promises = [];
        promises.push(new Promise(resolve => {
            dao.getViewRange(
              'main',
              'quotationCampaign',
              `["${compCode}","endTime",${currentDateTime}]`,
              `["${compCode}","endTime",${expiryDate}]`,
              null,
              (viewResult) => {
                resolve(viewResult);
              }
            );
          })
        );

        Promise.all(promises).then(data => {
          let validCampaign = {};
          let campaign;
          let campaignId;
          _.each(data, (campaignView, key) => {
            _.each(campaignView && campaignView.rows, rowObj => {
              campaign = _.get(rowObj, 'value');
              campaignId = _.get(rowObj, 'value.campaignId');
              if (campaign && campaign.impactedPlan && campaign.startTime && currentDateTime > Date.parse(campaign.startTime) && campaignId) {
                validCampaign[campaignId] = campaign;
              }
            });
          });
          callback(null, validCampaign);
        });
      }
  ], (err, validCampaign) => {
      if (err) {
          logger.error('ERROR in getValidCampaign: ', err);
          cb({success: false});
      } else {
          cb({success: true, validCampaign});
      }
  });
};

const getCampaignByIds = function(compCode, campaignIds) {
  let keys = _.map(campaignIds, campaignId => `["${compCode}","campaignId","${campaignId}"]`);
  return new Promise(resolve => {
    dao.getViewByKeys('main', 'proposalCampaign', keys, null).then((result) => {
      let validCampaign = {};
      _.each(result && result.rows, (row) => {
          if (row.value && row.value.campaignId && row.value.impactedPlan) {
            validCampaign[row.value.campaignId] = row.value;
          }
      });
      resolve({validCampaign});
    });
  });
};

/**
 * Returns the plan details of the version used by the quotation.
 *
 * @param {*} session
 * @param {*} quotation
 */
const getPlanDetailsByQuot = function (session, quotation) {
  const planDetails = {};
  let planDetailsWithCampaign = {};
  let plans = quotation.plans;
  if (quotation.quotType === 'SHIELD') {
    const planMap = {};
    _.each(quotation.insureds, subQuot => {
      _.each(subQuot.plans, plan => {
        planMap[plan.covCode] = {
          productId: plan.productId,
          covCode: plan.covCode
        };
      });
    });
    plans = _.values(planMap);
  }
  let promises = _.map(plans, plan => {
    let p;
    if (plan.productId) {
      const cached = global.quotCache.planDetails[plan.covCode];
      if (cached && plan.productId === getProductId(cached)) {
        p = Promise.resolve(cached);
      } else {
        // p = prodDao.getProduct(plan.productId);
        const pDetail = _.get(staticJson, `planDetails.${plan.covCode}`, null);
        if (!pDetail) {
          console.log(`QuotationHandler :: ${plan.covCode} not found in Directory.js`);
        }
        p = Promise.resolve(_.get(staticJson, `planDetails.${plan.covCode}`, null));
      }
    } 
    return p.then(product => {
      planDetails[product.covCode] = _.cloneDeep(product);
    });
  });
  let includedCampaignIds = [];
  _.each(quotation && quotation.plans, plan => {
    _.each(plan && plan.campaignIds, (campaignId) => {
      if (includedCampaignIds.indexOf(campaignId) === -1) {
        includedCampaignIds.push(campaignId);
      }
    });
  });

  if (!_.isEmpty(includedCampaignIds)) {
    promises.push(
      getCampaignByIds(session.agent.compCode, includedCampaignIds).then((result) => {
        if (result && result.validCampaign && !_.isEmpty(result.validCampaign)) {
          planDetailsWithCampaign = addCampaignToPlanDetails(planDetails, result.validCampaign);
        } else {
          return Promise.reject(`ERROR:: Cannot get campaignByIds:: Including campaign Ids:: ${includedCampaignIds} :: Result:: ${result && result.validCampaign}`);
        }
      })
    );
  }


  return Promise.all(promises).then(() => global.quotCache.fetching).then(() => {
    _.each(planDetails, planDetail => {
      _.each(global.quotCache.illustrationRates[planDetail._id], (rates, rateKey) => {
        planDetail.rates[rateKey] = rates;
      });
      let campaign = _.get(planDetailsWithCampaign, `${planDetail.covCode}.campaign`, []);
      planDetail.campaign = campaign;
    });
    return planDetails;
  });
};
module.exports.getPlanDetailsByQuot = getPlanDetailsByQuot;

/**
 * Remove unnecessary details for client side.
 *
 * @param {*} planDetails
 */
const trimClientPlanDetails = function (planDetails) {
  const details = {};
  _.each(planDetails, (planDetail, covCode) => {
    const detail = _.clone(planDetail);
    delete detail.rates;
    delete detail.formulas;
    details[covCode] = detail;
  });
  return details;
};

var quote = function(data, session, callback) {
  getPlanDetails(session, data.quotation.baseProductCode).then((planDetails) => {
    // TODO:: add globalValidation !!
    const quot = getQuotDriver(data.quotation).calculateQuotation(data.quotation, planDetails);
    var result = typeof quot === 'object' ? quot : JSON.parse(quot);
    logger.log('Quotation :: quote :: finish calc');

    var finalQuot = result.quotation || data.quotation;
    finalQuot.lastUpdateDate = formatDatetime(new Date());

    var basicPlan = planDetails[getCovCodeFromProductId(data.quotation.baseProductId)];
    /** Alternative */
    QuotUtils.updateProductIds(finalQuot, planDetails);
    const warnings = QuotUtils.getQuotationWarnings(basicPlan, data.quotation, null);
    return callback({
      success: !result.error && !data.error,
      errors: result.errors,
      quotWarnings: warnings,
      quotation: finalQuot,
      inputConfigs: result.inputConfigs
    });
  }) 
    /** DB */
    /**
    return prodDao.getProductSuitability().then((suitability) => {
      // save product id for product versioning
      QuotUtils.updateProductIds(finalQuot, planDetails);
      const warnings = QuotUtils.getQuotationWarnings(basicPlan, data.quotation, suitability);
      callback({
        success: !result.error && !data.error,
        errors: result.errors,
        quotWarnings: warnings,
        quotation: finalQuot,
        inputConfigs: result.inputConfigs
      });
    });
    }) 
    */
  /**
   * .catch((error) => {
    logger.error('Quotation :: quote :: failed to quote\n', error);
    callback({ success: false });
  });
  */
};
module.exports.quote = quote;

var getQuotation = function (data, session) {
  return new Promise((resolve, reject) => {
    if (data.quotation) {
      resolve(data.quotation);
    } else if (data.quotId) {
      if (session.quotation && session.quotation.id === data.quotId) {
        resolve(session.quotation);
      } else {
        quotDao.getQuotation(data.quotId, (quotation) => {
          resolve(quotation);
        });
      }
    } else {
      throw new Error('Cannot find quotation in session and request');
    }
  });
};

const prepareQuotPage = function (session, quotation, planDetails, requireFNA) {
  const bpDetail = planDetails[quotation.baseProductCode];
  return prodDao.getProductSuitability().then((suitability) => {
    const warnings = QuotUtils.getQuotationWarnings(bpDetail, quotation, suitability);
    return getAvailableInsureds(quotation, planDetails, requireFNA).then((availableInsureds) => {
      return new Promise((resolve) => {
        quote({ quotation }, session, (result) => {
          resolve({
            success: result.success,
            errors: result.errors,
            quotation: result.quotation,
            planDetails: trimClientPlanDetails(planDetails),
            inputConfigs: result.inputConfigs,
            quotWarnings: warnings,
            availableInsureds: availableInsureds
          });
        });
      });
    });
  });
};

module.exports.requote = function (data, session, cb) {
  getQuotation(data, session).then((quotation) => {
    return getPlanDetails(session, quotation.baseProductCode, true).then((planDetails) => {
      const requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
      QuotUtils.updateQuot(quotation, planDetails, false);
      return prepareQuotPage(session, quotation, planDetails, requireFNA).then((result) => {
        cb(result);
      });
    });
  }).catch((error) => {
    logger.error('Quotation :: requote :: failed to requote\n', error);
    cb({ success: false });
  });
};

const checkAllowCloneQuot = (quotation, bpDetail) => {
  const currentDate = new Date();
  if (dayDiff(currentDate, parseDatetime(quotation.createDate)) > 90) { // check if BI is older than 90 days
    logger.log('Quotation :: checkAllowCloneQuot :: quotation over 90 days');
    return Promise.resolve(false);
  }

  if (QuotUtils.hasCrossAge(quotation, bpDetail)) {
    logger.log('Quotation :: checkAllowCloneQuot :: proposer or insured age changed');
    return Promise.resolve(false);
  }

  return QuotUtils.getQuotClients(quotation).then((profiles) => {
    if (QuotUtils.hasProfileUpdate(quotation, profiles)) {
      logger.log('Quotation :: checkAllowCloneQuot :: client profile is updated');
      return false;
  }
    return new Promise((resolve) => {
      prodDao.canViewProduct(quotation.baseProductId, profiles[0], profiles[1], (product) => {
        if (!product) {
          logger.log('Quotation :: checkAllowCloneQuot :: product cannot be viewed');
          resolve(false);
        } else {
          resolve(true);
        }
      });
    });
  });
};

module.exports.cloneQuotation = function (data, session, cb) {
  const {confirm} = data;
  getQuotation(data, session).then((quotation) => {
    return getPlanDetails(session, quotation.baseProductCode, true).then((planDetails) => {
      const bpDetail = planDetails[quotation.baseProductCode];
      const requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
      return checkAllowCloneQuot(quotation, bpDetail).then((allowClone) => {
        if (!allowClone) {
          cb({ success: true, allowClone: false });
          return;
        }
        return checkAllowCreateQuot(requireFNA, session.agent, quotation.pCid, confirm).then((allowCreate) => {
          if (!allowCreate) {
            cb({ success: true, requireConfirm: true });
            return;
          }
          QuotUtils.updateQuot(quotation, planDetails, true);
          return prepareQuotPage(session, quotation, planDetails, requireFNA).then((result) => {
            cb(Object.assign(result, { allowClone: true }));
          });
        });
      });
    });
  }).catch((err) => {
    logger.error('Quotation :: cloneQuotation :: Failed to clone quotation\n', err);
    cb({ success: false });
  });
};

const checkAllowRequoteInvalidated = function (agent, quotation, requireFNA) {
  return QuotUtils.getQuotClients(quotation).then((profiles) => {
    const invalidProfile = _.find(profiles, profile => !validateClient(profile));
    if (invalidProfile) {
      logger.log('Quotation :: checkAllowRequoteInvalidated :: mandatory fields are missing');
      return false;
    }
    return Promise.resolve(requireFNA && getFNAInfo(quotation.pCid)).then((fnaInfo) => {
      if (requireFNA && !validateFNAInfo(fnaInfo)) {
        logger.log('Quotation :: checkAllowRequoteInvalidated :: FNA is not completed');
        return false;
      }
      return checkAllowQuotProduct(agent, quotation.baseProductId, profiles[0], profiles[1], fnaInfo).then((allowQuot) => {
        if (!allowQuot) {
          logger.log('Quotation :: checkAllowRequoteInvalidated :: product not viewable to client');
          return false;
        }
        return true;
      });
    });
  });
};

module.exports.requoteInvalid = function (data, session, callback) {
  const {confirm} = data;
  getQuotation(data, session).then((quotation) => {
    return getPlanDetails(session, quotation.baseProductCode, true).then((planDetails) => {
      const requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
      return checkAllowRequoteInvalidated(session.agent, quotation, requireFNA).then((allowRequote) => {
        if (!allowRequote) {
          callback({ success: true, allowRequote: false });
          return;
        }
        return checkAllowCreateQuot(requireFNA, session.agent, quotation.pCid, confirm).then((allowCreate) => {
          if (!allowCreate) {
            callback({ success: true, requireConfirm: true });
            return;
          }
          QuotUtils.updateQuot(quotation, planDetails, true);
          quotation.fund = null;
          return QuotUtils.updateClientFields(quotation, planDetails, requireFNA).then(() => {
            return prepareQuotPage(session, quotation, planDetails, requireFNA).then((result) => {
              callback(Object.assign(result, { allowRequote: true }));
            });
          });
        });
      });
    });
  }).catch((err) => {
    logger.error('Quotation :: requoteInvalidated :: Failed to requote\n', err);
    callback({ success: false });
  });
};

module.exports.resetQuot = function (data, session, callback) {
  const {keepConfigs, keepPolicyOptions, keepPlans, keepFunds} = data;
  getQuotation(data, session).then((quotation) => {
    if (!keepConfigs) {
      quotation.ccy = null;
      quotation.paymentMode = null;
      quotation.isBackDate = 'N';
    }
    if (!keepPolicyOptions) {
      _.each(quotation.policyOptions, (value, key) => {
        quotation.policyOptions[key] = null;
      });
    }
    if (!keepPlans) {
      quotation.plans = null;
    }
    if (!keepFunds) {
      quotation.fund = null;
    }
    quotation.extraFlags.reset = true;
    quote(data, session, (result) => {
      if (result.quotation && result.quotation.extraFlags) {
        delete result.quotation.extraFlags.reset;
      }
      callback(result);
    });
  }).catch((err) => {
    logger.error('Quotation :: resetQuot :: Failed to reset quotation\n', err);
    callback({ success: false });
  });
};

module.exports.updateRiderList = function (data, session, callback) {
  const {quotation, newRiderList} = data;
  const plans = [quotation.plans[0]];
  getPlanDetails(session, quotation.baseProductCode).then(planDetails => {
    _.each(newRiderList, (covCode) => {
      if (covCode !== quotation.baseProductCode) {
        const existingPlan = _.find(quotation.plans, (plan) => plan.covCode === covCode);
        if (existingPlan) {
          plans.push(existingPlan);
        } else {
          plans.push({ covCode });
        }
      }
    });
    quotation.plans = plans;
    quote(data, session, callback);
  }).catch(err => {
    logger.error('Quotation :: updateRiderList :: Failed to update rider list\n', err);
    callback({ success: false });
  });
};

module.exports.getFundDetails = function (data, session, cb) {
  let {productId, invOpt, paymentMethod} = data;
  if (!paymentMethod) {
    cb({
      success: true,
      funds: []
    });
    return;
  }
  const covCode = getCovCodeFromProductId(productId);
  getPlanDetails(session, covCode).then((planDetails) => {
    let bpDetail = planDetails[covCode];
    return QuotUtils.getFunds(session.agent.compCode, bpDetail.fundList, paymentMethod, invOpt).then((funds) => {
      cb({
        success: true,
        funds: funds
      });
    });
  }).catch((error) => {
    logger.error('Quotation :: getFundDetails : failed to get funds\n', error);
    cb({ success: false });
  });
};

module.exports.allocFunds = function(data, session, callback) {
  let {invOpt, portfolio, fundAllocs, hasTopUpAlloc, topUpAllocs, adjustedModel} = data;
  getQuotation(data, session).then((quotation) => {
    return getPlanDetails(session, quotation.baseProductCode).then((planDetails) => {
      let paymentMethod = quotation.policyOptions.paymentMethod;
      let fundCodes = _.map(fundAllocs, (alloc, fundCode) => fundCode);
      fundCodes = _.filter(fundCodes, funcCode => (fundAllocs[funcCode] || (hasTopUpAlloc && topUpAllocs[funcCode])));
      return QuotUtils.getFunds(session.agent.compCode, fundCodes, paymentMethod, invOpt).then((funds) => {
        let selectedFunds = _.map(funds, (fund) => {
          return {
            fundCode: fund.fundCode,
            fundName: fund.fundName,
            alloc: fundAllocs[fund.fundCode],
            topUpAlloc: hasTopUpAlloc ? topUpAllocs[fund.fundCode] : null
          };
        });
        quotation.fund = {
          invOpt: invOpt,
          portfolio: invOpt === 'buildPortfolio' ? portfolio : null,
          funds: selectedFunds,
          adjustedModel
        };
        quote(data, session, callback);
      });
    });
  }).catch((error) => {
    logger.error('Quotation :: allocFunds :: failed to allocate funds\n', error);
    callback({ success: false });
  });
};

module.exports.updateClients = (data, session, callback) => {
  getQuotation(data, session).then((quotation) => {
    const requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
    return Promise.resolve(!requireFNA || validateClientFNAInfo(quotation.pCid)).then((completed) => {
      if (!completed) {
        callback({
          success: true,
          errorMsg: 'FNA is invalidated. Please visit and complete FNA again.'
        });
        return;
      }
      return getPlanDetails(session, quotation.baseProductCode).then((planDetails) => {
        return getAvailableInsureds(quotation, planDetails, requireFNA).then((availableInsureds) => {
          callback({
            success: true,
            availableInsureds: availableInsureds
          });
        });
      });
    });
  }).catch((error) => {
    logger.error('Quotation :: updateClients :: failed to update client info\n', error);
    callback({ success: false });
  });
};

module.exports.selectInsured = (data, session, callback) => {
  const {cid, covClass} = data;
  getQuotation(data, session).then((quotation) => {
    if (covClass) {
      const promises = [];
      promises.push(getProfile(quotation.pCid));
      promises.push(getProfile(cid));
      promises.push(getCompanyInfo());
      return Promise.all(promises).then(([proposer, insured, companyInfo]) => {
        return getPlanDetails(session, quotation.baseProductCode).then((planDetails) => {
          const basicPlan = planDetails[quotation.baseProductCode];
          quotation.insureds[cid] = QuotUtils.genBasicQuotation(basicPlan, session.agent, companyInfo, proposer, insured);
          quote({ quotation: quotation }, session, (result) => {
            const resultQuot = result.quotation || quotation;
            resultQuot.insureds[cid].plans[0].covClass = covClass;
            quote({ quotation: resultQuot }, session, callback);
          });
        });
      });
    } else {
      delete quotation.insureds[cid];
      quote(data, session, callback);
    }
  }).catch((error) => {
    logger.error('Quotation :: selectInsured :: failed to select insured\n', error);
    callback({ success: false });
  });
};

module.exports.updateShieldRiderList = (data, session, callback) => {
  const {quotation, newRiderList, cid} = data;
  const requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
  const subQuot = quotation.insureds[cid];
  const plans = [subQuot.plans[0]];
  return getPlanDetails(session, quotation.baseProductCode).then(planDetails => {
    _.each(newRiderList, (covCode) => {
      if (covCode !== subQuot.baseProductCode) {
        const existingPlan = _.find(subQuot.plans, (plan) => plan.covCode === covCode);
        if (existingPlan) {
          plans.push(existingPlan);
        } else {
          plans.push({ covCode });
        }
      }
    });
    subQuot.plans = plans;
    if (requireFNA && plans.length > 1) {
      needDao.getItem(quotation.pCid, needDao.ITEM_ID.FE).then((fe) => {
        if (_.get(fe, 'owner.aRegPremBudget')) {
          quote(data, session, callback);
        } else {
          callback({
            success: true,
            errorMsg: 'Cash Premium (RP Budget) is required for selected riders for AXA Shield.'
          });
        }
      });
    } else {
      quote(data, session, callback);
    }
  });
};

const saveQuotation = (session, quotation) => {
  return new Promise((resolve) => {
    logger.log('Quotation :: saveQuotation :: saving quotation');
    if (!quotation) {
      throw new Error('Quotation is empty');
    }
    quotation.type = 'quotation';
    quotation.lastUpdateDate = formatDatetime(new Date());
    if (!quotation.id) {
      quotDao.genQuotationId(session.agent.agentCode, (id) => {
        quotation.id = id;
        resolve(true);
      });
    } else {
      resolve(false);
    }
  }).then((isNewQuotation) => {
    return new Promise((resolve) => {
      if (quotation.clientChoice) {
        delete quotation.clientChoice;
      }
      if (session.platform && isNewQuotation) {
        if (quotation._rev) {
          delete quotation._rev;
        }
        if (quotation._id) {
          delete quotation._id;
        }
        if (quotation._attachments) {
          delete quotation._attachments;
        }
      }
      quotDao.upsertQuotation(quotation.id, quotation, (result) => {
        resolve(result);
      });
    }).then((result) => {
      if (!result || !result.rev) {
        throw new Error('Failed to get rev from saving quotation');
      }
      logger.log('Quotation :: saveQuotation :: saved quotation');
      session.quotation = quotation;
      return Promise.resolve(
        quotation.quickQuote || bundleDao.onSaveQuotation(session, quotation.pCid, quotation.id, isNewQuotation)
      ).then(() => result.rev);
    });
  });
};

const saveAttachment = (quotation, attId, rev, data) => {
  return quotDao.upsertAttachment(quotation.id, attId, rev, data).then((result) => {
    if (result && result.rev) {
      logger.log('Quotation :: saveAttachment :: saved proposal');
      return result.rev;
    } else {
      throw new Error('Failed to get rev from saving proposal');
    }
  });
};

const handleSave = (session, quotation, attachments) => {
  let promise;
  if (quotation.quickQuote) {
    promise = saveQuotation(session, quotation);
  } else {
    promise = bundleDao.updateStatus(quotation.pCid, bundleDao.BUNDLE_STATUS.HAVE_BI).then((bundle) => {
      quotation.bundleId = bundle && (bundle.id || bundle._id);
      return saveQuotation(session, quotation);
    });
  }
  _.each(attachments, (att) => {
    logger.log('Quotation :: handleSave :: saving attachment:', att.saveAttId);
    promise = promise.then((rev) => {
      return saveAttachment(quotation, att.saveAttId, rev, att.data);
    });
  });
  return promise;
};

const getProposalFuncPerms = (session, quotation, readonly) => {
  logger.log('Quotation :: getProposalFuncPerms :: checking for function permissions');
  if (readonly) {
    logger.log('Quotation :: getProposalFuncPerms :: readonly');
    return Promise.resolve({
      requote: false,
      clone: false
    });
  }
  if (quotation.quickQuote) {
    logger.log('Quotation :: getProposalFuncPerms :: quickquote');
    return Promise.resolve({
      requote: false,
      clone: true
    });
  } else {
    logger.log('Quotation :: getProposalFuncPerms :: other shit');
    return bundleDao.getProposalFuncPerms(quotation.pCid, quotation.id, session.agent.channel.type === 'FA');
  }
};

/** DISCARD */
const validateQuotation = (agent, quotation) => {
  // var requireFNA = agent.channel.type === 'AGENCY' && !quotation.quickQuote;
  // if (!requireFNA) {
    return Promise.resolve(true);
  // } else {
  //  return prodDao.getProductSuitability().then((suitability) => {
  //    return needDao.getItem(quotation.pCid, needDao.ITEM_ID.FE).then((fe) => {
  //      return getQuotDriver().validateQuotSuitability(suitability, { fe: fe }, quotation);
  //    });
  //  });
  // }
};

var prepareProposal = function (data, session, cb, createProposal, readonly) {
  const {agent} = session;
  getQuotation(data, session).then((quotation) => {
    return getPlanDetailsByQuot(session, quotation).then((planDetails) => {
      return new Promise(function(resolve, reject) { return resolve(null)}).then((appId) => {
        logger.log('Quotation :: prepareProposal :: begin preparing pdfs');
        return session.agent.channel.type === 'AGENCY' ? quotation.quickQuote : !appId;
      }).then((standalone) => {
        return ProposalUtils.prepareProposalData(quotation, planDetails, false).then((proposalData) => {
          const {illustrations, errorMsg, warningMsg} = proposalData;
          if (errorMsg) {
            cb({ success: true, errorMsg });
            return;
          }
          let promise;
          if (createProposal) {
            promise = ProposalUtils.genAttachments(agent, quotation, planDetails, standalone, proposalData);
          } else {
            promise = ProposalUtils.getAttachments(agent, quotation, planDetails, standalone, null, true);
          }
          return promise.then((attachments) => {
            const saveAttachments = _.filter(attachments, (att) => !!att.saveAttId);
            return Promise.resolve(
              createProposal
              // Well, no need to save
              // && handleSave(session, quotation, saveAttachments)
            ).then(() => {
              return getProposalFuncPerms(session, quotation, readonly).then((funcPerms) => {
                const pdfs = [];
                _.each(attachments, (att) => {
                  if (!att.hidden) {
                    const pdf = {
                      id: att.id,
                      tabLabel: att.tabLabel,
                      label: att.label,
                      allowSave: !!att.allowSave && !readonly,
                      fileName: att.fileName
                    };
                    // Add data for response, replace save func
                    _.setWith(pdf, `data`, att.data, typeof att.data);
                    if (att.attId) {
                      pdf.docId = quotation.id;
                      pdf.attachmentId = att.attId;
                    } else if (att.data) {
                      pdf.data = att.data;
                    }
                    pdfs.push(pdf);
                  }
                });
                cb({
                  success: true,
                  proposal: pdfs,
                  quotation: quotation,
                  illustrateData: illustrations,
                  planDetails: trimClientPlanDetails(planDetails),
                  warningMsg: warningMsg,
                  funcPerms: funcPerms
                });
              });
            });
          });
        });
      });
    });
  }).catch((error) => {
    logger.error('Quotation :: prepareProposal :: failed to get quotation\n', error);
    cb({ success: false });
  });
};

module.exports.genProposal = function (data, session, cb) {
  getQuotation(data, session).then((quotation) => {
    return validateQuotation(session.agent, quotation);
  }).then((validResult) => {
    if (validResult === true) {
      prepareProposal(data, session, cb, true);
    } else {
      cb({
        success: true,
        errorMsg: validResult || 'Please change the selected product/payment mode or input the relevant budget in FNA.'
      });
    }
  }).catch((err) => {
    logger.error('Quotation :: genProposal :: failed to gen proposal\n', err);
    cb({ success: false });
  });
};

module.exports.getProposal = function (data, session, cb) {
  prepareProposal(data, session, cb, false, data.readonly);
};

module.exports.getEmailTemplate = function (data, session, callback) {
  const {standalone} = data;
  quotDao.getEmailTemplate((result) => {
    if (result && !result.error) {
      callback({
        success: true,
        clientSubject: result.clientSubject,
        clientContent: standalone ? result.clientContentStandalone : result.clientContent,
        agentSubject: result.agentSubject,
        agentContent: standalone ? result.agentContentStandalone : result.agentContent,
        embedImgs: result.embedImgs
      });
    } else {
      callback({ success: false });
    }
  });
};

module.exports.emailProposal = function (data, session, callback) {
  const {agent} = session;
  const {emails, authToken, webServiceUrl} = data;
  if (emails) {
    getQuotation(data, session).then((quotation) => {
      return bundleDao.getAppIdByQuotId(quotation.pCid, quotation.id).then((appId) => {
        getApplication(appId, (application) => {
          let standalone = session.agent.channel.type === 'AGENCY' ? quotation.quickQuote : !appId;
          let isShield = _.get(quotation, 'quotType') === 'SHIELD';
          let isProposalSigned = _.get(application, 'isProposalSigned');
          let requiredPdfs = {};
          _.each(emails, (email) => {
            _.each(email.attachmentIds, (id) => {
              requiredPdfs[id] = true;
            });
          });
          return getPlanDetailsByQuot(session, quotation).then((planDetails) => {
            return ProposalUtils.getAttachments(agent, quotation, planDetails, standalone, _.keys(requiredPdfs)).then((attachments) => {
              return getCompanyInfo().then((companyInfo) => {
                _.each(emails, (email) => {
                  const allAttachments = _.map(email.attachmentIds, (attId) => {
                    const attachment = _.find(attachments, att => att.id === attId);
                    let attData = attachment.data;
                    let isProtect =false;
                    if (isShield) {
                      const agentCode = quotation.agent.agentCode;
                      const dob = parseDate(quotation.pDob);
                      attachment.agentPassword = agentCode.replace(/ /g, '').substr(agentCode.length - 6, 6).toLowerCase();
                      attachment.clientPassword = dob.format('ddmmmyyyy').toUpperCase();
                    }

                    if ((!standalone && !isShield) || (!standalone && isShield && isProposalSigned)) {
                      if (email.id === 'agent' && attachment.agentPassword) {
                        isProtect = true;
                        attData = CommonFunctions.protectBase64Pdf(attachment.data, attachment.agentPassword);
                      } else if (email.id === 'client' && attachment.clientPassword) {
                        isProtect = true;
                        attData = CommonFunctions.protectBase64Pdf(attachment.data, attachment.clientPassword);
                      }
                    }
                    return {
                      isProtectBase: isProtect,
                      fileName: attachment.fileName || attachment.id + '.pdf',
                      data: attData
                    };
                  });
                  _.each(email.embedImgs, (value, key) => {
                    allAttachments.push({
                      fileName: key,
                      cid: key,
                      data: value
                    });
                  });
                  EmailUtils.sendEmail({
                    from: companyInfo.fromEmail,
                    to: _.join(email.to, ','),
                    title: email.title,
                    content: email.content,
                    attachments: allAttachments,
                    userType: email.id,
                    agentCode: email.agentCode,
                    dob: email.dob,
                    isQQ: email.isQQ,
                  }, (result) => {
                    logger.log('Quotation :: emailProposal :: email sent');
                  }, authToken, webServiceUrl);
                });
                callback({ success: true });
              });
            });
          });
        });
      });
    }).catch((error) => {
      logger.error('Quotation :: emailProposal :: failed to prepare proposal pdfs\n', error);
      callback({ success: false });
    });
  } else {
    callback({ success: true });
  }
};

module.exports.getPdfToken = (data, session, callback) => {
  const {docId, attachmentId} = data;
  TokenUtils.getPdfToken(docId, attachmentId, session).then((token) => {
    callback({
      success: true,
      token
    });
  }).catch((error) => {
    logger.error('Quotation :: getPdfToken :: failed to get pdf token\n', error);
    callback({ success: false });
  });
};

// module.exports.alterate = function(data, session, callback) {

//   // var allocations = data.quotation.allocations;
//   var newAlters = data.newAlters;

//   getPlanDetails(session, data.quotation.baseProductId, (basicPlanCode, planDetails) => {
//     if (!session.planDetails[basicPlanCode]) {
//       session.planDetails = planDetails;
//     }

//     var validCodes = []
//     var plan = planDetails[data.planCode];
//     for (var f in plan.alterationList) {
//       var alter = plan.alterationList[f];
//       validCodes.push(alter.alterationCode);
//     }

//     var errors = {};
//     var alteration = [];

//     logger.log('update alteration:', validCodes);

//     for (var n = 0; n < newAlters.length; n++) {
//       alter = newAlters[n];
//       var error = false;
//       if (validCodes.indexOf(alter.alterationCode) >= 0) {
//         for (var m=n+1; m < newAlters.length; m++) {
//           var calter = newAlters[m];
//           if (alter.alterationCode == calter.alterationCode &&
//             ((alter.fromYear <= calter.fromYear && calter.fromYear <= alter.toYear)
//             || (alter.fromYear <= calter.toYear && calter.toYear <= alter.toYear )
//             || (calter.fromYear <= alter.fromYear && alter.fromYear <= calter.toYear)
//             || (calter.fromYear <= alter.toYear && alter.toYear <= calter.toYear)
//             || alter.fromYear > alter.toYear)) {
//             error = "invalid_alteration_period"
//             break;
//           }
//         }
//       } else {
//         error = ["invalid_alteration_code", alter.alterationCode];
//       }
//       if (isNaN(alter.amount) || alter.amount == 0) {
//         error = ["invalid_alteration_amount", alter.amount];
//       }
//       if (!error) {
//         alteration.push(alter);
//       } else {
//         errors[n] = error;
//       }
//     }
//     data.error = JSON.stringify(errors);
//     logger.log('update alteration:', errors);
//     if (Object.keys(errors).length == 0) {
//       data.quotation.alteration = alteration;
//     }
//     quote(data, session, callback);
//   });
// };

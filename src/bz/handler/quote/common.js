
// const clientDao     = require('../../cbDao/client');
// const companyDao    = require('../../cbDao/company');
// const needDao       = require('../../cbDao/needs');
// const prodDao       = require('../../cbDao/product');

const staticJson = require('../../../../static/Directory');

const QuotDriver    = require('../../Quote/QuotDriver');
const ShieldQuotDriver = require('../../Quote/ShieldQuotDriver');

const logger = global.logger || console;

const getQuotDriver = (quotation) => {
  return quotation && quotation.quotType === 'SHIELD' ?
      new ShieldQuotDriver() : new QuotDriver();
};

const getProfile = (profileId) => {
  /** 
  if (!profileId) {
    return Promise.resolve(null);
  } else {
    // return clientDao.getProfile(profileId, true);
    */
    return staticJson.profile;
  // }
};

const getCompanyInfo = () => {
  // return new Promise(companyDao.getCompanyInfo);
  return new Promise( function (callback) { callback(staticJson.companyInfo);});
};

const getFNAInfo = function (cid) {
  const promises = [];
  promises.push(needDao.getItem(cid, needDao.ITEM_ID.PDA));
  promises.push(needDao.getItem(cid, needDao.ITEM_ID.FE));
  promises.push(needDao.getItem(cid, needDao.ITEM_ID.NA));
  return Promise.all(promises).then(([pda, fe, fna]) => {
    return {pda, fe, fna};
  });
};

const validateClient = function (client) {
  // R-eBI-P1-1 completeness of client profile
  return !!(client.dob && client.gender && client.residenceCountry && client.nationality && client.industry && client.occupation && client.isSmoker);
};

const validateFNAInfo = function (fnaInfo) {
  // R-eBI-P1-2 completeness of FNA
  return fnaInfo &&
    fnaInfo.pda && fnaInfo.pda.isCompleted &&
    fnaInfo.fe && fnaInfo.fe.isCompleted &&
    fnaInfo.fna && fnaInfo.fna.isCompleted;
};

const validateClientFNAInfo = function (cid) {
  const promises = [];
  promises.push(needDao.getItem(cid, needDao.ITEM_ID.PDA));
  promises.push(needDao.getItem(cid, needDao.ITEM_ID.FE));
  promises.push(needDao.getItem(cid, needDao.ITEM_ID.NA));
  return Promise.all(promises).then(([pda, fe, fna]) => validateFNAInfo({pda, fe, fna}));
};

/**
 * Checks the eligibility and suitability of the product.
 *
 * @param {*} agent
 * @param {*} productId
 * @param {*} proposer
 * @param {*} insured
 * @param {*} fnaInfo
 */
const checkAllowQuotProduct = function (agent, productId, proposer, insured, fnaInfo) {
  return prodDao.getProductSuitability().then((suitability) => {
    return new Promise((resolve) => {
      prodDao.canViewProduct(productId, proposer, insured, resolve);
    }).then((product) => {
      if (!product) {
        logger.log('Products :: checkAllowQuotProduct :: product not found');
        return false;
      }
      var result = new QuotDriver().filterProducts(suitability, [product], agent, [proposer, insured], {
        optionsMap: global.optionsMap
      });
      if (!result || !result.productList || !result.productList.length) {
        logger.log('Products :: checkAllowQuotProduct :: product filtered out');
        return false;
      }
      if (fnaInfo && !new QuotDriver().validateProdSuitability(suitability, fnaInfo, product, [proposer, insured])) {
        logger.log('Products :: checkAllowQuotProduct :: product is not suitable');
        return false;
      }
      return true;
    });
  });
};

module.exports = {
  getQuotDriver,
  getProfile,
  getCompanyInfo,
  getFNAInfo,
  validateClient,
  validateFNAInfo,
  validateClientFNAInfo,
  checkAllowQuotProduct
};

const _ = require('lodash');
const _getOr = require('lodash/fp/getOr');
const moment = require('moment');
const logger = global.logger || console;

const commonApp = require('../common');
const _application = require('./application');
const _signature = require('./signature');
const _payment = require('./payment');
const _submission = require('./submission');
const dao = require('../../../cbDaoFactory').create();
const bDao = require('../../../cbDao/bundle');
const cDao = require('../../../cbDao/client');
const nDao = require('../../../cbDao/needs');
const quotDao = require('../../../cbDao/quotation');
const appDao = require('../../../cbDao/application');

var _getStepperSections = function() {
  return {
    items: [
      {
        'id': 'stepApp',
        'type': 'section',
        'title': 'Application',
        'detailSeq': 1,
        'items': [
          {
            'id': 'app',
            'type': 'shieldAppForm'
          }
        ]
      },
      {
        'id': 'stepSign',
        'type': 'section',
        'title': 'Signature',
        'detailSeq': 2,
        'items': [
          {
            'id': 'sign',
            'type': 'shieldSignature'
          }
        ]
      },
      {
        'id': 'stepPay',
        'type': 'section',
        'title': 'Payment',
        'detailSeq': 3,
        'items': [
          {
            'id': 'pay',
            'type': 'shieldPayment'
          }
        ]
      },
      {
        'id': 'stepSubmit',
        'type': 'section',
        'title': 'Submit',
        'detailSeq': 4,
        'items': [
          {
            'id': 'submit',
            'type': 'shieldSubmission'
          }
        ]
      }
    ]
  };
};

var _genParentApplicationDoc = function (session, quotation, bundle, sections, template) {
  let now = moment().toISOString();
  let quotationId = _.get(quotation, 'id');
  let pCid = _.get(quotation, 'pCid');
  let insureds = _.keys(quotation.insureds);
  let iCids = [];
  let isPhSameAsLa = false;
  let ccy = '';
  _.forEach(insureds, (iCid) => {
    if (pCid === iCid) {
      isPhSameAsLa = true;
    } else {
      iCids.push(iCid);
    }
    let subQuotation = quotation.insureds[iCid];
    if (ccy.length === 0) {
      ccy = subQuotation.ccy;
    }
  });

  let iCidMapping = {};
  _.forEach(iCids, (iCid) => {
    iCidMapping[iCid] = [];
  });
  if (isPhSameAsLa) {
    iCidMapping[pCid] = [];
  }

  let parentApplication = Object.assign({
    //Parent only
    id: '',
    type: 'masterApplication',
    childIds:[],
    iCids: iCids,
    iCidMapping: iCidMapping,
    appStep: 0,
    isPolicyNumberGenerated: false,
    isSubmittedStatus: false,
    isFailSubmit: false,
    isProposalSigned: false,
    isAppFormProposerSigned: false,
    isAppFormInsuredSigned: _.times(iCids.length, _.constant(false)),
    isStartSignature: false,
    isFullySigned: false,
    isMandDocsAllUploaded: false,
    signExpiryShown: false,
    applicationSubmittedDate: 0,
    applicationInforceDate: 0,
    quotation: quotation,
    payment: {},
    submission: {}
  }, {
    //Common
    bundleId: _.get(bundle, 'id'),
    pCid: _.get(quotation, 'pCid'),
    applicationForm:{
      values: {}
    },
    biSignedDate: 0,
    applicationSignedDate: 0,
    supportDocuments: {},
    isValid: true,
    isCrossAge: false,
    applicationStartedDate: now,
    lastUpdateDate: now,
    createDate: now,
    compCode: session.agent.compCode,
    dealerGroup: session.agent.channel.code,
    agentCode: session.agent.agentCode,
    agent: quotation.agent,
    quotationDocId: quotationId
  });

  let channel = session.agent ? session.agent.channel.code : '';
  let formValues = parentApplication.applicationForm.values;
  let oneyearpremium = quotation.premium;
  let payMode = quotation.paymentMode;
  switch (payMode) {
    case 'S':
      oneyearpremium = quotation.premium * 2;
      break;
    case 'Q':
      oneyearpremium = quotation.premium * 4;
      break;
    case 'M':
      oneyearpremium = quotation.premium * 12;
      break;
    default:
      oneyearpremium = quotation.premium;
  }
  let extra = {
    isPhSameAsLa: isPhSameAsLa ? 'Y' : 'N',
    ccy: ccy,
    channel: commonApp.getChannelType(channel),
    channelName: channel,
    premium: oneyearpremium
  };

  let laProfilePromise = [];

  _.forEach(iCids, (iCid) => {
    if (!isPhSameAsLa || iCid !== pCid) {
      laProfilePromise.push(new Promise((resolve, reject) => {
        //getLAProfile
        cDao.getClientById(iCid, function(laDoc) {
          if (laDoc && !laDoc.error) {
            laDoc.cid = laDoc._id;
            let laValue = {
              personalInfo: laDoc,
              declaration: {},
              policies: {
                ROP_01: ''
              },
              extra: Object.assign({}, extra, {isCompleted: false}),
              insurability: {
                'LIFESTYLE01': laDoc.isSmoker
              }
            };

            //handleLaRop
            if (extra.channel !== 'FA'){
              let ccRopId = _.get(quotation, 'clientChoice.recommendation.rop_shield.ropBlock.iCidRopAnswerMap.' + iCid);
              laValue.policies.ROP_01 = _.get(quotation, 'clientChoice.recommendation.rop_shield.ropBlock.' + ccRopId);
            }
            resolve(laValue);
          } else {
            reject();
          }
        });
      }));
    }
  });

  return Promise.all(laProfilePromise).then((laValues) => {
    formValues.insured = laValues;

    //getPHProfile
    return new Promise((resolve) => {
      cDao.getClientById(parentApplication.pCid, function(phDoc) {
        let hasTrustedIndividual = 'N';
        dao.getDoc(bundle.fna[nDao.ITEM_ID.PDA], (pdaDoc) => {
          if (pdaDoc && !pdaDoc.error) {
            logger.log('INFO: genApplicationDoc - getPHProfile - populate PDA data', quotationId);
            hasTrustedIndividual = _.get(pdaDoc, 'trustedIndividual', 'N');
          }

          commonApp.removeInvalidProfileProp(phDoc);
          phDoc.cid = phDoc._id || phDoc.id;

          //populate personal info
          formValues.proposer = {
            personalInfo: phDoc,
            declaration: {
              trustedIndividuals: (hasTrustedIndividual === 'Y' ? phDoc.trustedIndividuals : {}),
              ccy: quotation.ccy
            },
            policies: isPhSameAsLa ? {ROP_01: ''} : {},
            extra: Object.assign({}, extra, {
              hasTrustedIndividual
            }),
            insurability: {
              'LIFESTYLE01': phDoc.isSmoker
            }
          };

          //handlePhRop
          if (extra.channel !== 'FA'){
            let ccRopId = _.get(quotation, 'clientChoice.recommendation.rop_shield.ropBlock.iCidRopAnswerMap.' + pCid);
            formValues.proposer.policies.ROP_01 = _.get(quotation, 'clientChoice.recommendation.rop_shield.ropBlock.' + ccRopId);
          }
          resolve();
        });
      });
    });
  }).then(() => {
    //setExtraProp
    let phDependants = formValues.proposer.personalInfo.dependants;

    //set relations
    _.forEach(phDependants, (dep) => {
      let insured = _.filter(formValues.insured, (la) => {
        return la.personalInfo.cid === dep.cid;
      });

      if (_.get(insured, '[0].personalInfo')) {
        insured[0].personalInfo.relationship = dep.relationship;
        insured[0].personalInfo.relationshipOther = dep.relationshipOther;
      }
    });
  }).then((res) => {
    //handlePlans
    logger.log('INFO: genApplicationDoc - handlePlans', quotationId);

    let quotationInsuredArray = _.get(quotation, 'insureds');
    let formValuesInsuredArray = _.get(formValues, 'insured') || {};
    let quotationCids = Object.keys(quotationInsuredArray);

    let formValuesIndexCidMapping = {};

    formValuesInsuredArray.forEach((insured, index) => {
      let formValuesCid = _.get(insured, 'personalInfo.cid');
      formValuesIndexCidMapping[formValuesCid] = index;
    });

    let planDetails, groupedPlanDetails;
    _.each(quotationCids, (cid, key) => {
      planDetails = commonApp.transformQuotationToPlanDetails(quotation, cid, false);
      groupedPlanDetails = commonApp.transformQuotationToPlanDetails(quotation, cid, true);
      if (_.get(quotation, 'pCid') === cid ) {
        formValues.proposer.planDetails = planDetails;
        formValues.proposer.groupedPlanDetails = groupedPlanDetails;
      } else if (_.isNumber(formValuesIndexCidMapping[cid]) && formValues.insured[formValuesIndexCidMapping[cid]]) {
        formValues.insured[formValuesIndexCidMapping[cid]].planDetails = planDetails;
        formValues.insured[formValuesIndexCidMapping[cid]].groupedPlanDetails = groupedPlanDetails;
      }
    });

    return new Promise((resolve) => {
      dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, (mapping) => {
        // set extra value such as channel
        formValues.appFormTemplate = mapping[quotation.baseProductCode].appFormTemplate;
        let app = parentApplication;

        commonApp.getOptionsList(app, template, () => {
          //Remove generated values which is not found in template
          let appTemplate = template.items[0].items[0]; //contain items of menuSection
          let appValuesOrg = app.applicationForm.values;
          let appValuesTemp = _.cloneDeep(app.applicationForm.values);
          let appValuesNew = _.cloneDeep(app.applicationForm.values);
          let trigger = {};

          //for create trigger object using template
          logger.log('INFO: _genParentApplicationDoc - replaceAppFormValuesByTemplate start', quotationId);
          commonApp.replaceAppFormValuesByTemplate(appTemplate, appValuesOrg, appValuesTemp, trigger, [], 'en', null);
          logger.log('INFO: _genParentApplicationDoc - replaceAppFormValuesByTemplate end', quotationId);

          //remove appForm values which is not found in template + trigger
          for (let key in appValuesOrg) {
            if (key === 'proposer') {
              logger.log('INFO: _genParentApplicationDoc - removeAppFormValuesByTrigger: handle', key, quotationId);
              commonApp.removeAppFormValuesByTrigger(key, appValuesOrg[key], appValuesNew[key], appValuesOrg[key], trigger[key], false,appValuesOrg);
            } else if (key === 'insured') {
              for (let i = 0; i < appValuesOrg[key].length; i ++) {
                let iOrgValues = appValuesOrg[key][i];
                let iNewValues = appValuesNew[key][i];
                let iTrigger = trigger[key][i];
                logger.log('INFO: _genParentApplicationDoc - removeAppFormValuesByTrigger: handle', key, i, quotationId);
                commonApp.removeAppFormValuesByTrigger(key, iOrgValues, iNewValues, iOrgValues, iTrigger, false,appValuesOrg);
              }
            }
          }

          app.applicationForm.values = appValuesNew;

          logger.log('INFO: genApplicationDoc - end', quotationId);
          resolve(app);
        });
      });
    });

  });
};

var _genChildApplicationDocs = function (session, pApplication, quotation, bundle, sections, template) {
  let now = moment().toISOString();

  let childApplicationDefault = Object.assign({
    id: '',
    type: 'application',
    parentId: '',
    parentAttachmentIds: [],
    iCid: '',
    policyNumber: '',
    isApplicationSigned: false,
    isMandDocsUploaded: false,
    isBackDate: false,
    appFormAttachmentId: ''
  }, {
    //Common
    bundleId: _.get(bundle, 'id'),
    pCid: pApplication.pCid,
    applicationForm:{
      values: {
        proposer:{},
        insured:[]
      }
    },
    quotation: {},
    isValid: true,
    isCrossAge: false,
    supportDocuments: {},
    payment: {},
    biSignedDate: 0,
    applicationSignedDate: 0,
    applicationStartedDate: now,
    lastUpdateDate: now,
    createDate: now,
    compCode: session.agent.compCode,
    dealerGroup: session.agent.channel.code,
    agentCode: session.agent.agentCode,
    agent: quotation.agent,
    quotationDocId: quotation.id
  });

  let childApplications = [];
  let cidsOfLA = [];

  if (_.get(pApplication, 'applicationForm.values.proposer.extra.isPhSameAsLa') === 'Y') {
    //Split quotation to 2 applications
    cidsOfLA.push(pApplication.pCid);
  }

  cidsOfLA = _.union(cidsOfLA, pApplication.iCids);

  _.forEach(cidsOfLA, (iCid) => {
    //Split quotation to 2 applications
    let subQuotPlans = _getOr([], `insureds.${iCid}.plans`, quotation);
    let needToSplit = _.filter(subQuotPlans, obj => {return obj.covCode !== 'ASIM';}).length > 0;

    let childApp = _.cloneDeep(childApplicationDefault);
    childApp.iCid = iCid;

    //child quotation with specific iCid
    let childQuot = _.cloneDeep(quotation);
    childQuot.insureds = _.cloneDeep(_.get(quotation, `insureds[${iCid}]`, {}));

    childApp.quotation = childQuot;
    childApp.applicationForm.values.planDetails = commonApp.transformQuotationToPlanDetails(quotation, iCid, false, true);
    childApplications.push(childApp);

    if (needToSplit) {
      childApp = _.cloneDeep(childApplicationDefault);
      childApp.iCid = iCid;
      childApp.quotation = childQuot;
      childApp.applicationForm.values.planDetails = commonApp.transformQuotationToPlanDetails(quotation, iCid, false, false);
      childApplications.push(childApp);
    }
  });

  _application.saveParentApplicationValuesToChild(pApplication, childApplications);

  return [pApplication, ...childApplications];
};

//=ApplicationHandler.apply
module.exports.applyAppForm = function(data, session, callback) {
  let isFaChannel = session.agent.channel.type === 'FA';
  let { quotId } = data;
  let result = {
    success: false,
    stepper: {
      sections: _getStepperSections(),
      index: {
        current: 0,
        completed: -1,
        active: 0
      },
      enableNextStep: false
    },
    template: {},
    application: {}
  };

  let cache = {
    bundle: {},
    quotation: {},
    profile: {},
    template: {}
  };

  logger.log('INFO: applyAppForm - start', quotId);
  if (!quotId) {
    logger.log('INFO: applyAppForm - end [RETURN=-100]', quotId);
    // callback({success: false, error:'quotId null'});
    return;
  }

  new Promise((resolve, reject) => {
    quotDao.getQuotation(quotId, (quot) => {
      if (quot && !quot.error) {
        cache.quotation = quot;
        resolve(quot);
      } else {
        reject(new Error('Fail to get Quotation ' + _.get(quot, 'error')));
      }
    });
  }).then((quotation) => {
    return bDao.getCurrentBundle(_.get(quotation, 'pCid')).then((bundle) => {
      if (bundle && !bundle.error) {
        cache.bundle = bundle;
        //getOptionsList will be run in below step
        return _application.getAppFormTemplate(quotation).then((templateRes) => {
          if (bundle.isFnaReportSigned) {
            logger.log('INFO: applyAppForm - frozenAppFormTemplateFnaQuestions', quotId);
            commonApp.frozenAppFormTemplateFnaQuestions(templateRes.template);
          }
          return templateRes;
        });
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then((templateRes) => {
    return _genParentApplicationDoc(session, cache.quotation, cache.bundle, templateRes.sections, templateRes.template).then((pApplication) => {
      return commonApp.populateFromBundle(pApplication, templateRes.sections, isFaChannel).then((pApplication) => {
        return new Promise((resolve) => {
          commonApp.getOptionsList(pApplication, templateRes.template, () => {
            result.template = templateRes.template;
            result.application = pApplication;
            resolve(pApplication);
          });
        });
      });
    }).then((pApplication) => {
      return _genChildApplicationDocs(session, pApplication, cache.quotation, cache.bundle, templateRes.sections, templateRes.template);
    });
  }).then((applications) => {
    if (applications.length === 0) {
      throw new Error('Fail to Generate Parent and Child Doc ' + quotId);
    }

    return new Promise((resolve) => {
      commonApp.getShieldApplicationIds(session, applications.length).then((ids) => {
        _.forEach(ids, (id, i) => {
          logger.log('INFO: applyAppForm - getShieldApplicationIds', quotId, '->', id);
          applications[i].id = id;
        });

        resolve(applications);
      });
    }).then((apps) => {
      let parentApplications = _.filter(apps, (app) => { return app.hasOwnProperty('childIds'); });
      let childApplications = _.filter(apps, (app) => { return app.hasOwnProperty('parentId'); });
      let parentApplication = parentApplications[0];

      let parentAppId = parentApplication.id;
      let clientAppIds = childApplications.map((app) => {
        let mapping = _.get(parentApplication, 'iCidMapping[' + app.iCid + ']');
        if (mapping instanceof Array) {
          let isAXAShield = (_.filter(_.get(app, 'applicationForm.values.planDetails.planList'), obj => {return obj.covCode !== 'ASIM';}).length > 0 ) ? false : true;
          mapping.push({
            applicationId: app.id,
            policyNumber: '',
            covCode:  (isAXAShield) ? 'ASIM' : 'ASP'
          });
        }
        return app.id;
      });

      parentApplication.childIds = clientAppIds;
      _.forEach(childApplications, (app) => {
        app.parentId = parentAppId;
        if (app.iCid === app.pCid) {
          app.appFormAttachmentId = 'appPdf';
        } else {
          app.appFormAttachmentId = 'appPdf' + app.iCid;
        }
      });

      return parentApplications.concat(childApplications);
    });
  }).then((applications) => {
    let updAppPromises = applications.map((app) => {
      return new Promise((resolve, reject) => {
        appDao.updApplication(app.id, app, (res) => {
          if (res && !res.error) {
            resolve(res);
          } else {
            reject(_.get(res, 'error'));
          }
        });
      });
    });
    return Promise.all(updAppPromises);
  }).then((res) => {
    dao.updateViewIndex('main', 'summaryApps');
    dao.updateViewIndex('main', 'quotationByAgent');
    return bDao.onCreateApplication(_.get(cache, 'quotation.pCid'), _.get(cache, 'quotation.id'), result.application.id);
  }).then((upRes) => {
    logger.log('INFO: applyAppForm - update profile application count', quotId, upRes);
    if (upRes && !upRes.error) {
      return cDao.getProfile(_.get(cache, 'quotation.pCid'), true).then((profile)=>{
        cache.profile = profile;
        return bDao.getApplyingApplicationsCount(result.application.bundleId);
      }).then((count)=>{
        let pCid = _.get(cache, 'quotation.pCid');
        cache.profile.applicationCount = count;
        return cDao.updateProfile(pCid, cache.profile);
      });
    } else {
      throw new Error('Fail to update Bundle ' + _.get(cache.bundle, 'id') + ' ' + _.get(upRes, 'error'));
    }
  }).then((res)=>{
    if (res && !res.error) {
      dao.updateViewIndex('main', 'contacts');
      result.application.agentChannelType = isFaChannel ? 'Y' : 'N';

      result.success = true;

      logger.log('INFO: applyAppForm - end [RETURN=100]', quotId);
      callback(Object.assign(result, {success: true}));
    } else {
      throw new Error('Fail to update Profile ' + _.get(cache.profile, 'id') + ' ' + _.get(res, 'error'));
    }
  }).catch((error) => {
    logger.error('ERROR: applyAppForm - end [RETURN=-101]', quotId, error);
    // callback({success: false});
  });
};

const _calcApplicationActiveStep = function(app) {
  let appActiveStep = commonApp.EAPP_STEP.APPLICATION;

  if (_.get(app, 'applicationForm.values.proposer.extra.isCompleted', false)) {
    //Check step application
    appActiveStep++;
  }

  if (appActiveStep === commonApp.EAPP_STEP.SIGNATURE && app.isAppFormProposerSigned) {
    //Check step signature
    appActiveStep++;
  }

  if (appActiveStep === commonApp.EAPP_STEP.PAYMENT && _.get(app, 'appCompletedStep', -1) === commonApp.EAPP_STEP.PAYMENT) {
    //Check step payment
    appActiveStep++;
  }

  return appActiveStep;
};

//=ApplicationHandler.goApplication
module.exports.continueApplication = function(data, session, callback) {
  let isFaChannel = session.agent.channel.type === 'FA';
  let { appId } = data;
  let result = {
    success: false,
    warningMsg: {
      msgCode: 0,
      showSignExpiryWarning: false
    },
    stepper: {
      sections: _getStepperSections(),
      index: {
        current: 0,
        completed: -1,
        active: 0
      },
      enableNextStep: false
    },
    template: {},
    application: {},
    signature: {}
  };
  // let crossAgeData = {appId: appId};

  logger.log('INFO: continueApplication - start', appId);
  if (!appId) {
    logger.log('INFO: continueApplication - end [RETURN=-100]', appId);
    // callback({success: false, error:'appId null'});
    return;
  }

  _application.getApplication(appId).then((app) => {
    result.application = app;

    let appStep = _.get(app, 'appStep', commonApp.EAPP_STEP.APPLICATION);
    let appCompletedStep = _.get(app, 'appCompletedStep', commonApp.EAPP_STEP.APPLICATION - 1);

    result.stepper.index.current = _.min([appCompletedStep + 1, appStep]);
    result.stepper.index.active = _.max([_calcApplicationActiveStep(app), appStep]);
    result.stepper.index.completed = appCompletedStep;

    return bDao.getCurrentBundle(_.get(app, 'pCid'));
  }).then((bundle) => {
    let continueAppStep = result.stepper.index.current;

    let initProcess = [];
    if (continueAppStep === commonApp.EAPP_STEP.APPLICATION) {
      initProcess.push(
        _application.getAppFormTemplateWithOptionList(session, result.application).then((templateRes) => {
          if (bundle.isFnaReportSigned) {
            logger.log('INFO: continueApplication - frozenAppFormTemplateFnaQuestions', appId);
            commonApp.frozenAppFormTemplateFnaQuestions(templateRes.template);
          }
          return Object.assign(templateRes, {success:true});
        })
      );
    } else if (continueAppStep === commonApp.EAPP_STEP.SIGNATURE) {
      if (session.platform) {
        initProcess.push(_signature.getSignatureInitPdfString(session, appId));
      } else {
        initProcess.push(_signature.getSignatureInitUrl(session, appId));
      }
    } else if (continueAppStep === commonApp.EAPP_STEP.PAYMENT) {
      initProcess.push(_payment.getPaymentTemplate(session, false, _.get(result.application, 'payment.totCashPortion', 0), isFaChannel));
    } else {
      initProcess.push(_submission.getSubmissionTemplate(session, false));
    }
    return Promise.all(initProcess);

  }).then((initResults) => {
    let continueAppStep = result.stepper.index.current;

    logger.log('INFO: continueApplication - initProcess completed', appId);
    if (initResults.length > 0 && !initResults[0].success) {
      throw new Error('Fail to init step ' + continueAppStep);
    }
    let initResult = initResults[0];


    if (continueAppStep === commonApp.EAPP_STEP.SIGNATURE) {
      result.signature = initResult.signature;
      result.warningMsg = Object.assign({}, initResult.warningMsg, {showSignExpiryWarning: false});
    } else {
      result.template = initResult.template;
    }

    if (continueAppStep !== commonApp.EAPP_STEP.APPLICATION) {
      return;
    }

    logger.log('INFO: continueApplication - process for step', continueAppStep, appId);
    return new Promise((resolve) => {
      if (initResult.sections) {
        return commonApp.populateFromBundle(result.application, initResult.sections, isFaChannel).then((fvApp)=>{
          result.application = fvApp;
          resolve(true);
        });
      } else {
        resolve(false);
      }
    }).then((isGetPolicyNumber) => {
      if (isGetPolicyNumber) {
        //getPNumber
        return new Promise((resolve) => {
          let values = result.application.applicationForm.values;

          logger.log('INFO: continueApplication - getPolicyNumber isGen=', result.application.isPolicyNumberGenerated, 'for', appId);
          if (result.application.isPolicyNumberGenerated) {
            resolve();
          } else {
            //determinate if next menu is insurability
            let menus = values.menus || [];
            let checkedMenu = values.checkedMenu || [];
            let completedMenus = values.completedMenus || [];
            let targetMenu = 'menu_insure';
            let toGetPolicyNumber = false;

            _.forEach(menus, (menu, index) => {
              if (checkedMenu.indexOf(menu) === -1 || completedMenus.indexOf(menu) === -1) {
                if (menu === targetMenu) {
                  toGetPolicyNumber = true;
                }
                return false;
              }
            });

            logger.log('INFO: continueApplication - getPolicyNumber toGet=', toGetPolicyNumber ,'for', appId);
            if (toGetPolicyNumber) {
              _application.getPolicyNumbers(result.application).then((apps) => {
                result.application = apps.parentApp;

                let upChildPromise = apps.childApps.map((childApp) => {
                  return new Promise((upResolve, upReject) => {
                    childApp.lastUpdateDate = moment().toISOString();
                    appDao.updApplication(childApp.id, childApp, (res) => {
                      if (res && !res.error) {
                        upResolve(res);
                      } else {
                        upReject(new Error('Fail to update application ' + childApp.id + ' ' + _.get(res, 'error')));
                      }
                    });
                  });
                });

                return Promise.all(upChildPromise).then((upResults) => {
                  resolve();
                });
              });
            } else {
              resolve();
            }
          }
        });
      } else {
        return;
      }
    });
  }).then(() => {
    //callBack
    logger.log('INFO: continueApplication - callBack', appId);

    // delete appPdf if the values changed
    let signExpireWarningDay = global.config.signExpireWarningDay;
    logger.debug('DEBUG: check signature expiry for showing warning: ', result.application.biSignedDate , result.application.appStatus , result.application.signExpiryShown, appId);
    // check signature expiry for showing warning
    if (result.application.biSignedDate
      && !result.application.isSubmittedStatus
      && !result.application.signExpiryShown) {
      let expiryTime = moment().subtract(signExpireWarningDay, 'days');
      let biSignDate = moment(result.application.biSignedDate);
      if (biSignDate.valueOf() < expiryTime.valueOf()) {
        result.warningMsg.showSignExpiryWarning = true;
      }
    }

    return _application.checkCrossAge(result.application).then((caResp) => {
      if (caResp.success) {
        result.crossAge = caResp;
        if (caResp.status === commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED) {
          // result.warningMsg.showCrossedAgeSignedMsg = true;
          result.warningMsg.msgCode = caResp.status;
        }
        if (caResp.crossedAge) {
          result.application.isCrossAge = true;
        }

        if (_.get(caResp, 'crossedAgeCid.length', 0) > 0) {
          return caResp.crossedAgeCid;
        }
      }
      return [];
    }).then((crossedAgeCid) => {
      if (crossedAgeCid.length === 0) {
        return;
      }
      logger.log('INFO: continueApplication - checkCrossAge', appId, crossedAgeCid)

      let childIds = [];
      _.forEach(crossedAgeCid, (iCid) => {
        let mappingList = result.application.iCidMapping[iCid];
        _.forEach(mappingList, (mapping) => {
          childIds.push(mapping.applicationId);
        });
      });

      return _application.getChildApplications(childIds).then((childApps) => {
        return Promise.all(childApps.map((childApp) => {
          childApp.isCrossAge = true;
          childApp.lastUpdateDate = moment().toISOString();

          return new Promise((resolve, reject) => {
            appDao.upsertApplication(childApp._id, childApp, (resp) => {
              if (resp && !resp.error) {
                resolve();
              } else {
                reject(new Error('Fail to update Application ' + childApp._id + ' ' + _.get(resp, 'error')));
              }
            });
          });
        }));
      }).then((upResults) => {
        return;
      });
    });
  }).then(() => {
    result.application.lastUpdateDate = moment().toISOString();

    appDao.upsertApplication(result.application.id, result.application, (resp) => {
      if (resp && !resp.error) {
        result.application._rev = resp.rev;
        return;
      } else {
        throw new Error('Fail to update Application ' + result.application.id + ' ' + _.get(resp, 'error'));
      }
    });
  }).then(() => {
    result.application.agentChannelType = isFaChannel ? 'Y' : 'N';
    result.success = true;

    logger.log('INFO: continueApplication - end [RETURN=100]', appId);
    callback(result);
  }).catch((error) => {
    logger.error('ERROR: continueApplication - end [RETURN=-101]', appId, error);
    // callback({success: false});
  });
};

const _ = require('lodash');
const logger = global.logger || console;
const dao = require('../../../cbDaoFactory').create();
const fcRecommendationShieldId = 'fcRecommendation_shield';
const commonAppHandler = require('../common');


/*
  * MOVE TO COMMON FUNCTION
// has to make sure input parm arrOne & arrTwo are arrayType
var isTwoArraysEqual = function(arrOne, arrTwo) {
  if (!arrOne || !arrTwo) {
    return false;
  }
  return _.isEmpty(_.difference(arrOne, arrTwo)) && _.isEmpty(_.difference(arrTwo, arrOne));
};
*/

module.exports.getClientChoiceDefaultString_shield = function(insuredList) {
  logger.log('INFO: getClientChoiceDefaultString_shield - start');
  const lang = 'en';
  let defaultRecommendBenefit = '';
  let defaultRecommendLimit = '';
  let insuredListCopy = _.cloneDeep(insuredList);
  let insuredPlanListMap = {};

  let getInsuredNameList = function(currentInsuredCid) {
    let nameList = '';
    let samePlanInsuredList = [];

    _.forEach(insuredPlanListMap, (planList, iCid) => {
      if (commonAppHandler.isTwoArraysEqual(insuredPlanListMap[iCid], insuredPlanListMap[currentInsuredCid])) {
        samePlanInsuredList.push(insuredListCopy[iCid]);
      }
    });

    _.forEach(samePlanInsuredList, (insured, index) => {
      insured.hasRead = true;
      nameList += insured.iFullName;
      if (index !== samePlanInsuredList.length - 1) {
        nameList += ', ';
      }
    });
    return nameList;
  };

  return new Promise(function(resolve, reject) {
    dao.getDocFromCacheFirst(fcRecommendationShieldId, function(res){
      if (res && !res.error) {
        let defaultTextList = _.get(res, 'items', []);

        _.forEach(insuredListCopy, (insured, iCid) => {
          insuredPlanListMap[iCid] = [];
          _.forEach(_.get(insured, 'plans'), (plan) => {
            insuredPlanListMap[iCid].push(plan.planCode);
          });
        });

        _.forEach(insuredListCopy, (insured) => {
          // hasRead means this insured's data has been used.
          if (!insured.hasRead) {
            // get insured name list whose basic plan and riders and same
            let nameList = getInsuredNameList(insured.iCid);
            let objFoundText;
            defaultRecommendBenefit += nameList + '\n';
            defaultRecommendLimit += nameList + '\n';
            // get default text from doc
            _.forEach(defaultTextList, (objDefaultText) => {
              if (commonAppHandler.isTwoArraysEqual(objDefaultText.planList, insuredPlanListMap[insured.iCid])) {
                objFoundText = objDefaultText;
              }
            });
            if (objFoundText) {
              defaultRecommendBenefit += objFoundText.benefits[lang] + '\n\n';
              defaultRecommendLimit += objFoundText.limitations[lang] + '\n\n';
              /* 20171228 Does not need to check age under 1 for Shield
              if (_.get(insured, 'iAge') === 0) {
                defaultRecommendLimit += objFoundText.additionalLimitations[lang] + '\n\n';
              }
              */
            }
          }
        });
        logger.log('INFO: getClientChoiceDefaultString_shield - end', _.keys(insuredList));
        resolve({
          // hasClientChoice:hasClientChoice,
          defaultRecommendBenefit:_.trim(defaultRecommendBenefit),
          defaultRecommendLimit:_.trim(defaultRecommendLimit)
        });
      } else {
        logger.error('ERROR: getClientChoiceDefaultString_shield - get fcRecommendationShieldId fails', res);
        reject(new Error('Fail to get fcRecommendationShieldId' + _.get(res, 'error')));
      }
    });
  }).catch((error)=>{
    logger.error('ERROR: getClientChoiceDefaultString_shield - catched error', _.keys(insuredList), error);
  });
};

/** DO NOT REMOVE recommendationValues */
module.exports.setMenuItemValues_shield = function(menuValues, quot, recommendationValues) {
  const lang = 'en';
  let anyInsuredChoseROP = false;
  let insuredRopValue = '';
  let quotRopBlock = _.get(quot, 'clientChoice.recommendation.rop_shield.ropBlock');
  let shieldInsuredPlans = [];
  const planNameMapping = {
    ASIMSA: 'Shield Plan A',
    ASIMSB: 'Shield Plan B',
    ASIMSC: 'Shield Standard',
    ASPA: 'Basic Care A',
    ASPB: 'Basic Care B',
    ASPC: 'Basic Care Standard',
    ASGC: 'General Care',
    ASGCB: 'General Care',
    ASHCD: 'Home Care',
    ASHC: 'Home Care',
  };

  for (let i = 0; i < 6; i++) {
    insuredRopValue = _.get(quotRopBlock, 'shieldRopAnswer_' + i, '');
    if (insuredRopValue) {
      _.set(menuValues, 'ropBlock.shieldRopTable.shieldRopAnswer_' + i, insuredRopValue);
      /** DO NOT REMOVE recommendationValues */
      _.set(recommendationValues, 'rop_shield.ropBlock.shieldRopAnswer_' + i, insuredRopValue);
      if (insuredRopValue === 'Y') {
        anyInsuredChoseROP = true;
      }
    }
  }
  if (anyInsuredChoseROP) {
    _.set(menuValues, 'ropBlock.ropQ2', _.get(quotRopBlock, 'ropQ2'));
    _.set(menuValues, 'ropBlock.ropQ3', _.get(quotRopBlock, 'ropQ3'));
    _.set(menuValues, 'ropBlock.ropQ1sub3', _.get(quotRopBlock, 'ropQ1sub3'));
    /** DO NOT REMOVE recommendationValues */
    _.set(recommendationValues, 'rop_shield.ropBlock.ropQ2', _.get(quotRopBlock, 'ropQ2'));
    _.set(recommendationValues, 'rop_shield.ropBlock.ropQ3', _.get(quotRopBlock, 'ropQ3'));
    _.set(recommendationValues, 'rop_shield.ropBlock.ropQ1sub3', _.get(quotRopBlock, 'ropQ1sub3'));
  }

  _.forEach(_.get(quot, 'insureds'), (insured) => {
    /** DO NOT REMOVE iFullName, cid */
    const iFullName = _.get(insured, 'iFullName');
    const cid = _.get(insured, 'iCid');
    let arrPlanList = [];

    _.forEach(_.get(insured, 'plans'), (plan) => {
      // arrPlanList.push(planNameMapping[plan.planCode]);
      arrPlanList.push(_.get(plan, 'covName.' + lang, ''));
    });

    /** DO NOT REMOVE iFullName, cid */
    shieldInsuredPlans.push({
      shieldInsured: iFullName + ':',
      shieldPlan: _.join(arrPlanList, ', '),
      fullName: iFullName,
      cid
    });
  });
  menuValues.shieldInsuredPlans = shieldInsuredPlans;
  /** DO NOT REMOVE recommendationValues */
  recommendationValues.extra.shieldInsuredPlans = shieldInsuredPlans;
};

module.exports.setMenuItemTemplate_shield = function(menuTemplateCopy, iCidRopAnswerMap, insuredList) {
  let count = 0;
  let ropBlock = _.find(menuTemplateCopy.items, (block)=>{
    return block.id === 'ropBlock';
  });
  let ropAnswerArr = [];
  let ropAnswerObj = {
    "type": "QRADIOGROUP",
    "horizontal": true,
    "mandatory": true,
    "options": [
      {
        "value": "N",
        "title": "No"
      },
      {
        "value": "Y",
        "title": "Yes"
      }
    ]
  };

  _.forEach(insuredList, (insured) => {
    let id = 'shieldRopAnswer_' + count;
    let title = insured.iFullName;
    let ropAnswerObjCopy = _.assign({}, ropAnswerObj, {
      id: id,
      title
    });
    ropAnswerArr.push(ropAnswerObjCopy);
    count++;
    iCidRopAnswerMap[insured.iCid] = id;
  });

  _.set(ropBlock, 'items[1].items', ropAnswerArr);
};

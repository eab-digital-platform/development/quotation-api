const _ = require('lodash');
const moment = require('moment');
const crypto = require('crypto');
const Iconv = require('iconv').Iconv;
const logger = global.logger || console;

const dao = require('../../cbDaoFactory').create();
const bDao = require('../../cbDao/bundle');
const quotDao = require('../../cbDao/quotation');
const appDao = require('../../cbDao/application');
const approvalDao = require('../../cbDao/approval');
const companyDao = require('../../cbDao/company');
const needsHandler = require('../../NeedsHandler');
const quotHandler = require('../QuotationHandler');
const _application = require('./shield/application');
const CommonFunctions = require('../../CommonFunctions');
const utils = require('../../Quote/utils');
const DateUtils = require('../../../common/DateUtils');
const { sendEmail } = require('../../utils/EmailUtils');
const ProposalUtils = require('../quote/ProposalUtils');
const async = require('async');

const ApprovalHandler = require('../../ApprovalHandler');
const productUtils = require('../../../common/ProductUtils');

var {
  callApi,
  // callApiByGet,
  // callApiComplete
} = require('../../utils/RemoteUtils');

const EAPP_STEP = {
  APPLICATION: 0,
  SIGNATURE: 1,
  PAYMENT: 2,
  SUBMISSION: 3
};

const PDF_NAME = {
  FNA: 'fnaReport',
  BI: 'proposal',
  eAPP: 'appPdf',
  eCPD: 'eCpdPdf'
};

const TEMPLATE_NAME = {
  APP_FORM_MAPPING: 'application_form_mapping',
  PAYMENT: 'paymentTmpl',
  PAYMENT_SHIELD: 'paymentTmpl_shield',
  SUBMISSION: 'submissionTmpl',
  SUBMISSION_SHIELD: 'submissionTmpl_shield',
};

const BUNDLE_POPULATE = {
  WHOLE_SECTION_IDS: [
    'insurability'
  ],
  PERSONAL_DETAILS_IDS: [
    // Section: Personal Details
    'organization', 'organizationCountry', 'passExpDate', 'postalCode',
    'addrBlock', 'addrStreet', 'unitNum', 'addrEstate', 'addrCity'
  ],
  NOT_POPULATE_IDS: [
    'insAllNoInd'
  ],
  REQUIRE_POPULATE_IDS: [
    // Section: Personal Details
    'organization', 'organizationCountry', 'passExpDate', 'postalCode',
    'addrBlock', 'addrStreet', 'unitNum', 'addrEstate', 'addrCity',

    // Section: Residencial & Insurance Info
    // Residency Questions
    'EAPP-P6-F1', 'EAPP-P6-F2', 'EAPP-P6-F3', 'EAPP-P6-F4', 'EAPP-P6-F5',
    // Foreigner Questions
    'faDate', 'familyResCountry', 'familyResCity', 'reside5yrs', 'res5Frm',
    'res5To', 'res5Country', 'res5City', 'permanent', 'work', 'study', 'cob',
    'family', 'immigrate', 'visit', 'other', 'resultOther', 'hasTravelPlans',
    'travelPlans', 'travlCountry', 'travlCity', 'travlCityDrn', 'travlCityFrq',

    // Section: Insurability Information
    'HW01','HW02','HW03','HW03a','HW03b','HW03c','HW03c1',
    'INS01','INS01_DATA', 'INS02','INS02_DATA',
    'LIFESTYLE01a','LIFESTYLE01a_OTH','LIFESTYLE01b','LIFESTYLE01c',
    'LIFESTYLE02','LIFESTYLE02a_1','LIFESTYLE02a_2','LIFESTYLE02b_1','LIFESTYLE02b_2','LIFESTYLE02c_1','LIFESTYLE02c_2',
    'LIFESTYLE03','LIFESTYLE03_DATA', 'LIFESTYLE04','LIFESTYLE04_DATA',  'LIFESTYLE05','LIFESTYLE05_DATA',
    'FAMILY01', 'FAMILY01_DATA', 'REG_DR01', 'REG_DR01_DATA',
    'HEALTH_GIO01', 'HEALTH_GIO01_DATA', 'HEALTH_GIO02', 'HEALTH_GIO02_DATA',
    'HEALTH01', 'HEALTH01_DATA','HEALTH02', 'HEALTH02_DATA', 'HEALTH03', 'HEALTH03_DATA',
    'HEALTH04', 'HEALTH04_DATA','HEALTH05', 'HEALTH05_DATA', 'HEALTH06', 'HEALTH06_DATA',
    'HEALTH07', 'HEALTH07_DATA', 'HEALTH08', 'HEALTH08_DATA',  'HEALTH09', 'HEALTH09_DATA',
    'HEALTH10', 'HEALTH10_DATA',  'HEALTH11', 'HEALTH11_DATA',  'HEALTH12', 'HEALTH12_DATA',
    'HEALTH13', 'HEALTH13_DATA',  'HEALTH14', 'HEALTH14_DATA',  'HEALTH15', 'HEALTH15a',
    'HEALTH16', 'HEALTH16a', 'HEALTH17', 'HEALTH17_DATA',

    'HEALTH_HIM01','HEALTH_HIM01_DATA','HEALTH_HIM02','HEALTH_HIM02_DATA','HEALTH_HIM03','HEALTH_HIM03_DATA','HEALTH_HIM04','HEALTH_HIM04_DATA',
    'HEALTH_HER01','HEALTH_HER01_DATA','HEALTH_HER02','HEALTH_HER02_DATA','HEALTH_HER03','HEALTH_HER03_DATA','HEALTH_HER04','HEALTH_HER04_DATA','HEALTH_HER05','HEALTH_HER05_DATA','HEALTH_HER06','HEALTH_HER06_DATA','HEALTH_HER07','HEALTH_HER07_DATA','HEALTH_HER08','HEALTH_HER08_DATA',

    // Section: Declaration
    // Bankruptcy
    'BANKRUPTCY01',
    // PEP
    'PEP01', 'PEP01_DATA', 'PEP01a', 'PEP01b', 'PEP01c', 'PEP01d', 'PEP01e1', 'PEP01e2', 'PEP01e3',
    'PEP01e4', 'PEP01e5', 'PEP01e6', 'PEP01e7', 'PEP01f1', 'PEP01f2', 'PEP01f3', 'PEP01f4', 'PEP01f5',
    'PEP01f6', 'PEP01f7',
    // e-Policy
    'EPOLICY01',
    // Roadshow Declaration
    'ROADSHOW01', 'ROADSHOW02', 'ROADSHOW03', 'ROADSHOW04',
    // Trusted Individual
    'TRUSTED_IND01', 'TRUSTED_IND02', 'TRUSTED_IND04', 'TRUSTED_IND031', 'TRUSTED_IND032',
    // FATCA(Forign Account Tax Compliance Act)
    // 'FATCA01', 'FATCA01b', 'FATCA02', 'FATCA03',
    // 'CRS01', 'CRS01_DATA',
    'FCD_01', 'FCD_02', 'FCD_03'

    // Source of Funds
    // 'FUND_SRC01', 'FUND_SRC02', 'FUND_SRC03', 'FUND_SRC04', 'FUND_SRC05', 'FUND_SRC06',
    // 'FUND_SRC07', 'FUND_SRC08', 'FUND_SRC09', 'FUND_SRC10', 'FUND_SRC11', 'FUND_SRC12',
    // 'FUND_SRC13', 'FUND_SRC14', 'FUND_SRC15', 'FUND_SRC16', 'FUND_SRC17', 'FUND_SRC18',
    // 'FUND_SRC19', 'FUND_SRC20', 'FUND_SRC21', 'FUND_SRC22', 'FUND_SRC23', 'FUND_SRC24',
    // 'FUND_SRC25', 'FUND_SRC26', 'FUND_SRC27', 'FUND_SRC28', 'FUND_SRC29', 'FUND_SRC30',
    // 'FUND_SRC31', 'FUND_SRC32', 'FUND_SRC33', 'FUND_SRC34', 'FUND_SRC35', 'FUND_SRC36',
    // 'FUND_SRC37', 'FUND_SRC38', 'FUND_SRC39'
  ]
};

const CAPITAL_LETTER_IDS = ['FUND_SRC03', 'FUND_SRC04', 'FUND_SRC05', 'FUND_SRC06', 'FUND_SRC07', 'FUND_SRC08', 'FUND_SRC09', 'FUND_SRC10', 'FUND_SRC13', 'FUND_SRC14', 'FUND_SRC15', 'FUND_SRC16', 'FUND_SRC17', 'FUND_SRC18', 'FUND_SRC19', 'FUND_SRC20', 'FUND_SRC22'];

const FNA_QUESTIONS = ['ROADSHOW01', 'ROADSHOW02', 'ROADSHOW03', 'ROADSHOW04', 'TRUSTED_IND01', 'TRUSTED_IND02', 'TRUSTED_IND04'];

const CROSSAGE_MODE = {
  NEAREST_AGE: 10,
  NEXT_BIRTHDAY: 20
};

const CROSSAGE_STATUS = {
  NO_CROSSAGE: 0,
  WILL_CROSSAGE: 100,
  WILL_CROSSAGE_NO_ACTION: 150,
  CROSSED_AGE_SIGNED: 200,
  CROSSED_AGE_NO_ACTION: 250,
  CROSSED_AGE_NOTSIGNED: 300
};

const CLIENT_FIELDS_TO_UPDATE = ['organization', 'addrBlock', 'addrStreet', 'unitNum', 'addrEstate', 'addrCity', 'organizationCountry', 'passExpDate', 'postalCode'];

const APP_FORM_SHIELD_GROUP_HEALTH_QN = [
  'HEALTH_SHIELD_01', 'HEALTH_SHIELD_02', 'HEALTH_SHIELD_03', 'HEALTH_SHIELD_04', 'HEALTH_SHIELD_05',
  'HEALTH_SHIELD_06', 'HEALTH_SHIELD_07', 'HEALTH_SHIELD_08', 'HEALTH_SHIELD_09', 'HEALTH_SHIELD_10',
  'HEALTH_SHIELD_11', 'HEALTH_SHIELD_12', 'HEALTH_SHIELD_13', 'HEALTH_SHIELD_14', 'HEALTH_SHIELD_15',
  'HEALTH_SHIELD_16', 'HEALTH_SHIELD_17', 'HEALTH_SHIELD_18'
];

const APP_FORM_SHIELD_GROUP_HEALTH_F_QN = [
  'HEALTH_SHIELD_20', 'HEALTH_SHIELD_21', 'HEALTH_SHIELD_22', 'HEALTH_SHIELD_23'
];

var _getChannelType = function(channel){
  let channelFA = 'FA';
  let channelAgent = 'AGENCY';
  channel = channel.toUpperCase();
  if (channel === 'Broker'.toUpperCase() || channel === 'Synergy'.toUpperCase()) {
    return channelFA;
  } else {
    return channelAgent;
  }
};

var _getPolicyNumberFromApi = function(numOfPolicyNumber, isShieldProduct, callback) {
  var companyCD = '5';
  if (isShieldProduct) {
    companyCD = '3';
  }

  logger.log('INFO: _getPolicyNumberFromApi - start', companyCD);
  try {
    callApi('/policyNumber/assignPolicyNumber', {
      companyCD: companyCD,
      requestedPolicyNO: numOfPolicyNumber
    }, data=>{
        if (data && data.retrievePolicyNumberResponse && data.retrievePolicyNumberResponse.policyNO) {
          logger.log('INFO: _getPolicyNumberFromApi - end [RETURN=1]');
          callback(data.retrievePolicyNumberResponse.policyNO);
        } else {
          // let now = new Date();
          // cb([now.getTime().toString()]);
          logger.log('INFO: _getPolicyNumberFromApi - end [RETURN=-1]');
          callback(false);
        }
    });
  } catch (e) {
    logger.error('ERROR: _getPolicyNumberFromApi - end [RETURN=-2]', e);
    let now = new Date();
    callback([now.getTime().toString()]);
  }
};

var _fillValuesByForm = function(items, desValues, srcValues, populateIds, isWholeSectionPopulate = false) {
  let hasChange = false;
  _.forEach(items, (item)=>{
    if (item.id) {
      if ((isWholeSectionPopulate || _.includes(populateIds, item.id)) && srcValues[item.id]
        && !_.includes(BUNDLE_POPULATE.NOT_POPULATE_IDS, item.id)) {
        if (typeof (srcValues[item.id]) === 'object') {
          // let v = difference(desValues[item.id], srcValues[item.id])
          // if (Object.keys(v).length > 0) {
            desValues[item.id] = srcValues[item.id];
            hasChange = true;
          // }
        } else if (desValues[item.id] !== srcValues[item.id]) {
          desValues[item.id] = srcValues[item.id];
          hasChange = true;
        }
      } else {
        hasChange |= _fillValuesByForm(item.items, desValues, srcValues, populateIds, isWholeSectionPopulate);
      }
    } else {
      hasChange |= _fillValuesByForm(item.items, desValues, srcValues, populateIds, isWholeSectionPopulate);
    }
  });
  return hasChange;
};

var _populateFromBundle = function(application, sections, isFaChannel){
  return new Promise((resolve)=>{
    let appId = _.get(application, 'id');
    logger.log('INFO: populateFromBundle - start', appId);

    let populateIds = isFaChannel ? BUNDLE_POPULATE.PERSONAL_DETAILS_IDS : BUNDLE_POPULATE.REQUIRE_POPULATE_IDS;

    let hasChange = false;
    // formValues = bundle.formValues;
    bDao.getFormValuesByCid(application.pCid).then((formValues)=>{
      // set the proposer
      if (formValues && sections && _.get(formValues, application.pCid)) {
        logger.log('INFO: populateFromBundle - set proposer', appId);

        // _.forEach(application.applicationForm.values.proposer, (section, sectionId)=>{
        //   _.forEach(section, (question, questionId)=>{
        //     // mapping application json to get values from bundle
        //     section[questionId] = _.get(formValues[application.pCid][sectionId], questionId);
        //   });
        // });
        if (!_.get(application.applicationForm.values, 'proposer')) {
          application.applicationForm.values.proposer = {};
        }

        // clientValues is the bundle's appForm values
        let clientValues = formValues[application.pCid];
        _.forEach(sections, (section)=> {
          _.forEach(section.items, (menu)=> {

            let isWholeSectionPopulate = _.includes(BUNDLE_POPULATE.WHOLE_SECTION_IDS, menu.id) && !isFaChannel;
            // cForm is the template, like 'insurability''s form template
            // let cForm = _.find(menu.items, (f)=>{return f.subType=='proposer'});
            let questionBlocks = _.get(_.find(menu.items, (f)=>{return f.subType === 'proposer';}), 'items');

            if (questionBlocks) {
              _.forEach(clientValues, (cFormValues, keyCFormValues)=>{
                let appFormValues = _.get(application, 'applicationForm.values.proposer.' + keyCFormValues, {});
                hasChange |= _fillValuesByForm(questionBlocks, appFormValues, cFormValues, populateIds, isWholeSectionPopulate);
                _.set(application, 'applicationForm.values.proposer.' + keyCFormValues, appFormValues);
              });
            }

            // if (questionBlocks && clientValues[menu.id]) {
            //   // cFormValues is from bundle
            //   let cFormValues = clientValues[menu.id];
            //   // appFormValues is from application json
            //   let appFormValues = application.applicationForm.values.proposer[menu.id] || {};
            //   hasChange |= _fillValuesByForm(questionBlocks, appFormValues, cFormValues);
            //   application.applicationForm.values.proposer[menu.id] = appFormValues;
            // }

          });
        });
      }
      // set for insured if it is not the same one
      if (application.sameAs === 'N' && application.iCid || _.get(application, 'iCids', []).length > 0) {
        logger.log('INFO: populateFromBundle - set insured', appId);

        let iCids = null;
        if (_.get(application, 'iCids', []).length > 0) {
          iCids = _.get(application, 'iCids');
        } else {
          if (typeof application.iCid === 'string') {
            iCids = [application.iCid];
          } else {
            iCids = application.iCid;
          }
        }

        let insureds = null;
        if (application.applicationForm.values.insured) {
          insureds = application.applicationForm.values.insured;
        } else {
          application.applicationForm.values.insured = insureds = [];
        }

        _.forEach(iCids, (cid, idx) => {
          if (insureds.length <= idx) {
            insureds.push({});
          }
          if (formValues && sections && application.applicationForm.values.insured
            && _.get(formValues, cid)) {
            let insFormValues = insureds[idx];
            let clientValues = formValues[cid];
            _.forEach(sections, (section)=> {
              _.forEach(section.items, (form)=> {

                let isWholeSectionPopulate = _.includes(BUNDLE_POPULATE.WHOLE_SECTION_IDS, form.id) && !isFaChannel;
                // _.forEach(forms.items, (form) => {
                let cForm = _.find(form.items, (f)=>{return f.subType === 'insured';});

                if (cForm) {
                  _.forEach(clientValues, (cFormValues, keyCFormValues)=>{
                    // let appFormValues = _.get(application, 'applicationForm.values.proposer.'+ keyCFormValues, {});
                    let appFormValues = _.get(insFormValues, keyCFormValues, {});
                    hasChange |= _fillValuesByForm(cForm.items, appFormValues, cFormValues, populateIds, isWholeSectionPopulate);
                    // _.set(application, 'applicationForm.values.proposer.'+ keyCFormValues, appFormValues);
                    insFormValues[keyCFormValues] = appFormValues;
                  });
                  }

                // if (cForm && clientValues[form.id]) {
                //   let cFormValues = clientValues[form.id];
                //   let appFormValues = insFormValues[form.id] || {};
                //   hasChange |= _fillValuesByForm(cForm.items, appFormValues, cFormValues);
                //   insFormValues[form.id] = appFormValues;
                // }

                });
              // })
            });
          }
        });
      }

      /** temporarily disable hasChange block, will resume this block after bundle population works fine.*/
      // if (hasChange) {
      //   if (application._attachments && application._attachments.appPdf) {
      //     delete application._attachments.appPdf;
      //   }
      //   application.applicationForm.values.checkedMenu = [];
      //   application.appStep = 0;
      // }

      logger.log('INFO: populateFromBundle - end', appId);
      resolve(application);
    });
  });
};

var _getShieldApplicationIds = function(session, numOfIds){
  logger.log('INFO: _getShieldApplicationIds - start', numOfIds);
  let agentCode = null;
  if (session.agent){
    agentCode = session.agent.agentCode;
  }

  return appDao.createShieldApplicaitonNumbers(agentCode, numOfIds);
};


var _getApplicationId = function(session, callback){
  logger.log('INFO: getApplicationId - start');
  let agentCode = null;
  if (session.agent){
    agentCode = session.agent.agentCode;
  }
  appDao.createApplicaitonNumber(agentCode, function(appId){
    logger.log('INFO: getApplicationId - end', appId);
    callback(appId);
  });
};

var _removeInvalidProfileProp = function(doc){
  delete doc._rev;
  delete doc._attachments;
};

var _getOptions = function(optionsLookUpList, id, cb){
  dao.getDoc(id,
    function(optionsDoc){
      optionsLookUpList[id] = optionsDoc.options;
      cb();
    }
  );
};

var _populateOptions = function(template, optionsLookUpList){
  if (template.optionsTable || (template.options &&  typeof template.options === 'string')){
    var optionsTable = template.optionsTable || template.options;
    if (optionsLookUpList[optionsTable]){
      template.options = optionsLookUpList[optionsTable];
    }
  } else {
    if (template.items){
      for (let i = 0; i < template.items.length; i++){
        if (template.items[i]) {
          _populateOptions(template.items[i], optionsLookUpList);
        } else {
        }
      }
    }
  }
};

var _getOptionsList = function(application, template, callback){
  logger.log('INFO: getOptionsList - start', application._id);

  var optionsLookUpList = {};
  var values = application.applicationForm.values;
  let payors = [];
  if (values.proposer){
    let proposer = values.proposer;
    let payor = {title:proposer.personalInfo.firstName, value: proposer.personalInfo.cid};
    payors.push(payor);
  }
  if (values.insured){
    let insured = values.insured;
    for (let i = 0; i < values.insured.length; i++){
      if (insured[i].personalInfo){
        let payor = {title:insured[i].personalInfo.firstName, value: insured[i].personalInfo.cid};
        payors.push(payor);
      }
    }
  }

  payors.push({
    'title': 'Other',
    'value': 'other'
  });

  optionsLookUpList.payors = payors;

  let _getYearsOptions = function() {
    let d = new Date();
    let n = d.getFullYear();
    let yrOps = [];
    for (let yr = n; yr > n - 80; yr --){
      yrOps.push({
        'value': '' + yr,
        'title': '' + yr
      });
    }
    return yrOps;
  };
  optionsLookUpList.years = _getYearsOptions();

  _getOptions(optionsLookUpList, 'branches', function(){
    _populateOptions(template, optionsLookUpList);
    logger.log('INFO: getOptionsList - end', application._id);
    callback();
  });
};

var _replaceAppFormValuesByTemplate = function(template, orgValues, newValues, trigger, topLevelTrigger, lang, ccy) {
  if (!template.type) {
    logger.error('ERROR: replaceAppFormValuesByTemplate - type', template.type);
    return;
  }

  //prepare trigger array for removing value later
  try {
    if (!(template.type.toUpperCase() === 'MENU' ||
      template.type.toUpperCase() === 'MENUSECTION' ||
      template.type.toUpperCase() === 'MENUITEM' ||
      template.type.toUpperCase() === 'TAB' ||
      template.type.toUpperCase() === 'BLOCK' ||
      template.type.toUpperCase() === 'HBOX' ||
      template.type.toUpperCase() === 'FBOX' ||
      template.type.toUpperCase() === '12BOX')) {

      if (template.id) {
        if (!trigger[template.id]) {
          trigger[template.id] = [];
        }

        if (template.trigger) {
          if (template.trigger instanceof Array) {
            if (_.toLower(template.triggerType) === 'and') {
              //convert triggerType = 'AND' to dynamicCondition
              let dcTrigger = {
                id: '',
                type: 'dynamicCondition',
                value: ''
              };

              _.forEach(template.trigger, (t, i) => {
                let idPrefix = (i === 0 ? '' : ',');
                let valuePrefix = (i === 0 ? '' : ' && ');
                let valueRef = '#' + (i + 1);

                let dcId = t.id;
                if (_.includes(t.id, '/') && !_.startsWith(t.id, '@') && !_.startsWith(t.id, '#') && !_.startsWith(t.id, '$')) {
                  dcId = '@' + _.replace(dcId, '/', '.');
                }
                dcTrigger.id += idPrefix + dcId;

                if (t.value) {
                  if (_.toUpper(t.type) === 'SHOWIFEQUAL') {
                    dcTrigger.value += valuePrefix + valueRef + '==\'' + t.value + '\'';
                  } else if (_.toUpper(t.type) === 'SHOWIFNOTEQUAL') {
                    dcTrigger.value += valuePrefix + valueRef + '!=\'' + t.value + '\'';
                  } else if (_.toUpper(t.type) === 'SHOWIFGREATER') {
                    dcTrigger.value += valuePrefix + valueRef + '>\'' + t.value + '\'';
                  } else if (_.toUpper(t.type) === 'SHOWIFEXIST') {
                    let _lookUpValues = valueRef.split(',');
                    let newValueRef = '';
                    for (let idx = 0; idx < _lookUpValues.length; idx ++) {
                      newValueRef += (idx !== 0 ? ',' : '')  + _.toUpper(_lookUpValues[idx]);
                    }
                    dcTrigger.value += valuePrefix + '[' + newValueRef + '].indexOf(\'' + t.value + '\')';
                  } else {
                    logger.log('INFO: replaceAppFormValuesByTemplate - prepare trigger data for [id=', t.id, '; type=', t.type, '] - did not handle');
                  }
                } else {
                  if (_.toUpper(t.type) === 'EMPTY') {
                    dcTrigger.value += valuePrefix + '!' + valueRef;
                  } else if (_.toUpper(t.type) === 'NOTEMPTY') {
                    dcTrigger.value += valuePrefix + '!!' + valueRef + '&&' + valueRef + '!= \'\'';
                  } else {
                    logger.log('INFO: replaceAppFormValuesByTemplate - prepare trigger data for [id=', t.id, '; type=', t.type, '] - did not handle');
                  }
                }
              });

              logger.debug('DEBUG: replaceAppFormValuesByTemplate', template.trigger, '->', dcTrigger);

              trigger[template.id].push(dcTrigger);
            } else {
              trigger[template.id].push.apply(trigger[template.id], template.trigger);
            }
          } else {
            trigger[template.id].push(template.trigger);
          }
        }
      }
    }

    //must have trigger for current object if top level trigger exists
    if (topLevelTrigger.length > 0 && template.id) {
      if (!trigger[template.id]) {
        trigger[template.id] = [];
      }
      trigger[template.id].push.apply(trigger[template.id], topLevelTrigger);
    }
  } catch (e) {
    logger.error('ERROR: replaceAppFormValuesByTemplate - prepare trigger data for [id=', template.id, '; type=', template.type, ']', e);
  }

  //perform recursive call
  if (template.type.toUpperCase() === 'TABS') {
    let proposerIndex = _.findIndex(template.items, (tab) => { return tab.subType === 'proposer'; });

    template.items.forEach(function(item, index) {
      if (item.subType === 'proposer') {
        if (!trigger[item.subType]) {
          trigger[item.subType] = {};
        }

        _replaceAppFormValuesByTemplate(item, orgValues[item.subType], newValues[item.subType], trigger[item.subType], [], lang, ccy);
      } else {
        let insuredIndex = index > proposerIndex && proposerIndex >= 0 ? index - 1 : index;
        if (!trigger[item.subType]) {
          trigger[item.subType] = [];
        }
        if (insuredIndex > trigger[item.subType].length - 1) {
          trigger[item.subType].push({});
        }

        _replaceAppFormValuesByTemplate(item, orgValues[item.subType][insuredIndex], newValues[item.subType][insuredIndex], trigger[item.subType][insuredIndex], [], lang, ccy);
      }
    });
  } else if (template.type.toUpperCase() === 'TABLE' || template.type.toUpperCase() === 'TABLE-BLOCK') {
    let tableTemplateList = template.items;
    let oValue = template.id ? orgValues[template.id] : orgValues;
    let nValue = template.id ? newValues[template.id] : newValues;

    if (oValue) {
      if (oValue instanceof Array) {
        for (let i = 0; i < oValue.length; i ++) {
          tableTemplateList.forEach(function(item, index) {
            _replaceAppFormValuesByTemplate(item, oValue[i], nValue[i], trigger, [], lang, ccy);
          });
        }
      } else {
        tableTemplateList.forEach(function(item, index) {
          _replaceAppFormValuesByTemplate(item, oValue, nValue, trigger, [], lang, ccy);
        });
      }
    }
  } else if (template.type.toUpperCase() === 'MENU' ||
    template.type.toUpperCase() === 'MENUSECTION' ||
    template.type.toUpperCase() === 'MENUITEM' ||
    template.type.toUpperCase() === 'TAB' ||
    template.type.toUpperCase() === 'BLOCK' ||
    template.type.toUpperCase() === 'HBOX' ||
    template.type.toUpperCase() === 'FBOX' ||
    template.type.toUpperCase() === '12BOX' ||
    template.type.toUpperCase() === 'CARDCONTAINER') {

    let currTrigger = [];
    //pass current level trigger to lower level
    if (template.trigger) {
      if (template.trigger instanceof Array) {
        currTrigger.push.apply(currTrigger, template.trigger);
      } else {
        currTrigger.push(template.trigger);
      }
    }
    //if not handle topLevelTrigger, pass it to lower level
    if (topLevelTrigger.length > 0 && !template.id) {
      currTrigger.push.apply(currTrigger, topLevelTrigger);
    }

    template.items.forEach(function(item, index) {
      let oValue = template.id ? orgValues[template.id] : orgValues;
      let nValue = template.id ? newValues[template.id] : newValues;

      if (oValue) {
        if (template.id && !trigger[template.id]) {
          trigger[template.id] = {};
        }
        _replaceAppFormValuesByTemplate(item, oValue, nValue, (template.id ? trigger[template.id] : trigger), currTrigger, lang, ccy);
      }
    });
  }

  //replace values
  try {
    if (template.type.toUpperCase() === 'DATEPICKER' || (template.type.toUpperCase() === 'READONLY' && template.subType && template.subType.toUpperCase() === 'DATE')) {
      if (template.id && orgValues[template.id]) {
        if (template.dateFormat) {
          let date = new Date(orgValues[template.id]).getTime();
          newValues[template.id] = CommonFunctions.getFormattedDate(date, template.dateFormat);
        } else {
          let isCorrectPattern = /^(\d{1,2})\/\d{1,2}\/\d{4}$/.test(orgValues[template.id]);
          if (!isCorrectPattern) {
            let date = new Date(orgValues[template.id]).getTime();
            newValues[template.id] = CommonFunctions.getFormattedDate(date);
          }
        }
      }
    } else if (['READONLY', 'TEXT', 'TEXTONLY'].indexOf(template.type.toUpperCase()) >= 0 && _.toUpper(_.get(template, 'subType', '')) === 'CURRENCY') {
      if (template.id && (orgValues[template.id] || (template.showZeroValue && orgValues[template.id] === 0))) {
        newValues[template.id] = utils.getCurrency(orgValues[template.id], utils.getCurrencySign(ccy && (template.ccy || ccy)), template.hasOwnProperty('decimal') ? template.decimal : 0);
      }
    } else if (template.type.toUpperCase() === 'CHECKBOX' || template.type.toUpperCase() === 'CHECKBOXPOPUP') {
      if (template.id && orgValues[template.id]) {
        newValues[template.id] = orgValues[template.id] === 'Y' ? 'Yes' : (orgValues[template.id] === 'N' ? 'No' : '');
      }
    } else if (template.type.toUpperCase() === 'SATABLE') {
      let policies = [
          'pendingLife', 'pendingCi', 'pendingTpd', 'pendingPaAdb', 'pendingTotalPrem',
          'existLife', 'existCi', 'existTpd', 'existPaAdb', 'existTotalPrem',
          'replaceLife', 'replaceCi', 'replaceTpd', 'replacePaAdb', 'replaceTotalPrem',
          'pendingLifeAXA', 'pendingCiAXA', 'pendingTpdAXA', 'pendingPaAdbAXA', 'pendingTotalPremAXA',
          'existLifeAXA', 'existCiAXA', 'existTpdAXA', 'existPaAdbAXA', 'existTotalPremAXA',
          'replaceLifeAXA', 'replaceCiAXA', 'replaceTpdAXA', 'replacePaAdbAXA', 'replaceTotalPremAXA',
          'pendingLifeOther', 'pendingCiOther', 'pendingTpdOther', 'pendingPaAdbOther', 'pendingTotalPremOther',
          'existLifeOther', 'existCiOther', 'existTpdOther', 'existPaAdbOther', 'existTotalPremOther',
          'replaceLifeOther', 'replaceCiOther', 'replaceTpdOther', 'replacePaAdbOther', 'replaceTotalPremOther'

        ];

      for (let i = 0; i < policies.length; i ++) {
        let tableItemId = policies[i];
        if (orgValues[tableItemId]) {
          newValues[tableItemId] = utils.getCurrency(orgValues[tableItemId], '', template.hasOwnProperty('decimal') ? template.decimal : 0);
        } else {
          newValues[tableItemId] = '0';
        }
      }
    } else if (template.options instanceof Array) {
      if (template.id && orgValues[template.id]) {
        for (let i = 0; i < template.options.length; i ++){
          let option = template.options[i];
          if (option.value === orgValues[template.id]) {
            newValues[template.id] = typeof option.title === 'object' ? option.title[lang] : option.title;
            break;
          }
        }
      }
    } else if (typeof template.options === 'string') {
      if (template.id && orgValues[template.id]) {
        let optionsMap = global.optionsMap[template.options];
        let options = optionsMap ? optionsMap.options : [];
        for (let i = 0; i < options.length; i ++){
          let option = options[i];
          if (option.value === orgValues[template.id]) {
            newValues[template.id] = typeof option.title === 'object' ? option.title[lang] : option.title;
            break;
          }
        }
      }
    }
  } catch (e) {
    logger.error('ERROR: replaceAppFormValuesByTemplate - replace value for [id=', template.id, '; type=', template.type, ']', e);
  }
};

var _checkTriggerConditionOnAppFormValues = function(orgValues, newValues, validRefValues, key, trigger,rootValues) {
  let checkValues = null;
  if (trigger.id && trigger.type) {
    let ids = trigger.id.split('/');
    if (ids.length > 1) {
      //for different level trigger
      let parent = ids[0];
      let child = ids[1];

      checkValues = validRefValues[parent] ? validRefValues[parent][child] : null;
    } else {
      //for same level trigger
      checkValues = orgValues[trigger.id];
    }

    if (trigger.value) {
      if (trigger.type.toUpperCase() === 'SHOWIFEQUAL') {
        return checkValues === trigger.value;
      } else if (trigger.type.toUpperCase() === 'SHOWIFNOTEQUAL') {
        return checkValues !== trigger.value;
      } else if (trigger.type.toUpperCase() === 'SHOWIFEXIST') {
        let lookUpValues = trigger.value.split(',');
        for (let i in lookUpValues) {
          if (checkValues && checkValues.toUpperCase() === lookUpValues[i].toUpperCase()) {
            return true;
          }
        }
        return false;
      } else if (trigger.type.toUpperCase() === 'DYNAMICCONDITION') {
        try {
          let triggerIds = trigger.id.split(',');
          let lookUpValues = trigger.value;
          _.forEach(triggerIds, (id, i)=> {
            if (_.startsWith(id, '@')) {
              checkValues = _.get(validRefValues, id.replace('@', ''), '');
            }
            else if (_.startsWith(id, '#')) {
              checkValues = _.get(rootValues, id.replace('#', ''), '');
            } else {
              checkValues = _.get(orgValues, id, '');
            }

            lookUpValues = _.replace(lookUpValues, new RegExp(`#${i + 1}` + '\\b','g'), '\'' + checkValues + '\'');
          });

          return eval(lookUpValues);
        } catch (e) {
          logger.error('ERROR: checkTriggerConditionOnAppFormValues: in handling Trigger:', trigger.id, trigger.type, e);
        }
      } else {
        logger.log('INFO: checkTriggerConditionOnAppFormValues: Unhandled Trigger:', trigger.id, trigger.type);
      }
    } else if (trigger.ccys) {
      let ccys = trigger.ccys;

      let parent = ids[0];
      let ccy = validRefValues[parent] ? validRefValues[parent].ccy : null;

      for (let i = 0; i < ccys.length; i++){
        if (ccy === ccys[i].ccy){
          if (checkValues >= ccys[i].amount) {
            return true;
          }
        }
      }
      return false;
    }
  }

  return true;
};

var _removeAppFormValuesByTrigger = function(valuesKey, orgValues, newValues, validRefValues, trigger, isNewValuesForReport,rootValues) {
  let excludeCheck = {
    extra: '*',
    personalInfo: '*',
    groupedPlanDetails: '*',
    policies: [
      'pendingLife', 'pendingCi', 'pendingTpd', 'pendingPaAdb', 'pendingTotalPrem',
      'existLife', 'existCi', 'existTpd', 'existPaAdb', 'existTotalPrem',
      'replaceLife', 'replaceCi', 'replaceTpd', 'replacePaAdb', 'replaceTotalPrem',
      'pendingLifeAXA', 'pendingCiAXA', 'pendingTpdAXA', 'pendingPaAdbAXA', 'pendingTotalPremAXA',
      'existLifeAXA', 'existCiAXA', 'existTpdAXA', 'existPaAdbAXA', 'existTotalPremAXA',
      'replaceLifeAXA', 'replaceCiAXA', 'replaceTpdAXA', 'replacePaAdbAXA', 'replaceTotalPremAXA',
      'pendingLifeOther', 'pendingCiOther', 'pendingTpdOther', 'pendingPaAdbOther', 'pendingTotalPremOther',
      'existLifeOther', 'existCiOther', 'existTpdOther', 'existPaAdbOther', 'existTotalPremOther',
      'replaceLifeOther', 'replaceCiOther', 'replaceTpdOther', 'replacePaAdbOther', 'replaceTotalPremOther',
      'replacePolTypeOther','replacePolTypeAXA'
    ],
    declaration: [
      'ccy', 'trustedIndividuals'
    ],
    insured: ['extra', 'groupedPlanDetails', 'declaration', 'personalInfo'],
    proposer: ['extra', 'groupedPlanDetails', 'declaration', 'personalInfo']
  };

  for (let key in orgValues) {
    let tg = trigger[key];

    if (tg) {
      if (tg instanceof Array) {
        let isShow = false;
        for (let i = 0; i < tg.length; i ++) {
          if (_checkTriggerConditionOnAppFormValues(orgValues, newValues, validRefValues, key, tg[i],rootValues)) {
            isShow = true;
            break;
          }
        }

        if (tg.length > 0) {
          if (!isShow) {
            logger.log('INFO: removeAppFormValuesByTrigger: Remove data - level:', valuesKey, ', key:', key);
            if (newValues[key] instanceof Array) {
              newValues[key] = [];
            } else {
              delete newValues[key];
            }
          }
        }
      } else {
        _removeAppFormValuesByTrigger(key, orgValues[key], newValues[key], validRefValues, tg, isNewValuesForReport,rootValues);
      }
    } else if (excludeCheck[valuesKey] instanceof Array ? excludeCheck[valuesKey].indexOf(key) < 0 : (_.get(excludeCheck, valuesKey, '') !== '*')) {
      logger.log('INFO: removeAppFormValuesByTrigger: deleteKey - level:', valuesKey, ', key:', key);
      if (newValues[key] instanceof Array) {
        newValues[key] = [];
      } else {
        delete newValues[key];
      }
    }
  }
};

var _updateClientFormValues = function(bundle, fValues) {
  // save appForm to bundle
  logger.log('INFO: updateClientFormValues - start', bundle.id);
  let formValues = bundle.formValues || {};
  let cid = fValues.personalInfo.cid;
  try {
    if (!formValues[cid] && fValues) {
      formValues[cid] = _.clone(fValues);
    } else {
      _.forEach(fValues, (section, sectionId)=>{
        // Object.assign(formValues[cid][sectionId], section);
        if (!formValues[cid][sectionId]) {
          formValues[cid][sectionId] = _.clone(section);
        } else {
          Object.assign(formValues[cid][sectionId], section);
        }
      });
    }
  } catch (ex) {
    logger.error('ERROR: updateClientFormValues', bundle.id, ex);
    formValues[cid] = fValues;
  }
  delete formValues[cid].channel;

  bundle.formValues = formValues;

  logger.log('INFO: updateClientFormValues - end', bundle.id);
};

var _frozenTemplate = function (template) {
  let type = (template.type) ? template.type.toUpperCase() : '';
  if (type === 'PICKER') {
    template.type = 'READONLY';
    template.subType = 'picker';
  } else if (type === 'DATEPICKER') {
    template.type = 'READONLY';
    template.subType = 'date';
  } else if (type === 'CHECKBOX') {
    template.disabled = true;
    if (!_.isEmpty(template.disableTrigger)) {
      delete template.disableTrigger;
    }
  } else if (type === 'TEXT') {
    template.type = 'READONLY';
  } else if (type === 'QRADIOGROUP') {
    template.disabled = true;
    if (!_.isEmpty(template.disableTrigger)) {
      delete template.disableTrigger;
    }
  } else if (type === 'TABLE') {
    template.allowEdit = false;
    template.mandatory = false;
    template.disabled = true;
    // _frozenTemplateItem(template);
  } else if (type === 'SATABLE') {
    template.readonly = true;
    // } else if (type == 'GOFNA') {
    //   template.disabled = true;
  } else {
    template.mandatory = false;
    // if (type == 'BLOCK' && template.laAddress) {
    //   template.laAddress = false;
    // }
    _frozenTemplateItem(template);
  }
};

const _frozenTemplateItem = (template) => {
  if (template.items) {
    for (let i = 0; i < template.items.length; i++) {
      _frozenTemplate(template.items[i]);
    }
  }
};

const _frozenTemplateMobile = function (template) {
  let type = (template.type) ? template.type.toUpperCase() : '';
  if (type === 'PICKER') {
    template.disabled = true;
  } else if (type === 'DATEPICKER') {
    template.disabled = true;
  } else if (type === 'CHECKBOX') {
    template.disabled = true;
    if (!_.isEmpty(template.disableTrigger)) {
      delete template.disableTrigger;
    }
  } else if (type === 'TEXT') {
    template.disabled = true;
  } else if (type === 'QRADIOGROUP') {
    template.disabled = true;
    if (!_.isEmpty(template.disableTrigger)) {
      delete template.disableTrigger;
    }
  } else if (type === 'TABLE') {
    template.allowEdit = false;
    template.mandatory = false;
    template.disabled = true;
    // _frozenTemplateItem(template);
  } else if (type === 'SATABLE') {
    template.readonly = true;
    template.disabled = true;
    // } else if (type == 'GOFNA') {
    //   template.disabled = true;
  } else {
    template.mandatory = false;
    // if (type == 'BLOCK' && template.laAddress) {
    //   template.laAddress = false;
    // }
    _frozenTemplateMobileItem(template);
  }
};

const _frozenTemplateMobileItem = template => {
  if (template.items) {
    for (let i = 0; i < template.items.length; i++) {
      _frozenTemplateMobile(template.items[i]);
    }
  }
};

const _frozenAppFormTemplateFnaQuestions = function(template) {
  if (FNA_QUESTIONS.indexOf(_.get(template, 'id')) >= 0) {
    let type = _.toUpper(_.get(template, 'type'));
    if (type === 'PICKER') {
      template.type = 'READONLY';
      template.subType = 'picker';
    } else if (type === 'DATEPICKER') {
      template.type = 'READONLY';
      template.subType = 'date';
    } else if (type === 'TEXT') {
      template.type = 'READONLY';
    } else if (type === 'QRADIOGROUP') {
      template.disabled = true;
      if (!_.isEmpty(template.disableTrigger)) {
        delete template.disableTrigger;
      }
    } else if (type === 'CHECKBOX') {
      template.disabled = true;
      if (!_.isEmpty(template.disableTrigger)) {
        delete template.disableTrigger;
      }
    }
  } else if (template.items) {
    for (let i = 0; i < template.items.length; i++) {
      _frozenAppFormTemplateFnaQuestions(template.items[i]);
    }
  }
}

var _getReportTemplates = function(tplFiles, tplId, index, callback) {
  if (typeof tplId === 'string') {
    logger.log('INFO: getReportTemplates (string)', tplId);
    dao.getDocFromCacheFirst(tplId, function(tpl) {
      if (tpl && !tpl.error) {
        tplFiles.push(tpl);
      }
      callback();
    });
  } else {
    if (tplId && tplId.length > index) {
      logger.log('INFO: getReportTemplates (array)', index, tplId[index]);
      dao.getDocFromCacheFirst(tplId[index], function(tpl) {
        if (tpl && !tpl.error) {
          tplFiles.push(tpl);
        }
        _getReportTemplates(tplFiles, tplId, index + 1, callback);
      });
    } else {
      callback();
    }
  }
};

var _checkAppFormByPerson = function(clientValues, menuItemValueKey, showQuestions) {
  let menuItemValue = clientValues[menuItemValueKey];
  if (menuItemValue) {
    let question = showQuestions[menuItemValueKey].question;
    //
    for (let cDataKey in menuItemValue) {
      if (question.indexOf(cDataKey) < 0) {
        let visible = false;
        if (menuItemValue[cDataKey] instanceof Array) {
          if (menuItemValue[cDataKey].length > 0) {
            visible = true;
          }
        } else if (menuItemValue[cDataKey]) {
          visible = true;
        }
        //handle Trusted Individual
        if (menuItemValueKey === 'declaration' && cDataKey.indexOf('TRUSTED_IND') >= 0 && clientValues.extra) {
          visible = clientValues.extra.hasTrustedIndividual === 'Y';
        }

        if (visible) {
          question.push(cDataKey);
        }
      }
    }
  }
};



var _checkLaIsAdult = function(app) {
  let insured = app.applicationForm.values.insured;
  if (insured && insured.length > 0) {
    for (let i = 0; i < insured.length; i ++) {
      let la = insured[i];
      if (la.personalInfo && la.personalInfo.age >= 18) {
        return true;
      }
    }
  }
  return false;
};

var _checkIsLaSignedByProposer = function(app, insuredIndex) {
  let insuredAge = _.get(app, `applicationForm.values.insured[${insuredIndex}].personalInfo.age`, 16);
  if (insuredAge < 16) {
    return true;
  }
  return false;
};

var _genSignDocAuth = function(docid, docts) {
  const secretKey = global.config.signdoc.secretKey;
  const input = docid + docts + secretKey;
  const iconv = new Iconv('UTF-8', 'ISO-8859-1');
  const buffer = iconv.convert(input);
  const hash = crypto.createHash('sha1');
  hash.update(buffer);
  const md = hash.digest();
  const auth = md.toString('base64');
  return auth;
};

const _transformQuotationToPlanDetails = (quotation, cid, groupDetails, primaryApplicaiton) => {
  let planDetails, plans, subQuotation;
  // Sub Quotation includes the Basic plan and rider for 1 LA
  subQuotation  = _.cloneDeep(_.get(quotation, `insureds.${cid}`));
  plans = _.cloneDeep(_.get(quotation, `insureds.${cid}.plans`));

  if (groupDetails) {
    planDetails = _seperatePlanDetailForShield(subQuotation);
  } else if (primaryApplicaiton) {
    subQuotation.plans = _.filter(plans, obj => {return obj.covCode === 'ASIM';});
    planDetails = _transformSinglePlanDetail(subQuotation);
  } else if (primaryApplicaiton === false ) {
    subQuotation.plans = _.filter(plans, obj => { return obj.covCode !== 'ASIM';});
    planDetails = _transformSinglePlanDetail(subQuotation);
  } else {
    planDetails = _transformSinglePlanDetail(subQuotation);
  }

  return planDetails;
};

const _seperatePlanDetailForShield = (subQuotation) => {
  const shieldPlanType = 'Health Plan';
  const planTypeMapping = {
    A: 'Plan A',
    B: 'Plan B',
    C: 'Standard Plan',
    D: ''
  };
  let planDetails = _transformSinglePlanDetail(subQuotation);
  let orginalPlanList = _.get(planDetails, 'planList');
  orginalPlanList = _.map(orginalPlanList, (planList, key) => {
    let planTypeDesc = planTypeMapping[planList.covClass];
    let covNameDesc = {};
    _.forEach(planList.covName, (name, nkey) => {
      let replaceTarget = ' (' + planTypeDesc + ')';
      if (_.endsWith(name, replaceTarget)) {
        covNameDesc[nkey] = _.replace(name, replaceTarget, '');
      } else {
        covNameDesc[nkey] = name;
      }
    });

    planList = Object.assign(planList, {
      planType: shieldPlanType,
      planTypeDesc: planTypeDesc,
      covNameDesc: covNameDesc
    });

    if (planList.covCode === 'ASIM') {
      planList.cashPortion =  _.get(subQuotation, 'policyOptions.cashPortion');
      planList.cpfPortion = _.get(subQuotation, 'policyOptions.cpfPortion');
      planList.medisave = _.get(subQuotation, 'policyOptions.medisave');
    }

    return planList;
  });

  let basicPlanList = _.filter(orginalPlanList, obj => {return obj.covCode === 'ASIM';});
  let riderPlanList = _.filter(orginalPlanList, obj => {return obj.covCode !== 'ASIM';});

  planDetails = _.omit(planDetails, 'planList');
  planDetails.basicPlanList = basicPlanList;
  planDetails.riderPlanList = riderPlanList;
  planDetails.totalRiderPremium = _.get(subQuotation, 'totRiderPrem');

  return planDetails;
};

const _transformSinglePlanDetail = (subQuotation) => {
  let typeMap = {
    'SAV': 1
  };
  let planDetails, biName, baseProductCode, plans, pIndex, plan;
  let changedPlans = _.cloneDeep(subQuotation.plans);
  _.forEach(changedPlans, (p)=>{
    if (p.saViewInd !== 'Y') {
      p.sumInsured = ' - ';
    }
    if (_.has(p, 'multiplyBenefit') && p.othSaInd !== 'Y') {
      p.multiplyBenefit = ' - ';
    }
  });
  planDetails = {
    planList: changedPlans,
    type: typeMap[subQuotation.baseProductCode],
    ccy: subQuotation.ccy,
    isBackDate: subQuotation.isBackDate,
    paymentMode:subQuotation.paymentMode,
    riskCommenDate:subQuotation.riskCommenDate,
    totYearPrem:subQuotation.premium,
    premium: subQuotation.premium,
    sumInsured: subQuotation.sumInsured
  };

  if (subQuotation.plans[0].covClass) {
    planDetails.covClass = subQuotation.plans[0].covClass;
  }

  if (subQuotation.fund && subQuotation.fund.funds) {
    planDetails.fundList = subQuotation.fund.funds;
  }

  if (subQuotation.policyOptions) {
    planDetails = Object.assign(planDetails, subQuotation.policyOptions);
  }

  biName = null;
  baseProductCode = subQuotation.baseProductCode;
  plans = subQuotation.plans;
  for (pIndex = 0; pIndex < plans.length; pIndex++){
    plan = plans[pIndex];
    if (plan.covCode === baseProductCode){
      biName = plan.covName;
    }
  }
  planDetails.planTypeName = biName;
  return planDetails;
};

var _getIdFromSignDocId = function(signDocId) {
  var result = {
    docId:'',
    attId:''
  };

  var ids = signDocId.split('_');
  result.docId = ids[0];
  if (ids.length > 1) {
    result.attId = ids[1];
  }
  return result;
};

/**
 * agentCode = session.agentCode
 * agentData = session.agent
 */
var isSuppDocsReadOnly = function(app, agentData, agentCode) {
  logger.log('INFO: getSupportDocuments - isSuppDocsReadOnly', app.id);
  const faFirmCode = 'MM1';
  const suppDocsReadOnlyCodes = ['A', 'R', 'E']; // 'Approved', 'Rejected', 'Expired'
  const isFaChannel = _.get(agentData, 'channel.type') === 'FA';
  const isFaFirm = _.get(agentData, 'rawData.userRole') === faFirmCode;

  return new Promise((resolve)=>{
    bDao.getApplicationByBundleId(_.get(app, 'bundleId'), app.id).then((bApp)=>{
      if (_.get(bApp, 'appStatus') === 'APPLYING') {
        // If agent is the creator, can upload, otherwise cannot upload.
        resolve(!(_.get(app.quotation.agent, 'agentCode') === agentCode));
      } else if (_.get(bApp, 'appStatus') === 'INVALIDATED' || _.get(bApp, 'appStatus') === 'INVALIDATED_SIGNED') {
        resolve(true);
      } else {
        let approvalDocId;

        if (app.type === 'masterApplication') {
          approvalDocId = ApprovalHandler.getMasterApprovalIdFromMasterApplicationId(app.id);
        } else {
          approvalDocId = app.policyNumber;
        }
        approvalDao.searchApprovalCaseById(approvalDocId, function(resp){

          if (_.includes(suppDocsReadOnlyCodes, resp.foundCase.approvalStatus)) {
            // If a case is approved, rejected, expired, support document readonly.
            resolve(true);
          } else {
            if (isFaChannel && isFaFirm) {
              // FA Firm can +never + upload
              resolve(true);
            } else {
              resolve(!ApprovalHandler.canApproveCase(agentData, resp.foundCase));
            }
          }
        });
      }
    });
  });
};

var getPayerInfo = function(app) {
  logger.log('INFO: getPayerInfo', app._id);
  if (_.get(app, 'applicationForm.values.proposer.declaration.FUND_SRC02') === 'other') {
    return {
      isThirdPartyPayer: true,
      payerName: _.get(app, 'applicationForm.values.proposer.declaration.FUND_SRC03') + ' ' +  _.get(app, 'applicationForm.values.proposer.declaration.FUND_SRC04')
    };
  } else {
    let payerName = '';
    if (_.get(app, 'applicationForm.values.proposer.declaration.FUND_SRC02') === _.get(app, 'pCid')) {
      payerName = _.get(app, 'quotation.pFullName');
    } else if (_.get(app, 'applicationForm.values.proposer.declaration.FUND_SRC02') === _.get(app, 'iCid')) {
      payerName = _.get(app, 'quotation.iFullName');
    } else {
      logger.log('INFO: getPayerInfo - no payer is found', app._id);
    }

    return {
      isThirdPartyPayer: false,
      payerName: payerName
    };
  }
};

const _updateAndReturnApp = function(app, cb) {
  async.waterfall([
    (callback) => {
      //Update Application
      appDao.updApplication(app.id, app, () => {
        callback(null, app.id);
      });
    }, (aId, callback) => {
      appDao.getApplication(aId, (fApp) => {
        callback(null, fApp);
      });
    }
  ], (err, finalApplication) => {
    if (err) {
      logger.error(`ERROR in initial payment store: ${err}`);
      cb(err, {success: false});
    } else {
      cb(null, finalApplication);
    }
  });
};

const _getAllPolicyIdsStrFromMaster = (iCidMapping) => {
  let policyIds = '';
  _.each(iCidMapping, cidArray => {
    _.each(cidArray, policyObj => {
      if (policyObj && policyObj.policyNumber && policyIds === '') {
        policyIds = policyObj.policyNumber;
      } else if (policyObj && policyObj.policyNumber) {
        policyIds = `${policyIds}, ${policyObj.policyNumber}`;
      }
    });
  });
  return policyIds;
};

var _getAgeForCheckingCrossAge = function(mode, iAge, iDob) {
  let result = {};
  let now = moment().toDate();
  let future = moment().add(global.config.crossAgeDay, 'days').toDate();

  if (mode === CROSSAGE_MODE.NEAREST_AGE) {
    result.ageCrossLine = iAge + 1;
    result.currentAge = DateUtils.getNearestAge(now, moment(iDob).toDate());
    result.futureAge = DateUtils.getNearestAge(future, moment(iDob).toDate());
  } else if (mode === CROSSAGE_MODE.NEXT_BIRTHDAY) {
    result.ageCrossLine = iAge + 1;
    result.currentAge = DateUtils.getAgeNextBirthday(now, moment(iDob).toDate());
    result.futureAge = DateUtils.getAgeNextBirthday(future, moment(iDob).toDate());
  }
  return result;
};

var _mergePdfBase64ToAppFormPdf = function (app, pdfStr, callback) {
  logger.log('INFO: mergePdfBase64ToAppFormPdf - start', app._id);
  //get eApp form and merge with premium payment

  async.waterfall([
    (cb) => {
      appDao.getAppAttachment(app._id, PDF_NAME.eAPP, (appPdf) => {
        if (appPdf.success) {
          cb(null, appPdf);
        } else {
          cb('Fail to get attachment');
        }
      });
    },
    (appPdf, cb) => {
      if (pdfStr && pdfStr.length > 0) {
        logger.log('INFO: mergePdfBase64ToAppFormPdf - pdfString length', pdfStr.length, app._id);
        CommonFunctions.mergePdfs([appPdf.data, pdfStr], function(mergedPdf) {
          if (mergedPdf) {
            logger.log('INFO: mergePdfBase64ToAppFormPdf - mergedPdf done', app._id);
            cb(null, mergedPdf);
          } else {
            cb('Fail to merge pdf');
          }
        });
      } else {
        cb('pdfStr length = 0');
      }
    },
    (mergedPdf, cb) => {
      dao.uploadAttachmentByBase64(app._id, PDF_NAME.eAPP, app._rev, mergedPdf, 'application/pdf', function(res) {
        if (res && res.rev && !res.error) {
          app._rev = res.rev;
          cb(null, app);
        } else {
          cb('Fail to upload Pdf attachment');
        }
      });
    }
  ], (err, newApp) => {
    if (err) {
      logger.error('ERROR: mergePdfBase64ToAppFormPdf - end [RETURN=-100]', app._id, err);
      callback(false);
    } else {
      logger.log('INFO: mergePdfBase64ToAppFormPdf - end [RETURN=100]', app._id);
      callback(newApp);
    }
  });
};

const _saveParentValuestoChild = function(parentApplication, updateObj, callback) {
  let childIds = _.get(parentApplication, 'childIds');
  _application.getChildApplications(childIds).then(childApplications => {
      let updAppPromises = childApplications.map((app) => {
        app = _.assignIn(app, updateObj);
        return new Promise((resolve, reject) => {
          appDao.updApplication(app.id, app, (res) => {
            if (res && !res.error) {
              resolve(res);
            } else {
              reject(new Error('Fail to update application ' + app.id + ' ' + _.get(res, 'error')));
            }
          });
        });
      });

      return Promise.all(updAppPromises).then(results =>{
        callback(null, parentApplication);
      }, rejectReason => {
        callback(`ERROR in _saveParentPaymenttoChild with Reject Reason,  ${rejectReason}`);
      }).catch((error)=>{
        logger.error(`ERROR in _saveParentPaymenttoChild ${error}`);
        callback(`ERROR in _saveParentPaymenttoChild, ${error}`);
      });
    }
  ).catch(error => {
    logger.error(`ERROR in _saveParentPaymenttoChild: ${error}`);
  });
};

const _getEmailContent = function (content, params, embedImgs) {
  let ret = content;
  _.each(params, (value, key) => {
    ret = ret.replace(new RegExp('{{' + key + '}}', 'g'), value);
  });
  // _.each(embedImgs, (value, key) => {
  //   ret = ret.replace(new RegExp('cid:' + key, 'g'), 'data:image/png;base64,' + value);
  // });
  return ret;
};

const _prepareEmailAttachments = function (pdfs, email, quotation, application) {
  let promises = [];
  //Password logic for attachments
  let password;
  if (email.receiverType === 'agent') {
    let agentCode = quotation.agent.agentCode;
    password = agentCode.substr(agentCode.length - 6, 6);
  } else {
    let dob = new Date(quotation.pDob);
    password = dob.format('ddmmmyyyy').toUpperCase();
  }
  let isShield = _.get(quotation, 'quotType', '') === 'SHIELD';
  // _.each(email.attachmentIds, (attachmentId) =>
  _.each(_.keys(_.get(pdfs, email.id, {})), (attachmentId) => {
    promises.push(new Promise((resolve) => {
      let pdf = pdfs[email.id][attachmentId];
      if (attachmentId === 'fna'){
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: 'fna_report.pdf',
            data: data
          });
        });
      } else if (attachmentId === 'bi' && ProposalUtils.checkIsShield(quotation)){
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: 'policy_illustration.pdf',
            data: data
          });
        });
      } else if (attachmentId === 'bi' && !ProposalUtils.checkIsShield(quotation)){
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: 'policy_illustration_document(s).pdf',
            data: data
          });
        });
      } else if (attachmentId === 'prodSummary'){
        resolve({
          isProtectBase:false,
          fileName: 'product_summary.pdf',
          data: pdf
        });
      } else if (attachmentId === 'shield_product_summary' && _.get(application, 'isProposalSigned')){
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: 'AXA Shield Product Summary.pdf',
            data: data
          });
        });
      }  else if (attachmentId === 'shield_product_summary' && !_.get(application, 'isProposalSigned')){
        resolve({
          isProtectBase:false,
          fileName: 'AXA Shield Product Summary.pdf',
          data: pdf
        });
      } else if (attachmentId === 'phs') {
        resolve({
          isProtectBase:false,
          fileName: 'product_highlight_sheet.pdf',
          data: pdf
        });
      } else if (attachmentId === 'fib') {
        resolve({
          isProtectBase:false,
          fileName: 'fund_info_booklet.pdf',
          data: pdf
        });
      } else if (attachmentId.indexOf(PDF_NAME.eAPP) > -1 && isShield) {
        let fileName = _.get(application, `supportDocuments.values.policyForm.sysDocs.${attachmentId}.title`, 'eapp_form') + '.pdf';
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: fileName,
            data: data
          });
        });
      } else if (attachmentId === 'app') {
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: 'eapp_form.pdf',
            data: data
          });
        });
      } else if (attachmentId === 'eCpd') {
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: 'eCPD_form.pdf',
            data: data
          });
        });
      }
    }));
  });
  return Promise.all(promises);
};

async function getPdfs (emails, generatedPdfs, failedGenPdfIds, isShield, cid, compCode, lang) {
  const tempPdfs = {};
  const tempFailedPdfs = {};
  
  for(let i = 0; i < emails.length; i += 1) {
    const id = emails[i].id;
    let requiredPdfs = {};
    generatedPdfs[emails[i]] = {};
    failedGenPdfIds[emails[i]] = {};
    _.each(emails[i].attachmentIds, (id) => {
      requiredPdfs[id] = true;
    })
      _.set(tempPdfs, emails[i].id, {});
      _.set(tempFailedPdfs, emails[i].id, {});
      if (requiredPdfs.fna) {
        // bDao.getCurrentBundle(cid).then(bundle=>{
          tempPdfs[id].fna = await new Promise(resolve => {
          quotDao.getQuotation(emails[i].quotId, (quotation)=>{
            bDao.getBundle(cid, quotation.bundleId, (bundle) => {
              if (bundle.isFnaReportSigned){
                dao.getAttachment(bundle.id, PDF_NAME.FNA, (pdfResult)=>{
                  resolve(pdfResult.data);
                });
              } else {
                needsHandler.generateFNAReport({cid}, session, (pdfResult) => {
                  resolve(pdfResult.fnaReport);
                });
              }
            });
          });
        });
      } 
      if (requiredPdfs.bi){
        tempPdfs[id].bi = await new Promise(resolve => {
          quotDao.getQuotationPDF(emails[i].quotId, false, (pdfResult)=>{
            resolve(pdfResult.data);
          })
        });
      }
      /*
      * Let product Summary handled by ProposalUtils function
      else if (id === 'prodSummary'){
        promises.push(new Promise((psResolve)=>{
          quotDao.getQuotation(email.quotId,(quotation)=>{
            ProposalUtils.getProductSummaries(compCode, quotation, lang).then((pdfResult) => {
              generatedPdfs[email.id][id] = pdfResult;
              psResolve();
            });
          });
        }));
      }
      */
      if (requiredPdfs.shield_product_summary){
        tempPdfs[id].shield_product_summary = await new Promise(resolve => {
          quotDao.getQuotation(emails[i].quotId,(quotation)=>{
            quotHandler.getPlanDetailsByQuot(session, quotation).then((planDetails) => {
              ProposalUtils.getExtraAttachmentPdf("shield_product_summary", session.agent, quotation, planDetails, true).then((pdfResult) => {
                resolve(pdfResult);
              });
            });
          });
        }) 
      }
      if (requiredPdfs.phs) {
        tempPdfs[id].phs = await new Promise(resolve => {
          quotDao.getQuotation(emails[i].quotId, (quotation)=>{
            ProposalUtils.getProductHighlightSheets(compCode, quotation).then((pdfResult)=>{
              resolve(pdfResult);
            });
          });
        });
      }
      if (requiredPdfs.fib) {
        tempPdfs[id].fib = await new Promise(resolve => {
          quotDao.getQuotation(emails[i].quotId, (quotation)=>{
            ProposalUtils.getFundInfoBooklet(quotation.baseProductId, lang).then((pdfResult)=>{
              resolve(pdfResult);
            });
          });
        });
      } 
      if (requiredPdfs[PDF_NAME.eAPP] > -1 && isShield) {
        tempPdfs[id].eAPP = await new Promise(resolve => {
          appDao.getAppAttachment(emails[i].appId, id, function(pdfResult) {
            resolve(pdfResult.data);
          });
        })
      } 
      if (requiredPdfs.app) {
        tempPdfs[id].app = await new Promise(resolve => {
          appDao.getAppAttachment(emails[i].appId, PDF_NAME.eAPP, function(pdfResult) {
            resolve(pdfResult.data);
          });
        });
      }
      if (requiredPdfs.eCpd) {
        tempPdfs[id].eCpd = await new Promise(resolve => {
          appDao.getAppAttachment(emails[i].appId, PDF_NAME.eCPD, function(pdfResult) {
            resolve(pdfResult.data);
          });
        });
      } 
      if (requiredPdfs.prodSummary) {
        tempFailedPdfs[id].prodSummary = true;
      }
      if (requiredPdfs.FPXProdSummary) {
        tempFailedPdfs[id].FPXProdSummary = true;
      }
  }
  return {
    generatedPdfs: tempPdfs,
    failedGenPdfIds: tempFailedPdfs
  };
};

const preparePdfs = function(emails, cid, session, lang){
  let compCode = session.agent.compCode;
  return new Promise((resolve) => {
    let generatedPdfs = {};
    let failedGenPdfIds = {};
    let promises = [];
    let isShield = emails.isShield;
    resolve(getPdfs(emails, generatedPdfs, failedGenPdfIds, isShield, cid, compCode, lang));
  });
};

async function sendEmailsToClientAndAgent(companyInfo, emails, session, pdfs, agent, failedGenPdfIds, cid, authToken, webServiceUrl){
  for(let i = 0; i < emails.length; i++) {
    await new Promise(resolve => {
      quotDao.getQuotation(emails[i].quotId, ((quotation)=>{
        emails[i].quotation = quotation;
        let standalone = session.agent.channel.type === 'AGENCY' ? quotation.quickQuote : !emails[i].appId;
  
        return quotHandler.getPlanDetailsByQuot(session, quotation).then((planDetails) => {
          appDao.getApplication(emails[i].appId, ((application)=>{
            logger.log('INFO: sendApplicationEmail - _prepareEmailAttachments', cid);
  
            // Step 2: Use the generated pdfs, to generate attachments
            return _prepareEmailAttachments(pdfs, emails[i], emails[i].quotation, application).then((appAttachments)=>{
              // Step 3: failedGenPdfIds will become input parameter to generate attachments in product functions
              // return ProposalUtils.getAttachments(agent, quotation, planDetails, standalone, _.keys(failedGenPdfIds[email.id])).then((productAttachments) => {
                return ProposalUtils.getAttachments(agent, quotation, planDetails, standalone, _.keys(failedGenPdfIds[emails[i].id])).then((productAttachments) => {
  
                let allAttachments = [];
                allAttachments = allAttachments.concat(appAttachments);
                allAttachments = allAttachments.concat(productAttachments);
                _.each(emails[i].embedImgs, (value, key) => {
                  allAttachments.push({
                    fileName: key,
                    cid: key,
                    data: value
                  });
                });
                logger.log('INFO: sendApplicationEmail - sendEmail callApi', cid, emails[i].quotId);
                sendEmail({
                  from: companyInfo.fromEmail,
                  to: emails[i].to[0],
                  title: emails[i].title,
                  content: emails[i].content,
                  attachments: allAttachments,
                  userType: emails[i].id.indexOf('agent') > -1 ? 'agent': 'client',
                  agentCode: emails[i].agentCode,
                  dob: emails[i].dob
                }, (result) => {
                  logger.log('INFO: sendApplicationEmail - sendEmail result', cid, result);
                  resolve();
                }, authToken, webServiceUrl);
              });
            });
          }));
        });
      }));
    })
  }
};

const _sendApplicationEmail = function(data, session, cb) {
  const {agent} = session;
  let {emails, cid, lang, isShield, authToken, webServiceUrl} = data;
  logger.log('INFO: sendApplicationEmail - start', cid);

  emails.isShield = isShield;
  if (emails){
    logger.log('INFO: sendApplicationEmail - preparePdfs', cid);

    // Step 1: generate the pdfs for every emails, return a successfully generated pdfs and failed generated pdfs ids list.
    // preparePdfs(emails, cid, session, lang).then((pdfs)=>{
    // Callback first to prevent time out in sending email
    cb({ success: true });
    preparePdfs(emails, cid, session, lang).then((pdfResult)=>{
      const pdfs = pdfResult.generatedPdfs;
      const failedGenPdfIds = pdfResult.failedGenPdfIds;

      companyDao.getCompanyInfo((companyInfo) => {
        sendEmailsToClientAndAgent(companyInfo, emails, session, pdfs, agent, failedGenPdfIds, cid, authToken, webServiceUrl);
      });
    }).catch((error) => {
      logger.error('ERROR: sendApplicationEmail - preparePdfs', cid, error);
    });
  } else {
    logger.log('INFO: sendApplicationEmail - end [RETURN=2]', cid);
    cb({success: true});
  }
};

const _prepareSubmissionEmails = function(emailValues, session, cb){
  let {emailContent, agentName, agentContactNum, agentEmail,
    agentTitle, clientName, clientEmail, clientId,
    clientTitle, policyNumber, submissionDate, quotId, attKeys, appId, receiveEmailFa, isShield = false} = emailValues;
  let {clientSubject, clientContent, agentSubject, agentContent, embedImgs} = emailContent;
  let params = {
    agentName: agentName,
    agentContactNum: agentContactNum,
    clientName: clientName,
    policyNumber: policyNumber,
    agentTitle: agentTitle,
    clientTitle: clientTitle,
    submissionDate: submissionDate
  };
  let emails = [];
  // let attachments = [];
  let data = {};

  logger.log('INFO: prepareSubmissionEmails - start', appId);

  if (receiveEmailFa === 'Y') {
    emails.push({
      id: 'agent',
      receiverType:'agent',
      to: [agentEmail],
      title: _getEmailContent(agentSubject, params),
      content: _getEmailContent(agentContent, params, embedImgs),
      attachmentIds: [],
      embedImgs: embedImgs,
      quotId: quotId,
      appId: appId
    });
    emails.push({
      id: 'client',
      receiverType:'client',
      to: [clientEmail],
      title: _getEmailContent(clientSubject, params),
      content: _getEmailContent(clientContent, params, embedImgs),
      attachmentIds: attKeys,
      embedImgs: embedImgs,
      quotId: quotId,
      appId: appId
    });
    data.emails = emails;
    data.cid = clientId;
    data.isShield = isShield;
    _sendApplicationEmail(data, session, (res) => {
      logger.log('INFO: prepareSubmissionEmails - end', appId);
      cb(res);
    });
  } else {
    cb({ success: true });
  }
};

const _transformSuppDocsPolicyFormValues = function(app){
  logger.log('INFO: _transformSuppDocsPolicyFormValues', app.id);
  let requireTransformIds = ['axasam', 'chequeCashierOrder', 'teleTransfter', 'cash'];
  let initPayMethod = _.get(app, 'payment.initPayMethod');

  if (_.includes(requireTransformIds, initPayMethod)) {
    let optDoc = _.get(app, 'supportDocuments.values.policyForm.optDoc');
    let mandDocs = _.get(app, 'supportDocuments.values.policyForm.mandDocs');
    if (_.isEmpty(optDoc.axasam) && _.isEmpty(optDoc.chequeCashierOrder)
      && _.isEmpty(optDoc.teleTransfer) && _.isEmpty(optDoc.cash)) {
        app.isMandDocsAllUploaded = false;
        logger.log('INFO: appFormSubmission - transformSuppDocsPolicyFormValues, isMandDocsAllUploaded turned to false');
    }
    Object.assign(
      mandDocs,
      {
        axasam: optDoc.axasam,
        chequeCashierOrder: optDoc.chequeCashierOrder,
        teleTransfer: optDoc.teleTransfer,
        cash: optDoc.cash,
      }
    );
    optDoc.axasam = [];
    optDoc.chequeCashierOrder = [];
    optDoc.teleTransfer = [];
    optDoc.cash = [];
  }
};

const _updateChildSupportDocumentValue = function(parentApplication, callback) {
  logger.log('INFO: _updateChildSupportDocumentValue [parentAppId: ', parentApplication.id, ' ]');
  if (_.get(parentApplication, 'type') === 'masterApplication') {
    let childIds = _.get(parentApplication, 'childIds');
    _application.getChildApplications(childIds).then(childApplications => {
      let updAppPromises = childApplications.map((app) => {
        _.set(app, 'supportDocuments.values', {
          policyForm: _.get(parentApplication, 'supportDocuments.values.policyForm'),
          proposer: _.get(parentApplication, 'supportDocuments.values.proposer'),
          insured: _.get(parentApplication, 'supportDocuments.values.' + app.iCid),
        });
        return new Promise((resolve, reject) => {
          appDao.updApplication(app.id, app, (res) => {
            if (res && !res.error) {
              resolve(res);
            } else {
              reject(new Error('Fail to update application ' + app.id + ' ' + _.get(res, 'error')));
            }
          });
        });
      });

      return Promise.all(updAppPromises).then(results =>{
        callback(null, parentApplication);
      }, rejectReason => {
        callback(`ERROR in _saveParentPaymenttoChild with Reject Reason,  ${rejectReason}`);
      }).catch((error)=>{
        logger.error(`ERROR in _saveParentPaymenttoChild ${error}`);
        callback(`ERROR in _saveParentPaymenttoChild, ${error}`);
      });

    }).catch(error => {
      logger.error(`ERROR in _saveParentPaymenttoChild: ${error}`);
    });
  } else {
    callback('Application is Not Shield', parentApplication);
  }
};

const _deletePendingSubmitList = function(app) {
  logger.log('INFO: closeSuppDocs - deletePendingSubmitList');
  return new Promise((resolve) => {
    if (_.get(app, 'supportDocuments.pendingSubmitList')) {
      // remove the attachments
      appDao.deleteAppAttachments(app.id, _.get(app, 'supportDocuments.pendingSubmitList'), (resp) => {
        if (resp.success) {
          let changedApplication = resp.application;
          _updateChildSupportDocumentValue(changedApplication, () => {
            // remove pendingSubmitList
            delete changedApplication.supportDocuments['pendingSubmitList'];
            resolve({
              success: true,
              application: changedApplication
            });
          });
        } else {
          resolve({success: false});
        }
      });
    } else {
      resolve({
        success: true,
        application: app
      });
    }
  });
};

const _isShowHealthDeclaration = function(app){
  const daysAgoTime = moment().subtract(global.config.after_pEAppSigned_days, 'days').endOf('day');
  const pEAppSignedTime = moment(_.get(app, 'applicationSignedDate')).endOf('day');
  return pEAppSignedTime.valueOf() <= daysAgoTime.valueOf();
};

// has to make sure input parm arrOne & arrTwo are arrayType
const _isTwoArraysEqual = function(arrOne, arrTwo) {
  if (!arrOne || !arrTwo) {
    return false;
  }
  return _.isEmpty(_.difference(arrOne, arrTwo)) && _.isEmpty(_.difference(arrTwo, arrOne));
};

module.exports.EAPP_STEP = EAPP_STEP;
module.exports.PDF_NAME = PDF_NAME;
module.exports.TEMPLATE_NAME = TEMPLATE_NAME;
module.exports.CAPITAL_LETTER_IDS = CAPITAL_LETTER_IDS;
module.exports.FNA_QUESTIONS = FNA_QUESTIONS;
module.exports.CROSSAGE_MODE = CROSSAGE_MODE;
module.exports.CROSSAGE_STATUS = CROSSAGE_STATUS;
module.exports.CLIENT_FIELDS_TO_UPDATE = CLIENT_FIELDS_TO_UPDATE;
module.exports.APP_FORM_SHIELD_GROUP_HEALTH_QN = APP_FORM_SHIELD_GROUP_HEALTH_QN;
module.exports.APP_FORM_SHIELD_GROUP_HEALTH_F_QN = APP_FORM_SHIELD_GROUP_HEALTH_F_QN;

module.exports.getChannelType = _getChannelType;
module.exports.getPolicyNumberFromApi = _getPolicyNumberFromApi;
module.exports.populateFromBundle = _populateFromBundle;
module.exports.getShieldApplicationIds = _getShieldApplicationIds;
module.exports.getApplicationId = _getApplicationId;
module.exports.removeInvalidProfileProp = _removeInvalidProfileProp;
module.exports.getOptionsList = _getOptionsList;
module.exports.replaceAppFormValuesByTemplate = _replaceAppFormValuesByTemplate;
module.exports.removeAppFormValuesByTrigger = _removeAppFormValuesByTrigger;
module.exports.updateClientFormValues = _updateClientFormValues;
module.exports.frozenTemplate = _frozenTemplate;
module.exports.frozenTemplateMobile = _frozenTemplateMobile;
module.exports.frozenAppFormTemplateFnaQuestions = _frozenAppFormTemplateFnaQuestions;
module.exports.getReportTemplates = _getReportTemplates;
module.exports.checkAppFormByPerson = _checkAppFormByPerson;
module.exports.checkLaIsAdult = _checkLaIsAdult;
module.exports.checkIsLaSignedByProposer = _checkIsLaSignedByProposer;
module.exports.genSignDocAuth = _genSignDocAuth;
module.exports.transformQuotationToPlanDetails = _transformQuotationToPlanDetails;
module.exports.getIdFromSignDocId = _getIdFromSignDocId;

module.exports.isSuppDocsReadOnly = isSuppDocsReadOnly;
module.exports.getPayerInfo = getPayerInfo;
module.exports.updateAndReturnApp = _updateAndReturnApp;
module.exports.getAllPolicyIdsStrFromMaster = _getAllPolicyIdsStrFromMaster;
module.exports.getAgeForCheckingCrossAge = _getAgeForCheckingCrossAge;
module.exports.mergePdfBase64ToAppFormPdf = _mergePdfBase64ToAppFormPdf;
module.exports.saveParentValuestoChild = _saveParentValuestoChild;
module.exports.sendApplicationEmail = _sendApplicationEmail;
module.exports.prepareSubmissionEmails = _prepareSubmissionEmails;
module.exports.transformSuppDocsPolicyFormValues = _transformSuppDocsPolicyFormValues;
module.exports.updateChildSupportDocumentValue = _updateChildSupportDocumentValue;
module.exports.deletePendingSubmitList = _deletePendingSubmitList;
module.exports.isShowHealthDeclaration = _isShowHealthDeclaration;

module.exports.isTwoArraysEqual = _isTwoArraysEqual;

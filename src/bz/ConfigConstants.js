module.exports = {
  SearchLatency: 2000,
  //SearchMinLenght: 3,
  SearchMinLenght: 1,
  TablePageSize: 10,
  TableMaxSize: 100,
  DateTimeFormat: "dd mmm yyyy HH:MM",
  DateTimeFormat2: "dd/mm/yyyy HH:MM",
  DateTimeFormat3: "DD/MM/YYYY",
  MomentDateTimeFormatForApproval: "DD/MM/YYYY HH:mm",
  DateTimeWithTime: "DD/MM/YYYY HH:mm:ss",
  DateFormat: "dd/mm/yyyy",
  DateFormatwithTime: "YYYY-MM-DD, h:mm:ss a",
  TimeFormat: "HH:MM",
  TimeoutSecond: 100,
  SessionExpiry_Min: 15,        // session expire at 15 minutes
  SessionAlertMin: 10,    // show alert starting at 10 minutes
  SessionIntervalCheck_Millsec: 10000,  //default 10 seconds
  DependantsMax: 10,             //default max dependant is 10
  DefaultEmailSender: 'mail@eabsystems.com', //TODO should be change the sender before deployment
  PendingToApprovalListLink: 'PendingToApprovalListLink', // TODO waiting for deep linking
  MOMENT_TIME_ZONE: 'Asia/Singapore',
  APPROVAL_ACCESS_ROLE: ['MM1', 'MM2', 'MA'],
  APPROVAL_FA_ACCESS_ROLE: ['DS', 'DM', 'MM1']
};

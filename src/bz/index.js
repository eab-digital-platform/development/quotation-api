"use strict";
global.logger = console;


var authFuncs = require('./AuthHandler');
var agentFuncs = require('./AgentHandler');
var clientFuncs = require('./ClientHandler');
var prodFuncs = require('./handler/ProductHandler');
var quotFuncs = require('./handler/QuotationHandler');
var needsFuncs = require('./NeedsHandler');
// var auditFuncs = require('./AuditHandler');
var fileFuncs = require('./FileHandler');
var applicationFuncs = require('./handler/ApplicationHandler');
var shieldApplicationFuncs = require('./handler/application/shield/index');
var clientChoiceFuncs = require('./handler/ClientChoiceHandler');
var emailFuncs = require('./EmailHandler');
var smsFuncs = require('./SMSHandler');
var approvalFuncs = require('./ApprovalHandler');
var approveNotifyFuncs = require('./ApprovalNotificationHandler');
var viewFuncs = require('./ViewHandler');
var invalidationFuncs = require('./InvalidationHandler');
// var commFuncs = require('../common/CommonUtils');
var nativeRunner = require('./nativeRunner');

global.JavaRunner = nativeRunner;

var handlers = {
  auth: authFuncs,
  agent: agentFuncs,
  client: clientFuncs,
  product: prodFuncs,
  quot: quotFuncs,
  needs: needsFuncs,
  application: applicationFuncs,
  shieldApplication: shieldApplicationFuncs,
  clientChoice: clientChoiceFuncs,
  approval: approvalFuncs,
  approvalNotification: approveNotifyFuncs,
  email: emailFuncs,
  sms: smsFuncs,
  file: fileFuncs,
  view: viewFuncs,
  invalidation: invalidationFuncs
};

// check referer
var isValidRequest = function(req, cb) {
  if (req.url && req.data && req.data.action) {
    // these 2 lines are for allowing concurrent login
    cb(true);
    return true;
    } else {
      cb(false, "error.invaid_request");
    }
  return false;
}

/**
 * 
 * @param {Object} req request object
 * @param {string} req.url request path
 * @param {string} req.data request data
 * @param {string} req.data.action request action
 * @return {Promise} promise result 
 */
module.exports.handleRequest = function(req) {
  return new Promise(function(resolve, reject){
    isValidRequest(req, (valid, message)=>{
      if (valid) {
        var handler = handlers[req.url];
        if (handler) {
          var func = handler[req.data.action];
          if (func && typeof func == 'function') {
            func(req.data, global.session, resolve);
          } else {
            reject("handler method is not found");
          }
        } else {
          reject("handler is not found");
        }
      } else {
        console.log("WARNING: Invalid path and action in req.url and req.data.action");
        console.log('req: ', req);
        reject("Invalid path and action in req.url and req.data.action");
      }
    })
  });
};

module.exports.handlers = handlers;
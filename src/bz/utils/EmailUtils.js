var {
    callApi
} = require('./RemoteUtils.js');

/*
    email: {
        from: ""
        to: ""
        title: ""
        content: "",
        attachments: [{
            fileName: "",
            data: "{base64 string}"
        }]
    }
*/
module.exports.sendEmail = function(email, cb) {
    callApi('/email/sendEmail', 
        email
    , cb)
}

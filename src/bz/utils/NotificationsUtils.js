'use strict';
const NOTIFICATION_TEMPLATE = require('../template/index');
const _map = require('lodash/fp/map');
const _get = require('lodash/fp/get');
const _getOr = require('lodash/fp/getOr');
const _pipe = require('lodash/fp/pipe');
const _intersection = require( 'lodash/fp/intersection');
const _isEmpty = require('lodash/fp/isEmpty');
const _replace = require('lodash/fp/replace');

const TYPE = {
  'SUBMISSION_NOTIFICATION': 1,
  'ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION': 2,
  'DIRECTOR_NOTIFICATION': 3,
  'CASE_APPROVAL_REMINDER': 4,
  'APPROVAL_EXPIRY_NOTIFICATION': 5,
  'APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT': 6,
  'APPROVED_NOTIFICATION_FOR_SELECTED_AGENT': 7,
  'REJECTED_NOTIFICATION': 8,
  'SUBMISSION_NOTIFICATION_FOR_DIRECTOR': 9,
  'SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION': 10,
  'PENDING_FOR_DOCUMENT_NOTIFICATION': 11,
  'SUPERVISOR_APPROVAL_EMAIL_ALL_DOCUMENTS': 12,
  'APPROVED_NOTIFICATION_FOR_FAFIRM': 13,
  'APPROVED_NOTIFICATION_FOR_FASUPERVISOR': 14,
  'APPROVED_SUPERVISOR_NOTIFICATION_FOR_FAFIRM': 15,
  'REJECTED_NOTIFICATION_SUPERVISOR_TO_FAFIRM': 16,
  'REJECTED_NOTIFICATION_FAFIRM': 17,
  'ADDITIONAL_DOCUMENT_UPLOADED_FA_NOTIFICATION': 18,
  'ADDITIONALDOC_HEALTHDECLARATION': 19,
  'ABNORMAL_APPROVAL_CASE_NOTIFICATION': 20,
  'ABNORMAL_SUPP_DOC_CASE_NOTIFICATION': 21
};

const isValidType = (type) => {
  return type >= 1 && type <= 21;
};

const constructString = (patterns, needToPick, props, key, source) => {
  const replaceFuncs = _map((pattern) => _replace(pattern, _get(pattern, props)), _intersection(needToPick, patterns));
  return _pipe([
    _get(key),
    ...replaceFuncs
  ])(source);
};

const getAxaLogo = () =>{
  return NOTIFICATION_TEMPLATE.AXA_LOGO.axa_logo_attachment;
}

const getTitle = (type, props) => {
  const TITLE_KEY = 'email.subject';
  switch (type) {
    case TYPE.SUBMISSION_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.SUBMISSION_NOTIFICATION),
      ['proposalNumber', 'productName', 'agentName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.SUBMISSION_NOTIFICATION
    );

    case TYPE.ADDITIONAL_DOCUMENT_UPLOADED_FA_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.ADDITIONAL_DOCUMENT_UPLOADED_FA_NOTIFICATION),
      ['proposalNumber', 'productName', 'agentName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.ADDITIONAL_DOCUMENT_UPLOADED_FA_NOTIFICATION
    );

    case TYPE.ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION),
      ['proposalNumber', 'productName', 'agentName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION
    );

    case TYPE.DIRECTOR_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.DIRECTOR_NOTIFICATION),
      ['proposalNumber', 'productName', 'agentName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.DIRECTOR_NOTIFICATION
    );

    case TYPE.CASE_APPROVAL_REMINDER:
    return _get('email.subject', NOTIFICATION_TEMPLATE.CASE_APPROVAL_REMINDER);

    case TYPE.APPROVAL_EXPIRY_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVAL_EXPIRY_NOTIFICATION),
      ['caseNumber', 'productName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.APPROVAL_EXPIRY_NOTIFICATION
    );

    case TYPE.APPROVED_NOTIFICATION_FOR_FAFIRM:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_FAFIRM),
      ['proposalNumber', 'productName', 'proposerName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_FAFIRM
    );

    case TYPE.APPROVED_SUPERVISOR_NOTIFICATION_FOR_FAFIRM:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVED_SUPERVISOR_NOTIFICATION_FOR_FAFIRM),
      ['proposalNumber', 'productName', 'agentName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.APPROVED_SUPERVISOR_NOTIFICATION_FOR_FAFIRM
    );

    case TYPE.APPROVED_NOTIFICATION_FOR_FASUPERVISOR:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_FASUPERVISOR),
      ['proposalNumber', 'productName', 'agentName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_FASUPERVISOR
    );

    case TYPE.APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT),
      ['proposalNumber', 'productName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT
    );

    case TYPE.APPROVED_NOTIFICATION_FOR_SELECTED_AGENT:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_SELECTED_AGENT),
      ['proposalNumber', 'productName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_SELECTED_AGENT
    );

    case TYPE.REJECTED_NOTIFICATION_SUPERVISOR_TO_FAFIRM:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION_SUPERVISOR_TO_FAFIRM),
      ['proposalNumber', 'productName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION_SUPERVISOR_TO_FAFIRM
    );

    case TYPE.REJECTED_NOTIFICATION_FAFIRM:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION_FAFIRM),
      ['proposalNumber', 'productName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION_FAFIRM
    );

    case TYPE.REJECTED_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION),
      ['proposalNumber', 'productName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION
    );

    case TYPE.SUBMISSION_NOTIFICATION_FOR_DIRECTOR:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.SUBMISSION_NOTIFICATION_FOR_DIRECTOR),
      ['proposalNumber', 'productName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.SUBMISSION_NOTIFICATION_FOR_DIRECTOR
    );

    case TYPE.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION),
      ['proposalNumber', 'productName', 'agentName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION
    );

    case TYPE.PENDING_FOR_DOCUMENT_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.PENDING_FOR_DOCUMENT_NOTIFICATION),
      ['caseNumber', 'productName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.PENDING_FOR_DOCUMENT_NOTIFICATION
    );

    case TYPE.ADDITIONALDOC_HEALTHDECLARATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.ADDITIONALDOC_HEALTHDECLARATION),
      ['adviserName', 'proposalNumber', 'proposerName'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.ADDITIONALDOC_HEALTHDECLARATION
    );

    case TYPE.ABNORMAL_APPROVAL_CASE_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.ABNORMAL_APPROVAL_CASE_NOTIFICATION),
      ['policyNumber', 'ENV_NAME'],
      props,
      TITLE_KEY,
      NOTIFICATION_TEMPLATE.ABNORMAL_APPROVAL_CASE_NOTIFICATION
    );

    case TYPE.ABNORMAL_SUPP_DOC_CASE_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.ABNORMAL_SUPP_DOC_CASE_NOTIFICATION),
      ['applicationId', 'ENV_NAME'],
      props,
      NOTIFICATION_TEMPLATE.ABNORMAL_SUPP_DOC_CASE_NOTIFICATION
    );
    
    case TYPE.SUPERVISOR_APPROVAL_EMAIL_ALL_DOCUMENTS:
    return _get(TITLE_KEY, NOTIFICATION_TEMPLATE.SUPERVISOR_APPROVAL_EMAIL_ALL_DOCUMENTS);

    default:
    return '';
  }
};

const getContent = (type, props) => {
  const TEMPLATE_KEY = 'email.template';
  switch (type) {
    case TYPE.SUBMISSION_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.SUBMISSION_NOTIFICATION),
      ['adviserName', 'productName', 'proposalNumber', 'submissionDate', 'proposerName', 'expiryDate', 'actionLink'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.SUBMISSION_NOTIFICATION
    );

    case TYPE.ADDITIONAL_DOCUMENT_UPLOADED_FA_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.ADDITIONAL_DOCUMENT_UPLOADED_FA_NOTIFICATION),
      ['agentName', 'caseNumber', 'expiryDate'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.ADDITIONAL_DOCUMENT_UPLOADED_FA_NOTIFICATION
    );

    case TYPE.ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION),
      ['agentName', 'caseNumber', 'proposerName', 'expiryDate'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION
    );

    case TYPE.DIRECTOR_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.DIRECTOR_NOTIFICATION),
      ['adviserName', 'managerName', 'submissionDate', 'proposalNumber', 'clientName', 'expiryDate'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.DIRECTOR_NOTIFICATION
    );

    case TYPE.CASE_APPROVAL_REMINDER:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.CASE_APPROVAL_REMINDER),
      ['actionLink'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.CASE_APPROVAL_REMINDER
    );

    case TYPE.APPROVAL_EXPIRY_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVAL_EXPIRY_NOTIFICATION),
      ['agentName', 'caseNumber', 'proposerName', 'submissionDate'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.APPROVAL_EXPIRY_NOTIFICATION
    );

    case TYPE.APPROVED_NOTIFICATION_FOR_FAFIRM:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_FAFIRM),
      ['proposalNumber', 'proposerName', 'supervisorName', 'adviserName', 'approvalDate', 'faAdminApprovalRemarksText'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_FAFIRM
    );

    case TYPE.APPROVED_NOTIFICATION_FOR_FASUPERVISOR:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_FASUPERVISOR),
      ['adviserName', 'proposalNumber', 'proposerName', 'supervisorName', 'approvalDate', 'approvalRemarksText'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_FASUPERVISOR
    );

    case TYPE.APPROVED_SUPERVISOR_NOTIFICATION_FOR_FAFIRM:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVED_SUPERVISOR_NOTIFICATION_FOR_FAFIRM),
      ['adviserName', 'proposalNumber', 'proposerName', 'supervisorName', 'submissionDate', 'expiryDate', 'actionLink'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.APPROVED_SUPERVISOR_NOTIFICATION_FOR_FAFIRM
    );

    case TYPE.APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT),
      ['adviserName', 'proposalNumber', 'proposerName', 'supervisorName', 'approvalDate', 'approvalRemarksText'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT
    );

    case TYPE.APPROVED_NOTIFICATION_FOR_SELECTED_AGENT:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_SELECTED_AGENT),
      ['adviserName', 'proposalNumber', 'proposerName', 'supervisorName', 'approvalDate', 'approvalRemarksText'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_SELECTED_AGENT
    );

    case TYPE.REJECTED_NOTIFICATION_SUPERVISOR_TO_FAFIRM:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION_SUPERVISOR_TO_FAFIRM),
      ['adviserName', 'proposalNumber', 'proposerName', 'supervisorName', 'rejectionDate', 'reasonForRejection', 'rejectionCommentText'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION_SUPERVISOR_TO_FAFIRM
    );

    case TYPE.REJECTED_NOTIFICATION_FAFIRM:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION_FAFIRM),
      ['adviserName', 'proposalNumber', 'proposerName', 'rejectionDate', 'reasonForRejection', 'faAdminRejectionCommentText', 'faAdminreasonForRejection'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION_FAFIRM
    );

    case TYPE.REJECTED_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION),
      ['adviserName', 'proposalNumber', 'proposerName', 'supervisorName', 'rejectionDate', 'reasonForRejection', 'rejectionCommentText'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION
    );

    case TYPE.SUBMISSION_NOTIFICATION_FOR_DIRECTOR:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.SUBMISSION_NOTIFICATION_FOR_DIRECTOR),
      ['adviserName', 'proposalNumber', 'proposerName'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.SUBMISSION_NOTIFICATION_FOR_DIRECTOR
    );

    case TYPE.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION),
      ['adviserName', 'submissionDate', 'proposalNumber', 'proposerName', 'expiryDate', 'actionLink'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION
    );

    case TYPE.PENDING_FOR_DOCUMENT_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.PENDING_FOR_DOCUMENT_NOTIFICATION),
      ['agentName', 'caseNumber', 'proposerName', 'submissionDate', 'expiryDate'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.PENDING_FOR_DOCUMENT_NOTIFICATION
    );

    case TYPE.ADDITIONALDOC_HEALTHDECLARATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.ADDITIONALDOC_HEALTHDECLARATION),
      ['adviserName', 'proposalNumber', 'proposerName'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.ADDITIONALDOC_HEALTHDECLARATION
    );

    case TYPE.SUPERVISOR_APPROVAL_EMAIL_ALL_DOCUMENTS:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.SUPERVISOR_APPROVAL_EMAIL_ALL_DOCUMENTS),
      ['agentName'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.SUPERVISOR_APPROVAL_EMAIL_ALL_DOCUMENTS
    );

    case TYPE.ABNORMAL_APPROVAL_CASE_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.ABNORMAL_APPROVAL_CASE_NOTIFICATION),
      ['policyNumber', 'FAILED_REASON', 'ENV_NAME'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.ABNORMAL_APPROVAL_CASE_NOTIFICATION
    );
    
    case TYPE.ABNORMAL_SUPP_DOC_CASE_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.ABNORMAL_SUPP_DOC_CASE_NOTIFICATION),
      ['applicationId', 'FAILED_REASON', 'ENV_NAME'],
      props,
      TEMPLATE_KEY,
      NOTIFICATION_TEMPLATE.ABNORMAL_SUPP_DOC_CASE_NOTIFICATION
    );

    default:
    return '';
  }
};

const getEmailObject = (type, props) => {
  if (!isValidType(type)) {
    throw 'Invalid email type, please check the email type.';
  }
  const sender = _getOr('', 'sender', props);
  const recipients = _getOr('', 'recipients', props);
  const cc = _getOr('', 'cc', props);
  if (_isEmpty(sender) || _isEmpty(recipients)) {
    throw 'Sender/recipients is empty or undefined, please check the email props body.';
  }
  if (_isEmpty(cc)) {
    return {
      from: sender,
      to: recipients,
      title: getTitle(type, props),
      content: getContent(type, props),
      attachments: getAxaLogo()
    };
  } else {
    return {
      from: sender,
      to: recipients,
      cc: cc,
      title: getTitle(type, props),
      content: getContent(type, props),
      attachments: getAxaLogo()
    };
  }

};

const getSMSMsg = (type, props) => {
  const SMS_KEY = 'sms';
  switch (type) {
    case TYPE.SUBMISSION_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.SUBMISSION_NOTIFICATION),
      ['productName', 'adviserName', 'submissionDate', 'proposalNumber', 'proposerName', 'expiryDate'],
      props,
      SMS_KEY,
      NOTIFICATION_TEMPLATE.SUBMISSION_NOTIFICATION
    );

    case TYPE.ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION),
      ['agentName', 'caseNumber', 'proposerName', 'expiryDate'],
      props,
      SMS_KEY,
      NOTIFICATION_TEMPLATE.ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION
    );

    case TYPE.DIRECTOR_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.DIRECTOR_NOTIFICATION),
      ['adviserName', 'managerName', 'submissionDate', 'proposalNumber', 'clientName', 'expiryDate'],
      props,
      SMS_KEY,
      NOTIFICATION_TEMPLATE.DIRECTOR_NOTIFICATION
    );

    case TYPE.CASE_APPROVAL_REMINDER:
    return _get('sms', NOTIFICATION_TEMPLATE.CASE_APPROVAL_REMINDER);

    case TYPE.APPROVAL_EXPIRY_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVAL_EXPIRY_NOTIFICATION),
      ['adviserName', 'submissionDate', 'proposalNumber', 'proposerName'],
      props,
      SMS_KEY,
      NOTIFICATION_TEMPLATE.APPROVAL_EXPIRY_NOTIFICATION
    );

    case TYPE.APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT),
      ['productName', 'proposalNumber', 'proposerName', 'supervisorName', 'approvalDate'],
      props,
      SMS_KEY,
      NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT
    );

    case TYPE.APPROVED_NOTIFICATION_FOR_SELECTED_AGENT:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_SELECTED_AGENT),
      ['proposalNumber', 'proposerName'],
      props,
      SMS_KEY,
      NOTIFICATION_TEMPLATE.APPROVED_NOTIFICATION_FOR_SELECTED_AGENT
    );

    case TYPE.REJECTED_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION),
      ['productName', 'proposalNumber', 'proposerName', 'supervisorName', 'rejectionDate'],
      props,
      SMS_KEY,
      NOTIFICATION_TEMPLATE.REJECTED_NOTIFICATION
    );

    case TYPE.SUBMISSION_NOTIFICATION_FOR_DIRECTOR:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.SUBMISSION_NOTIFICATION_FOR_DIRECTOR),
      ['proposalNumber', 'peroposerName'],
      props,
      SMS_KEY,
      NOTIFICATION_TEMPLATE.SUBMISSION_NOTIFICATION_FOR_DIRECTOR
    );

    case TYPE.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION),
      ['productName', 'adviserName', 'submissionDate', 'proposalNumber', 'proposerName', 'expiryDate'],
      props,
      SMS_KEY,
      NOTIFICATION_TEMPLATE.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION
    );

    case TYPE.ADDITIONALDOC_HEALTHDECLARATION:
    return constructString(
      _get('keys', NOTIFICATION_TEMPLATE.ADDITIONALDOC_HEALTHDECLARATION),
      ['adviserName', 'proposalNumber', 'proposerName'],
      props,
      SMS_KEY,
      NOTIFICATION_TEMPLATE.ADDITIONALDOC_HEALTHDECLARATION
    );

    case TYPE.PENDING_FOR_DOCUMENT_NOTIFICATION:
    return '';

    default:
    return '';
  }
};

const getSMSObject = (type, props) => {
  if (!isValidType(type)) {
    throw 'Invalid email type, please check the email type.';
  }
  const mobileNo = _getOr('', 'mobileNo', props);
  if (_isEmpty(mobileNo)) {
    throw 'Mobile number is empty or undefined, please check the mobile no props body.';
  }
  return {
    mobileNo,
    smsMsg: getSMSMsg(type, props)
  };
};

module.exports = { TYPE, getEmailObject, getSMSObject };

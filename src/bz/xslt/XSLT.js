var _pdf = require('html-pdf');
const getByLang = require('../utils/CommonUtils').getByLang;

var _ = require('lodash');
var logger = global.logger || console;
var js2xmlparser = require("js2xmlparser");
var { xsltProcess, xmlParse } = require('xslt-processor');

var prepareFnaReportXsl = function(reportTemplates, lang, startingPage, totalPage, callback){
  var headerFooterTemplate = require('./template.js');
  var {headerFooter} = headerFooterTemplate.commonTemplate;
  var {xsltHeader, xsltFooter, documentHeader, documentFooter} = headerFooter;
  var content = '';

  _.forEach(reportTemplates, (reportTemplate, index)=>{
    var templates = getByLang(reportTemplate.template, lang);
    var pageHeader = `<div style="margin: 0px 60px; position: relative;min-height: 100%;">`;
    var pageFooter = `</div>`;
    _.forEach(templates, template=>{
      content += pageHeader + template + pageFooter;
    });
  })

  return xsltHeader + content + xsltFooter;

}
module.exports.prepareFnaReportXsl = prepareFnaReportXsl;

module.exports.prepareDynamicReportXsl = function(reportHtml, options, incHeader){
  var headerFooterTemplate = require('./template.js');
  var {headerFooter} = headerFooterTemplate.commonTemplate;
  var {xsltHeader, xsltFooter} = headerFooter;
  var result = xsltHeader;

  if (incHeader){
    result += '<header id="firstHeader"   style="width: calc(100% - 60px);margin-left:60px;">'   + _.get(options, "header.first")    + '</header>';
    result += '<header id="defaultHeader" style="width: calc(100% - 120px);margin-left:60px; margin-right: 120px;">'   + _.get(options, "header.default")  + '</header>';
  }

  result += reportHtml + xsltFooter;
  return result;
}

module.exports.prepareDynamicReportHtml = function(reportTemplate, lang){
  var content     = '';
  var pageHeader  = `<div style="width: calc(100% - 120px);margin: 0px 60px; position: relative; min-height: 100%;">`;
  var pageFooter  = `</div>`;
  var templates   = reportTemplate[lang];
  _.forEach(templates, (template, index)=>{
    content += (index? "": "<div class=\"pagebreak\"/>") + pageHeader + template + pageFooter;
  });
  return content;
}


var prepareAppFormXSL = function(reportTemplates, lang, startingPage, totalPage, callback){
  var content = '';

  reportTemplates.forEach(function(template, index) {
    var template = getByLang(template.template, lang);
    content += template;
  })

  var head = '<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">';
  head += '<xsl:output method="html"></xsl:output>'
  var foot = '</xsl:stylesheet>';

  return head + content + foot;
};
module.exports.prepareAppFormXSL = prepareAppFormXSL;

var prepareSupervisorXSL = function(reportTemplates, lang, startingPage, totalPage, callback){
  var content = reportTemplates;

  var head = '<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">';
  head += '<xsl:output method="html"></xsl:output>'
  var foot = '</xsl:stylesheet>';

  return head + content + foot;
};
module.exports.prepareSupervisorXSL = prepareSupervisorXSL;

var prepareReportXSL = function(reportTemplate, lang, startingPage, totalPage, callback){
  var pageHeaderContent = getByLang(reportTemplate.header, lang) || '';
  var pageFooterContent = getByLang(reportTemplate.footer, lang) || '';
  var overlayContent = getByLang(reportTemplate.overlay, lang) || '';

  var headerFooterTemplate = require('./template.js');
  var commonTemplate = headerFooterTemplate.commonTemplate;
  var pageHeader = commonTemplate.headerFooter.pageHeader;
  var pageFooter = commonTemplate.headerFooter.pageFooter;

  var templates = getByLang(reportTemplate.template);
  var content = '';
  for (var i = 0; i < templates.length; i++){
    var thisPageFooterContent = pageFooterContent.replace('[[@PAGE_NO]]', startingPage + i).replace('[[@TOTAL_PAGE]]', totalPage);

    var pageContent = '<div>' + templates[i] + '</div>';

    var thisPageHeader = pageHeader.replace('%s', pageHeaderContent);
    var thispageFooter = pageFooter.replace('%s', thisPageFooterContent);
    
    content += overlayContent + thisPageHeader + pageContent + thispageFooter;    
  }

  var head = '<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">';
  head += '<xsl:output method="html"></xsl:output><xsl:template match="/">';
  var foot = '</xsl:template></xsl:stylesheet>';
  return head + content + foot;
};
module.exports.prepareReportXSL = prepareReportXSL;

module.exports.data2Xml = (data, callback) => {
    /** 
  let result = js2xmlparser.parse("person",data);
  result = result.replace('<person>', '');
  result = result.replace('</person>', '');
  callback(result);
*/
  var jr = global.JavaRunner;
  jr.jsonToXml(JSON.stringify(data), function (dataXml) {
    callback('<?xml version="1.0" encoding="UTF-8"?>' + dataXml);
  });
};

module.exports.xsltTransform = (xml, xsl, callback) => {
  /** 
  const outXmlString = xsltProcess(
    xmlParse(xml),
    xmlParse(xsl)
   
);
callback(outXmlString);
 */
  var jr = global.JavaRunner;
  jr.XSLTTransformer(xml, xsl, (xslt) => {
    callback(xslt);
  });
};

module.exports.prepareDynamicHtml = (body, options, style) =>{
  var commonTemplate = require('./template.js').commonTemplate;
  var styleHtml = '<style>' + style + '\n' + commonTemplate.style + '</style>';

  let result = '<html><head>' + styleHtml + '</head><body>' + body + '</body></head></html>';
  return  result;
}

module.exports.prepareHtml = (body, style) => {
  var commonTemplate = require('./template.js').commonTemplate;
  var styles = [style, commonTemplate.style];

  // issue of phantomJS 2, dpi for different platforms are different, ref: https://github.com/ariya/phantomjs/issues/12685
  // windows: 96 dpi, linux: 72 dpi
  if (process.platform !== 'win32') {
    styles.push('html{zoom:75%;}');
  }

  var styleHtml = '<style>' + _.join(styles, '\n') + '</style>';
  return '<html><head>' + styleHtml + '</head><body>' + body + '</body></head></html>';
};

//for one page content, the library will print a blank page at the end (total 2 pages printed)
//to fix this, remove css html{height:100%}
//=> commonTemplate.onePageStyle = commonTemplate.style, except removed html{height:100%}
module.exports.prepareHtmlOnePage = (body, style) => {
  var commonTemplate = require('./template.js').commonTemplate;
  var styles = [style, commonTemplate.onePageStyle];

  // issue of phantomJS 2, dpi for different platforms are different, ref: https://github.com/ariya/phantomjs/issues/12685
  // windows: 96 dpi, linux: 72 dpi
  if (process.platform !== 'win32') {
    styles.push('html{zoom:75%;}');
  }

  var styleHtml = '<style>' + _.join(styles, '\n') + '</style>';
  return '<html><head>' + styleHtml + '</head><body>' + body + '</body></head></html>';
};

module.exports.html2Pdf = (html, pdfOptions) => {
  let options = Object.assign({
    format: 'A4',
    timeout: 60000
  }, pdfOptions);
  logger.log('XSLT :: html2Pdf :: generating pdf:\n');
  return new Promise((resolve, reject) => {
    var startTime = new Date();
    _pdf.create(html, options).toBuffer((err, buffer) => {
      if (err) {
        logger.log('XSLT :: html2Pdf :: failed to generate pdf\n', err);
        reject(err);
        return;
      }
      var elapsedTimeMs = new Date() - startTime;
      logger.log("Html2Pdf Elapsed " + elapsedTimeMs + "ms");
      logger.log('XSLT :: html2Pdf :: pdf generated:', !!buffer);
      const result = buffer.toString('base64');
      resolve(buffer && buffer.toString('base64'));
    });
  });
};

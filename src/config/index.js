const _ = require("lodash");

const staticJson = require("../../static/Directory");

/**
 * Work as replace EASE-WEB initialize progress
 * using static rather than DB
 */
const globalInit = function globalInit() {
  global.optionsMap = {};

  const optionsMapKeys = [
    "marital",
    "country",
    "occupation",
    "industry",
    "ccy",
    //{ key: "productLine", docId: "productLine_template"},
    "relationship",
    "nationality",
    "countryTelCode",
    "residency",
    "city",
    "cityCategory",
    "assetClass",
    "riskRating",
    "portfolioModels",
    "idDocType",
    "pass"
  ];
  return Promise.all(
    optionsMapKeys.map(
      val =>
      new Promise(() => {
        const docId = val.docId || val;
        const key = val.key || val;
        global.optionsMap[key] = staticJson.getOptionsMap(docId);
      })
    )
  );
};

module.exports.init = function init(callback) {
  global.logger = console;
  global.logger.error = global.logger.log;
  global.memoryLog = () => {
    const used = process.memoryUsage().heapUsed / 1024 / 1024;
    return `RAM used ${Math.round(used * 100) / 100} MB`;
  };

  globalInit().then(() => {
    global.config = _.merge(global.config, staticJson.sysParameter);
  });

  if (callback) {
    callback();
  }
};
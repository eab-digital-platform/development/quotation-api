const Save = async (Model) => {
    const saveResult = await Model.save();
    return !saveResult.errors;
}

module.exports={
    Save,
}
const mongoose = require('../dbConnector');
const Schema = require('../dbConnector').Schema;

const modelName = `logging`;

const logSchema = new Schema({
    accessTime: String,
    function: String,
    sessionID: String,
    // message: String,
}, {
    versionKey: false,
})

module.exports = mongoose.model(modelName, logSchema, modelName);


const _ = require('lodash');
const dbHelper = require('../dbHelper');
const LogModel = require('./logModel');

const addLog = async({ accessTime, funcName, sessionID, message = ''}) => {
    const log = new LogModel({
        accessTime,
        function: funcName,
        sessionID,
        message,
    })
    const toSave = await dbHelper.Save(log);
    return {
        success: toSave,
    }
}

module.exports = {
    addLog,
}
const mongoose = require('mongoose');
const { MONGODB_HOST, MONGODB_USER, MONGODB_PWD, MONGODB_DB } = process.env;
const uri = `mongodb://${MONGODB_USER}:${MONGODB_PWD}@${MONGODB_HOST}/${MONGODB_DB}`;

mongoose.set('debug', true);

mongoose.connect(uri, { useNewUrlParser: true });

mongoose.connection.once('open', () => {
  console.log(`dbConnection :: mongoose :: open`);
});
mongoose.connection.on('connected', () => {
  console.log(`dbConnection :: mongoose :: connected`);
});
mongoose.connection.on('reconnect', () => {
    console.log('dbConnection :: mongoose :: reconnect'); 
});
mongoose.connection.on('error', (err) => {
  console.log(`dbConnection :: mongoose :: error`, err);
});

mongoose.connection.on('disconnected', () => {
  console.log('dbConnection :: mongoose :: disconnected');
});

module.exports = mongoose;
module.exports.Schema = mongoose.Schema;

'use strict';
const _ = require('lodash');

const execFile = require('child_process').execFile;
const profile = require('./profile.json');
const productSuitability = require('./productSuitability.json');
const companyInfo = require('./companyInfo.json');

// General JSON
const quotation = require('./Product/SAV_quotation_100k.json');

const extraParams = require('./Product/extraParams.json');
const planInfo = require('./Product/SAV_planInfo.json');

const sysParameter = require('./sysParameter.json');
const optionsMap = require('./optionsMap.json');
const data = require('./data.json');
const session = require('./session.json');
const pdfTemplates = require('./pdfTemplates.json');

const fs = require('fs');
const productList = require('./Mapping/Product.json');

const getProducts = async() => {
  const prodList = _.get(json, `productList`, null)
  if (prodList) {
    return prodList;
  }
  let result = _.cloneDeep(productList);
  for (let res in result) {
    const r = result[res];
    const imgBitmap = await new Promise((resolve, reject) => {
      fs.readFile(r.imgSrc, (error, data) => {
        if (error) throw error;
        resolve(data);
      })
    });
    const imgBase64 = new Buffer(imgBitmap).toString('base64');
    _.setWith(r, `imgSrc`, imgBase64, typeof imgBase64);
  }
  return result;
}

var json = {
  profile: profile,
  productSuitability: productSuitability,
  companyInfo: companyInfo,
  basicPlan: {
    SAV: require('./Product/BasicPlan/SAV.json'),
    BAA: require('./Product/BasicPlan/BAA.json'),
  },
  quotation: quotation,
  planDetails: require('./Product/planDetails.json'),
  extraParams: extraParams,
  planInfo: planInfo,
  sysParameter: sysParameter,
  data: data,
  session: session,
  rider: {
    'PPERA': require('./Product/Rider/PPERA.json'),
    'PPEPS': require('./Product/Rider/PPEPS.json'),
    'PET': require('./Product/Rider/PET.json'),
    'BBR': require('./Product/Rider/BBR.json'),
    'HIR': require('./Product/Rider/HIR.json'),
    'HMR': require('./Product/Rider/HMR.json'),
    'MER': require('./Product/Rider/MER.json'),
    'MRR': require('./Product/Rider/MRR.json'),
    'WIR': require('./Product/Rider/WIR.json'),
  },
  pdfTemplates,
}

/**
 * Under Construction
 */
const getRiderJson = function () {
  var searchList = null;

  this.Init = function () {
    if (!searchList) {
      execFile('find', [`${covCode}`], function (error, stdout, stderr) {
        searchList = stdout.split('\n');
      });
    }
  }

  this.ByID = function (covCode) {
    return require(`./Product/Rider/${covCode}.json`);
  }
}
/**
 * @return {object} optionsMap
 */
const getOptionsMap = (key) => {
  if (optionsMap.hasOwnProperty(key)) {
    return optionsMap[key];
  } 
  console.log(`Directory.js :: Key not found in optionsMap :: key: ${key}`);
  return null;
}

const getProductSuitability = () => {
  return productSuitability;
}

const getProfile = (profileId) => {
  if (!profileId) {
    return Promise.resolve(null); 
  }
  return new Promise((resolve) => resolve(profile));
}

module.exports = json;
module.exports.getRider = getRiderJson;
module.exports.getOptionsMap = getOptionsMap;
module.exports.getProductSuitability = getProductSuitability;
module.exports.getProfile = getProfile;

// New Add
module.exports.getProducts = getProducts;